<?php
/**
 * @package om_musa
 */

wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.ia.graph.js', array('d3'), '1.0', true );
wp_enqueue_script( 'iac-ec-new-business', get_template_directory_uri() . '/js/iac-ec-new-business.js', array('d3'), '1.0', true );


?>

<div class="swap objective two">

	<div class="item" data-swap="1" data-dimension="A">
		<span>Small Business Formation</span>
	</div>

	<div class="item" data-swap="0" data-dimension="B">
		<span>Growth of Small Businesses to Midsized Firms</span>
	</div>

	<div class="item" data-swap="0" data-dimension="C">
		<span>Venture Capital per Capita</span>
	</div>

	<div class="item" data-swap="0" data-dimension="D">
		<span>Self-Employment Income</span>
	</div>
 

</div>
	
		<div class="year-labels centered col-md-6">

			<div class="item col-md-3 col-xs-3 apple green"></div>
			<div class="item col-md-3 col-xs-3 purp"></div>

		</div>
 
	

	<div class="col-md-12 col-xs-12 circles">
		<div class="col-md-6 city-group" data-label="chi">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Chicago"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Chicago"></div>
		</div>

		<div class="col-md-6 city-group" data-label="hou">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Houston"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Houston"></div>
		</div>

		<div class="col-md-6 city-group" data-label="mia">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Miami"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Miami"></div>
		</div>


		<div class="col-md-6 city-group" data-label="nyc">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="NYC"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="NYC"></div>
		</div>

		<div class="col-md-6 city-group" data-label="sd">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="SD"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="SD"></div>
		</div>

		<div class="col-md-6 city-group" data-label="us">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="US"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="US"></div>
		</div>



	</div>

<div class="data-disclosure col-md-12">
	<div data-dimension="A">
			<h5>Small Business Formation<br>(Firms with &lt; 50 employees less than 1 year old as a % of all firms)</h5>
			<p>Prosperous regions feature high rates of small business creation.</p>
			<p>Miami-Dade County enjoys a high rate of startup formation compared to the US and other major metros.</p>
		

		<p class="source">Source: <a href="http://www.census.gov/ces/dataproducts/bds/" title="Census Business Dynamic Statistics or County Business Patterns">Census Business Dynamic Statistics or County Business Patterns</a>
			<br>Data Showing: Central County
		</p>

	</div>
	<div data-dimension="B">
				<h5>Growth of Small Businesses to Midsized Firms<br>(Firms with 50 to 250 employees less than 5 years old as a % of all firms)</h5>
			<p>The ability of small companies to mature into larger firms is key indicator of a community's entrepreurial business climate.
</p>
			<p>Despite Miami-Dade county's high rate of startup formation, only an average number of firms grow to be medium-sized.
</p>
		

		<p class="source">Source: <a href="http://www.census.gov/ces/dataproducts/bds/" title="Census Business Dynamic Statistics or County Business Patterns">Census Business Dynamic Statistics or County Business Patterns</a>
			<br>Data Showing: Central County
		</p>

	</div>

	<div data-dimension="C">
			<h5>Venture Capital Funding per Capita</h5>
			<p>Venture capital is the lifeblood of young start-ups.</p>
			<p>Miami-Dade county has low levels of venture capital funding compared with the US and other metros.</p>
		

		<p class="source">Source: VentureDeal/Headlight</a>
			<br>Data Showing: Central County
		</p>

	</div>

	<div data-dimension="D">
			<h5>Self-Employment Revenue<br>(Average receipts for non-employment firms)</h5>
			<p>Higher revenues often reflect the presence of more competitive sole proprietorships</p>
			<p>Miami metro family businesses make significantly less than their peers in other metros.</p>
			<p>Full circle: $60,000</p>

		<p class="source">Source: <a href="http://www.census.gov/econ/nonemployer/" title="Census Non-Employer Statistics">Census Non-Employer Statistics</a>
			<br>Data Showing: Metro-Level
		</p>

	</div>

</div>
