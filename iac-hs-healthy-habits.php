<?php
/**
 * @package om_musa
 */

	wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.ia.graph.js', array('d3'), '1.0', true );
	wp_enqueue_script( 'iac-ha-healthy-outcomes', get_template_directory_uri() . '/js/iac-hs-healthy-outcomes.js', array('d3'), '1.0', true );

	


?>
<!-- iac-hs-healthy-habits -->
 

<div class="swap objective two">

 
	<div class="item" data-swap="1" data-dimension="A">
		<span>Heart Disease Rate</span>
	</div>

	<div class="item" data-swap="0" data-dimension="B">
		<span>Stroke Rate</span>
	</div>

	<div class="item" data-swap="0" data-dimension="C">
		<span>HIV Rate</span>
	</div>

</div>
	
		<div class="col-md-6 centered year-labels">

			<div class="item col-md-3 col-xs-3 apple green"></div>
			<div class="item col-md-3 col-xs-3 purp"></div>

		</div>
 
	

	<div class="col-md-12 col-xs-12 circles">
		<div class="col-md-6 city-group" data-label="chi">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Chicago"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Chicago"></div>
		</div>

		<div class="col-md-6 city-group" data-label="hou">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Houston"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Houston"></div>
		</div>

		<div class="col-md-6 city-group" data-label="mia">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Miami"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Miami"></div>
		</div>


		<div class="col-md-6 city-group" data-label="nyc">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="NYC"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="NYC"></div>
		</div>

		<div class="col-md-6 city-group" data-label="sd">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="SD"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="SD"></div>
		</div>

		<div class="col-md-6 city-group" data-label="us">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="US"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="US"></div>
		</div>



	</div>

<div class="data-disclosure col-md-12">

	<div data-dimension="A">
		<h5>HEART DISEASE DEATH RATE PER 100K AMONG INDIVIDUALS AGE 35+</h5>
		<p>Heart disease is the leading cause of death in the US.</p>
		<p>Miami-Dade county's incidence of heart disease is falling and slightly below the US average.</p>
		<p class="source">Source: <a href="http://apps.nccd.cdc.gov/BRFSS-SMART/ListMMSAQuest.asp?yr2=2012&MMSA=All&cat=TU&qkey=8161&grp=0" title="Centers for Disease Control">Centers for Disease Control</a>
			<br>Data Showing: Central County
		</p>
	</div>

	<div data-dimension="B">
		<h5>STROKE DEATH RATE PER 100K AMONG INDIVIDUALS AGE 35+</h5>
		<p>Stroke is the leading cause of disability among adults in the US.</p>
		<p>Miami-Dade county's rate of stroke deaths is falling and significantly below the US average.</p>
		<p class="source">Source: <a href="http://nccd.cdc.gov/dhdspatlas/reports.aspx?geographyType=county&themeSubClassId=1&filterIds=4,3,2,7,10,9&filterOptions=1,1,1,1,1,1#report" title="Centers for Disease Control">Centers for Disease Control</a>
			<br>Data Showing: Central County
		</p>
	</div>

	<div data-dimension="C">
		<h5>DIAGNOSES OF HIV INFECTION PER 100K POPULATION</h5>
		<p>Miami has one of the highest incidents of HIV infection in the US.</p>
		<p>The Miami metro has a much higher rate of HIV than the US average and other major metros.</p>

		<p class="source">Source: <a href="http://www.cdc.gov/hiv/pdf/statistics_2011_HIV_Surveillance_Report_vol_23.pdf#Page=75" title="Centers for Disease Control">Centers for Disease Control</a>
			<br>Data Showing: Metro-level
		</p>
	</div>
	

</div>
