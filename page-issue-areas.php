<?php
/*
Template Name: Issue Areas Template
*/



/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package om_musa
 */

get_header(); ?>
<!-- page-issue-areas -->
	<div id="primary" class="content-area row">
		<main id="main" class="site-main col-md-12" role="main">

		  
 
									<div class="parent">

										<?php
											/* Include the Post-Format-specific template for the content.
											 * If you want to override this in a child theme, then include a file
											 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
											 */
											 
					 						get_template_part( 'home', 'issues' );


											?>
 
									
									</div>
							 

								
  
				

				

			 

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>