<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package om_musa
 */

get_header(); ?>
<!-- page -->
	<div id="primary" class="content-area row">
		<main id="main" class="site-main col-md-12" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

			<div class="background" style="background-image:url('<?php 
											$coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											echo $coverimageurl
										 ?>') ;" class="">	

			<!--	<?php 
				 		//		if ( is_page() && $post->post_parent > 0 ) { 
						//	 	$parent_title = get_the_title($post->post_parent);
						//	    ?> <h4 class="top kicker center align">
						//	    	<?php 
						//		 echo $parent_title; ?>
						//	    </h4> <?php
						//	}
				 		// ?>
				 		//
				 		//<?php 
						//	$story_kicker = get_post_meta( get_the_ID(), 'story-kicker', true );
						//	// check if the custom field has a value
						//	if( ! empty( $story_kicker ) ) {
						//	  ?> <h4 class="kicker center align"> <?php echo $story_kicker; ?></h4> <?php 
						//	} else {
						//		the_title( '<h4 class="kicker center align">', '</h4>' );
						//	}
						?>--> 
				<?php get_template_part( 'content', 'page' ); ?>


			</div>
			<div class="page-footer">
						<a href="<?php 
							$pageFooterLink = get_post_meta($post->ID, 'page-footer-cta-link', true);
							echo $pageFooterLink;
							?>" 

							title="<?php echo $pageFooterCTA;  ?>" 

							class="page-footer-link page-footer-link-icon">
								<?php 
				 			 	$pageFooterCTA = get_post_meta($post->ID, 'page-footer-cta', true);
				 				 if ($pageFooterCTA !="" ) {
				 				 	?>
				 			 	
				 			 		<?php echo $pageFooterCTA; ?>

				 			 	<?php
				 			 	

				 				 } else {
				 			 		?>
				 			 		<span>LEARN ABOUT OUR STUDY AND STORIES</span>
				 			 	<?php
				 			  

				 			 }?>
		 				
		 				</a>
				</div>				

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
