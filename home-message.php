<?php
/**
 * Message zone on homepage. 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package om_musa
 */

 ?>
<!-- home-message -->

  	<div class="col-md-12 site-main message" role="main">
					<div id="explore" class="col-md-12 centered section header">

			  			<h4 class="center align"><a href="/our-study/stories" title="Explore the 2014 Our Miami Report Stories">Explore the 2014 Our Miami Report Stories</a></h4>

			  		</div>
  		

  		<div id="message-wrapper" class="story jcarousel-wrapper">
  			

	  		<div class="jcarousel">
	  			
	  			 	

	  			 <ul class="hook row">


	  			 	<?php 
	  			 			$args = array(
								'post_type' => 'story',
								'posts_per_page' => 5,
								// Below argument is to include posts in Query. Need to add Hook field to posts.
								//'post_type' => array('story', 'post'),
								'order' => ASC,
								'post_parent' => 0,
								'orderby' => 'menu_order',
								/*'meta_query' => array(
														array(
															'key'       => 'home-slider',
															'value'     => 'yes',
														),
													)*/

							);
						 
							$quotesQuery = new WP_Query( $args );

	  			 	 ?>
	  			 	
	  			 	<?php if ( $quotesQuery->have_posts() ) : ?>

							<?php /* Start the Loop */ ?>
							<?php while ( $quotesQuery->have_posts() ) : $quotesQuery->the_post(); ?>

								<li class="post-wrapper col-md-12" style="background-image:url('<?php 
											$coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											echo $coverimageurl
										 ?>');">
									<?php
																	/* Include the Post-Format-specific template for the content.
																	 * If you want to override this in a child theme, then include a file
																	 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
																	 */
																	get_template_part( 'content', 'hook' );
																?>


								
									<div class="col-md-3 centered"> 
						      			<a href="#issues" class="scroll-link"><span class="anchor-link section arrow-down-icon">&nbsp;</span></a>
						    		</div>

								</li>

								

							<?php endwhile; ?>

							 

						<?php else : ?>

							<?php get_template_part( 'content', 'none' ); ?>

						<?php endif; 

							wp_reset_postdata();

						?>

	  			 </ul>	
	  			
	  		</div><!-- .jcarousel -->
	  		<!-- <span class="controls"><a href="#" class="jcarousel-control-prev"><img src="<?php // echo get_template_directory_uri()?>/images/left-arrow.png"></a></span>
	  		<span class="controls "><a href="#" class="jcarousel-control-next"><img src="<?php // echo get_template_directory_uri()?>/images/right-arrow.png"></a></span> -->

	  		<span class="controls"><a href="#" class="arrow-left-icon jcarousel-control-prev"></a></span>
	  		<span class="controls "><a href="#" class="arrow-right-icon jcarousel-control-next"></a></span>
	  	 
	  		<span class="controls dot jcarousel-pagination"></span>
	  		 
	  		

  		</div><!-- #message-wrapper -->

  		
  	</div><!-- .site-main.message -->
 
