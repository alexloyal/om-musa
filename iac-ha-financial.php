<?php
/**
 * @package om_musa
 */
	wp_enqueue_script( 'iac-ha-financial', get_template_directory_uri() . '/js/iac-ha-financial.js', array('d3'), '1.0', true );
	
    wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );
?>

<div class="iac-ha-financial-content">

	<div class="swap objective one">

		<div class="item" data-swap="1" data-dimension="A">
			<span>Median Household Income</span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>% of Population Living in Poverty</span>
		</div>

	</div>

	
	<div class="col-md-6 year-labels centered">
		<span class="cyan" data-value=""></span>
		<span class="purp" data-value=""></span>
	</div>

<div class="ia-ha-financial-resources">
		<div class="graph"></div>
</div>


<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->

<div class="data-disclosure">
	
	<div data-dimension="A">
		<h5>MEDIAN HOUSEHOLD INCOME</h5>

		<p>Median household income is a fundamental measure of the economic well-being of households.</p>

		<p>Miami-Dade County's income levels are significantly lower than the US other major metros.</p>

		<p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a></p>
	</div>

	<div data-dimension="B">
		<h5>% OF POPULATION LIVING IN POVERTY</h5>

		<p>Poverty is interconnected to a host of other quality of life factors, including health, education, and civic engagement.</p>

		<p>Poverty is increasing in Miami-Dade county.  21% of all residents live under the poverty line.  28% of children live in poverty, much more than other large regions and the US average.</p>

		<p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a></p>
	</div>
	 

</div>


</div>

 
 
 