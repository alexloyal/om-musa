<?php
/**
 * Message zone on homepage. 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package om_musa
 */

 ?>
<!-- home-about -->

  	<div class="col-md-12 site-main about" role="main">

  		<div id="about-title" class="col-md-12 centered section header">

  			

  		</div>

  		<div id="story-wrapper" class="story">
  			

	  		<div class="">
	  			 

	  			 	<?php 
	  			 			$args = array(
								'post_type' => 'page',
								'pagename' => 'about'  
							);
							$quotesQuery = new WP_Query( $args );

	  			 	 ?>
	  			 	
	  			 	<?php if ( $quotesQuery->have_posts() ) : ?>

							<?php /* Start the Loop */ ?>
							<?php while ( $quotesQuery->have_posts() ) : $quotesQuery->the_post(); ?>

								<div class="post-wrapper col-md-12">
									<?php
																	/* Include the Post-Format-specific template for the content.
																	 * If you want to override this in a child theme, then include a file
																	 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
																	 */
																	get_template_part( 'content', 'about' );
																?>


								</div>

								

							<?php endwhile; ?>

							 

						<?php else : ?>

							<?php get_template_part( 'content', 'none' ); ?>

						<?php endif; ?>
 
	  			
	  		</div><!-- # -->
	  		 
	  	 

	  		


  		</div>


  	</div>
 
