<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package om_musa
 */
?>


  <section class="row" id="contact">

  </section><!-- #contact -->


 </div><!-- #content -->



	<div class="top link">
	 
		<a href="#top"><span class="anchor-link arrow-up-icon top">&nbsp;</span></a>
	</div>
	

<footer id="footer" class="site-footer row" role="contentinfo">
		<div class="site-info col-md-12 centered">
			 <div class="col-md-5 col-sm-6 col-xs-12">
			 	<h5 class="site-title"><a href="http://www.miamifoundation.org" title="Miami Foundation"></a></h5>
			 	<?php dynamic_sidebar( 'footer-text' ); ?>
			 </div>

			 <div class="footer-menu col-md-2 col-sm-3 col-xs-6">
			 	<?php dynamic_sidebar( 'footer-menu-1' ); ?>
			 </div>

			 <div class="footer-menu col-md-2 col-sm-3 col-xs-6">
			 	<?php dynamic_sidebar( 'footer-menu-2' ); ?>
			 </div>

			 <div class="col-md-3 col-sm-6 col-xs-12">
			 		
			 		<div class="footer-social-wrapper col-md-12">
			 			<h4 class="col-md-3 col-xs-3">Follow</h4>
			 		<div class="social-navigation col-xs-6 col-md-9" role="social-navigation">
					<?php wp_nav_menu( array( 
												'theme_location' => 'social',
												'link_before' => '<span class="',
												'menu_class'      => 'menu list-line',
				          						'link_after' => '-icon"></span>' ) ); ?>
					


					</div><!-- .social-navigation -->
			 		</div>	

			 		<div class="footer-social-wrapper newsletter">
			 			<h4><a href="http://www.miamifoundation.org/page.aspx?pid=364" title="Newsletter Signup">Newsletter Signup</a></h4>
			 		</div>

					
			 </div><!-- .footer-social-wrapper -->
		</div><!-- .site-info -->
		<div class="credit col-md-12">
			<h5 id="data-analysis-by">Data &amp; Analysis by <a class="avalanche-consulting-icon" href="http://www.avalancheconsulting.com/" title="Avalanche Consulting"></a> </h5>
			<h5 id="design-by-musa"><span>Design by </span> <a class="musa" href="http://musacreative.co" title="MUSA Creative Lab">MUSA</a></h5>
		</div>
	</footer><!-- #colophon -->




 <script>
			
			var base = '<?php 
								$url = site_url();
								echo $url; 
						?>';

				
</script>
<?php 
	wp_enqueue_script( 'd3Afterburner', get_template_directory_uri() . '/js/d3Afterburner.js', array('triggers'), '0.1', true );						 
	wp_enqueue_script( 'interaction-sheet', get_template_directory_uri() . '/js/interaction-sheet.js', array('d3'),'0.1',true );	
?>
<?php wp_footer(); ?>
</div><!-- #wrapper -->
<script>
    var _gaq=[['_setAccount','UA-34611533-1'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>
</body>
</html>
