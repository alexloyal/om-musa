<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package om_musa
 */

get_header(); ?>
<!-- home -->

 <div id="main">

   <section class="row content-area" id="home-message">

    <span id="message" class="anchor"></span>
  
          <?php get_template_part('home','message'); ?> 
  




  </section><!-- #message -->

  <section class="row" id="home-issues">
     
      <span id="issues" class="anchor"></span>
      

          <?php get_template_part('home','issues'); ?>

    <!-- <a href="#news" class="scroll-link"><span class="anchor-link section arrow-down-icon">&nbsp;</span></a> -->

  </section><!-- #issues -->


  <section class="row" id="home-news">

    <span id="news" class="anchor"></span>
  
          <?php get_template_part('home','news'); ?>

    <!-- <a href="#about" class="scroll-link"><span class="anchor-link section arrow-down-icon">&nbsp;</span></a> -->

  </section><!-- #news -->


  <section class="row" id="home-about">

    <span id="about" class="anchor"></span>

    
          <?php get_template_part('home','about'); ?>
    


  </section><!-- #about -->


 </div><!-- #main -->


	 
	

 
<?php get_footer(); ?>
