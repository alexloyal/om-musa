<?php
/**
 * @package om_musa
 */
?>

<?php 

	    wp_enqueue_script( 'sea-level', get_template_directory_uri() . '/js/story-viz-sea-level.js', array('d3'), '1.0', true );
        wp_enqueue_style( 'om-musa-story-sea-level-style', get_template_directory_uri()  . '/css/story-sea-level.css');

 ?>
 <section class="mobile park-space-sea-level"></section>
 <section class="park-space-sea-level">
            <h4>Inches Above 1990 Sea Level</h4>
            <div class="sea-levels">
                <h5 class="sea-level-marker cyan">1990 Sea Level</h5>
            </div>
            <img src="<?php echo get_template_directory_uri() ?>/images/buoy.png" class="buoy">
         <div class="legend">
            <span id="slider-value"></span>
            <div class="toggle" data-value="2">
                <div id="sea-slider" class="cyan"></div>
            </div>

         </div>
</section>

 
 
 