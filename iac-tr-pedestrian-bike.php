<?php
/**
 * @package om_musa
 */
	wp_enqueue_script( 'iac-tr-pedestrian-bike', get_template_directory_uri() . '/js/iac-tr-pedestrian-bike.js', array('d3'), '1.0', true );
	
    wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );
?>

<div class="iac-tr-pedestrian-bike">

	<div class="swap objective one">

		<div class="item" data-swap="1" data-dimension="A">
			<span>% of People who Walk to Work</span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>% of Population with Walkable Access to Park</span>
		</div>

		<div class="item" data-swap="0" data-dimension="C">
			<span>Pedestrian Deaths</span>
		</div>

	 

		

	</div>


<div class="col-md-6 year-labels centered">
	<span class="cyan" data-value=""></span>
	<span class="purp" data=value=""></span>
</div>


<div class="iac-tr-pedestrian-bike">
		<div class="graph"></div>
</div>


<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->

<div class="data-disclosure">
	<div data-dimension="A">

		<h5>% OF COMMUTERS THAT WALK TO WORK</h5>
		<p>Walking to work removes cars from the road and improves personal fitness.</p>
	
		
		<p>A smaller percentage of workers walk to work in Miami-Dade than across the US and other major metros like San Diego and Chicago.</p>
	
		<p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a><br>
		Data Showing:  Central County</p>
	</div>	
	<div data-dimension="B">

		<h5>% OF POPULATION WITHIN 1/2 MILE OF PARK</h5>
		<p>Parks are better utilized when they are close to where people live.</p>
		
		<p>Nearly 75% of the City of Miami's population is within 1/2 mile of a park.  Miami trails more dense cities like San Francisco and New York, but is on par with San Diego.</p>
	
		<p class="source">Source: <a href="http://www.tpl.org/2014-city-park-facts" title="Trust for Public Land">Trust for Public Land</a><br>
		Data Showing:  City</p>
	</div>	
	<div data-dimension="C">

		<h5>PEDESTRIAN FATALITY RATE PER 100K POPULATION</h5>
		<p>Physical safety is a crucial component in fostering pedestrian-friendly neighborhoods.</p>
	
		<p>Miami-Dade has one of the higher rates of pedestrian fatalities among major metros.  Fortunately, 2012 saw a sizeable improvement, as fatalities decreased to 58 from 77 in 2011.</p>
	
		<p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a><br>
		Data Showing:  Central County</p>
	</div>	
</div>


</div>

 
 
 