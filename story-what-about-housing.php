<?php
/**
 * @package om_musa
 */
?>

<?php 

	 wp_enqueue_script( 'construction-costs-vs-home-sale-value', get_template_directory_uri() . '/js/construction-costs-vs-home-sale-value-viz.js', array('d3'), '1.0', true );
     wp_enqueue_script( 'bar-graph', get_template_directory_uri() . '/js/bar.graph.js', array('construction-costs-vs-home-sale-value'), '1.0', true );

 ?> 

 <section class="what-about-housing">
        <div class="legend">
            <div class="annotation">
                <span><span class="dot apple green">&bull;</span> <span class="dark grey text">2011</span></span>
                <span><span class="dot cyan">&bull;</span> <span class="dark grey text">2013</span></span>
            </div>
            <p class="annotation">% Growth</p>
           <div class="data-svg">

            <div class="city names">
                    <span class="arrow-right"></span>
                    <ul class="list-unstyled">
                        <li data-city="Chicago">CHI</li>
                        <li data-city="Houston">HOU</li>
                        <li data-city="Miami">MIA</li>
                        <li data-city="New York">NYC</li>
                        <li data-city="San Diego">SD</li>
                        <li data-city="US">US</li>

                    </ul>
            </div><!-- .city.names -->
            <div class="icons">
                <span class="purp construction-costs-icon">
                    
                    <img src="<?php echo get_template_directory_uri(); ?>/images/construction-costs-icon.png">

                    <p>Construction Cost</p>
                </span>
                <span class="purp median-sale-price-icon">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/median-sale-price.png">
                    <p>SFH Median Sale Price</p>
                </span>
            </div>

            <div class="data tracks">
                
                <div class="construction-cost">
                    

                    <span class="compare"></span>
                </div>
                <div class="median-sale-price">
                    
                    <span class="compare"></span>
                   
                </div>
            </div>
           </div>
        </div>
 </section>
 
 


 