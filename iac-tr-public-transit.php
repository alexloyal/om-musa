<?php
/**
 * @package om_musa
 */

wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.ia.graph.js', array('d3'), '1.0', true );
wp_enqueue_script( 'iac-tr-public-transit', get_template_directory_uri() . '/js/iac-tr-public-transit.js', array('d3'), '1.0', true );


?>

<div class="swap objective two">

	<div class="item" data-swap="1" data-dimension="A">
		<span>Public Transit Ridership</span>
	</div>

	<div class="item" data-swap="0" data-dimension="B">
		<span>Investment in Public Transit</span>
	</div>

	<div class="item" data-swap="0" data-dimension="C">
		<span>% of Workers Commuting via Public Transit</span>
	</div>
 

</div>
	<div class="col-md-6 centered year-labels">
		<div class="item col-md-3 col-xs-3 apple green"></div>
		<div class="item col-md-3 col-xs-3 purp"></div>
	</div>

	<div class="col-md-12 col-xs-12 circles">
		<div class="col-md-6 col-sm-6 city-group" data-label="chi">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Chicago"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Chicago"></div>
		</div>

		<div class="col-md-6 col-sm-6 city-group" data-label="hou">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Houston"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Houston"></div>
		</div>

		<div class="col-md-6 col-sm-6 city-group" data-label="mia">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Miami"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Miami"></div>
		</div>


		<div class="col-md-6 col-sm-6 city-group" data-label="nyc">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="NYC"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="NYC"></div>
		</div>

		<div class="col-md-6 col-sm-6 city-group" data-label="sd">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="SD"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="SD"></div>
		</div>

		<div class="col-md-6 col-sm-6 city-group" data-label="us">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="US"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="US"></div>
		</div>



	</div>

<div class="data-disclosure col-md-12">
	<div data-dimension="A">
			<h5>PER CAPITA RIDERSHIP<br>(UNLINKED PASSENGER TRIPS ACROSS ALL TRANSIT SYSTEMS PER METRO POPULATION)</h5>
			<p>Usage is an important indicator of a regional transit network's health.</p>


			<p>Ridership on Miami's transit system remains relatively low, with some modest improvement in the last two years.</p>

			<p class="source">Source: <a href="http://www.ntdprogram.gov/ntdprogram/pubs/dt/2010/excel/DataTables.htm" title="Federal Transit Administration">Federal Transit Administration</a><br>
			Data Showing:  Metro-level</p>
	</div>

	<div data-dimension="B">
		<h5>PER CAPITA TRANSIT OPERATING FUNDS<br>(ALL TRANSIT SYSTEMS, PER METRO POPULATION)</h5>

		<p>Efficient and reliable public transit systems require significant funding.</p>

		<p>Miami appears to be funding its metro rail system adequately when compared to other car-centric metros like San Diego and Houston.  But, our funding is 1/3 the levels seen in Seattle.</p>

		<p class="source">Source: <a href="http://www.ntdprogram.gov/ntdprogram/pubs/dt/2010/excel/DataTables.htm" title="Federal Transit Administration">Federal Transit Administration</a><br>
		Data Showing:  Metro-level</p>
	</div>

	<div data-dimension="C">

		<h5>% OF COMMUTERS THAT USE PUBLIC TRANSIT</h5>

		<p>Public transit can play a critical role in facilitating work commutes</p>

		<p>More Miami-Dade workers commute via public transit (bus, metro) than the average US commuter, but less than big metros with rail like New York and Chicago.</p>
		<p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a>
			<br>Data Showing:  Central County
		</p>
	</div>

</div>
