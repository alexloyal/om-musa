<?php
/**
 * @package om_musa
 */
	wp_enqueue_script( 'iac-hs-health-safety', get_template_directory_uri() . '/js/iac-ed-retain-graduates.js', array('d3'), '1.0', true );
	
    // wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );
?>

<div class="iac-iac-ed-retain-graduates-content">

	<div class="swap objective one">

		<div class="item" data-swap="1" data-dimension="A">
			<span>College Educated Workers Attracted/Retained</span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>Unemployment Rate for College-Educated Adults</span>
		</div>
 

	</div>

<div class="col-md-6 year-labels centered">
	<span class="apple green" data-value=""></span>
	<span class="cyan" data-value=""></span>
	
</div>

<div class="iac-ed-retain-graduates">
		<div class="graph"></div>
</div>



<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->

<div class="data-disclosure">
	<div data-dimension="A">
		<h5>NET MIGRATION AMONG PEOPLE WITH BACHELOR'S OR GRADUATE DEGREE<br>(PER 1000 Residents)</h5>
		<p>	Thriving regions are typically net importers of talent.</p>
		<p>Miami-Dade County enjoys a high rate of in-migration of college-educated people. But, we don't attract as many people on a per capita basis as New York or Houston.</p>
		<p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a>
			<br>Data Showing: Central County
		</p>
	</div>

	<div data-dimension="B">
		<h5>UNEMPLOYMENT BY EDUCATIONAL ATTAINMENT</h5>
		
		<p>The availability of employment opportunities helps educated workers and attracts new talent to a region.</p>
		<p>Miami-Dade workers with less education have higher unemployment, but those with college degrees are more likely to be unemployed than other metros across the US.</p>
		<p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a><br>
			Data Showing: Central County
		</p>
	</div>
	


	

</div>


</div>

 
 
 