<?php
/**
 * @package om_musa
 */

	wp_enqueue_script( 'iac-ha-equitable', get_template_directory_uri() . '/js/iac-ha-equitable.js', array('d3'), '1.0', true );
	


?>

 

<h4>Income Inequality (GINI Coefficient)</h4>

<div class="col-md-6 year-labels centered">
	<span class="apple green" data-value="2011"></span>
	<span class="cyan" data-value="2013"></span>
</div>

<div class="ia-ha-equitable">
		<div class="five marker right">.53</div>
		<div class="col-md-12 centered graph"></div>
		<div class="zero marker right">.39</div>
</div>



<div class="data-disclosure">

	<h5>INCOME INEQUALITY (GINI COEFFICIENT)</h5>

    <p>A Gini coefficient of zero indicates perfect equality (everyone has the same income). A Gini coefficient of one indicates extreme inequality.</p>

    
    <p>Miami-Dade county has more inequality than other major metros such as Chicago, San Diego, and Houston, but lower inequality than New York.</p>

    <p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a><br>Data Showing: Central County</p>

</div>

 
 
 
 
 
 