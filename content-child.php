<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package om_musa
 */
?>
<!-- content-child -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-6'); ?>>
	
	<div class="col-md-4 post-thumb">
		<?php 


		$attr = array(
				'class' => 'circle',
				'alt' => $post->post_title
			);
		echo get_the_post_thumbnail( $page->ID, 'thumbnail', $attr ); ?>
	</div>

	<div class="entry-header col-md-8">
		
		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

		<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
		
	</div>

	

	
</article><!-- #post-## -->