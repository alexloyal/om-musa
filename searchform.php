<?php
/**
 * The search form.
 *
 * @package om_musa
 */

?>

<section class="search-module">

	 

	<div class="col-md-9 search-form-wrapper">

		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			<label>
				<span class="screen-reader-text">Search for:</span>
				<input type="search" class="search-field" placeholder="Search …" value="" name="s" title="Search for:" />
			</label>
			 
		</form>

		<!-- home_url <?php  ?> -->

	</div>


</section>