<?php
/**
 * @package om_musa
 */
?>
<!-- story safety first -->
<?php 

//	wp_enqueue_script( 'pdata-svg', get_template_directory_uri() . '/js/story-viz-park-investment.js', array('d3'), '1.0', true );
//	wp_enqueue_script( 'bar-graph', get_template_directory_uri() . '/js/bar.graph.js', array('d3'), '1.0', true );
	wp_enqueue_style( 'om-musa-story-parks-population', get_template_directory_uri()  . '/css/story-parks-and-population.css');	

 ?>

  <h4>MIAMI-DADE PARK FUNDING growth
VS. POPULATION GROWTH. 2010 - 2013</h4>

  

 <div class="parks-population">

 		<img src="<?php echo get_template_directory_uri() ?>/images/parks-and-population.png" title="Parks and Population" class="viz" />
 		<img src="<?php echo get_template_directory_uri() ?>/images/parks-population-legend.png" title="Parks and Population" class="legend"/>


 </div>