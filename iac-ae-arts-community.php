<?php
/**
 * @package om_musa
 */

wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.ia.graph.js', array('d3'), '1.0', true );
wp_enqueue_script( 'iac-ae-arts-community', get_template_directory_uri() . '/js/iac-ae-arts-community.js', array('d3'), '1.0', true );


?>

<div class="swap objective two">

	<div class="item" data-swap="1" data-dimension="A">
		<span>Arts Attendance</span>
	</div>

	<div class="item" data-swap="0" data-dimension="B">
		<span>Arts Employment</span>
	</div>

	<div class="item" data-swap="0" data-dimension="C">
		<span>Art Dealers</span>
	</div>

	<div class="item" data-swap="0" data-dimension="D">
		<span>NEA Grants Per Capita</span>
	</div>

	<div class="item" data-swap="0" data-dimension="E">
		<span>Tourism Impact</span>
	</div>




</div>
	<div class="col-md-12 year-labels" data-dimension-active="">
		<div class="col-md-9 centered">

			<div class="item col-md-3 col-xs-3 apple green"></div>
			<div class="item col-md-3 col-xs-3 purp"></div>

		</div>
	</div>

	<div class="data-sets">
		<div class="data-set col-md-12" data-dimension="A">

			<h4>Miami-Dade County</h4>
				<div class="col-md-12 col-xs-12 circles">
					<div class="col-md-6 city-group" data-label="Attendance">
						<div class="col-md-6 col-xs-6 radial first" data-year="" data-segment="Attendance"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="" data-segment="Attendance"></div>

					</div>

					<div class="col-md-6 city-group" data-label="Revenue">
						<div class="col-md-6 col-xs-6 radial first" data-year="" data-segment="Revenue"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="" data-segment="Revenue"></div>

					</div>

					<div class="col-md-6 city-group" data-label="% Growth">
						<div class="col-md-6 col-xs-6 radial first" data-year="" data-segment="Percent Growth"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="" data-segment="Percent Growth"></div>

					</div>
				</div>
		</div>

		<div class="data-set col-md-12" data-dimension="B">

			<h4>Per capita (100k population)</h4>

			<div class="col-md-12 col-xs-12 circles" data-segment="1">
				<div class="col-md-6 city-group" data-label="chi">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Chicago"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Chicago"></div>
				</div>
				<div class="col-md-6 city-group" data-label="hou">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Houston"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Houston"></div>

				</div>
				<div class="col-md-6 city-group" data-label="mia">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Miami"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Miami"></div>

				</div>

				<div class="col-md-6 city-group" data-label="nyc">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="NYC"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="NYC"></div>

				</div>

				<div class="col-md-6 city-group" data-label="sd">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="SD"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="SD"></div>

				</div>

				<div class="col-md-6 city-group" data-label="us">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="US"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="US"></div>

				</div>
			</div>

		</div>

		<div class="data-set col-md-6" data-dimension="C">

			<h4>Per capita (100k population)</h4>
			<div class="col-md-12 col-xs-12 circles">
				<div class="col-md-6 city-group" data-label="chi">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Chicago"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Chicago"></div>
				</div>
				<div class="col-md-6 city-group" data-label="hou">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Houston"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Houston"></div>

				</div>
				<div class="col-md-6 city-group" data-label="mia">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Miami"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Miami"></div>

				</div>

				<div class="col-md-6 city-group" data-label="nyc">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="NYC"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="NYC"></div>

				</div>

				<div class="col-md-6 city-group" data-label="sd">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="SD"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="SD"></div>

				</div>

				<div class="col-md-6 city-group" data-label="us">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="US"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="US"></div>

				</div>
			</div>
		</div>

		<div class="data-set col-md-6" data-dimension="D">

			<h4>Per capita Grant Funding</h4>
			<div class="col-md-12 col-xs-12 circles">
				<div class="col-md-6 city-group" data-label="chi">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Chicago"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Chicago"></div>
				</div>
				<div class="col-md-6 city-group" data-label="hou">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Houston"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Houston"></div>

				</div>
				<div class="col-md-6 city-group" data-label="mia">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Miami"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Miami"></div>

				</div>

				<div class="col-md-6 city-group" data-label="nyc">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="NYC"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="NYC"></div>

				</div>

				<div class="col-md-6 city-group" data-label="sd">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="SD"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="SD"></div>

				</div>

				<div class="col-md-6 city-group" data-label="us">
					<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="US"></div>
					<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="US"></div>

				</div>
			</div>
		</div>

		<div class="data-set col-md-6" data-dimension="E">

			<div class="col-md-12 col-xs-12 circles">

				<div class="col-md-6 city-group" data-label="Growth, '11-'13">
					<div class="col-md-6 col-xs-6 radial first"></div>
					<div class="col-md-6 col-xs-6 radial second"></div>

				</div>

				<div class="col-md-6 city-group" data-label="2013">
					<div class="col-md-6 col-xs-6 radial first"></div>
					<div class="col-md-6 col-xs-6 radial second"></div>

				</div>

				<div class="col-md-6 city-group" data-label="2012">
					<div class="col-md-6 col-xs-6 radial first"></div>
					<div class="col-md-6 col-xs-6 radial second"></div>

				</div>

				<div class="col-md-6 city-group" data-label="2011">
					<div class="col-md-6 col-xs-6 radial first"></div>
					<div class="col-md-6 col-xs-6 radial second"></div>

				</div>


				<div class="col-md-6 city-group" data-label="2010">
					<div class="col-md-6 col-xs-6 radial first"></div>
					<div class="col-md-6 col-xs-6 radial second"></div>

				</div>

				<div class="col-md-6 city-group" data-label="2009">
					<div class="col-md-6 col-xs-6 radial first"></div>
					<div class="col-md-6 col-xs-6 radial second"></div>
				</div>


			</div>
		</div>


	</div>

<div class="data-disclosure col-md-12">
	<div data-dimension="A">
			<h5>Attendance at Arts Organizations</h5>

			<p>Public attendance at museums and performing arts centers is an indication of the community's cultural vitality and participation.</p>
			<p>Attendance at Miami-Dade's largest arts organizations has increased 11% in two years.  In 2013, nearly 1.7 million people visited these organizations.</p>


		<p class="source">Source: <a href="http://www.miamidadearts.org/" title="Miami-Dade County Department of Cultural Affairs">Miami-Dade County Department of Cultural Affairs (unpublished data)</a>.</p>

	</div>
	<div data-dimension="B">
			<h5>Employment in Arts &amp; Culture</h5>
			<p>The size of a cultural economy can be measured in terms of how many full-time workers are employed.</p>
			<p>Miami-Dade county has far fewer workers per capita in Arts &amp; Culture than the US and other major metros. Arts &amp; Culture includes Performing Arts organizations, Museums, and Independent Artists, Performers, and Writers.</p>
			<p>Full Circle: 1,500 jobs.</p>

		<p class="source">Source: <a href="http://www.bls.gov/cew/" title="Bureau of Labor Statistics">Bureau of Labor Statistics</a>
			<br>Data Showing: Central County
		</p>

	</div>

	<div data-dimension="C">
			<h5>Art Dealers Per Capita</h5>

			<p>The cultural economy is supported by private art collectors and galleries.</p>
			<p>Miami-Dade county has far more Art Dealers per capita than the US and most major metros with the exception of New York.</p>


		<p class="source">Source: <a href="http://www.bls.gov/cew/" title="Bureau of Labor Statistics">Bureau of Labor Statistics</a>
		<br>Data Showing: Central County</p>

	</div>

	<div data-dimension="D">
			<h5>National Endowment for the Arts Grants Per Capita</h5>

			<p>The US government funds local art through the National Endowment for the Arts.</p>
			<p>Miami-Dade county receives a smaller amount of grant dollars per capita than the US average. Chicago receives twice as much as Miami-Dade, and New York receives 36 times.</p>


		<p class="source">Source: <a href="http://apps.nea.gov/GrantSearch/" title="National Endowment for the Arts">National Endowment for the Arts</a>
			<br>Data Showing: Central County
		</p>

	</div>

	<div data-dimension="E">
			<h5>Tourism Impact	</h5>

			<p>Tourism plays a large role in boosting the Arts &amp; Cultural economy.</p>
			<p>More than 14 million people visited Greater Miami in 2013, an increase of 2.2% over 2012.</p>


		<p class="source">Source: <a href="http://partners.miamiandbeaches.com/~/media/files/gmcvb/partners/research%20statistics/annual_report_2013" title="Greater Miami Convention &amp; Visitors Bureau">Greater Miami Convention &amp; Visitors Bureau</a>
			<br>Data Showing: Central County
		</p>

	</div>

</div>
