<?php
/**
 * @package om_musa
 */

wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.ia.graph.js', array('d3'), '1.0', true );
wp_enqueue_script( 'iac-hs-healthy-choices', get_template_directory_uri() . '/js/iac-hs-healthy-choices.js', array('d3'), '1.0', true );


?>

<div class="swap objective two">

	<div class="item" data-swap="1" data-dimension="A">
		<span>% of Population who Smoke</span>
	</div>

	<div class="item" data-swap="0" data-dimension="B">
		<span>% OF POPULATION WHO ARE OBESE</span>
	</div>

	 

</div>
	<div class="col-md-6 col-xs-11 centered year-labels">
		<div class="col-md-6">

			<div class="item col-md-3 col-xs-3 apple green">2010</div>
			<div class="item col-md-3 col-xs-3 purp">2012</div>

		</div>
 
	</div>

	<div class="col-md-12 col-xs-12 circles">
		<div class="col-md-6 city-group" data-label="chi">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Chicago"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Chicago"></div>
		</div>

		<div class="col-md-6 city-group" data-label="hou">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Houston"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Houston"></div>
		</div>

		<div class="col-md-6 city-group" data-label="mia">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Miami"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Miami"></div>
		</div>


		<div class="col-md-6 city-group" data-label="nyc">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="NYC"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="NYC"></div>
		</div>

		<div class="col-md-6 city-group" data-label="sd">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="SD"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="SD"></div>
		</div>

		<div class="col-md-6 city-group" data-label="us">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="US"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="US"></div>
		</div>



	</div>

<div class="data-disclosure col-md-12">
	<div data-dimension="A">

		<h5>% OF ADULTS WHO ARE CURRENT SMOKERS</h5>
		
		<p>Historically, smoking has been one of the leading cause of preventable death in the US.</p>


		<p>Fewer adults in Miami-Dade county smoke than the US average and most major metros.</p>

		<p class="source">Source: <a href="http://apps.nccd.cdc.gov/BRFSS-SMART/ListMMSAQuest.asp?yr2=2012&MMSA=All&cat=TU&qkey=8161&grp=0" title="Centers for Disease Control">Centers for Disease Control</a><br>

		Data Showing: Central County</p>
	</div>

	<div data-dimension="B">

		<h5>% OF POPULATION WITH A BODY MASS INDEX OF 30 OR HIGHER</h5>
		
		<p>In recent years, obesity has rivaled smoking as the leading cause of preventable death in the US.</p>

		<p class="source">Source: <a href="http://apps.nccd.cdc.gov/BRFSS-SMART/ListMMSAQuest.asp?yr2=2012&MMSA=All&cat=TU&qkey=8161&grp=0" title="Centers for Disease Control">Centers for Disease Control</a>
			<br>Data Showing: Central County
		</p>
	</div>

</div>
