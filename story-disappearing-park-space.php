<?php
/**
 * @package om_musa
 */
?>

<?php 

	wp_enqueue_script( 'tourism-impact', get_template_directory_uri() . '/js/story-viz-disappearing-park-space.js', array('d3'), '1.0', true );
	wp_enqueue_style( 'om-musa-story-tourism-impact', get_template_directory_uri()  . '/css/story-tourism-impact.css');

 ?>

 	<div class="instructions">
 		<h4>MOVE SLIDER TO SEE MIAMI-DADE OVERNIGHT VISITORS BY YEAR AND THEIR ECONOMIC IMPACT.</h4>
 	</div>

	
	<div class="legend">
		
		<div id="tourism-slider"></div>

	</div>
 
 
 <div id="dudes">
 	
<!-- <div id="dude-overlay">
 	<span class="dude-icon active overlap"></span>
 	<span class="dude-icon overlap"></span>
 </div> -->
 </div>



 <svg version="1.1" id="usa" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100%" height="100%" viewBox="0 0 800 480" style="enable-background:new 0 0 800 400;"  preserveAspectRatio="xMinYMin meet">
		<style type="text/css">
		 
			.st0{fill:#00AFDB;}
		 
		</style>

		<polygon class="st0" points="78.069,40.158 80.148,67.305 73.912,75.658 65.596,58.952 65.596,48.511 57.281,44.335 51.045,54.776 
			65.596,100.715 57.281,188.419 66.15,228.741 90.542,248.977 103.015,290.739 121.724,290.739 148.748,322.062 179.93,315.798 
			227.742,338.768 270.458,338.768 304.657,355.473 304.657,361.736 319.208,376.354 333.76,363.825 352.468,368.002 369.099,401.412 
			385.729,420.207 393.341,412.699 396.123,403.501 442.697,365.301 479.273,371.083 520.849,353.385 546.873,369.166 
			574.898,363.825 581.135,376.354 576.977,399.324 593.607,428.56 604,439 616.469,439 612.747,411.854 596.36,352.878 
			651.878,280.588 687.096,190.893 728.728,171.713 732.885,140.391 776.54,113.245 772.382,75.658 749.516,67.305 730.807,96.539 
			676.759,111.157 655.97,140.391 624.789,142.479 628.686,149.512 576.977,175.89 570.74,165.449 583.213,146.655 564.504,134.126 
			572.82,113.245 543.716,109.069 533.322,140.391 529.165,165.449 516.692,167.537 510.456,152.919 514.613,115.333 531.243,94.451 
			568.661,90.275 547.874,79.833 518.77,65.217 510.456,67.305 483.433,86.099 466.801,79.833 491.746,56.864 481.353,48.511 
			450.171,50.599 425.226,40.158 "/>
			<g class="city dot" data-city="Miami" style="opacity: 1;">
				 <text class="city-name label" transform="translate(680,420)">Miami</text>
				 <circle class="city-circle active" stroke-width="9" r="18" transform="translate(618,430)"></circle>
			</g>
</svg>



<div class="values">
	<span class="visitors" data-value="">
		
	</span>
	<span class="tourism-dollars" data-value="">
		
	</span>
	<img src="<?php echo get_template_directory_uri() ?>/images/million-visitors.png" class="visitors-legend">
		<img src="<?php echo get_template_directory_uri() ?>/images/billion-dollar.png" class="tourism-dollars-legend">
	
</div>




 