<?php
/**
 * @package om_musa
 */
?>


<!-- story-parent ->

<section id="post-<?php the_ID(); ?>" <?php // post_class('col-md-9 centered'); ?>>
	<!- - <header class="entry-header">
		<?php // the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		
	</header><!- - .entry-header ->

	<div class="entry-content">
		<?php // the_content(); ?>
		



	</div> <!-  .entry-content ->



	 	
</section><!- #post-## -->





<?php 
	
 

// WP_Query arguments
		
		$storyID = $post->ID;
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$args = array (
			'post_type'              => 'story',
			'order'                  => 'ASC',
			'orderby'                => 'menu_order',
			'post_parent' 			 => $storyID,
			'paged' 			 	 => $papged,
			'posts_per_page'		 => -1			

		);

		// The Query
		$storyChildren = new WP_Query( $args );

		
		$counter = 0;

		// The Loop
		if ( $storyChildren->have_posts() ) {
			$post = $posts[0]; $c=0;
			while ( $storyChildren->have_posts() ) {
				$storyChildren->the_post();
				
				?>



				
				
				
				<!-- <div class="well">

					<?php //  echo $storyChildren->request; ?>

				</div> -->




				<?php  
				$counter++;
				// echo $counter;
				if( $counter == 1) { 

					get_template_part( 'story', 'first-child' );

				}   
				else {

		
					get_template_part( 'story', 'child' );

					
					
				} 
					
						
				

				

			}


		 
	
		?>
<!-- story-parent -->
			<div class="col-md-6 centered">

				<div class="col-md-4">

					<img src="<?php echo get_template_directory_uri(); ?>/images/share-icon.png" id="share-icon" class="icon">

					<div class="story-meta-box short">
						<h4><a href="/get-engaged/accelerator-grants/" title="Share Your Ideas">SHARE YOUR IDEAS</a></h4>
					</div>

					<div class="story-meta-box short social">
						<h4>SHARE THIS STORY</h4>
						<?php get_template_part( 'util', 'social-share' ); ?>
					</div>


				</div>
				<div class="col-md-4">
					<a href="/get-engaged/local-non-profits" title="Local Non-Profits">
					<img id="learn-icon" class="icon" src="<?php echo get_template_directory_uri(); ?>/images/learn-icon.png">
					</a>	
					<div class="story-meta-box med">
						<h4><a href="/get-engaged/local-non-profits" title="Local Non-Profits">Explore other non-profits</a>
						</h4>
					</div>
				</div>
				<div class="col-md-4">
					<a href="http://www.handsonmiami.org" title="Our Programs">
					<img id="volunteer-icon" class="icon" src="<?php echo get_template_directory_uri(); ?>/images/volunteer-icon.png">
					</a>
					<div class="story-meta-box med">
						<h4><a href="http://www.handsonmiami.org" target="_blank" title="Hands on Miami">Volunteer with HandsOn Miami</a> </h4>
					</div>
				</div>

			</div>


				


		<?php
			 
				

 


		} else {
			// no posts found
		}

		// Restore original Post Data
		wp_reset_postdata();

 ?>

 
