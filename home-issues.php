<?php
/**
 * Issues zone on homepage. 
 *
 *
 * @package om_musa
 */

 ?>
<!-- home-issues -->

		<div id="issues-title" class="col-md-12 centered"></div>

  		<div class="col-md-12 issue-wrapper">
  				<?php
					
					

					$args = array(
					  'orderby' => 'name',
					  'order' => 'ASC',
					  'parent' => 0,
					  'taxonomy'=>'issue-areas'
					  
					  );

					$issueAreas = get_terms('issue-areas',$args);
					
					  foreach($issueAreas as $issue) { 
					  //  echo '<p>Category: <a href="' . get_term_link( $issue ) . '" title="' . sprintf( __( "View all posts in %s" ), $issue->name ) . '" ' . '>' . $issue->name.'</a> </p> ';
					   // echo '<p> Description:'. $issue->description . '</p>';

					$issueName = (string)$issue->slug;

					?>
					<?php 	switch($issueName) {
									 			case "economy";
									 			$boton = "boton1";
									 			break;
									 			case "arts-culture";
									 			$boton = "boton7";
									 			break;
									 			case "civic-engagement";
									 			$boton = "boton6";
									 			break;
									 			case "environment-public-space";
									 			$boton = "boton2";
									 			break;
									 			case "health-safety";
									 			$boton = "boton3";
									 			break;
									 			case "housing-affordability";
									 			$boton = "boton4";
									 			break;
									 			case "transportation";
									 			$boton = "boton5";
									 			break;
									 			case "education";
									 			$boton = "boton8";
									 			break;
									 			
									 		} 

									 		?>

					 <div id="<?php echo $boton; ?>" class="issue-buffer col-md-3 col-xs-6" onmouseenter=funcion("<?php echo $boton; ?>") data-url="<?php echo get_term_link( $issue );  ?>">

						<div id="<?php echo $issueName; ?>" class="issue-thumb" >
							<div class="issue">
									<span class="issue-wrapper">
										<h4>
											<a href="<?php echo get_term_link( $issue );  ?>" title="<?php echo $issue->name; ?>">
												<?php echo $issue->name; ?></a>
										</h4>
									<p><?php echo $issue->description ;?></p>

									<?php 
										 
										 $moreArgs = array(
										 		'post_type' => 'story',
										 		'posts_per_page' => 1,
										 		'menu_order' => 10,
										 		'tax_query' => array(
										 				array(
										 			'taxonomy'=>'issue-areas',
										 			'field'=>'slug',
										 			'terms'=>$issueName
										 				)
										 			)
										 	);

										 $recentStoryQuery =  new WP_Query( $moreArgs );
										   
												?> <p class="go"><a href="<?php echo get_term_link( $issue );  ?>">Go</a></p> <?php
												





												// Restore original Post Data
												wp_reset_postdata();
										 


									 ?>	





									</span>
									
							</div><!-- .issue -->
							<div class="svg">
								<?php 

									 // Loading SVG for each issue area based on the issue slug,
									 // to make a loop and get the correspoinding template file.

									 		switch($issueName) {
									 			case "economy";
									 			get_template_part( 'svg', 'boton1' );
									 			break;
									 			case "arts-culture";
									 			get_template_part( 'svg', 'boton7' );
									 			break;
									 			case "civic-engagement";
									 			get_template_part( 'svg', 'boton6' );
									 			break;
									 			case "environment-public-space";
									 			get_template_part( 'svg', 'boton2' );
									 			break;
									 			case "health-safety";
									 			get_template_part( 'svg', 'boton3' );
									 			break;
									 			case "housing-affordability";
									 			get_template_part( 'svg', 'boton4' );
									 			break;
									 			case "transportation";
									 			get_template_part( 'svg', 'boton5' );
									 			break;
									 			case "education";
									 			get_template_part( 'svg', 'boton8' );
									 			break;

									 			
									 		}

							?></div>
						</div><!-- issue-thumb -->
							
							 
					</div><!-- issue-buffer -->
			 

				<?php					     } 


					?>
  		</div>
 
