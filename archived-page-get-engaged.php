<?php
/*
Template Name: Get Engaged
*/



/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package om_musa
 */

get_header(); ?>
<!-- page-get-engaged -->
	<div id="primary" class="content-area row">
		<main id="main" class="site-main col-md-12" role="main">

			<div class="background" style="background-image:url('<?php 
											$coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											echo $coverimageurl
										 ?>');" class="">

									<!--	 
										//		<?php 
										//		$story_kicker = get_post_meta( get_the_ID(), 'story-kicker', true );
										//			// check if the custom field has a value
										//			if( ! empty( $story_kicker ) ) {
										//			  ?> <h4 class="kicker center align"> <?php 
										//			echo $story_kicker; ?></h4> <?php 
										//			} else {
										//				the_title( '<h4 class="kicker center align">', '</h4>' );
										//			} ?>

									 -->

		 			
	  			 <div class="col-md-12 centered sub-page-container">

				  	<div class="col-md-6">
				  			<h1 class="entry-title internal page">Our Programs</h2>

				  			<?php global $post; 

	  			 			$parent = $post->ID;
	  			 			

	  			 			$args = array (
								'post_type' 		=> 'page',
								'post_parent' 		=> 1708,
								'order'				=> 'ASC',
								'orderby'			=> 'menu_order',
								'posts_per_page'    => -1
								
							);
							$child1_query = new WP_Query( $args ); ?>
							 

	  			 	 
	  			 	
			  			 	<?php if ( $child1_query->have_posts() ) : ?>

									<?php /* Start the Loop */ ?>
									<?php while ( $child1_query->have_posts() ) : $child1_query->the_post(); ?>
		 
											<div class="child">



												<?php
													/* Include the Post-Format-specific template for the content.
													 * If you want to override this in a child theme, then include a file
													 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
													 */
													 
							 						get_template_part( 'page', 'content-child-vertical' );


													?>
		 
											
											</div>
									 

										

									<?php endwhile; ?>

									 	

							<?php else : ?>

									<?php get_template_part( 'content', 'none' ); ?>

							<?php endif; ?>
						
							<?php om_musa_paging_nav(); ?>
					
							<?php wp_reset_postdata(); ?>
				  	</div>

				  	<div class="col-md-6">
				  			<h1 class="entry-title internal page">Local Non-Profits</h2>
				  		<?php global $post; 

	  			 			$parent = $post->ID;
	  			 			

	  			 			$args = array (
								'post_type' 		=> 'page',
								'post__in'			=> array( 1415 ),
								'order'				=> 'ASC'
								
							);
							$child2_query = new WP_Query( $args ); ?>
							 

	  			 	 
	  			 	
			  			 	<?php if ( $child2_query->have_posts() ) : ?>

									<?php /* Start the Loop */ ?>
									<?php while ( $child2_query->have_posts() ) : $child2_query->the_post(); ?>
		 
											<div class="child">



												<?php
													/* Include the Post-Format-specific template for the content.
													 * If you want to override this in a child theme, then include a file
													 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
													 */
													 
							 						get_template_part( 'page', 'content-child-vertical' );


													?>
		 
											
											</div>
									 

										

									<?php endwhile; ?>

									 	

							<?php else : ?>

									<?php get_template_part( 'content', 'none' ); ?>

							<?php endif; ?>
						
							<?php om_musa_paging_nav(); ?>
					
							<?php wp_reset_postdata(); ?>		
				  	</div>

	  			 		
	  			</div>			
	  			 		

			</div>	<!-- background -->

				<div class="page-footer">
						<a href="<?php 
							$pageFooterLink = get_post_meta($post->ID, 'page-footer-cta-link', true);
							echo $pageFooterLink;
							?>" 

							title="<?php echo $pageFooterCTA;  ?>" 

							class="page-footer-link page-footer-link-icon">
								<?php 
				 			 	$pageFooterCTA = get_post_meta($post->ID, 'page-footer-cta', true);
				 				 if ($pageFooterCTA !="" ) {
				 				 	?>
				 			 	
				 			 		<?php echo $pageFooterCTA; ?>

				 			 	<?php
				 			 	

				 				 } else {
				 			 		?>
				 			 		More
				 			 	<?php
				 			  

				 			 }?>
		 				
		 				</a>
				</div>
			
			 

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>