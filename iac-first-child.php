<?php
/**
 * @package om_musa
 */
?>
<!-- parent: taxonomy-issue-areas -->
<!-- iac-first-child -->
<?php 
wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.ia.graph.js', array('d3'), '1.0', true );
wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );	
 ?>


<div class="story-anchor col-md-12">

	<?php 
		// This finds the value set in the Menu Order field in the WP Editor.

		$menu_o = $wpdb->get_var( "SELECT menu_order FROM $wpdb->posts WHERE ID=" . $post->ID  ); ?>
				
		<span id="dot-<?php echo $menu_o; ?>" class="story-dot-navigation">

			<?php 
 			 	$storyKicker = get_post_meta($post->ID, 'story-kicker', true);
 			 if ($storyKicker !="" ) {
 			 	?>
 			 	<a href="#<?php echo $post->post_name; ?>" data-kicker="<?php echo $storyKicker; ?>" class="text">

 			 	<?php
 			 	

 			 } else {
 			 	?>
 			 	<a href="#<?php echo $post->post_name; ?>" data-kicker="" class="text">
 			 	<?php
 			  

 			 }?>
 				
 			</a>
   
   

		</span>

		<span data-trigger="<?php echo $menu_o; ?>"></span>

		 

</div>

<?php 
	// Utility to set a custom class via custom fields, to the article element below. 
	get_template_part('util','story-class' ); ?>

<?php 
	
	$story_class = get_post_meta( $post->ID, 'storyclass', true );
	if($story_class !="" ) {
		

		$storyClasses = array(
					'col-md-12 col-xs-12',
					(string)$story_class,
				);
	} else {


		$storyClasses = array(
					'col-md-12 col-xs-12'
					
				);
	} 
				
 ?>


<article id="post-<?php the_id(); ?>" <?php post_class($storyClasses); ?>>
	
	<div class="centered">

		<header class="entry-header">
			
			  <span id="<?php echo $post->post_name; ?>" class="anchor"></span>  

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<?php 
				$story_hook = get_post_meta( get_the_ID(), 'om_musa_story_hook', true );
				// check if the custom field has a value
				if( ! empty( $story_hook ) ) {
				  ?> <h2> <?php echo $story_hook; ?></h2> <?php 
				} else {
					the_excerpt(  );
				} ?>
 
			
		</header><!-- .entry-header -->

		<div class="entry-content col-md-12 centered">

			 
			
			<?php the_content(); ?>
			 
			


		</div><!-- .entry-content -->

		<div id="issue-area-objectives" class="col-md-12">
				 
				 
				 
				 <?php
					// This finds the post slugged About This Issue Area and excludes it from the query.
					// This query is for the 1,2,3 Objective boxes in the parent Issue Area section.				 	 
				 	$aboutID = url_to_postid('issue-area-data/'. $term .  '/about-this-issue-area' );
				 	
				 	$storyID = $post->ID;
				 	$iacChildArgs = array(
				 							'post_type'    => 'issue-area-content',
				 							'post_parent'  => $storyID,
				 							'orderby'	   => 'menu_order',
				 							'order'        => 'ASC',
				 							'status'	   =>	'publish',
				 							'posts_per_page' => -1,
				 							'post__not_in' => array($aboutID)
				 		);	

				 	$iacChildren = new WP_Query($iacChildArgs);

				 	if($iacChildren->have_posts() ) {
				 		while($iacChildren->have_posts() ) {
				 			$iacChildren->the_post(); ?>
				 			
				 			
				 			<div id="objective-<?php echo $storyID ?>" class="objective col-md-4 col-sm-4 col-xs-12">
				 				

				 				<?php $sub_menu_o = $wpdb->get_var( "SELECT menu_order FROM $wpdb->posts WHERE ID=" . $post->ID  ); ?>

				 			 
				 				<div class="objective-box">

				 						<?php 
				 							if ($sub_menu_o == 10) {
				 								
				 								?>
				 								<div class="apple green fill circle one">
				 									<span class="circle-label">
				 										<a href="#<?php echo $post->post_name ?>" class="white" title="<?php the_title(); ?>">1</a>
				 									</span>
				 								</div>

				 								<h4>
							 						<a href="#<?php echo $post->post_name ?>" class="apple green" title="<?php the_title(); ?>">
							 							<?php the_title(); ?>
							 						</a>
							 					</h4>
				 								<?php

				 							} else if ($sub_menu_o == 20) {

							 					?>
				 								<div class="cyan fill circle two">
				 									<span class="circle-label">
				 										<a href="#<?php echo $post->post_name ?>" class="white" title="<?php the_title(); ?>">2</a>

				 									</span>
				 								</div>

				 								<h4>
							 						<a href="#<?php echo $post->post_name ?>" class="cyan" title="<?php the_title(); ?>">
							 							<?php the_title(); ?>
							 						</a>
							 					</h4>
				 								<?php			
				 							} else if ($sub_menu_o == 30) {

				 								?>

				 								<div class="purp fill circle three">
				 									<span class="circle-label">
				 										<a href="#<?php echo $post->post_name ?>" class="white" title="<?php the_title(); ?>">3</a>
				 									</span>
				 								</div>
												<h4>
							 						<a href="#<?php echo $post->post_name ?>" class="purp" title="<?php the_title(); ?>">
							 							<?php the_title(); ?>
							 						</a>
							 					</h4>
				 								<?php
				 							} else if ($sub_menu_o == 40) {

				 								?>

				 								<div class="apple green fill circle four">
				 									<span class="circle-label">
				 										<a href="#<?php echo $post->post_name ?>" class="white" title="<?php the_title(); ?>">4</a>
				 									</span>
				 								</div>
												<h4>
							 						<a href="#<?php echo $post->post_name ?>" class="apple green" title="<?php the_title(); ?>">
							 							<?php the_title(); ?>
							 						</a>
							 					</h4>
				 								<?php
				 							}
				 						 ?>
				 				 
				 					

				 				</div>



				 			</div>

				 			

				 		<?php } // while
				 	
				 	} else {
									// no posts found
							}

		// Restore original Post Data
		wp_reset_postdata();

				  ?>
				
				 

		</div>

	</div>

	 
</article><!-- #post-## -->


