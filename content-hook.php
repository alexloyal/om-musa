<?php
/**
 * @package om_musa
 */
?>
<!-- content-hook -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-9 centered'); ?>>
	<header class="entry-header">

			<?php 
				$story_hook = get_post_meta( get_the_ID(), 'om_musa_story_hook', true );
				// check if the custom field has a value
				if( ! empty( $story_hook ) ) {
				  ?> <h1 class="story-hook"> <a href="<?php 

				  			$external_link = get_post_meta( get_the_id(), 'om_musa_story_custom_external_link', true );
									if (! empty($external_link ) ) {
										echo $external_link;
									} else { 
									 the_permalink(); 
									}

				   ?>" title="<?php echo $story_hook; ?>"><?php echo $story_hook; ?></a></h1> <?php 
				} else {
					?> <h1 class="story-hook"><a href="<?php 

							$external_link = get_post_meta( get_the_id(), 'om_musa_story_custom_external_link', true );
									if (! empty($external_link ) ) {
										echo $external_link;
									} else { 
									 the_permalink(); 
									}

					 ?>" title="<?php echo $story_hook; ?>">the_title();</a></h1>
					<?php
				} ?>


 

	 
	</header><!-- .entry-header -->

	<div class="entry-content">
		 <div class="tap quote">
									<a href="<?php 

									$external_link = get_post_meta( get_the_id(), 'om_musa_story_custom_external_link', true );
									if (! empty($external_link ) ) {
										echo $external_link;
									} else { 
									 the_permalink(); 
									}
									?>" class="<?php the_id(); ?>">
										<?php 
										$story_engage_image = get_post_meta( get_the_ID(), 'om_musa_story_engage_image', true );
										// check if the custom field has a value
										if( ! empty( $story_engage_image ) ) {
										  ?> <img src="<?php bloginfo('template_directory' ); echo $story_engage_image; ?>" /> <?php 
										} else {
											the_title( '<p>', '</p>' );
										} ?>
									</a>
		</div>
	</div><!-- .entry-content -->

 
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
