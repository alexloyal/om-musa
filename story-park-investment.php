<?php
/**
 * @package om_musa
 */
?>
<!-- story safety first -->
<?php 

	wp_enqueue_script( 'pdata-svg', get_template_directory_uri() . '/js/story-viz-park-investment.js', array('d3'), '1.0', true );
	wp_enqueue_script( 'bar-graph', get_template_directory_uri() . '/js/bar.graph.js', array('d3'), '1.0', true );
	wp_enqueue_style( 'om-musa-story-park-investment', get_template_directory_uri()  . '/css/story-public-space-park-investment.css');	

 ?>

  <h4>PER RESIDENT SPENDIG ON PARKS AND RECREATION</h4>

					<div class="vertical-track-limit">
						<h5>$200 per capita</h5>
					</div>

					<div class="vertical-track-limit two">
						<h5>$100 per capita</h5>
					</div>     

					<div class="vertical-track-limit bottom">
						&nbsp;
					</div>     

 <div class="park-investment">

 		<div class="data-container col-md-12"></div>

 		<div class="city years centered col-md-10 legend">
 			<div class="track"><span class="dot">&bull;</span></div>
 			<div class="year" data-year="2011"><span>2011</span></div>
 			<div class="year active" data-year="2013"><span>2013</span></div>
 		</div>

 </div>