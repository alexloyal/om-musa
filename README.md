OurMiami.org WordPress Manual
MUSA Creative Lab
Alejandro A. Leal //alej.co

Outline
1.	Introduction
2.	WordPress overview
a.	Dashboard overview
b.	Plugins used
3.	Technology overview
a.	Twitter Bootstrap
b.	jQuery version
c.	D3 Version
4.	WordPress Template Architecture
a.	Editing posts/pages
b.	Editing Story pages
c.	Issue Area Content templates
d.	Page templates
5.	Taxonomy
a.	How terms relate to posts and stories.
6.	Reference
a.	Visualization template files
b.	Custom Field keys












1.	Introduction
OurMiami.org was built as a customized WordPress theme. This theme is based on the open source starter theme called “_s” (pronounced Underscores). As such, it allows flexible template deployment; a critical need in the design and architecture process for OurMiami.org.
WordPress is one of the Web’s most robust content management systems and is currently used by millions of sites across the internet.
OurMiami.org is hosted on Media Temple’s dedicated WordPress service. The main domain is registered under GoDaddy. 


2.	WordPress overview and Content Architecture
Out of the box, WordPress is a blogging platform but can be developed into a system with a wider scope. OurMiami.org uses WordPress pages and posts publishing content, in addition to two custom post types, Stories and Issue Area Content Areas.
To login to the Administration Dashboard, follow this link:
http://ourmiami.org/wp-login.php
[Screenshot Dashboard with highlighted sections]
The main content areas of the site can be found in the Posts tab, the Pages tab, the Story tab, and the Issue Area Content tab. Taxonomy terms like Categories, Issue Areas, and Tags can be found within those sections where they can be used, so for example, Issue Areas can be found under both  Story and under Issue Area Content sections. 
[Sitemap with color-coded taxonomy and content architecture]

OurMiami.org uses the following third-party WordPress plugins:
-	YOAST SEO Plugin: For enhanced Search Engine Optimization.
-	Duplicator: For backups.
-	Cache Plugin (Installed by MediaTemple)
-	Domain Plugin (Installed by MediaTemple)

3.	Technology 
Twitter Boostrap
OurMiami.org was developed using Twitter’s Bootstrap framework version 3.1.1, including its responsive grid system for adjusting to screens of all sizes.
http://getbootstrap.com/
jQuery
In order to accomplish some of the design goals of the site, jQuery was implemented to adjust dimensions of certain HTML elements, as well as for enhancing interactions with areas of the site. 
http://jquery.com/
D3
The main data visualization areas of the site were developed using the D3 javascript library. 
http://d3js.org/
Modernizr
Parts of the site were made backwards compatible with older browsers via the Modernizr script.
Perfect	Scroll
This script was used in a few of the Issue Area visualizations that extended beyond the design.
Waypoints
This script is what triggers visualizations when the user reaches those specific sections. 

4.	WordPress Template Architecture
Template files for OurMiami.org follow the WordPress starter theme called “_s” (Underscores). http://underscores.me/. Content for OurMiami.org was distributed among Posts, Pages, Stories, and Issue Area content areas. Each of these content types is represented by template files that follow the default naming convention. For example:
page-blog.php – Corresponds to the page template file for displaying the blog landing page.
story-sea-level.php – Corresponds to the file for rendering the Sea Level story and visualization.
iac-ec-job-opportunities.php – Corresponds to the content area for the Job Opportunties goal in the education issue area (iac: issue area content, ec: economy). 
content-excerpt.php – Corresponds to the file that renders the excerpt on all pieces of content. 

4a. Editing Posts and pages 
 Editing blog posts and static pages is done via these two corresponding tabs in the WordPress dashboard. Hover each menu to see a list of options, where you can browse to all posts or pages, or compose a new post or page. Under Posts you will also find a link to an overview page for both Categories and Issue Area taxonomies. 
[posts-pages.jpg]
To edit a post or a page, navigate to the “All Stories/Pages” page. From there you can navigate in reverse chronological order, or use the search field to find a post or a page. For best results, search for the headline of the post or page.
The main editor is the same for all content types but there are small variations on publishing options for each. The main common parts that are used throughout the site are the headline field, the content editor, the categories picker, the Issue Area picker, the tag field, the Featured image field, the author field, the custom fields section, as well as the SEO panel. 
Customizations via Custom Fields
Certain parts of the site can be customized via values added in the custom fields section. For blog posts, the following customizable areas are possible:
Guest authors
[guest-author.jpg]
To change the author of the post as it is displayed, select the om_musa_guest_author option from the Name column under Custom Fields, and add the value. Save the post.
[circle-thumb.jpg]
To change the circle thumbnail that appears on the blog landing page, upload a new image and insert its URL in the value field of the om_musa_circle_thumb Custom Field. Otherwise the default OurMiami.org icon will appear.

4b. Editing Story pages
OurMiami.org stories correspond to those rotating features on the homepage slider. Each of these is a package of individual sections, each one being a single “story” item in the database. These are managed via the “Story” tab in the WordPress dashboard. Much like blog posts and pages, you can navigate to a list of all “Story” items, or you can view a list of associated taxonomies: Issue Areas or Tags. 
 [story-menu.jpg]
Each story package is anchored by a “parent” item, which controls the following elements on the page:
-	Main headline.
-	Placement on homepage slider.
-	Story “Hook” quote on homepage slider.
The rest of the sections are “child” story items. 
To navigate to each individual group of stories, from the WordPress dashboard hover over the “Story” menu item, then click on All Stories. 
Once in the Story list page, select the specific Issue Area the story package is associated with from the taxonomy drop-down menu to filter the results.
Parent items will lead a list of sub-items identified by underscores next to their name. 
In this example, “Civic Participation” is the story parent, and the subsections are below. 
[stories-taxonomy-sorted.jpg]
Story Page and Visualization Template Structure
The best way to find individual story sections is to search for the headline using the built-in WordPress Dashboard search. You can also browse the list of stories and filter by Date, Tags, and Issue Areas.
[story-search.jpg]
To edit the main copy and headline of an individual Story section, click on the headline to navigate to a post editor screen. In addition to headline and copy, only the following areas can me modified via the WordPress dashboard:
a.	Headline/Main story copy.
b.	Parent Section selection
c.	Order
d.	CSS class.
e.	Story kicker
The order and parent item of each story is determined by the Attributes panel in the Story back-end.  Only parent items can appear on the homepage slider. 
It’s best not to alter the order number as this will have an impact on the organization of each story as well as the dot navigation on the left side of the story page. 
For each Story package, all stories are setup using the following order.  The number can be any within the range listed below.
Parent – Order: 0-9 
First Story – Order: 10-19
Second Story – Order: 20-29
Third Story – Order: 30-39
Fourth Story – Order: 40-49
Fifth Story – Order: 50-59
Last Story (Where Do We Go From Here) – Order: 90
[park-space.jpg]
The story kicker is a short (one, two word) description of each story. Its main purpose is to illustrate what the story is about in the dot-navigation area of the Story package. 
[story-kicker-custom-field.jpg]
Likewise, the storyclass custom field can take various options to change the background color of the Story section. 
The options are: thick grey, stone grey. 
Below the “Investment” story kicker as well as the Thick Grey storyclass are visible on the Transportation story, Investment Pays Off. 
[dot-navigation-investment.jpg]
Each Story features a visualization coded in D3 and Javascript. Because of the sensitivity of the code, this portion of the story is only accessible via the site’s templates in the FTP server.
To edit the visualization navigate to the theme directory via FTP:
html > wp-content > themes > om-musa
Story template files also follow the same naming convention as other content template files, and are identified by the headline. Here is a complete list:
[ftp-story-template-files.jpg]
The last story in each package is called “Where do we go From Here” for all, but they can be recognized by the parent items they’re associated with. 
[where-do-we-go-parent-items.jpg]
To set the Issue Areas the main story package is associated with, select them via the “Where do we go From Here” story.  This will associate the Issue Area terms with the parent page. For example, to set the Issue Areas for the Affordability story:
http://ourmiami.org/stories/affordability/
Edit this item: 
http://ourmiami.org/wp-admin/post.php?post=1543&action=edit
Finally, you can customize the Twitter message that is displayed when a Story is shared. This is done by entering the custom message in the Custom Fields section under the om_musa_tweet key.
[om_musa_tweet.jpg]

	4c. Issue Area Content templates
Editing Issue Area Content
Issue Area Content templates are structured in a parent/sibling manner, much like Story pages, and can be searched or browsed via the WordPress dashboard. 
[issue-area-content-editor.jpg]
Filter the results via the Issue Area drop-down. 
For each Issue Area package, all stories are setup using the following order. 
Parent – Order: 0-9
First Issue Area Objective – Order: 10-19
Second IA Objective – Order: 20-29
Third IA Objective – Order: 30-39
Fourth IA Objective – Order: 40-49
Fifth IA Objective – Order: 50-59
[education-obj-2-issue-area.jpg]
As with the Story content type, you can only edit these items via the Issue Area Content type WordPress dashboard. 
a.	Headline/Main story copy.
b.	Parent Section selection
c.	Order
d.	CSS class.
e.	Dot-navigation kicker

Editing Issue Area Captions
Again, like the Story content type, each Issue Area visualization is coded onto the template files. These can be found in the Theme’s directory via FTP. 
html > wp-content > themes > om-musa
These template files follow a similar naming pattern to Story templates:
[issue-area-template-files.jpg]
The first set of letters represent the content type, named Issue Area Content (IAC) in the database. 
The second set corresponds to the Issue Area taxonomy:
	ae: Arts & Culture
	ce: Civic Engageent
	ec: Economy
	ed: Education
	ev: Environment
	ha: Housing & Affordability
	hs: Health & Safety
	tr: Transportation

The last section of the file-name corresponds to the keywords associated with the individual Issue Area Objective. Those are part of the headline of the story. For example:
[issue-area-example-job-opportunities.jpg]
Corresponds to this template file: 
iac-ec-job-opportunities.php
There are two additional template files, iac-child.php and iac-first-child.php. These are necessary for the parent/sibling template system for each Issue Area page. 
	4c. Page Templates
The rest of the content on the site is distributed among various static pages. These can be found in the Page Editor tab:
http://ourmiami.org/wp-admin/edit.php?post_type=page
[page-editor.jpg]
All pages are managed via the page editor. To edit each page, click on the headline to bring up the WordPress editor. Certain pages are setup using a parent/child relationship which allows for flexible templates. Those custom files are here:
[page-template-files.jpg]
Each page is labeled according to the theme, so for example:
page-get-engaged.php corresponds to http://ourmiami.org/get-engaged/
page-issue-areas.php corresponds to http://ourmiami.org/our-study/issue-areas/
And so on.
There are two additional sets of template files that allow for the creation of new Pages with child sections similar to the About section. That template uses these files:
page-parent-content.php
page-content-child.php

Creating a new parent/child page template
Hover over the Pages menu and click on Add New. Give a title and some body copy, then select Parent Page With Content from the Template drop-down. Publish the page.
[new-parent-page.jpg]
To add a child page, create another page, and select the original page from the Parent dropdown.
[custom-child-page.jpg]
Publish when ready. Here’s the result:
[new-parent-page-published.jpg]
Publications, Partners, and Public Space Challenge pages
These pages make use of the Link section in the WordPress dashboard. To add a new link to each, simply create a new item under the link menu and categorize accordingly. 
[links-dashboard.jpg]
Important fields are the Image Address field and the Rating field. You can change the order of each set of links via the Rating menu. 

5.	Taxonomy
The default taxonomies of the site are Categories and Tags. These can be assigned to posts, but also to content in the Story section. They are only publicly visible on blog pages. 
OurMiami.org also features a custom taxonomy created to organize Stories, called Issue Areas. Those are: 
[issue-area-dashboard.jpg]
These can be associated with Stories and are displayed at the bottom of each story. They can also be associated with blog posts and those displayed on the blog sidebar. 
The content for each Issue Area landing page is managed via the Issue Area Content tab (see 4c). 
6.	Reference
How to edit story/issue area captions and visualization?
Those files are found in the theme directory in the FTP server. 
[issue-area-template-files.jpg]
[ftp-story-template-files.jpg]

Customizations via Custom Fields
Here is a list of Custom Field Keys and what they can control on the site.
om_musa_circle_thumb – Paste a new image URL to update the circle logo. 
om_musa_engage_image – Add a new image URL to update the link graphic on the homepage slider.
om_musa_story_hook – Add a subhead to a story. This also displays on the homepage slider.
om_musa_tweet – Add a custom Tweet message to a post or a Story.
page_footer_cta – Language to be used on the footer link at the bottom of the page template.
page-footer-cta-link – URL to link to at the bottom of the page template. 
story-kicker – This updates the word in the dot-navigation menu on Story and Issue Area Content items.
[footer-link.jpg]


For additional information, visit https://wordpress.org/
