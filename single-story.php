<?php
/**
 * The template for displaying all single posts.
 *
 * @package om_musa
 */

get_header(); ?>
<!-- single-story -->
	<section id="page-branding" class="col-md-12">
		
	</section>

	<div id="primary" class="content-area row">
		<main id="main" class="site-main col-md-12" role="main">

 
 
		<?php while ( have_posts() ) : the_post(); ?>
			

			<?php if( count(get_post_ancestors($post->ID)) <= 0 ) { 
    			
    			?>

			<!-- 	style="background-image:url('<?php 
											// $coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											// echo $coverimageurl
										 ?>');" -->

			<div class="parent">				
			


				<?php get_template_part( 'story', 'parent' ); ?>

							

							
			</div>

		<?php } else { ?>

				

				

				<?php  ?>

				<?php get_template_part( 'story', 'child' ); ?>

			<?php // om_musa_post_nav(); ?>

						


		<?php 
		}; // end else

		endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>