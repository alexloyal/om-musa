<?php
/**
 * @package om_musa
 */
?>

<?php 

	 wp_enqueue_script( 'traffic-volunteering', get_template_directory_uri() . '/js/residents-born-outside-civic-participation-viz.js', array('d3'), '1.0', true );
     // wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.graph.js', array('d3'), '1.0', true );

 ?>
 <section class="migration-impact-civic-participation">
        
        <div class="annotation">
            <span class="cyan dot">% of drivers with 30min. + commute</span>
            <span class="purp dot">% of residents that volunteer</span>
        </div>

 

       
<div class="legend col-md-10 centered">

        <div id="" class="city names col-md-12">
            <div data-city="Chicago" class="city-name">CHI</div>
            <div data-city="Houston" class="city-name">HOU</div>
            <div data-city="Miami" class="city-name">MIA</div>
            <div data-city="NYC" class="city-name">NYC</div>
            <div data-city="San Diego" class="city-name">SD</div>
            <div data-city="US" class="city-name">U.S.</div>
        </div>

        <div class="toggle" data-value="0">
            <div class="value" data-value="2010">2010</div>
            <div class="slider"></div>
            <div class="value" data-value="2011">2011</div>
        </div>


</div>
        
    </section>
 
 
 