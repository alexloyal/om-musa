<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package om_musa
 */
?>
<div class="col-md-7 centered sub-page-container">
	<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12 centered'); ?>>
		

		 

		<div class="col-md-10 col-xs-10 centered">
			<div class="entry-wrapper">
				<header class="entry-header">
				<?php 
									$story_hook = get_post_meta( get_the_ID(), 'om_musa_story_hook', true );
									// check if the custom field has a value
									if( ! empty( $story_hook ) ) {
									  ?> <h1 class="entry-title internal"> <?php echo $story_hook; ?></h1> <?php 
									} else {
										the_title( '<h1 class="entry-title internal">', '</h1>' );
									} ?>

			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php the_content(); ?>
				 <?php // get_template_part( 'util', 'social-share' ); ?>
			</div><!-- .entry-content -->

			 
			
			</div>
			<footer class="entry-footer">
				<?php edit_post_link( __( 'Edit', 'om-musa' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->
</div>
