<?php
/*
Template Name: What is Our Miami
*/



/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package om_musa
 */

get_header(); ?>
<!-- page-about -->
	<div id="primary" class="content-area row">
		<main id="main" class="site-main col-md-12" role="main">



		  <?php while ( have_posts() ) : the_post(); ?>
 
							<div class="background" style="background:url('<?php 
											$coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											echo $coverimageurl
										 ?>') no-repeat;" class="">			

										<?php
											/* Include the Post-Format-specific template for the content.
											 * If you want to override this in a child theme, then include a file
											 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
											 */
											 
					 						get_template_part( 'content', 'about-page' );


											?>
 							</div>

 							<div class="page-footer">
									<a href="<?php 
										$pageFooterLink = get_post_meta($post->ID, 'page-footer-cta-link', true);
										echo $pageFooterLink;
										?>" 

										title="<?php echo $pageFooterCTA;  ?>" 

										class="page-footer-link page-footer-link-icon">
											<?php 
							 			 	$pageFooterCTA = get_post_meta($post->ID, 'page-footer-cta', true);
							 				 if ($pageFooterCTA !="" ) {
							 				 	?>
							 			 	
							 			 		<?php echo $pageFooterCTA; ?>

							 			 	<?php
							 			 	

							 				 } else {
							 			 		?>
							 			 		More
							 			 	<?php
							 			  

							 			 }?>
					 				
					 				</a>
							</div>
									
									
							 

								
  		<?php endwhile; // end of the loop. ?>
				

				

			 

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>