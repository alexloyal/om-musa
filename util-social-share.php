<!-- util-social-share -->
<?php // global $post 
	
?>

<?php 
				  ?>

<div class="social-share">
	<?php 
			$parentLink = wp_get_shortlink($post->post_parent);
			$storyType = $post->post_type;

	 ?>

	 
	<div class="twitter net">
		<a data-url="
			<?php 
				if ($storyType == "story") {
					echo $parentLink;
				} else {
					echo wp_get_shortlink();
				}
			 ?>
		" 
		href="http://twitter.com/share?url=<?php if ($storyType == "story") {
					echo $parentLink;
				} else {
					echo wp_get_shortlink();
				} ?>;text=<?php 

				$tweetcopy = get_post_meta( get_the_ID(), 'om_musa_tweet', true );
				// check if the custom field has a value
				
				if( ! empty( $tweetcopy ) ) {

					

					echo urlencode($tweetcopy); // url encode
			
				} else {
			
					echo urlencode(the_title( )); // url encode
				
						}; ?>" 
		class="share-twitter twitter-icon" target="_blank"></a>
	</div>

	<div class="facebook net">
		<a data-url="<?php if ($storyType == "story") {
					echo $parentLink;
				} else {
					//echo wp_get_shortlink();
					the_permalink();
				} ?>" 
			href="http://www.facebook.com/share.php?u=<?php if ($storyType == "story") {
					echo $parentLink;
				} else {
					// echo wp_get_shortlink();
					the_permalink();
				} ?>" 
			class="share-facebook facebook-icon" target="_blank"></a>
		


		

	</div>

	

	<div class="contact net"><a href="/about/contact" class="email-icon" title="Contact Us"></a></div>
</div>

 