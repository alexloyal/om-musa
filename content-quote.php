<?php
/**
 * @package om_musa
 */
?>
<!-- content-quote -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-9 centered'); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<?php om_musa_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		 <div class="tap quote circle">
									<a href="<?php the_permalink(); ?>" class="<?php the_id(); ?>">
										Tap here to explore this story
									</a>
		</div>
	</div><!-- .entry-content -->

 
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
