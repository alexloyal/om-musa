<?php
/**
 * @package om_musa
 */
	wp_enqueue_script( 'iac-hs-health-safety', get_template_directory_uri() . '/js/iac-hs-health-safety.js', array('d3'), '1.0', true );
	
    wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );
?>

<div class="iac-hs-health-safety-content">

	<div class="swap objective one">

		<div class="item" data-swap="1" data-dimension="A">
			<span>Homicide Rate</span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>Robbery Rate</span>
		</div>

		<div class="item" data-swap="0" data-dimension="C">
			<span>Child Abuse</span>
		</div>

	</div>

<div class="col-md-6 year-labels centered">
	<span class="cyan" data-value=""></span>
	<span class="purp" data-value=""></span>
</div>


<div class="ia-hs-health-safety">
		<div class="graph"></div>
</div>


<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->

<div class="data-disclosure">
	<div data-dimension="A">
		<h5>MURDER &amp; NON-NEGLIGENT MANSLAUGHTER PER 100K POPULATION</h5>
		<p>Incidents of murder &amp; manslaughter threaten the safety and quality of life of residents.</p>
		<p>The Miami metro has a high murder rate - higher than most metros and the US. Fortunately, the murder rate has fallen in the last two years.</p>
		<p class="source">Source: <a href="http://www.fbi.gov/about-us/cjis/ucr/crime-in-the-u.s/2010/crime-in-the-u.s.-2010/tables/table-6" title="FBI Uniform Crime Report">FBI Uniform Crime Report</a></p>
	</div>
	<div data-dimension="B">
		<h5>ROBBERY PER 100K POPULATION</h5>
		<p>Robbery is one of the most common crimes in the US and produces both financial and physical harm.</p>
		<p>The Miami metro has a higher robbery rate than the US overall, but lower than most major metros.</p>
		<p class="source">Source: <a href="http://www.fbi.gov/about-us/cjis/ucr/crime-in-the-u.s/2010/crime-in-the-u.s.-2010/tables/table-6" title="FBI Uniform Crime Report">FBI Uniform Crime Report</a></p>
	</div>
	<div data-dimension="C">
		<h5>VERIFIED CHILD ABUSE RATE PER 1K CHILDREN</h5>
		<p>Child abuse has long-lasting physical, psychological, and behavioral consequences.</p>
		<p>Miami-Dade county's rate of reported child abuse is about half the rate of the state of Florida.</p>
		<p class="source">Source: <a href="http://dcfdashboard.dcf.state.fl.us/index.cfm?page=preview_sub&mcode=M0736&geoarea_id=R36&target=&purpose_id=sit&durationtype=YTD&fiscal=2011" title="Florida Department of Children &amp; Families">Florida Department of Children &amp; Families</a></p>
	</div>

</div>


</div>

 
 
 