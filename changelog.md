om-musa v1.0
=======================


om-musa v0.9.9.6
=======================
• Responsive style ehnacements.
• Park Space story initialized.


om-musa v0.9.9.3
=======================
• Testing myWebHook.


om-musa v0.9.9.2
=======================
• Mobile CSS fixes.


om-musa v0.9.8.7
=======================
• Web production updates and QA fixes.



om-musa v0.9.8.6
=======================
• CSS adjustments and template modifications per feedback from MF.


om-musa v0.9.8.5
=======================
• More CSS stylings and adjustments.


om-musa v0.9.8.4
=======================
• Arts Issue area complete.
• Added support for Perfect Scroll containers.

om-musa v0.9.8.3
=======================
• Checkpoint Christian.
• Styling updates, Issue Area data almost complete. Arts in place, need to style.
• Still missing one objective for education.


om-musa v0.9.8
=======================
• First initial review of content. Need a few more updates.

om-musa v0.9.7
=======================
• Second pass over data. Making improements on interactions.


om-musa v0.9.6
=======================
• First pass over data, making copy edits and data edits.
• Single blog page styles in place, need to adjust comment form for comment-author styling.
• Responsive styles adjustments for screens 1024px and above.

om-musa v0.9.5.9
=======================
• Transitions added to Issue Area pages.
• First QA for data. Need to adjust Arts and Education issue areas.

om-musa v0.9.5.1
=======================
• Integrated jQuery Perfect Scroll plugin (http://noraesae.github.io/perfect-scrollbar/) for longer data sets.

om-musa v0.9.5
=======================
• Issue Area data finessing by the Mighty Chris Gonzalez in place.
• Pending data sets for Arts and Education IAs.


om-musa v0.9.4
=======================
• Static pages styling and structure close to complete.
• Issue area tabs in place. Data adjusments for Health and Safety story.

om-musa v0.9.3.1
=======================
• Affordability story CSS stylings and interactivity improvements.

om-musa v0.9.2
=======================
• Interactive transitions added to Issue Area pages.
• Static page modifications. 

om-musa v0.9.1
=======================
• Data fixes by Chris Gonzalez.


om-musa v0.9
=======================
• Issue Area page architecture in place. Need to adjust Data sources and mapping.
 


om-musa v0.8.9
=======================
• Data scripts working (thanks to the mighty Chris Gonzalez) on Issue Area templates. 
• Housing and affordability IA page complete. Health and Safety at 80%.
• Integrated jQuery waypoints.
• Styling adjustments.  
 

om-musa v0.8.8
=======================
 • Page structure for Issue Area section almost complete. 
 • Developed styles for single blog post page. Need to adjust comment template. 


om-musa v0.8.7
=======================
 • Web production on pages for developing page styles.


om-musa v0.8.6
=======================
• Optimized and streamlined script for Urban Mobility. 

om-musa v0.8.5
=======================
• Defined more variable zones for Issue Area tempaltes to allow for faster deployment for pending Issue Area pages.
• Integrated data toggles to Issue Area templates for other data dimensions.
• New sections will still be needed for other data integration. 


om-musa v0.8.4
=======================
• Added support for Publication and Partner link categories. Each page will render its respective link category, in alphabetical order, along with a description, title, and background thumbnail. 

om-musa v0.8.3
=======================
• Fixed issue with Load More ajax action on blog page where button was overlapping the added posts when clikced. 

• Added support for Custom Social Share buttons on pages and posts.

om-musa v0.8.2
=======================
• About Us page structure almost done, default page template design elements in place. Added support for Parter Custom Post Type. Need to add queries for new Partners post type to page. 

om-musa v0.8.1
=======================
• Affordability story in place. Need to add interaction sheets. 

om-musa v0.8
=======================
• Affordability vs Impact on poverty almost complete. Need to figure out discrepancy in poverty alert when all data is displayed.

om-musa v0.7.9
=======================
• What About Hosuing construction cost vs median homesale value data bound to DOM elements. Need to paint stylesheets.

om-musa v0.7.8
=======================
• Data binding for Affordability and Its Impact on Poverty story complete. 
• About page template structure in place. Need to adjust styling and query parameters.

om-musa v0.7.7.5
=======================
• Adjustments to about page, parent page template. 
• Adding ability to use custom field value to place homepage slider.


om-musa v0.7.7
=======================
• Adjusted style sheets for demo presentation. 


om-musa v0.7.6
=======================
• Issue area page structure in place. Need to adjust styles.

om-musa v0.7.5
=======================
• Civic Participation stories in place.
• Further CSS adjustments. 

om-musa v0.7.4.9
=======================
• Residents Born Outside story structure in place. Need to add radials. 


om-musa v0.7.4.8
=======================
• Employment Growth v. Unemployment Rate chart in place. Need to adjust styles.


om-musa v0.7.4.7
=======================
• Parsed data for Affordability and Its Impact on Poverty story.


om-musa v0.7.4.6
=======================
• What Drives This story complete.
• Integrated second dimension into Lack of Civic Participation story.

om-musa v0.7.4.5
=======================
• What Drives This story elements in place.

om-musa v0.7.4.4
=======================
• Let's Talk About Affordability story in place. 

om-musa v0.7.4.3
=======================
• Traffic & Volunteering story under Civic Participation complete. 


om-musa v0.7.4.2
=======================
• Added HTML structure for Issue Area Content toggles and graphics.


om-musa v0.7.4.1
=======================
• Started Civic Participation story.

om-musa v0.7.4
=======================
• Refined styles on homepage.
• Refined responsive styles for vertical tablet screen size. 


om-musa v0.7.3.9
=======================
• Developed shell for Issue Area pages.
• Added support for content pages for each Issue Area. See readme for instructions on how to use.
• Removed script that deployed sub-menu on internal sections. 


om-musa v0.7.3.8
=======================
• Completed left-nav story navigation.


om-musa v0.7.3.7
=======================
• Education story package at 95%.
• Adjusted styling, improved submenu bar deployment.


om-musa v0.7.3.6
=======================
• Education story package at 90%.


om-musa v0.7.3.5
=======================
• Incorporated Let's Start From The Bottom chart. 
• Added styles for single page view.
• Modified About page structure. 


om-musa v0.7.3.4
=======================
• Added styles for blog. 


om-musa v0.7.3.3
=======================
• Fixed viewport issue for Firefox.
• Second purple track will not render.


  

om-musa v0.7.3.2
=======================
• Our Education Dashboard story at 80%.

  

om-musa v0.7.3.1
=======================
• Our Education Dashboard story at 60%.
• Added Tween percentage to circle. 
• Added ability to create double circle.
  


om-musa v0.7.3
=======================
• Our Education Dashboard story at 40%.
• Developed basic D3 data circle. 
• Installed Chartwell font. 


om-musa v0.7.2
=======================
• Our education dashboard HTML and WordPress integration in place. 
• Adjusted Menu Order functionality. Moving forward, it will be broken by by teens.
	Top post = 10
	Second post = 20
	Third post = 40
	Fourth post = 40

	Where do we go post = 90

Commenting tools and share tools get added at the top of post 90. 

• Added support for custom story class for easier background coloring. 
• Adjusted CSS for side-dot navigation. Each dot has an ID that corresponds to the Story menu order, this is targeted by the stylesheet for grouping and placement. 


om-musa v0.7.1.1
=======================
• New version name.

om-musa v0.7.1
=======================
• Education story at 30%: The ABC's of Miami Education in place. 

om-musa v0.7
=======================
• More CSS tweaks.


om-musa v0.6.9.6
=======================
• Made more adjustments to Urban Mobility story.


om-musa v0.6.9.5
=======================
• Ajusted styles. 
• Added cursor:pointer; argument to clickable SVG elements.


om-musa v0.6.9.4
=======================
• More styling adjustments for story page.
• Added kicker section above first sub-story content, linking back to parent story item.


om-musa v0.6.9.3
=======================
• Refinements to Urban Mobility story styling.



om-musa v0.6.9.2
=======================
• Adjustment to triggers.js script.




om-musa v0.6.9.1
=======================
• Urban Mobility story is at 99%. 
• More adjustments at urban mobility.


om-musa v0.6.9
=======================
• Adjusted more styling on homepage. 
• Urban Mobility story is at 99%. 


om-musa v0.6.8
=======================
• Adjusted more styling on homepage. 




om-musa v0.6.7
=======================
• Adjusted more styling on homepage. 
• Created page structure for other story pages.



om-musa v0.6.6
=======================
• Urban Mobility at 99%.

om-musa v0.6.5
=======================
• Safety story at 99%.



om-musa v0.6.4
=======================
• Safety story at 90%.
• Added basic animations.



om-musa v0.6.3
=======================
• Adjusted styles for homepage.
• Safety story at 45%.


om-musa v0.6.2
=======================
• Investment infographic at 90%.



om-musa v0.6.1
=======================
• Investment infographic at 50%.



om-musa v0.6
=======================
• First infographic complete. 


om-musa v0.5.9.1
=======================
• Style adjustments.

om-musa v0.5.9
=======================
• Developed SVG data for Investment Story. 
• Adjusted styles for homepage. 


om-musa v0.5.8
=======================
• Refined styles for homepage.
• Adjusted search drop-down.
• Improved dot navigation on story page template. 

om-musa v0.5.7
=======================
• Completed interaction for city names on mobile story.


om-musa v0.5.6
=======================
• Adjusted positioning of demo infographic.
• Added more style elements to story page.
• Coded first part of dot navigation for story pages. 


om-musa v0.5.5
=======================
• Styled tag links under blog & news.
• Added accent colors.
• Adjusted D3 script for Transportation page.

om-musa v0.5.4
=======================
• Made further adjustments to CSV delivery method.
• Styled sub-menu elements for better positioning. 
• Adjusted dimensions for first story in issue area. 
• Added first responsive breakpoints.

om-musa v0.5.3
=======================
• Installed D3 library.
• Added support for CSV data file integration.
• Wrote outline for first infographic. 
• Added animation for circle chart.


om-musa v0.5.2
=======================
• Added home-page style properties to match comps.
• Added design elements for home-page styling. 
• Added icon font for logos. 


om-musa v0.5.1
=======================
• Incorporated issue area SVGs.


om-musa v0.5
=======================
• Added support for taphover touch device class. 
• Adjusted sub-menu deployment to expand content area.
• Added icons for main navigation elements.
• Added basic styling to header. 
• Adjusted animation on issue hover.
• Adjusted display of blog & news tags.
• Added Gotham web font.


om-musa v0.4.6
=======================
• Added support for transparent background image on home news.


om-musa v0.4.5
=======================
• Added CSS3 transitions for menu items, issue area thumbnails.
• Added scrolling arrows for section navigation and back-to-top scroll.


om-musa v0.4.4
=======================
• Added meta box for homepage quote on story content type. 
• Added parent page template to display child pages.
• Added default page template for content sub-pages. 
• Added support for Get Engaged active sub menu.


om-musa v0.4.3
=======================
• Added support for loading archive posts via ajax call. 
• Added permalink for most recent story on issue area thumbnails on homepage. 


om-musa v0.4.2
=======================
• Added support for deployed sub-menu on search results and blog page.
• Added support for non-active sub-menus to reveal upon hover, restoring default sub-menu expansion depending on page (blog, search results).
• Added support for loading more search results via ajax.
• Adjusted behavior of content well when sub-menu is expanded. 

 
om-musa v0.4.1
=======================
• Developed structure for single post page.
• Added support for active sub-menu on blog page and single post page. 

om-musa v0.4
=======================
• Added support for blog page. 
• Added support for infinite scroll on blog page. 
• Styled individual blog items on blog page. 
• Styled social media icons.
• Added search icon.


om-musa v0.3
=======================
• Completed coding structure for homepage zones and elements.
• Added support for back-to-top scroll link. 
• Added About section mapped to page with slug about.
• Added support for footer menus. 
• Integrated Issue Area content type on homepage via WP_Query custom object. 
• Added support for issue area animation upon hover.

om-musa v0.2.3
=======================
• Added smooth scrooll for home sections.
• Added News section scroller mapped to news category. 
• Added Auhtor avatar, author name, post meta, and tag list to news section. 



om-musa v0.2.2
=======================
• Added support for anchor navigation.
• Refined jQuery logic for element height measurement and palce-setting.
• Added support for issue area thumbnail integration.
• Added support for scaling when hovering over issue area thumbnail. 



om-musa v0.2.1
=======================
• Updated readme.


om-musa v0.2
=======================
 • Base structure in place.
 • Main and sub navigation UI in place.
 • Added icons to navigation elements. 
 • Main action quote navigation UI in place. 
 • Set background image to story pane.
 • Styled drop-down and set placement. 
 • Finished UI base elements for #message zone.  
 • Added anchor smooth scrolling. 
 • Created Story custom post type.
 • Created Issue Areas taxonomy.
 • Integrated Issue Areas taxonomy to post post type.
 • Set equal heights to lower zones.



om-musa v0.1 - Initial deploy.
=======================
• Wrote outlining elements for HTML architecture of site. 
• Installed Modernizr script for modern browser feature detection.
• Installed jQuery v. 2.1.
• Installed jCarousel for slider controls. 
• Installed Bootstrap framework for grid system, interactive elements, and jQuery foundation.
• Installed _s framework for theme development on WordPress.


