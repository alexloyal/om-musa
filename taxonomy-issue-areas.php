<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package om_musa
 */

get_header(); 


/* 
This gets the Issue Area slug, which we will map to a query for the Issue Area Content type. 
They must match in order for this template to load the content.
*/

global $taxonomy,$term;

?>
<!-- taxonomy-issue-areas -->
 
 <?php 

 			// WP_Query arguments
			$args = array (
				'post_type'              => 'issue-area-content',
				'order'                  => 'ASC',
				'orderby'                => 'menu_order',
				'posts_per_page'		 => -1,
				'tax_query'				 => array(
													
													array(

														'taxonomy' => 'issue-areas',
														'field'	   => 'slug',
														'terms'	   =>  $term

														)		
											)
			);

			// The Query
			$issueAreaContent = new WP_Query( $args );

			// The Loop
			if ( $issueAreaContent->have_posts() ) {
				while ( $issueAreaContent->have_posts() ) {
					$issueAreaContent->the_post();
					
					?> 
					<?php $menu_o = $wpdb->get_var( "SELECT menu_order FROM $wpdb->posts WHERE ID=" . $post->ID  ); ?>


					 
					<?php 

						if ($menu_o <= 9) {

							get_template_part( 'iac', 'first-child' );
						
						} else {
						
							get_template_part( 'iac', 'child' );

						}

					  ?>

					<?php
				}

			} else {
				
				// no posts found



			}// else

			// Restore original Post Data
			wp_reset_postdata();

  ?>






<?php get_footer(); ?>
