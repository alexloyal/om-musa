<?php
/**
 * @package om_musa
 */


	wp_enqueue_script( 'iac-ed-student-expenditures', get_template_directory_uri() . '/js/iac-ed-student-expenditures.js', array('d3'), '1.0', true );
	
	// wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );	
    
?>

<div class="iac-ed-quality-education">

		<div class="swap objective one">

			<div class="item" data-swap="1" data-dimension="A">
				<span>Per Pupil Educational Spending</span>
			</div>

			<div class="item" data-swap="0" data-dimension="B">
				<span>Teacher Quality</span>
			</div>

			<div class="item" data-swap="0" data-dimension="C">
				<span>% of Schools Rated 'A' and 'B'</span>
			</div>

			<div class="item" data-swap="0" data-dimension="D">
				<span>Magnet School Enrollment</span>
			</div>

		</div>
	<div class="col-md-6 year-labels centered">
		<span class="cyan"></span>
		<span class="purp"></span>
	</div>


	<div class="iac-ed-education">
			<div class="graph"></div>
	</div>



	<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->

	<div class="data-disclosure">

		
			<div data-dimension="A">
				<h5>INSTRUCTIONAL EXPENDITURES PER STUDENT</h5>
				
				<p>The availability of instructional resources is an important contributor to the quality of a school.</p>


				<p>Miami-Dade County spends less per pupil than the US average.</p>

				<p class="source">Source: <a href="http://nces.ed.gov/ccd/elsi/quickFacts.aspx" title="National Center for Education Statistics">National Center for Education Statistics</a><br>
					Data Showing:  Central County / National</p>
			</div>

			<div data-dimension="B">
				<h5>% OF CLASSES TAUGHT BY HIGHLY QUALIFIED TEACHERS</h5>
				<p>Effective teachers are the single most important factor in furthing student achievement.</p>

				<p>Miami-Dade County has a high rate of qualified teachers, but a lower rate than the US.  And, the county rate has fallen in two years.</p>

				<p class="source">Source: <a href="http://doeweb-prd.doe.state.fl.us/eds/nclbspar/year0809/nclb0809.cfm?dist_schl=13_101" title="Florida Department of Education">Florida Department of Education</a><br>Data Showing:  Central County / State</p>
			</div>

			<div data-dimension="C">
				<h5>% OF SCHOOLS RATED 'A' OR 'B'</h5>

				<p>High quality schools improve the academic performance of all students.</p>

				<p>Miami-Dade county has a smaller percentage of top-rated schools than Florida, and the county percentage has fallen dramatically in the past two years. </p>

				<p class="source">Source: <a href="http://schoolgrades.fldoe.org/default.asp" title="Florida Department of Education">Florida Department of Education</a>
					<br>Data Showing:  Central County / State</p>

			</div>

			<div data-dimension="D">
				<h5>MAGNET SCHOOL ENROLLMENT AS % OF TOTAL</h5>

				<p>Magnet schools can provide students with specialized instruction in areas such as arts and science. Magnet schools can also be a powerful tool in creating diverse student bodies. </p>

				<p>Miami-Dade county has a very high percentage of students attending magnet schools.</p>

				<p class="source">Source: <a href="http://nces.ed.gov/ccd/elsi/tableGenerator.aspx; https://app1.fldoe.org/flbpso/nclbchoice/bpsoDirectory/directory.aspx" title="Florida Department of Education">Florida Department of Education</a><br>Data Showing:  Central County / State</p>

			</div>
		
		

		

	</div>

	


</div>

 
 
 