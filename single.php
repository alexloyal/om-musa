<?php
/**
 * The template for displaying all single posts.
 *
 * @package om_musa
 */

get_header(); ?>
<!-- single -->

	 


	<div id="primary" class="content-area row">
		<main id="main" class="site-main col-md-12" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<div class="background" style="background-image:url('<?php 
											$coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											echo $coverimageurl
										 ?>');" class="">				
			
		<!-- 	<h4 class="kicker center align">Blog &amp; News</h4> -->

				<?php get_template_part( 'content', 'single' ); ?>

							

							
			</div>



			<?php // om_musa_post_nav(); ?>

						
							
						
			

		<?php endwhile; // end of the loop. ?>

		

		</main><!-- #main -->
		<div class="page-footer col-md-12">
				<h4><a href="/blog" class="page-footer-link page-footer-link-icon"><span>Back to Articles</span></a></h4>
			</div>
	</div><!-- #primary -->


<?php get_footer(); ?>