<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package om_musa
 */
?>
<!-- content-stories -->

<div class="col-md-9 centered sub-page-container">
	<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12 centered'); ?>>
		

		 
		<div class="col-md-12 center entry-wrapper">
			<header class="entry-header">
				<?php 

			 			the_title( '<h1 class="entry-title internal center align">', '</h1>' );

						$story_hook = get_post_meta( get_the_ID(), 'om_musa_story_hook', true );
						// check if the custom field has a value
						if( ! empty( $story_hook ) ) {
						  ?> <h2 class="subhead"> <?php echo $story_hook; ?></h2> <?php 
						} else {
							 
						} ?>

			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php the_content(); ?>

				<div class="stories col-md-12">
					 	<?php 

								// WP_Query arguments
								$args = array (
									'post_type'         => 'story',
									'posts_per_page' 	=> -1,
							 		'post_parent' 		=> 0,
							 		'meta_key'			=> 'om_musa_story_custom_external_link',
							 		//'meta_value'		=> '',
							 		'meta_compare' 		=> 'NOT EXISTS'
							 		// 'post__not_in' => array(2746)	
								);

								// The Query
								$stories = new WP_Query( $args );

								// The Loop
								if ( $stories->have_posts() ) {
									while ( $stories->have_posts() ) {
										$stories->the_post();

										?>
									<div class="issue-buffer col-md-3 col-xs-6" data-url="<?php the_permalink(); ?>">

										<?php 
											$circle_thumb = get_post_meta( get_the_ID(), 'om_musa_circle_thumb', true );
											if( ! empty( $circle_thumb ) ) {
												?> <div class="issue-thumb" style="background-image:url(<?php echo $circle_thumb ;?>);">
											
											<?php 
											} else {
													$attr = array(
													'class' => 'circle',
													'alt' => $post->post_title
												);
													?>
													<div class="issue-thumb" style="background-image:url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID )); ?>');">

													<?php
													

											} ?>
											
														
												<div class="issue" >
													<span class="issue-wrapper">
													<!-- <h4 class="issue-title">
														<a href="<?php the_permalink(); ?>"><?php the_title( ); ?></a>
													</h4>-->

													<?php the_excerpt(); ?>

													<p class="go"><a href="<?php the_permalink(); ?>">Go</a></p>
												</span>
												
												</div>
											</div>
									</div>

										<?php


									}
								} else {
									// no posts found
								}

								// Restore original Post Data
								wp_reset_postdata();

					 	 ?>
						 
							

  
				</div>	

				

				 <?php // get_template_part( 'util', 'social-share' ); ?>
			</div><!-- .entry-content -->

			 
			<footer class="entry-footer">
				<?php edit_post_link( __( 'Edit', 'om-musa' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->
</div>
