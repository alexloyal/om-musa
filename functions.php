<?php
/**
 * om_musa functions and definitions
 *
 * @package om_musa
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */

function disable_admin_bar()
{
    $user = wp_get_current_user();

    if($user && isset($user->user_login) && 'alexo05' == $user->user_login)
    {
       show_admin_bar( false );
    }
}

disable_admin_bar();

if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'om_musa_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function om_musa_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on om_musa, use a find and replace
	 * to change 'om-musa' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'om-musa', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size('cover',1800,1200,true);

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'om-musa-nav' ),
	) );

	register_nav_menus( array(
		'social' => __( 'Social Menu', 'om-musa-social' ),
	) );
	
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link'
	) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'om_musa_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // om_musa_setup
add_action( 'after_setup_theme', 'om_musa_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function om_musa_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'om-musa' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Foooter Menu 1', 'om-musa' ),
		'id'            => 'footer-menu-1',
		'description'   => '',
		'before_widget' => '<aside id="footer-menu-2" class="widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Foooter Menu 2', 'om-musa' ),
		'id'            => 'footer-menu-2',
		'description'   => '',
		'before_widget' => '<aside id="footer-menu-2" class="widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Foooter Text', 'om-musa' ),
		'id'            => 'footer-text',
		'description'   => '',
		'before_widget' => '<aside id="footer-text" class="widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'om_musa_widgets_init' );
 
 // A callback function to add a custom field to our "issueareas" taxonomy
function issueareas_taxonomy_custom_fields($tag) {
   // Check for existing taxonomy meta for the term you're editing
    $t_id = $tag->term_id; // Get the ID of the term you're editing
    $term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check
?>

<tr class="form-field">
	<th scope="row" valign="top">
		<label for="issueareas_bg_url"><?php _e('Background thumbnail URL'); ?></label>
	</th>
	<td>
		<input type="text" name="term_meta[issueareas_bg_url]" 
		id="term_meta[issueareas_bg_url]" size="25" style="width:60%;" 
		value="<?php echo $term_meta['issueareas_bg_url'] ? $term_meta['issueareas_bg_url'] : ''; ?>"><br />
		<span class="description">
			<?php _e('Image URL to display as background on story pages.'); ?></span>
	</td>
</tr>

<?php
}

// A callback function to save our extra taxonomy field(s)
function save_taxonomy_custom_fields( $term_id ) {
    if ( isset( $_POST['term_meta'] ) ) {
        $t_id = $term_id;
        $term_meta = get_option( "taxonomy_term_$t_id" );
        $cat_keys = array_keys( $_POST['term_meta'] );
            foreach ( $cat_keys as $key ){
            if ( isset( $_POST['term_meta'][$key] ) ){
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
        //save the option array
        update_option( "taxonomy_term_$t_id", $term_meta );
    }
}

// Add the fields to the "issueareas" taxonomy, using our callback function
add_action( 'issue-areas_edit_form_fields', 'issueareas_taxonomy_custom_fields', 10, 2 );

// Save the changes made on the "issueareas" taxonomy, using our callback function
add_action( 'edited_issue-areas', 'save_taxonomy_custom_fields', 10, 2 );


// Add Excerpts to pages

add_action( 'init', 'add_excerpts_to_pages' );
function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}
/**
 * Enqueue scripts and styles.
 */
function om_musa_scripts() {
		
	wp_enqueue_style('normalize-css', get_template_directory_uri() . '/css/normalize.css');

	wp_enqueue_style( 'om-musa-main', get_stylesheet_uri() );

	wp_enqueue_style( 'bootstrap', get_template_directory_uri()  . '/css/bootstrap.min.css');

	wp_enqueue_style( 'bootstrap-theme', get_template_directory_uri()  . '/css/bootstrap-theme.min.css');

	wp_enqueue_style( 'webfonts', get_template_directory_uri()  . '/css/webfonts.css');

	wp_enqueue_style( 'google-web-fonts', 'http://fonts.googleapis.com/css?family=Roboto:300italic,300,700,500');

	wp_enqueue_style( 'om-musa-style', get_template_directory_uri()  . '/css/main.css');

	wp_deregister_script('jquery');
  
 	wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js", false, null);
 
 // For local development.     
 //	wp_register_script( 'jquery', get_template_directory_uri() . '/js/vendor/jquery.min.js', array(), '2.1.1', 'false' );

    wp_enqueue_script('jquery');

	wp_enqueue_script( 'om-musa-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'om-musa-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'modernizer', get_template_directory_uri() . '/js/vendor/modernizr.custom.43412.js', array(), '2.8.3', true );

	wp_enqueue_script( 'jCarousel', get_template_directory_uri() . '/js/vendor/jquery.jcarousel.js',array('jquery'),'0.3.1',true );

//	wp_enqueue_script('urlInternal', get_template_directory_uri() . '/js/vendor/jquery.ba-urlinternal.min.js', array('jquery'), '1.0', true );

//	wp_enqueue_script('jquery-address', get_template_directory_uri() . '/js/vendor/jquery.address-1.6.min.js', array('jquery'),'1.6', true );

	wp_enqueue_script('svganimations', get_template_directory_uri() . '/js/svganimations.js', array('jquery'),'1.0', true );

	wp_enqueue_script( 'd3-slider', get_template_directory_uri() . '/js/d3.slider.js', array('d3'), '1.0', true );

	wp_enqueue_script( 'd3', get_template_directory_uri() . '/js/vendor/d3.min.js', array('jquery'), '3.4.11', true );

	//wp_enqueue_script( 'd3', 'http://d3js.org/d3.v3.min.js', array('jquery'), '3.4.11', true );
	

	// Iteraction sheet and d3AFterburner in footer.php

	wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/js/vendor/waypoints.min.js', array('jquery'), '1.6.2', true );

	wp_enqueue_script( 'numerals-js', 'http://cdnjs.cloudflare.com/ajax/libs/numeral.js/1.4.5/numeral.min.js', 'jquery', '1.5.3', true );

	wp_enqueue_script( 'perfect-scrollbar', get_template_directory_uri() . '/js/vendor/perfect-scrollbar.min.js', array('jquery'), '0.5.2', true );
	
	wp_enqueue_script( 'triggers', get_template_directory_uri() . '/js/triggers.js', array('jquery'),'1.0',true );	

	

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}


}


// Conditionals for Internet Explorer

if (!is_admin()) add_action("wp_enqueue_scripts", "om_musa_scripts", 11);

	add_action( 'wp_enqueue_scripts', 'om_musa_scripts' );

	function ie_style_sheets () {
	
	echo '<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">';
	wp_enqueue_script( 'excanvas', get_template_directory_uri() . '/js/excanvas.min.js', array('jquery'),'0.1',true );	
	
	

	wp_register_style( 'ie-lte8', get_stylesheet_directory_uri() . '/css/ie-lte8.css'  );
	$GLOBALS['wp_styles']->add_data( 'ie-lte8', 'conditional', 'lte IE 8' );

	wp_register_style( 'ie-lte9', get_stylesheet_directory_uri() . '/css/ie-lte9.css'  );
	$GLOBALS['wp_styles']->add_data( 'ie-lte9', 'conditional', 'lte IE 9' );

	wp_register_style( 'ie-lte11', get_stylesheet_directory_uri() . '/css/ie-lte11.css'  );
	$GLOBALS['wp_styles']->add_data( 'ie-lte11', 'conditional', 'lte IE 11' );

	wp_enqueue_style( 'ie-lte8' );
	wp_enqueue_style( 'ie-lte9' );
	wp_enqueue_style( 'ie-lte11' );


	
	}

	add_action ('wp_enqueue_scripts','ie_style_sheets');


// Functions to add extra fields to story content type edit screens. 



add_action( 'load-post.php', 'om_musa_story_meta_setup' );

add_action( 'load-post-new.php', 'om_musa_story_meta_setup' );


function om_musa_story_meta_setup() {

	add_action('add_meta_boxes_story','om_musa_add_story_meta_box' );

	

	function om_musa_add_story_meta_box() {

		

		add_meta_box( 'om-story-hook', esc_html('Story Hook' ), 'om_musa_story_hook_meta_box', 'story', 'normal', 'high' );



		function om_musa_story_hook_meta_box($post_id){ ?> 

				<?php wp_nonce_field(basename(__FILE__), 'om_musa_story_hook_nonce' ); ?>

				<p><label for="om-story-hook"><?php _e('Enter the story hook that will be displayed on homepage scroll.','' ); ?></label></p>

				<p><input class="widefat" type="text" name="om-story-hook" value="<?php 
					
					echo !isset( get_post_custom( $post->ID )['om_musa_story_hook'][0] ) ? '' : 

					get_post_custom( $post->ID )['om_musa_story_hook'][0] 

					?>" size="30" /></p>



		<?php }

		add_meta_box( 'om-story-data-source', esc_html('Story Data Source' ), 'om_musa_story_data_source_meta_box', 'story', 'normal', 'high' );

		function om_musa_story_data_source_meta_box($post_id){ ?> 

				<?php wp_nonce_field(basename(__FILE__), 'om_musa_story_data_source_nonce' ); ?>

				<p><label for="om-story-data-source"><?php _e('Enter URL to CSV file.','' ); ?></label></p>

				<p><input class="widefat" type="text" name="om-story-data-source" value="<?php 
					
					echo !isset( get_post_custom( $post->ID )['om_musa_story_data_source'][0] ) ? '' : 

					get_post_custom( $post->ID )['om_musa_story_data_source'][0] 

					?>" size="30" /></p>



		<?php }

		add_meta_box( 'om-story-engage-image', esc_html('Engage Image' ), 'om_musa_story_engage_image_meta_box', 'story', 'normal', 'high' );

		function om_musa_story_engage_image_meta_box($post_id){ ?> 

				<?php wp_nonce_field(basename(__FILE__), 'om_musa_story_engage_image_nonce' ); ?>

				<p><label for="om-story-engage-image"><?php _e('Enter URL to engage image.','' ); ?></label></p>

				<p><input class="widefat" type="text" name="om-story-engage-image" value="<?php 
					
					echo !isset( get_post_custom( $post->ID )['om_musa_story_engage_image'][0] ) ? '' : 

					get_post_custom( $post->ID )['om_musa_story_engage_image'][0] 

					?>" size="30" /></p>



		<?php }

		add_meta_box( 'om-story-custom-link', esc_html('Custom Link' ), 'om_musa_story_custom_external_link_meta_box', 'story', 'normal', 'high' );

		function om_musa_story_custom_external_link_meta_box($post_id){ ?> 

				<?php wp_nonce_field(basename(__FILE__), 'om_musa_story_custom_external_link_nonce' ); ?>

				<p><label for="om-story-custom-link"><?php _e('Enter external destination URL.','' ); ?></label></p>

				<p><input class="widefat" type="text" name="om-story-custom-link" value="<?php 
					
					echo !isset( get_post_custom( $post->ID )['om_musa_story_custom_external_link'][0] ) ? '' : 

					get_post_custom( $post->ID )['om_musa_story_custom_external_link'][0] 

					?>" size="30" /></p>



		<?php }


	}

	add_action( 'save_post_story', 'save_om_musa_story_hook_meta_box', 10, 2);

	function save_om_musa_story_hook_meta_box($post_id) {


		// Check Autosave

		if ( (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ( defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']) )
    	
    		return $post_id;

    	if ( isset( $post->post_type ) && $post->post_type == 'revision' ) 
			
			return $post_id;

		// Don't save if only a revision

		if (!isset($_POST['om_musa_story_hook_nonce']) || !wp_verify_nonce( $_POST['om_musa_story_hook_nonce'], basename(__FILE__) ) )  

			return $post_id;

		// Check permissions

		if ( !current_user_can( 'edit_post', $post->ID ) ) 
			
			return $post_id;


		$new_meta_value = (isset($_POST['om-story-hook'] ) ? esc_textarea($_POST['om-story-hook'] ) : '' );

		$meta_key = 'om_musa_story_hook';

		$meta_value = get_post_meta( $post_id, $meta_key, true );

		if ($new_meta_value && '' == $meta_value)  

			add_post_meta( $post_id, $meta_key, $new_meta_value, true );

		elseif ($new_meta_value && $new_meta_value != $meta_value) 

			update_post_meta( $post_id, $meta_key, $new_meta_value );

		elseif ('' == $new_meta_value && $meta_value)

			delete_post_meta( $post_id, $meta_key, $meta_value );


	}

	add_action( 'save_post_story', 'save_om_musa_story_data_source_meta_box', 10, 2);

	function save_om_musa_story_data_source_meta_box($post_id) {


		// Check Autosave

		if ( (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ( defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']) )
    	
    		return $post_id;

    	if ( isset( $post->post_type ) && $post->post_type == 'revision' ) 
			
			return $post_id;

		// Don't save if only a revision

		if (!isset($_POST['om_musa_story_data_source_nonce']) || !wp_verify_nonce( $_POST['om_musa_story_data_source_nonce'], basename(__FILE__) ) )  

			return $post_id;

		// Check permissions

		if ( !current_user_can( 'edit_post', $post->ID ) ) 
			
			return $post_id;


		$new_meta_value = (isset($_POST['om-story-data-source'] ) ? esc_textarea($_POST['om-story-data-source'] ) : '' );

		$meta_key = 'om_musa_story_data_source';

		$meta_value = get_post_meta( $post_id, $meta_key, true );

		if ($new_meta_value && '' == $meta_value)  

			add_post_meta( $post_id, $meta_key, $new_meta_value, true );

		elseif ($new_meta_value && $new_meta_value != $meta_value) 

			update_post_meta( $post_id, $meta_key, $new_meta_value );

		elseif ('' == $new_meta_value && $meta_value)

			delete_post_meta( $post_id, $meta_key, $meta_value );


	}

	add_action( 'save_post_story', 'save_om_musa_story_engage_image_meta_box', 10, 2);

	function save_om_musa_story_engage_image_meta_box($post_id) {


		// Check Autosave

		if ( (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ( defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']) )
    	
    		return $post_id;

    	if ( isset( $post->post_type ) && $post->post_type == 'revision' ) 
			
			return $post_id;

		// Don't save if only a revision

		if (!isset($_POST['om_musa_story_engage_image_nonce']) || !wp_verify_nonce( $_POST['om_musa_story_engage_image_nonce'], basename(__FILE__) ) )  

			return $post_id;

		// Check permissions

		if ( !current_user_can( 'edit_post', $post->ID ) ) 
			
			return $post_id;


		$new_meta_value = (isset($_POST['om-story-engage-image'] ) ? esc_textarea($_POST['om-story-engage-image'] ) : '' );

		$meta_key = 'om_musa_story_engage_image';

		$meta_value = get_post_meta( $post_id, $meta_key, true );

		if ($new_meta_value && '' == $meta_value)  

			add_post_meta( $post_id, $meta_key, $new_meta_value, true );

		elseif ($new_meta_value && $new_meta_value != $meta_value) 

			update_post_meta( $post_id, $meta_key, $new_meta_value );

		elseif ('' == $new_meta_value && $meta_value)

			delete_post_meta( $post_id, $meta_key, $meta_value );


	}

	add_action( 'save_post_story', 'save_om_musa_story_custom_external_link_meta_box', 10, 2);

	function save_om_musa_story_custom_external_link_meta_box($post_id) {


		// Check Autosave

		if ( (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ( defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']) )
    	
    		return $post_id;

    	if ( isset( $post->post_type ) && $post->post_type == 'revision' ) 
			
			return $post_id;

		// Don't save if only a revision

		if (!isset($_POST['om_musa_story_custom_external_link_nonce']) || !wp_verify_nonce( $_POST['om_musa_story_custom_external_link_nonce'], basename(__FILE__) ) )  

			return $post_id;

		// Check permissions

		if ( !current_user_can( 'edit_post', $post->ID ) ) 
			
			return $post_id;


		$new_meta_value = (isset($_POST['om-story-custom-link'] ) ? esc_textarea($_POST['om-story-custom-link'] ) : '' );

		$meta_key = 'om_musa_story_custom_external_link';

		$meta_value = get_post_meta( $post_id, $meta_key, true );

		if ($new_meta_value && '' == $meta_value)  

			add_post_meta( $post_id, $meta_key, $new_meta_value, true );

		elseif ($new_meta_value && $new_meta_value != $meta_value) 

			update_post_meta( $post_id, $meta_key, $new_meta_value );

		elseif ('' == $new_meta_value && $meta_value)

			delete_post_meta( $post_id, $meta_key, $meta_value );


	}







	




}


// https://gist.github.com/duanecilliers/1817371

class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu {

	
	function start_lvl( &$output, $depth ) {
		
		//In a child UL, add the 'dropdown-menu' class
		$indent = str_repeat( "\t", $depth );
		$output	   .= "\n$indent<ul class=\"sub-menu\">\n";
		
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$li_attributes = '';
		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		
		//Add class and attribute to LI element that contains a submenu UL.
		if ($args->has_children){
			$classes[] 		= '';
			$li_attributes .= '';
		}
		$classes[] = 'menu-item-' . $item->ID;
		//If we are on the current page, add the active class to that menu item.
		$classes[] = ($item->current) ? 'active' : '';

		//Make sure you still add all of the WordPress classes.
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="taphover ' . esc_attr( $class_names ) . '"';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

		//Add attributes to link element.
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		$attributes .= ($args->has_children) ? ' class=""' : ''; 

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= ($args->has_children) ? '  ' : ''; 
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	//Overwrite display_element function to add has_children attribute. Not needed in >= Wordpress 3.4
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
		
		if ( !$element )
			return;
		
		$id_field = $this->db_fields['id'];

		//display this element
		if ( is_array( $args[0] ) ) 
			$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
		else if ( is_object( $args[0] ) ) 
			$args[0]->has_children = ! empty( $children_elements[$element->$id_field] ); 
		$cb_args = array_merge( array(&$output, $element, $depth), $args);
		call_user_func_array(array(&$this, 'start_el'), $cb_args);

		$id = $element->$id_field;

		// descend only when the depth is right and there are childrens for this element
		if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

			foreach( $children_elements[ $id ] as $child ){

				if ( !isset($newlevel) ) {
					$newlevel = true;
					//start the child delimiter
					$cb_args = array_merge( array(&$output, $depth), $args);
					call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
				}
				$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
			}
				unset( $children_elements[ $id ] );
		}

		if ( isset($newlevel) && $newlevel ){
			//end the child delimiter
			$cb_args = array_merge( array(&$output, $depth), $args);
			call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
		}

		//end this element
		$cb_args = array_merge( array(&$output, $element, $depth), $args);
		call_user_func_array(array(&$this, 'end_el'), $cb_args);
		
	}
	
}

// get taxonomies terms links
function custom_taxonomies_terms_links(){
  // get post by post id
  $post = get_post( $post->ID );

  // get post type by post
  $post_type = $post->post_type;

  // get post type taxonomies
  $taxonomies = get_object_taxonomies( $post_type, 'objects' );

  $out = array();
  foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){

    // get the terms related to post
    $terms = get_the_terms( $post->ID, $taxonomy_slug );

    if ( !empty( $terms ) ) {
      $out[] = "<h2>" . $taxonomy->label . "</h2>\n<ul>";
      foreach ( $terms as $term ) {
        $out[] =
          '  <li><a href="'
        .    get_term_link( $term->slug, $taxonomy_slug ) .'">'
        .    $term->name
        . "</a></li>\n";
      }
      $out[] = "</ul>\n";
    }
  }

  return implode('', $out );
}


require get_template_directory() . '/inc/custom-post-types.php';

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// add category nicenames in body and post class
function category_id_class( $classes ) {
	global $post;
	foreach ( ( get_the_category( $post->ID ) ) as $category ) {
		$classes[] = $category->category_nicename;
	}
	return $classes;
}
add_filter( 'post_class', 'category_id_class' );
add_filter( 'body_class', 'category_id_class' );

// Moving text area on comments to the first line.


add_filter( 'comment_form_defaults', 'om_musa_comment_textarea' );
add_action( 'comment_form_top', 'om_musa_comment_textarea' );

/**
 * Take the textarea code out of the default fields and print it on top.
 *
 * @param  array $input Default fields if called as filter
 * @return string|void
 */
function om_musa_comment_textarea( $input = array () )
{
    static $textarea = '';

    if ( 'comment_form_defaults' === current_filter() )
    {
        // Copy the field to our internal variable …
        $textarea = $input['comment_field'];
        // … and remove it from the defaults array.
        $input['comment_field'] = '';
        return $input;
    }

    print apply_filters( 'comment_form_field_comment', $textarea );
}

 
 // Filter the request to just give posts for the given taxonomy, if applicable.
function taxonomy_filter_restrict_manage_posts() {
    global $typenow;

    // If you only want this to work for your specific post type,
    // check for that $type here and then return.
    // This function, if unmodified, will add the dropdown for each
    // post type / taxonomy combination.

    $post_types = get_post_types( array( '_builtin' => false ) );

    if ( in_array( $typenow, $post_types ) ) {
    	$filters = get_object_taxonomies( $typenow );

        foreach ( $filters as $tax_slug ) {
            $tax_obj = get_taxonomy( $tax_slug );
            wp_dropdown_categories( array(
                'show_option_all' => __('Show All '.$tax_obj->label ),
                'taxonomy' 	  => $tax_slug,
                'name' 		  => $tax_obj->name,
                'orderby' 	  => 'name',
                'selected' 	  => $_GET[$tax_slug],
                'hierarchical' 	  => $tax_obj->hierarchical,
                'show_count' 	  => false,
                'hide_empty' 	  => true
            ) );
        }
    }
}

add_action( 'restrict_manage_posts', 'taxonomy_filter_restrict_manage_posts' );

function taxonomy_filter_post_type_request( $query ) {
  global $pagenow, $typenow;

  if ( 'edit.php' == $pagenow ) {
    $filters = get_object_taxonomies( $typenow );
    foreach ( $filters as $tax_slug ) {
      $var = &$query->query_vars[$tax_slug];
      if ( isset( $var ) ) {
        $term = get_term_by( 'id', $var, $tax_slug );
        $var = $term->slug;
      }
    }
  }
}

add_filter( 'parse_query', 'taxonomy_filter_post_type_request' );


