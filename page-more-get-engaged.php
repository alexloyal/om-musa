<?php
/*
Template Name: More Ways To Get Engaged
*/



/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package om_musa
 */

get_header(); ?>
<!-- page-get-engaged -->
	<div id="primary" class="content-area row">
		<main id="main" class="site-main col-md-12" role="main">

			<div class="background" style="background-image:url('<?php 
											$coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											echo $coverimageurl
										 ?>');" class="">

									<!--	 
										//		<?php 
										//		$story_kicker = get_post_meta( get_the_ID(), 'story-kicker', true );
										//			// check if the custom field has a value
										//			if( ! empty( $story_kicker ) ) {
										//			  ?> <h4 class="kicker center align"> <?php 
										//			echo $story_kicker; ?></h4> <?php 
										//			} else {
										//				the_title( '<h4 class="kicker center align">', '</h4>' );
										//			} ?>

									 -->

		 			
	  			 <div class="col-md-12 centered sub-page-container">

				   	
				   	<?php
						$moreGetEngaged = get_bookmarks( array(
							'orderby'        => 'rating',
							'order'          => 'ASC',
							'category_name'  => 'More Ways To Get Engaged'
						));

						
						foreach ( $moreGetEngaged as $moreGetEngagedItem ) { ?>
						<div class="col-md-6">
							<div class="col-md-4 col-xs-4 post-thumb" >
								 <img src="<?php printf($moreGetEngagedItem->link_image); ?>" title="<?php $moreGetEngagedItem->link_name; ?>">
									
								 
							</div>
								<div class="entry-header col-md-8 col-xs-8">
									
									<h4 class="issue-title">
										<?php printf( '<a class="relatedlink" href="%s">%s</a><br />', $moreGetEngagedItem->link_url, $moreGetEngagedItem->link_name ); ?>
									</h4>

									<div class="entry-summary">
										<p><?php printf($moreGetEngagedItem->link_description); ?></p>

										<p class="go">
											<?php printf( '<a class="relatedlink" href="%s">%s</a><br />', $moreGetEngagedItem->link_url, "Go" ); ?>
										</p>
							   
									</div><!-- .entry-summary -->
									
								</div>

									
												
										 
									
							</div>


						<?php
						    
						}
						?>


	  			 		
	  			</div>			
	  			 		

			</div>	<!-- background -->

				<div class="page-footer">
						<a href="<?php 
							$pageFooterLink = get_post_meta($post->ID, 'page-footer-cta-link', true);
							echo $pageFooterLink;
							?>" 

							title="<?php echo $pageFooterCTA;  ?>" 

							class="page-footer-link page-footer-link-icon">
								<?php 
				 			 	$pageFooterCTA = get_post_meta($post->ID, 'page-footer-cta', true);
				 				 if ($pageFooterCTA !="" ) {
				 				 	?>
				 			 	
				 			 		<?php echo $pageFooterCTA; ?>

				 			 	<?php
				 			 	

				 				 } else {
				 			 		?>
				 			 		More
				 			 	<?php
				 			  

				 			 }?>
		 				
		 				</a>
				</div>
			
			 

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>