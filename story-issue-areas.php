<?php
/**
 * @package om_musa
 */
?>
<!-- story-issue-areas -->
	<span id="story-issues" class="story-dot-navigation">

			 

	</span>
   

				<div id="story-issue-areas" class="story type-story">

					<div class="col-md-9 centered">
						<h4 class="section-title">ISSUE AREAS THIS STORY IS RELATED TO</h4>
					
						<div class="related-issue-areas">

								<?php
							
									$issueAreas = get_the_terms( $post->ID, 'issue-areas' );
									foreach($issueAreas as $issue) { 
									  //  echo '<p>Category: <a href="' . get_term_link( $issue ) . '" title="' . sprintf( __( "View all posts in %s" ), $issue->name ) . '" ' . '>' . $issue->name.'</a> </p> ';
									   // echo '<p> Description:'. $issue->description . '</p>';

									$issueName = (string)$issue->slug; ?>
									

									<div id="<?php echo $boton; ?>" class="issue-buffer col-md-3 col-xs-9">
										<?php 
													
													$t_id = $issue->term_id;	
													$term_meta =  get_option( "taxonomy_term_$t_id" );
											 		$bgImage = 	$term_meta['issueareas_bg_url'];
											 		$issueAreaUrl = get_term_link($t_id, 'issue-areas');
											 		 

										 ?>
											 	

											<a href="<?php echo $issueAreaUrl; ?>" title="<?php echo $issueName; ?>">
													<img src="<?php echo get_template_directory_uri(); echo $bgImage ;?>" class="issue-thumb" >
													

											</a>
													
											<h4><?php echo $issue->name; ?></h4>
											 
									</div><!--  -->

									<?php }  ?>

						</div>

					</div>

				</div><!-- .story-issue-areas -->


 

