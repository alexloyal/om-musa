<?php
/**
 * @package om_musa
 */
	wp_enqueue_script( 'iac-hs-access-healthcare', get_template_directory_uri() . '/js/iac-hs-access-healthcare.js', array('d3'), '1.0', true );
	
    wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );
?>
<!-- iac-hs-access-healthcare -->
<div class="iac-ha-financial-content">

	<div class="swap objective one">

		<div class="item" data-swap="1" data-dimension="A">
			<span>HEALTH INSURANCE COVERAGE</span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>PRIMARY CARE PHYSICIANS</span>
		</div>

	</div>


<div class="col-md-6 year-labels centered">
	<span class="cyan" data-value=""></span>
	<span class="purp" data-value=""></span>
</div>


<div class="ia-hs-access-to-healthcare">
		<div class="graph"></div>
</div>


<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->

	<div class="data-disclosure">
			<div data-dimension="A">
				<h5>% OF POPULATION WITH HEALTH INSURANCE COVERAGE</h5>
				<p>Increased health insurance coverage improves health across the community.</p>
				<p>Miami-Dade County has a much lower rate of insurance coverage than the US average and other major metros.</p>
				<p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a>
					<br>Data Showing: Central County
				</p>
			</div>

			<div data-dimension="B">
				<h5>PRIMARY CARE PHYSICIANS PER 100K RESIDENTS</h5>
				<p>Greater availability of physicians can be associated with improved community health.</p>
				<p>Miami-Dade County has slightly more physicians per capita than the US average.</p>
				<p class="source">Source: <a href="http://www.arf.hrsa.gov/download.htm" title="Department of Health &amp; Human Services">Department of Health &amp; Human Services</a><br>
					Data Showing: Central County
				</p>
			</div>
	</div>


</div>

 
 
 