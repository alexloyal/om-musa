<?php
/**
 * @package om_musa
 */
?>

<?php 

	 wp_enqueue_script( 'employment-growht-vs-unemployment-rate', get_template_directory_uri() . '/js/employment-growth-vs-unemployment-rate-viz.js', array('d3'), '1.0', true );
     // wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.graph.js', array('d3'), '1.0', true );

 ?>
 <section class="unemployment-growht-vs-unemployment-rate centered" data-graph="employment growth v. unemployment rate">
        
        
    <div class="annotation col-md-12 centered" data-value="0">
            <span class="highlight apple green"><span class="dot">&bull;</span>Employment Growth</span>
            <span class="highlight cyan"><span class="dot">&bull;</span>Unemployment Rate</span>
                
        </div>



<div class="graph"></div>

         


       
<div class="legend col-md-10 centered">

        <div id="" class="city names centered col-md-12">
            <div data-city="Chicago" class="city-name">CHI</div>
            <div data-city="Houston" class="city-name">HOU</div>
            <div data-city="Miami" class="city-name">MIA</div>
            <div data-city="NYC" class="city-name">NYC</div>
            <div data-city="San Diego" class="city-name">SD</div>
            <div data-city="US" class="city-name">U.S.</div>
        </div>

        


</div>

<div class="explore toggle" data-value="2">
            <div class="value" data-value="2011" data-index="0">2011</div>
            <div class="slider"></div>
            <div class="value" data-value="2013" data-index="1">2013</div>
</div>        
    </section>
 
 
 