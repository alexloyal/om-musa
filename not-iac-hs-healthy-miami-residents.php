<?php
/**
 * @package om_musa
 */

	wp_enqueue_script( 'iac-ha-healthy-outcomes', get_template_directory_uri() . '/js/iac-hs-healthy-outcomes.js', array('d3'), '1.0', true );
	


?>

 <!-- iac-hs-healthy-residents -->

<div class="swap objective two">

 
	<div class="item" data-swap="0" data-dimension="A">
		<span>Heart Disease Death Rate per 100K Residents</span>
	</div>

	<div class="item" data-swap="0" data-dimension="B">
		<span>Stroke Death Rate per 100K Residents</span>
	</div>

	<div class="item" data-swap="0" data-dimension="C">
		<span>% of Population with HIV</span>
	</div>

</div>
	<div class="col-md-12 year-labels">
		<div class="col-md-6">

			<div class="item col-md-3 col-xs-3 apple green"></div>
			<div class="item col-md-3 col-xs-3 purp"></div>

		</div>
		<div class="col-md-6">

			<div class="item col-md-3 col-xs-3 apple green"></div>
			<div class="item col-md-3 col-xs-3 purp"></div>

		</div>
	</div>

	<div class="col-md-12 circles">
		<div class="col-md-6 city-group" data-label="chi">
			<div class="col-md-6 radial first" data-year="" data-city="Chicago"></div>
			<div class="col-md-6 radial second" data-year="" data-city="Chicago"></div>
		</div>

		<div class="col-md-6 city-group" data-label="hou">
			<div class="col-md-6 radial first" data-year="" data-city="Houston"></div>
			<div class="col-md-6 radial second" data-year="" data-city="Houston"></div>
		</div>

		<div class="col-md-6 city-group" data-label="mia">
			<div class="col-md-6 radial first" data-year="" data-city="Miami"></div>
			<div class="col-md-6 radial second" data-year="" data-city="Miami"></div>
		</div>


		<div class="col-md-6 city-group" data-label="nyc">
			<div class="col-md-6 radial first" data-year="" data-city="NYC"></div>
			<div class="col-md-6 radial second" data-year="" data-city="NYC"></div>
		</div>

		<div class="col-md-6 city-group" data-label="sd">
			<div class="col-md-6 radial first" data-year="" data-city="SD"></div>
			<div class="col-md-6 radial second" data-year="" data-city="SD"></div>
		</div>

		<div class="col-md-6 city-group" data-label="us">
			<div class="col-md-6 radial first" data-year="" data-city="US"></div>
			<div class="col-md-6 radial second" data-year="" data-city="US"></div>
		</div>



	</div>

<div class="data-disclosure col-md-12">
	<p>Historically, smoking has been one of the leading cause of preventable death in the US.</p>


	<p>Fewer adults in Miami-Dade county smoke than the US average and most major metros.</p>

	<p class="source">Source: Centers for Disease Control</p>

</div>
