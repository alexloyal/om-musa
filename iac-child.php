<?php
/**
 * @package om_musa
 */
?>
<!-- iac-child -->

<?php $menu_o = $wpdb->get_var( "SELECT menu_order FROM $wpdb->posts WHERE ID=" . $post->ID  ); ?>

 


<div class="story-anchor col-md-12">

		 	
		<span id="dot-<?php echo $menu_o; ?>" class="story-dot-navigation" data-section="<?php echo $post->post_name; ?>">

			
 			 <?php 
 			 	$storyKicker = get_post_meta($post->ID, 'story-kicker', true);
 			 if ($storyKicker !="" ) {
 			 	?>
 			 	<a href="#<?php echo $post->post_name; ?>" data-kicker="<?php echo $storyKicker; ?>" class="">

 			 	<?php
 			 	

 			 } else {
 			 	?>
 			 	<a href="#<?php echo $post->post_name; ?>" data-kicker="" class="">
 			 	<?php
 			  

 			 }?>
 				
 			</a>
   

		</span>

			
		<span data-trigger="<?php echo $menu_o; ?>"></span>
  
</div>


<?php 

		// This allows to add a custom css class to the post in order to style differently.

		// get_template_part('util','story-class' ); ?>

		<?php 
			
			$story_class = get_post_meta( $post->ID, 'storyclass', true );
			if($story_class !="" ) {
				

				$storyClasses = array(
							'col-md-12 col-xs-12',
							'post',
							(string)$story_class,
							'menu-order-'. (string)$menu_o
						);
			} else {


				$storyClasses = array(
							'col-md-12 col-xs-12',
							'post',
							'menu-order-'. (string)$menu_o
							
						);
			} 
						
		 ?>


<article id="post-<?php the_id(); ?>" <?php post_class($storyClasses); ?>>

	<div class="centered">

		<header class="entry-header">
			
			<?php  
				?>  <span id="<?php echo $post->post_name; ?>" class="anchor"></span> <?php
			?>

		
			
			 

			
		</header><!-- .entry-header -->

		<?php 
			
			// Conditional to modify section class based on its menu order.

			
			if ($menu_o == 90 ) {
				
				$storyClass = "col-md-11";
			} else {

				$storyClass = "col-md-4";
			}

		 ?>
		 

		 <!-- Menu Order: <?php echo $menu_o; ?> -->
		<div class="entry-content <?php echo $storyClass ?>">

			<?php //if ( $post->post_name == 'miami-dade-residents-possess-sufficient-financial-resources'  ) { 
				 // Closes if
				// }
			?>

			<?php if ($menu_o == 10 ): ?>
				
							 

								<h3 class="objective one">
									<?php 
										$issue_areas = get_the_terms($post->ID, $taxonomy );
										foreach ($issue_areas as $issue_area) {echo $issue_area->name; }
									?>
									Goal</h3>										
							
								<div class="apple green fill circle">
									<span class="circle-label">
										1
									</span>
								</div>


											
			<?php endif ?>

			<?php if ($menu_o == 20 ): ?>
												

								<h3 class="objective two">
									<?php 
										$issue_areas = get_the_terms($post->ID, $taxonomy );
										foreach ($issue_areas as $issue_area) {echo $issue_area->name; }
									?>
									Goal</h3>	


								<div class="cyan fill circle">
									<span class="circle-label">
									2

									</span>
								</div>

			<?php endif ?>

			<?php if ($menu_o == 30 ): ?>
												
										 

								<h3 class="objective three">
									<?php 
										$issue_areas = get_the_terms($post->ID, $taxonomy );
										foreach ($issue_areas as $issue_area) {echo $issue_area->name; }
									?>
									Goal</h3>
								
								<div class="purp fill circle">
									<span class="circle-label">
									3
									</span>
								</div>

			<?php endif ?>

			<?php if ($menu_o == 40 ): ?>
												
										 

								<h3 class="objective three">
									<?php 
										$issue_areas = get_the_terms($post->ID, $taxonomy );
										foreach ($issue_areas as $issue_area) {echo $issue_area->name; }
									?>
									Goal</h3>
								
								<div class="apple green fill circle">
									<span class="circle-label">
									4
									</span>
								</div>

			<?php endif ?>


			<?php the_title( '<h3 class="entry-title">Goal:  ', '</h3>' ); ?>
			<!-- <?php echo get_the_title( $post->post_parent );	  ?> -->

			<?php the_content(); ?>
			 
			


		</div><!-- .entry-content -->

		<div id="ia-data-<?php echo $post->ID; ?>" class="data-svg">
			


			<?php if ($menu_o == 10 ) : ?>

					<?php
	     			

					$issue_areas = get_the_terms($post->ID, $taxonomy );
								   foreach ($issue_areas as $issue_area) { 
	     				
		 				switch ($issue_area->slug) {
		 					case 'housing-affordability':
		 							get_template_part( 'iac-ha', 'financial' );
		 						break;
		 					case 'health-safety':
		 							get_template_part( 'iac-hs', 'access-healthcare' );
		 						break;	
		 					case 'education':
		 							get_template_part( 'iac-ed', 'quality-education' );
		 						break;
		 					case 'transportation':
		 							get_template_part( 'iac-tr', 'pedestrian-bike' );
		 						break;
		 					case 'economy':
		 							get_template_part( 'iac-ec', 'job-opportunities' );
		 						break;
		 					case 'environment-public-space':
		 							get_template_part( 'iac-ev', 'clean-air-water' );
		 						break;
		 					case 'arts-culture':
		 							get_template_part( 'iac-ae', 'cultural-opportunities' );
		 						break;	
		 					case 'civic-engagement':
		 							get_template_part( 'iac-ce', 'engaged-citizenry' );
		 						break;		
		 							
		 					default:
		 						# code...
		 						break;
		 				}
					}
		 			
					
			?>
			
			<?php endif ?>

			<?php if ($menu_o == 20 ) : ?>

			<?php
	     			

					$issue_areas = get_the_terms($post->ID, $taxonomy );
								   foreach ($issue_areas as $issue_area) { 
	     				
		 				switch ($issue_area->slug) {
		 					case 'housing-affordability':
		 							get_template_part( 'iac-ha', 'affordable' );
		 						break;
		 					case 'health-safety':
		 							get_template_part( 'iac-hs', 'healthy-choices' );
		 						break;	
		 					case 'education':
		 							get_template_part( 'iac-ed', 'thrive-academically' );
		 						break;

		 					case 'transportation':
		 							get_template_part( 'iac-tr', 'public-transit' );
		 						break;
		 					case 'economy':
		 							get_template_part( 'iac-ec', 'new-business' );
		 						break;
		 					case 'environment-public-space':
		 							get_template_part( 'iac-ev', 'quality-public-spaces' );
		 						break;
		 					case 'arts-culture':
		 							get_template_part( 'iac-ae', 'arts-community' );
		 						break;						
		 					default:
		 						# code...
		 						break;
		 				}
					}
		 			
					
			?>

				 
			
			<?php endif ?>

			<?php if ($menu_o == 30 ) : ?>
				<?php $issue_areas = get_the_terms($post->ID, $taxonomy );
								   foreach ($issue_areas as $issue_area) { 
	     				
		 				switch ($issue_area->slug) {
		 					case 'housing-affordability':
		 							get_template_part( 'iac-ha', 'equitable' );
		 						break;
		 					case 'health-safety':
		 							get_template_part( 'iac-hs', 'healthy-habits' );
		 						break;	
		 					case 'education':
		 							get_template_part( 'iac-ed', 'retain-graduates' );
		 						break;
		 					case 'transportation':
		 							get_template_part( 'iac-tr', 'roadway-network' );
		 						break;
		 					case 'environment-public-space':
		 							get_template_part( 'iac-ev', 'sustainability' );
		 						break;	

		 					default:
		 						# code...
		 						break;
		 				}
					} ?>
					 
			<?php endif ?>

			<?php if ($menu_o == 40 ) : ?>
				<?php $issue_areas = get_the_terms($post->ID, $taxonomy );
								   foreach ($issue_areas as $issue_area) { 
	     				
		 				switch ($issue_area->slug) {
		 					
		 					case 'health-safety':
		 							get_template_part( 'iac-hs', 'health-safety' );
		 						break;	
		 					
		 					default:
		 						# code...
		 						break;
		 				}
					} ?>
					
			<?php endif ?>


			

		</div>

 

		

	</div>

	 
</article><!-- #post-## -->




