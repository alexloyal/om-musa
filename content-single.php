<?php
/**
 * @package om_musa
 */
?>

<div class="col-md-9 col-xs-12 centered sub-page-container">

	<article id="post-<?php the_ID(); ?>" <?php post_class('col-xs-12 col-md-12 centered'); ?>>
	 

		 <div class="col-md-3 col-xs-3 post-thumb">
			
			<span class="avatar">
				<!-- circle thumb -->
				<?php 
					//$author_thumb = get_the_author_meta('user_url'); 
					$author_thumb = get_post_meta( get_the_ID(), 'om_musa_circle_thumb', true );
						if ( $author_thumb !="" ) { ?>
					<img src="<?php echo $author_thumb ?>"> <?php
				} else { ?>
				<img src="<?php echo get_template_directory_uri()?>/images/Our_Miami_Logos_circle.png" title="Our Miami">
					<?php	}
				

				?>
				<!-- <img src="<?php //echo get_template_directory_uri()?>/images/amy-web.jpg"> -->
			</span>
			<span class="pubby caps"><p>Published by</p></span>
			<span class="author caps"><p>
				
					<?php 
				$guest_author = get_post_meta( get_the_ID(), 'om_musa_guest_author', true );
				// check if the custom field has a value
				if( ! empty( $guest_author ) ) {
				  ?><?php echo $guest_author; ?> <?php 
				} else {
					?> <?php the_author(); ?>
					<?php
				} ?>



					



			</p></span>
			<span class="pub date caps"><h5><?php the_time('n.j.y');  ?></h5></span>
			<span class="tag list caps">
				<span class="tag-kicker">Tagged Under</span><br>	
				<?php the_tags('', ', ', '<br />'); ?>


			</span>
			<span class="tag list caps issue-areas">
				<span class="tag-kicker">Issue Areas this post is related to:</span><br>
				<?php the_terms( $post->ID, 'issue-areas', '', '' ); ?><br>
			</span>

		</div>

		<div class="col-md-9 col-xs-9 main content">
				<div class="entry-wrapper">
						<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

						<div class="entry-meta">
							
						</div><!-- .entry-meta -->
					</header><!-- .entry-header -->

					<div class="entry-content">

						<?php the_content(); ?>
					<!-- 	 <div class="tap quote">
													<a href="<?php the_permalink(); ?>" class="<?php the_id(); ?>">
														More
													</a>
						</div> -->
					</div><!-- .entry-content -->

					<?php get_template_part( 'util', 'social-share' ); ?>
				</div>	

				

		</div>	

		<footer class="col-xs-12 col-md-12">
		 

					<?php edit_post_link( __( 'Edit', 'om-musa' ), '<span class="edit-link">', '</span>' ); ?>


									<?php
											// If comments are open or we have at least one comment, load up the comment template
											if ( comments_open() || '0' != get_comments_number() ) :
												comments_template();
											endif;
										?>
					
				</footer><!-- .entry-footer -->
	
</article><!-- #post-## -->

</div>
