<?php
/**
 * @package om_musa
 */
	wp_enqueue_script( 'iac-ae-cultural-opportunities', get_template_directory_uri() . '/js/iac-ae-cultural-opportunities.js', array('d3'), '1.0', true );
	
    wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );
?>
<!-- iac-ae-cultural-opportunities -->

<div class="iac-ae-cultural-opportunities">

	<div class="swap objective one">

		<div class="item" data-swap="1" data-dimension="A">
			<span>School Enrollment in Arts Courses</span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>Languages Taught in School</span>
		</div>

		<div class="item" data-swap="0" data-dimension="C">
			<span>Foreign Born</span>
		</div>

		<div class="item" data-swap="0" data-dimension="D">
			<span>Restaurants per capita (PER 100K PERSONS)</span>
		</div>

	 

		

	</div>

<div class="col-md-6 year-labels centered">
	<span class="cyan" data-value=""></span>
	<span class="purp" data-value=""></span>
</div>


<div class="iac-ae-cultural-opportunities">
		<div class="graph"></div>
</div>


<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->


<div class="data-disclosure col-md-12">
	<div data-dimension="A">
			<h5>Enrollment in Public School Arts Courses</h5>
		
			<p>Early participation in arts courses can help establish a lifelong interest in cultural pursuits.</p>


			<p>Enrollment in arts courses at Miami-Dade public schools has risen slightly during the past two years.</p>

			<p class="source">Source: <a href="http://www.fldoe.org/eias/eiaspubs/pubstudent.asp" title="Florida Department of Education">Florida Department of Education</a><br>
			Data Showing:  Central County	</p>


	</div>

	<div data-dimension="B">
			<h5>Languages Taught in Public Schools</h5>
		
			<p>Learning a second language is associated with a host of cognitive beneifts, to say nothing of its cultural value.</p>


			<p>Ten foreign languages are taught in Miami schools. This number has remained constant in recent years.</p>

			<p class="source">Source: <a href="http://www.fldoe.org/eias/eiaspubs/pubstudent.asp" title="Florida Department of Education">Florida Department of Education</a>
				<br>Data Showing:  Central County</p>
	</div>

	<div data-dimension="C">
			<h5>% of Residents Born in Other Countries</h5>
		
			<p>Foreign born residents not only contirbute to a region's cultural vibrancy, but are also more likely to be entrepreneurs.</p>


			<p>Among major metropolitan areas, Miami has the highest proportion of foreign born residents.</p>

			<p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census Bureau">US Census Bureau</a>
				<br>Data Showing: Central County
			</p>

	</div>

	<div data-dimension="D">
			<h5>Restaurants per Capita</h5>
		
			<p>Restaurants contribute to the culture of a community and help attract new residents and tourists.</p>


			<p>Miami-Dade county has more full-service restaurants per capita than the US and most major metros with the exception of New York.</p>

			<p class="source">Source: <a href="http://www.bls.gov/cew/" title="Bureau of Labor Statistics">Bureau of Labor Statistics</a>
				<br>Data Showing: Central County
			</p>

	</div>

</div>



</div>

 
 
 