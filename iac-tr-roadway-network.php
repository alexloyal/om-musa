<?php
/**
 * @package om_musa
 */
	wp_enqueue_script( 'iac-tr-roadway-network', get_template_directory_uri() . '/js/iac-tr-roadway-network.js', array('d3'), '1.0', true );
	
    wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );
?>

<div class="iac-tr-roadway-network-content">

	<div class="swap objective one">

		<div class="item" data-swap="1" data-dimension="A">
			<span>Average Automobile Commute</span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>Average Price of Congestion per Car per Year</span>
		</div>

		<div class="item" data-swap="0" data-dimension="C">
			<span>Investment in Roadways</span>
		</div>

	</div>


<div class="col-md-6 year-labels centered">
	<span class="apple green" data-value=""></span>
	<span class="cyan" data-value=""></span>
</div>


<div class="iac-tr-roadway-network">
		<div class="graph"></div>
</div>


<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->

<div class="data-disclosure">
<div data-dimension="A">
		<h5>% OF AUTO COMMUTERS WITH<br>			
		TRAVEL TIMES OF MORE THAN 30 MINUTES</h5>
		<p>Long commute times result in decreased productivity and lower quality of life.</p>
		<p>Nearly 50% of Miami-Dade residents spend 30 minutes or more in their car commuting each day, a higher percentage than the US and comparable metros like San Diego and Houston.</p>
	

	<p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a><br>
	Data Showing:  Central County</p>

</div>
<div data-dimension="B">
		<h5>CONGESTION COST PER AUTO COMMUTER</h5>
		<p>Congestion is a waste of both time and money.</p>
		<p>The cost of congestion in the Miami metro is on par with major metros such as Houston, but remains lower than the US average for large regions and dense metros like Chicago and New York.  The average commuter in the metro loses 47 hours a year in traffic, for a total economic cost of $3.8 billion.</p>
	

	<p class="source">US data is average for large metros<br>Source: <a href="http://mobility.tamu.edu/ums/" title="Texas A&amp;M Transporation Institute">Texas A&amp;M Transporation Institute</a><br>
	Data Showing:  Metro-level</p>

</div>

<div data-dimension="C">
		<h5>Per Capita Transportation Expenditures</h5>
		<p>Local transportation spending can address critical needs unmet by state and federal expenditures.</p>
		<p>In 2011, Miami-Dade County spent more than $148 million on roadways. Funding has increased $2.5 million since 2009</p>
	

	<p class="source">Source: <a href="http://www.miamidade.gov/budget/" title="City of Miami Annual Budget">City of Miami Annual Budget</a><br>
		US data is average for large metros.</p>

</div>

</div>


</div>

 
 
 