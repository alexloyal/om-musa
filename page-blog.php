<?php
/*
Template Name: Blog Template
*/



/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package om_musa
 */

get_header(); ?>
<!-- page-blog -->
	<div id="primary" class="content-area row">
		<main id="main" class="site-main col-md-12" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
										 
										 		
									 
			<?php endwhile; // end of the loop. ?>				

 					<?php 
			 			$args = array (
						'post_type' 		=> 'post',
						// 'category_name'     => 'news',
						'pagination'		=>true
						
						
					);
					$wp_query = new WP_Query( $args );
					$wp_query->query('&paged='.$paged);
					$max = $wp_query->max_num_pages;
			 		$paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
			 		
			 		wp_localize_script(
			 			'triggers',
			 			'ajax_posts',
			 			array(
			 				'startPage' => $paged,
			 				'maxPages' => $max,
			 				'nextLink' => next_posts($max, false),
			 				'posts_per_page'	=> 3
			 			)
			 		);


	  			 	 ?>
	  			 	
	  			 	<?php if ( $wp_query->have_posts() ) : ?>

							<?php /* Start the Loop */ ?>
							<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
 
									



										<?php
											/* Include the Post-Format-specific template for the content.
											 * If you want to override this in a child theme, then include a file
											 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
											 */
											 
					 						get_template_part( 'content', 'excerpt' );


											?>
 
									
								 
							 

								

							<?php endwhile; ?>

							 	

					<?php else : ?>

						<?php get_template_part( 'content', 'none' ); ?>

					<?php endif; ?>
			
						<?php om_musa_paging_nav(); ?>
			
						<?php wp_reset_postdata(); ?>

				

			<div id="more-posts"></div>
				

			 <div id="tag-cloud" class="col-md-10 centered">
			 	<h4>Tags</h4>

			 	<div class="col-md-12 tag cloud">
			 		<?php 

			 		$tagArgs = array(
			 				'format' => 'flat'
			 			);
			 		wp_tag_cloud( $tagArgs ); ?>
			 	</div>

			 </div><!-- -->

			 

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>