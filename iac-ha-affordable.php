<?php
/**
 * @package om_musa
 */

wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.ia.graph.js', array('d3'), '1.0', true );
wp_enqueue_script( 'iac-ha-affordable', get_template_directory_uri() . '/js/iac-ha-affordable.js', array('d3'), '1.0', true );


?>

<div class="swap objective two">

	<div class="item" data-swap="1" data-dimension="A">
		<span>Renters Spending 30% or More on Housing</span>
	</div>

	<div class="item" data-swap="0" data-dimension="B">
		<span>Cost of Living Index</span>
	</div>

	<div class="item" data-swap="0" data-dimension="C">
		<span>Home-ownership Rate</span>
	</div>

	<div class="item" data-swap="0" data-dimension="D">
		<span>Average construction cost of a new home</span>
	</div>

	<div class="item" data-swap="0" data-dimension="E">
		<span>Median Sales Price of Existing Single-Family Homes</span>
	</div>

</div>
	
		<div class="year-labels centered col-md-6">

			<div class="item col-md-3 col-xs-3 apple green"></div>
			<div class="item col-md-3 col-xs-3 purp"></div>

		</div>
		
	

	<div class="col-md-12 col-xs-12 circles">
		<div class="col-md-6 city-group" data-label="chi">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Chicago"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Chicago"></div>
		</div>

		<div class="col-md-6 city-group" data-label="hou">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Houston"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Houston"></div>
		</div>

		<div class="col-md-6 city-group" data-label="mia">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Miami"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Miami"></div>
		</div>


		<div class="col-md-6 city-group" data-label="nyc">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="NYC"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="NYC"></div>
		</div>

		<div class="col-md-6 city-group" data-label="sd">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="SD"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="SD"></div>
		</div>

		<div class="col-md-6 city-group" data-label="us">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="US"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="US"></div>
		</div>



	</div>

<div class="data-disclosure col-md-12">
	<div data-dimension="A">
			<h5>% OF HOUSEHOLDS THAT ARE COST-BURDENED<br>(Renters spending 30% or more on housing)</h5>

			<p>When housing costs are high relative to incomes, households don’t have the disposable income needed for food, health care, and transportation. </p>

			<p>Miami-Dade households are some of the most cost-burdened in the US due to the high cost of housing. 65% of county renters spend 30% or more of their income on housing, a much higher percentage than the typical American. </p>

			<p class="source">Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a><br>Data Showing: Central County</p>
	</div>

	<div data-dimension="B">
			<h5>COST OF LIVING INDEX</h5>
			
			<p>This index measures the relative prices of goods and services versus the U.S. average.</p>

			<p>The Miami metro has a higher cost of living than the US, but lower than other major metros like Chicago, New York, and San Diego.</p>

			<p class="source">Source: <a href="http://www.greaterspokane.org/community-overview/79-cost-of-living.html" title="C2ER">C2ER</a><br>Data Showing: Metro-level</p>
			
			
	</div>

	<div data-dimension="C">
			<h5>HOMEOWNERSHIP RATE</h5>
			
			<p>Homeownership can have many positive effects, offering stability and retirement equity growth to citizens and enticing them to take care of their homes and communities.</p>

			<p>Miami-Dade county has a homeownership rate that is typical of major metros. 59% of the county population (of all ages) lives in a home that they own, a lower percentage than the US.</p>

			<p>Source: <a href="http://factfinder2.census.gov" title="US Census American Community Survey">US Census American Community Survey</a><br>Data Showing: Central County</p>
			
			
	</div>

	<div data-dimension="D">
			<h5>AVERAGE CONSTRUCTION COST OF A NEW<br>SINGLE FAMILY HOME (VALUE ON HOUSING PERMIT)</h5>
			
			<p>The cost of new construction has a large impact on the ultimate selling price of a home.  And, single-family homes support working families primarily, not outside investors in condos.</p>

			<p>Miami-Dade county has a below-average cost of construction for single-family homes. But, the average permitted cost of a new single-family home in Miami-Dade County ($234,000) is 33% higher than two years ago (according to permits issued).  By comparison, U.S. prices grew just 9%.</p>

			<p>Data for NYC is an average for surrounding boroughs that build SF homes (Queens, Bronx, Staten Island).</p>

			<p>Full circle: $380,000.</p>

			<p class="source">Source: <a href="http://censtats.census.gov/bldg/bldgprmt.shtml" title="US Census Bureau Censtats">US Census Bureau Censtats</a><br>Data Showing: Central County</p>
			
	</div>

	<div data-dimension="E">
		<h5>MEDIAN SALES PRICE OF EXISTING SINGLE-FAMILY HOMES</h5>

		<p>The price of single-family homes most affect working families living in the region, while the price of condos affect outside investors. </p>

		<p>The median sale price of single-family homes in the Miami metro is above the US median and Chicago, but significantly less than San Diego and New York. And, local prices have been growing much faster than the US and other major metros.</p>

		<p class="source">Source: <a href="http://www.realtor.org/topics/metropolitan-median-area-prices-and-affordability/data" title="National Association of Realtors">National Association of Realtors</a><br>Data Showing: Metro-level</p>

	</div>

	 

</div>
