<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package om_musa
 */
?>
<!-- page-content-child -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-6 col-xs-12'); ?>>
	
	<div class="col-md-4 col-xs-4 post-thumb" >
		
		<?php 
			$circle_thumb = get_post_meta( get_the_ID(), 'om_musa_circle_thumb', true );
			if( ! empty( $circle_thumb ) ) {
				?> <img src="<?php echo $circle_thumb ;?>" title="<?php the_title(); ?>">
			
			<?php 
			} else {
					$attr = array(
					'class' => 'circle',
					'alt' => $post->post_title
				);
					echo get_the_post_thumbnail( $page->ID, 'thumbnail', $attr ); 

			} ?>

		 
	</div>

	<div class="entry-header col-md-8 col-xs-8">
		
		

		<div class="entry-summary">
				<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
				<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->

		<div class="entry-link">
			<a href="<?php the_permalink(); ?>" data-kicker="<?php echo $storyKicker; ?>" class="kicker">
			<?php 
 			 	$storyKicker = get_post_meta($post->ID, 'story-kicker', true);
 			 if ($storyKicker !="" ) {
 			 	?>
 			 	
 			 		<?php echo $storyKicker; ?>

 			 	<?php
 			 	

 			 } else {
 			 	?>
 			 	More
 			 	<?php
 			  

 			 }?>
 				
 			</a>
		</div>
		
	</div>

	

	
</article><!-- #post-## -->