<?php
/**
 * @package om_musa
 */
?>
<!-- story-first-child -->
<div class="story-anchor col-md-12">
	<?php $menu_o = $wpdb->get_var( "SELECT menu_order FROM $wpdb->posts WHERE ID=" . $post->ID  ); ?>
				
		<span id="dot-<?php echo $menu_o; ?>" class="story-dot-navigation">

			<?php 
 			 	$storyKicker = get_post_meta($post->ID, 'story-kicker', true);
 			 if ($storyKicker !="" ) {
 			 	?>
 			 	<a href="#<?php echo $post->post_name; ?>" data-kicker="<?php echo $storyKicker; ?>" class="text">

 			 	<?php
 			 	

 			 } else {
 			 	?>
 			 	<a href="#<?php echo $post->post_name; ?>" data-kicker="" class="text">
 			 	<?php
 			  

 			 }?>
 				
 			</a>
   
   

		</span>

		 

</div>

<?php get_template_part('util','story-class' ); ?>

<?php 
	
	$story_class = get_post_meta( $post->ID, 'storyclass', true );
	if($story_class !="" ) {
		

		$storyClasses = array(
					'col-md-12',
					(string)$story_class,
				);
	} else {


		$storyClasses = array(
					'col-md-12'
					
				);
	} 
				
 ?>


<article id="post-<?php the_id(); ?>" <?php post_class($storyClasses); ?>>
	
	<div class="centered">

		<header class="entry-header">
			
			  <span id="<?php echo $post->post_name; ?>" class="anchor"></span>  

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<?php 
				$story_hook = get_post_meta( get_the_ID(), 'om_musa_story_hook', true );
				// check if the custom field has a value
				if( ! empty( $story_hook ) ) {
				  ?> <h2> <?php echo $story_hook; ?></h2> <?php 
				} else {
					the_excerpt(  );
				} ?>

			
		</header><!-- .entry-header -->

		<div class="entry-content col-md-4 col-lg-4">

			<div class="kicker">
			<span>Story</span> <a href="<?php echo get_permalink($post->post_parent); ?>" title="$post->post_parent">
			<?php echo get_the_title( $post->post_parent );	  ?></a>
			</div>
			
			<?php the_content(); ?>
			 
			


		</div><!-- .entry-content -->

		<div id="data-svg-<?php echo $post->ID ?>" class="data-svg col-md-8">
				<!-- data-svg goes here -->
				
				<?php if ( $post->ID == '1502'  ) { ?>

					<h4><br>Percentage of drivers that spend 30 minutes or more on commute.</h4>
				<?php
     
	 				 get_template_part( 'story', 'how-mobile' );
						}
				 ?>

				 <?php if ( $post->ID == '1548'  ) { ?>

				 	<h4>UNEMPLOYMENT RATE (%) BY EDUCATION ATTAINMENT</h4>
					 
				<?php
     
	 				 get_template_part( 'story', 'abcs-miami-education' );
						}

					if ( $post->ID == '1558'  ) { ?>


 
					<?php


	     
		 				get_template_part( 'story', 'lack-civic-participation' );
						
						}

					if ( $post->ID == '1535'  ) { ?>
 
					<?php
	     
		 				get_template_part( 'story', 'affordability' );
						
						}	


					if ( $post->ID == '1568'  ) { ?>
 
					<?php
	     
		 				get_template_part( 'story', 'sea-level' );
						
						}		

				 ?>

		</div>

	</div>

	<?php 
								$story_hook = get_post_meta( get_the_ID(), 'om_musa_story_hook', true );
								// check if the custom field has a value
								if( ! empty( $story_hook ) ) {
								  ?> <div data-quote="<?php echo $story_hook; ?>" class="col-md-6 centered"> <?php 
								} else {
									?><div data-quote="<?php 
									
									$excerpt = strip_tags(get_the_excerpt());
        									   echo $excerpt; 
        							
        								?>" class="col-md-6 centered"> <?php
								} ?>

								</div>

	 
</article><!-- #post-## -->

								
