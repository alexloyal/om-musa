<?php
/**
 * News zone on homepage. 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package om_musa
 */

 ?>
<!-- home-news -->

  	<div class="col-md-12 site-main news" role="main">

  		<span id="news-title" class="col-md-12 centered section header">

  						<h4 class="center align">Blog &amp; News</h4>

  					</span>

  		<div id="news-wrapper" class="story jcarousel-wrapper">
  			

	  		<div class="jcarousel">
	  			 
	  				

	  			 <ul class="news row">

	  			 	<?php 
	  			 			$args = array (
								'post_type' => 'post',
								//'category_name'          => 'blog',
								'posts_per_page'         => '3'
							);
							$homeNews = new WP_Query( $args );

	  			 	 ?>
	  			 	
	  			 	<?php if ( $homeNews->have_posts() ) : ?>

							<?php /* Start the Loop */ ?>
							<?php while ( $homeNews->have_posts() ) : $homeNews->the_post(); ?>

								<li id="post-<?php the_id(); ?>"class="post-wrapper col-md-12 blue background">

									
									<div class="background">

										<?php
											/* Include the Post-Format-specific template for the content.
											 * If you want to override this in a child theme, then include a file
											 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
											 */
											get_template_part( 'content', 'news' );
											?>
									
									</div>

									<style type="text/css">
										#post-<?php the_id(); ?> div.background:after {
											background:url('<?php 
											$coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											echo $coverimageurl
										 ?>') no-repeat;
											background-blend-mode:multiply;
											opacity:0.5;
											display: block;	
											content:'';
											z-index:-2;
											width:100%;
											height:100%;
											background-size: 100%;
										}
									</style>
								</li>

								

							<?php endwhile; ?>

							 

						<?php else : ?>

							<?php get_template_part( 'content', 'none' ); ?>

						<?php endif; ?>

						<?php wp_reset_postdata(); ?>

	  			 </ul>	
	  			
	  		</div><!-- .jcarousel -->
	  		<span class="controls"><a href="#" class="arrow-left-icon jcarousel-control-prev"></a></span>
	  		<span class="controls"><a href="#" class="arrow-right-icon jcarousel-control-next"></a></span>
	  	 

	  		<span class="controls dot jcarousel-pagination"></span> 


  		</div>


  	</div>
 
