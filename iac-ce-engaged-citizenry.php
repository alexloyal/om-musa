<?php
/**
 * @package om_musa
 */
	wp_enqueue_script( 'iac-ce-engaged-citizenry', get_template_directory_uri() . '/js/iac-ce-engaged-citizenry.js', array('d3'), '1.0', true );
	
    wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );
?>

<div class="iac-ce-engaged-citizenry">

	<div class="swap objective one">

		<div class="item" data-swap="1" data-dimension="A">
			<span>Voter Participation Rate</span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>Volunteer Rate</span>
		</div>

		<div class="item" data-swap="0" data-dimension="C">
			<span>Charitable Contributions</span>
		</div>

		<div class="item" data-swap="0" data-dimension="D">
			<span>YOUNG ADULT VOLUNTEER RATE</span>
		</div>

	 

		

	</div>

<div class="col-md-6 year-labels centered">
	<span class="cyan" data-value=""></span>
	<span class="purp" data-value=""></span>
</div>



<div class="iac-ce-engaged-citizenry">
		<div class="graph"></div>
</div>


<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->


<div class="data-disclosure col-md-12">
	<div data-dimension="A">
			<h5>VOTER PARTICIPATION</h5>
		
			<p>High levels of voter turnout help ensure that public policy reflects the desire of the electorate.</p>

			<p>Miami-Dade residents vote at levels that are comparable to major metros, and higher than the US average.</p>

			<p class="source">Source: <a href="http://election.dos.state.fl.us/elections/resultsarchive/Index.asp?ElectionDate=11/6/2012&DATAMODE=" title="Florida Department of State">Florida Department of State</a>
				<br>Data Showing: Central County
			</p>
	</div>

	<div data-dimension="B">
			<h5>% OF RESIDENTS THAT VOLUNTEER</h5>
		
			<p>Volunteering provides positive benefits to both the community and the volunteers themselves.</p>

			<p>In a recent study, Miami metro residents were found to volunteer the least among major metros.  Just 15% of residents volunteer, versus 27% for the US.</p>

			<p class="source">Source: <a href="http://election.dos.state.fl.us/elections/resultsarchive/Index.asp?ElectionDate=11/6/2012&DATAMODE=" title="Florida Department of State">Florida Department of State</a>
				<br>Data Showing: Metro-level
			</p>
	</div>

	<div data-dimension="C">
			<h5>CHARITABLE CONTRIBUTIONS (AS A % OF ADJUSTED GROSS INCOME)</h5>
		
			<p>Charitable giving is crucial in addressing community needs that can't be solved by local government.</p>

			<p>Miami-Dade county residents donate to charities at a much lower rate than residents at other major metros.</p>

			<p class="source">Source: <a href="http://www.irs.gov/uac/SOI-Tax-Stats-Individual-Income-Tax-Return-Form-1040-Statistics" title="IRS">IRS</a>
				<br>Data Showing: Central County
			</p>

	</div>

	<div data-dimension="D">
			<h5>% OF RESIDENTS AGE 25 TO 34 THAT VOLUNTEER</h5>
		
			<p>Early participation in volunteer activities promotes lifelong civic involvement.</p>


			<p>Young adults in Miami-Dade volunteer at a lower rate than other major metros.</p>

			<p class="source">Source: <a href="http://www.volunteeringinamerica.gov/export.cfm?year=2011#filters" title="Corporation for National &amp; Community Service">Corporation for National &amp; Community Service</a>
				<br>Data Showing: Metro-level
			</p>

	</div>

</div>



</div>

 
 
 