<?php
/**
 * @package om_musa
 */

	wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.ia.graph.js', array('d3'), '1.0', true );
	wp_enqueue_script( 'iac-ev-quality-public-space', get_template_directory_uri() . '/js/iac-ev-quality-public-space.js', array('d3'), '1.0', true );


?>

<div class="iac-ev-quality-public-spaces">

	<div class="swap objective two">

		<div class="item" data-swap="1" data-dimension="A">
			<span>Parkland per Capita</span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>Public Investment in Parks &amp; Public Spaces</span>
		</div>

		<div class="item" data-swap="0" data-dimension="C">
			<span>Public Libraries Expenditures</span>
		</div>





	</div>

	
		<div class="col-md-6 year-labels centered">

			<div class="item col-md-3 col-xs-3 apple green"></div>
			<div class="item col-md-3 col-xs-3 purp"></div>

		</div>
		
	

	<div class="col-md-12 col-xs-12 circles">
		<div class="col-md-6 city-group" data-label="chi">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Chicago"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Chicago"></div>
		</div>

		<div class="col-md-6 city-group" data-label="hou">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Houston"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Houston"></div>
		</div>

		<div class="col-md-6 city-group" data-label="mia">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="Miami"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="Miami"></div>
		</div>


		<div class="col-md-6 city-group" data-label="nyc">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="NYC"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="NYC"></div>
		</div>

		<div class="col-md-6 city-group" data-label="sd">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="SD"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="SD"></div>
		</div>

		<div class="col-md-6 city-group" data-label="us">
			<div class="col-md-6 col-xs-6 radial first" data-year="" data-city="US"></div>
			<div class="col-md-6 col-xs-6 radial second" data-year="" data-city="US"></div>
		</div>

	</div>

<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->

<div class="data-disclosure col-md-12">
	<div data-dimension="A">
			<h5>Parkland Per 1,000 residents by City</h5>

			<p>Parks are an important part of connecting the community with nature, recreation, and each other.</p>

			<p>Miami-Dade county has one of the lowest levels of parks per capita among its peers, including densely populated New York and Chicago.</p>

			<p class="source">Source: <a href="http://www.tpl.org/2014-city-park-facts" title="Trust for Public Land">Trust for Public Land</a>
				<br>Data Showing: City
			</p>
	</div>

	<div data-dimension="B">

			<h5>Per Capita Spending on Parks &amp; Recreation</h5>

			<p>Parks are government-funded infrastructure, just like roads, and cost money to acquire, build and maintain.</p>

			<p>The city of Miami spends less on parks per capita than most large cities.  In a recent report, Miami ranked 43 out of 51 cities.</p>

			<p class="source">Source: <a href="http://www.tpl.org/2014-city-park-facts" title="Trust for Public Land">Trust for Public Land</a><br>
			Data Showing: City</p>

	</div>

	<div data-dimension="C">

			<h5>Annual Public Libraries Expenditures per Resident</h5>

			<p>Public libraries provide communities with a host of vital resources and materials.</p>

			<p>Miami-Dade county spends less on libraries per capita than the US average.  And, spending has fallen significantly in recent years.</p>

			<p class="source">Source: <a href="http://www.imls.gov/research/pls_data_files.aspx" title="American Library Association">American Library Association</a><br>
				Data Showing: Library System</p>

	</div>


</div>



</div>
