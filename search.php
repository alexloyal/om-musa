<?php
/**
 * The template for displaying search results pages.
 *
 * @package om_musa
 */

get_header(); ?>

	<section id="primary" class="content-area row">
		<main id="main" class="site-main col-md-12" role="main">

			<section id="search-results" class="col-md-10 centered">
				<?php if ( have_posts() ) : ?>

				<header class="page-header col-md-12">
					<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'om-musa' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				</header><!-- .page-header -->

				<?php get_search_form(); ?>
		
				<?php /* Start the Loop */ 



				?>
		 

				<?php while ( have_posts() ) : the_post(); ?>

					<?php

					

					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					get_template_part( 'content', 'search' );
					?>



				<?php endwhile; ?>

				

				<?php om_musa_paging_nav(); 


				?>

			<?php else : ?>

				<?php get_template_part( 'content', 'none' ); ?>

			<?php endif; ?>

	</section>

		</main><!-- #main -->
	</section><!-- #primary -->


<?php get_footer(); ?>
