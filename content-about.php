<?php
/**
 * @package om_musa
 */
?>
<!-- content-about -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-11 centered'); ?>>
	<header class="entry-header">
		<?php the_title( '<h3 class="entry-title center align thin">', '</h3>' ); ?>

		<div class="entry-meta">
			 
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php the_content(); ?>	

		 <div class="tap quote">
									<a href="/get-engaged" class="eng-<?php the_id(); ?>">
										<img src="<?php bloginfo('template_directory' ); ?>/images/gears.png" /> <br>
										<h4 class="engaged-link">Get Engaged</h4>
									</a>
		</div>
		 <?php //get_template_part( 'util', 'social-share' ); ?>
	</div><!-- .entry-content -->

 
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
