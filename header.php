<?php 
header("Access-Control-Allow-Origin: *"); 
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package om_musa
 */

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="google-site-verification" content="_gZ6mJx0wh3OSsAEjamnMah5hD3qKCdH6R7uPe9ppRw" />
<meta name="msvalidate.01" content="47E028D77BD85B8CF8031E2A5C1C2609" />
	

<?php if(is_singular()) {
		
		// $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

	?>
		<meta property="og:site_name" content="Our Miami"/>
		<meta property="og:url" content="<?php the_permalink(); ?>" />
		<meta property="og:title" content="<?php the_title(); ?>" />
		<meta property="og:description" content="<?php the_excerpt(); ?>" />
		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/images/our-miami-report-share.png?v1.0" />
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:site" content="@MiamiFoundation" />
		<meta name="twitter:title" content="<?php the_title(); ?>" />
		<meta name="twitter:description" content="<?php the_excerpt(); ?>" />
		<meta name="twitter:image" content="<?php echo get_template_directory_uri(); ?>/images/our-miami-report-share.png?v1.0" />
		<meta name="twitter:url" content="<?php the_permalink(); ?>" />


	<?php

} else {
	?>
		<meta property="og:site_name" content="Our Miami"/>
		<meta property="og:url" content="<?php bloginfo('url'); ?>" />
		<meta property="og:title" content="<?php bloginfo('name'); ?>" />
		<meta property="og:description" content="<?php bloginfo('description'); ?>" />
		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/images/our-miami-report-share.png?v1.0" />
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:site" content="@MiamiFoundation" />
		<meta name="twitter:title" content="<?php bloginfo('name'); ?>" />
		<meta name="twitter:description" content="<?php bloginfo('description'); ?>" />
		<meta name="twitter:image" content="<?php echo get_template_directory_uri(); ?>/images/our-miami-report-share.png?v1.0" />
		<meta name="twitter:url" content="<?php bloginfo('url') ?>" />
		



	<?php


}?>

<title><?php wp_title( '|', true, 'right' ); ?></title>
<style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<span id="top"></span>
     <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

<div id="wrapper" class="container-fluid">
  
	  <header class="site-header row" role="banner" id="tools">
		
			<div class="site-branding col-xs-12 col-sm-8 col-md-9">
				
				<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'om-musa' ); ?></a>	

				<h1 class="site-title col-sm-5 col-md-4"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

				<nav id="site-navigation" class="navigation col-sm-7 col-md-8" role="navigation">
						<!-- <button class="menu-toggle"><?php // _e( 'Primary Menu', 'om-musa' ); ?></button> -->
						<?php wp_nav_menu( array( 
													'theme_location' => 'primary',
													//'link_before' => '<span class="menuicon"></span>',
				          							'link_after' => '',
				          							'walker' => new Bootstrap_Walker_Nav_Menu
													 ) ); ?>
				</nav><!-- #site-navigation -->
				
			</div>

				<div id="search" class="col-xs-1 col-md-1 taphover">
							<span class="search-icon">&nbsp;</span>
						
							<ul class="search-wrapper sub-menu">
								<li class="col-md-12"><?php get_search_form(); ?></li>
							</ul>

				</div>

			 
				<div class="col-xs-9 col-sm-2 col-md-2 social-navigation" role="social-navigation">
						<?php wp_nav_menu( 

									array( 
													'theme_location' => 'social',
													'link_before' => '<span class="',
													'menu_class'      => 'menu list-line',
					          						'link_after' => '-icon"></span>'


					          						) 
					          				); 

					          				?>
				</div><!-- .social-navigation -->

				<div class="col-md-12 flat">
					<div class="col-md-9 col-xs-9 flat green medium border">&nbsp;</div>
					<div id="cyan" class="col-md-3 col-xs-3 flat cyan medium border">&nbsp;</div>
				</div>


		


	  </header><!-- #tools -->


<div id="content" class="site-content row">
