<?php
/**
 * @package om_musa
 */
?>
<!-- content-excerpt -->
<div class="background" style="background-image:url('<?php 
											$coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											echo $coverimageurl
										 ?>');" class="">

	<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-8 col-sm-8 centered well'); ?>>
		 
		 <div class="meta col-md-3 col-xs-3">
			
			<span class="avatar">
				<!-- circle thumb -->
				<?php 
					//$author_thumb = get_the_author_meta('user_url'); 
					$author_thumb = get_post_meta( get_the_ID(), 'om_musa_circle_thumb', true );
						if ( $author_thumb !="" ) { ?>
					<img src="<?php echo $author_thumb ?>"> <?php
				} else { ?>
				<img src="<?php echo get_template_directory_uri()?>/images/Our_Miami_Logos_circle.png" title="Our Miami">
					<?php	}
				

				?>
				<!-- <img src="<?php //echo get_template_directory_uri()?>/images/amy-web.jpg"> -->
			</span>
			 <span class="pubby caps"><p>Published by</p></span>
			<span class="author caps"><p>
					<?php 
				$guest_author = get_post_meta( get_the_ID(), 'om_musa_guest_author', true );
				// check if the custom field has a value
				if( ! empty( $guest_author ) ) {
				  ?><?php echo $guest_author; ?> <?php 
				} else {
					?> <?php the_author(); ?>
					<?php
				} ?>
			</p></span>
			<span class="pub date caps"><h5><?php the_time('n.j.y');  ?></h5></span>
			<span class="tag list caps">
				<span class="tag-kicker">Tagged Under</span><br>	
				<?php the_tags('', ', ', '<br />'); ?>


			</span>
			<span class="tag list caps issue-areas">
				<span class="tag-kicker">Issue Areas this post is related to:</span><br>
				<?php the_terms( $post->ID, 'issue-areas', '', '' ); ?><br>
			</span>

		</div>

		<div class="col-md-9 col-sm-9 col-xs-9 main excerpt">
					<header class="entry-header">
						<a href="<?php the_permalink(); ?>" class="<?php the_id(); ?>">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</a>
						<div class="entry-meta">
							
						</div><!-- .entry-meta -->
					</header><!-- .entry-header -->

					<div class="entry-content">

						<?php the_excerpt(); ?>
						 <div class="tap quote">
													<a href="<?php the_permalink(); ?>" class="<?php the_id(); ?>">
														More
													</a>
						</div>
					</div><!-- .entry-content -->

					<?php get_template_part( 'util', 'social-share' ); ?>

		</div>	

	 
	</article><!-- #post-## -->

</div>
