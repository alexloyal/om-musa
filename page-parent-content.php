<?php
/*
	Template Name: Parent Page With Content
*/

/**	
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package om_musa
 */

get_header(); ?>
<!-- page-parent-content -->
	<div id="primary" class="content-area row">
		<main id="main" class="site-main col-md-12" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

			<div class="background" style="background-image:url('<?php 
											$coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											echo $coverimageurl
										 ?>');" class="">	


										 		<?php 
											//		$story_kicker = get_post_meta( get_the_ID(), 'story-kicker', true );
													// check if the custom field has a value
											//		if( ! empty( $story_kicker ) ) {
													  ?> 
													  <!--	<h4 class="kicker center align">  -->
													  <?php 
											//		  echo $story_kicker; ?>
														<!--	</h4>  -->
														<?php 
											//		} else {
											//			the_title( '<h4 class="kicker center align">', '</h4>' );
											//		} ?>

										 


				<?php get_template_part( 'content', 'page-wide' ); ?>


				 <div class="col-md-7 centered sub-page-container">

				  		 
				 
	  			 		<?php global $post; 

	  			 			$parent = $post->ID;

	  			 			$args = array (
								'post_type' 		=> 'page',
								'post_parent' 		=> $parent,
								'order'				=> 'ASC',
								'orderby'			=> 'menu_order',
								'posts_per_page'    => -1
								
							);
							$parentQuery = new WP_Query( $args ); ?>
							 

	  			 	 
	  			 	
	  			 	<?php if ( $parentQuery->have_posts() ) : ?>

							<?php /* Start the Loop */ ?>
							<?php while ( $parentQuery->have_posts() ) : $parentQuery->the_post(); ?>
 
									<div class="child">



										<?php
											/* Include the Post-Format-specific template for the content.
											 * If you want to override this in a child theme, then include a file
											 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
											 */
											 
					 						get_template_part( 'page', 'content-child' );


											?>
 
									
									</div>
							 

								

							<?php endwhile; ?>

							 	

					<?php else : ?>

							<?php get_template_part( 'content', 'none' ); ?>

					<?php endif; ?>
				
					<?php om_musa_paging_nav(); ?>
			
					<?php wp_reset_postdata(); ?>
	  			 </div>	


			</div>
			



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
