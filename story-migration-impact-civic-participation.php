<?php
/**
 * @package om_musa
 */
?>

<!-- story-migration-impact-civic-participation -->

<?php 

	 wp_enqueue_script( 'residents-born-outside', get_template_directory_uri() . '/js/residents-born-outside-civic-participation-viz.js', array('d3'), '1.0', true );
     wp_enqueue_script( 'radial-graph-city', get_template_directory_uri() . '/js/radial.citylabel.graph.js', array('d3'), '1.0', true );
     wp_enqueue_script( 'bar-graph', get_template_directory_uri() . '/js/bar.graph.js', array('d3'), '1.0', true );

 ?>
 <section class="residents-born-outside-civic-participation">
        <h4>Percent of residents born out of the state or abroad.</h4>
  
 <div class="data-svg col-md-10 centered">

    <div class="five marker">50%</div>
            
    <div class="graph"></div>

    <div class="radial"></div>

    <div id="" class="city names col-md-12">
            <div data-city="Chicago" class="city-name">CHI</div>
            <div data-city="Houston" class="city-name">HOU</div>
            <div data-city="Miami" class="city-name">MIA</div>
            <div data-city="NYC" class="city-name">NYC</div>
            <div data-city="San Diego" class="city-name">SD</div>
            
    </div>

    <div class="annotation purp">
        <p>% of residents that volunteer</p>
    </div>

 </div>
 

       
<div class="legend col-md-10 centered">

        

        <div class="toggle" data-value="2">
            <div class="value" data-value="2010">2010</div>
            <div class="slider"></div>
            <div class="value" data-value="2012">2012</div>
        </div>


</div>
        
    </section>
 
 
 