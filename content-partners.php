<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package om_musa
 */
?>
<!-- content-partners -->

<div class="col-md-8 centered sub-page-container">
	<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12 centered'); ?>>
		

		 
		<div class="col-md-10 center entry-wrapper">
			<header class="entry-header">
				<?php 

			 			the_title( '<h1 class="entry-title internal center align">', '</h1>' );

						$story_hook = get_post_meta( get_the_ID(), 'om_musa_story_hook', true );
						// check if the custom field has a value
						if( ! empty( $story_hook ) ) {
						  ?> <h2 class="subhead"> <?php echo $story_hook; ?></h2> <?php 
						} else {
							 
						} ?>

			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php the_content(); ?>

				<div class="partners col-md-12">
					 
					 <?php
						$partners = get_bookmarks( array(
							'orderby'        => 'name',
							'order'          => 'ASC',
							'category_name'  => 'Partners'
						));

						
						foreach ( $partners as $partner ) { ?>

							<div class="issue-buffer col-md-4 col-xs-6">
									<div class="issue-thumb" style="background-image:url(<?php printf($partner->link_image); ?>);">
												
										<div class="issue" >
											<span class="issue-wrapper">
											<h4 class="issue-title">
												<?php printf( '<a class="relatedlink" href="%s">%s</a><br />', $partner->link_url, $partner->link_name ); ?>
											</h4>

											<p><?php printf($partner->link_description); ?></p>

											<p class="go"><a href="<?php echo $partner->link_url ?>">Go</a></p>
										</span>
										
										</div>
									</div>
							</div>


						<?php
						    
						}
						?>
				</div>	

				

				 <?php // get_template_part( 'util', 'social-share' ); ?>
			</div><!-- .entry-content -->

			 
			<footer class="entry-footer">
				<?php edit_post_link( __( 'Edit', 'om-musa' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->
</div>
