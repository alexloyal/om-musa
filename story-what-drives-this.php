<?php
/**
 * @package om_musa
 */
?>

<?php 

	 wp_enqueue_script( 'cost-of-living-vs-household-income', get_template_directory_uri() . '/js/cost-of-living-vs-household-income-viz.js', array('d3'), '1.0', true );
     // wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.graph.js', array('d3'), '1.0', true );

 ?>

 <!-- story-what-drives-this -->

 <section class="what-drives-this">
        
        <div class="data-svg"></div>
        

     <div class="annotation col-md-11 col-xs-12 centered" data-value="0">
            <span class="highlight" data-value="Affordable">Affordable</span>
            <span class="highlight" data-value="Not So Affordable">Not So Affordable</span>
            <span class="highlight" data-value="Unaffordable">Unaffordable</span>
        </div>


       
<div class="legend col-md-11 col-xs-12 centered">

        <div id="" class="city names col-md-12">
            <div data-city="Chicago" class="city-name">CHI</div>
            <div data-city="Houston" class="city-name">HOU</div>
            <div data-city="Miami" class="city-name">MIA</div>
            <div data-city="NYC" class="city-name">NYC</div>
            <div data-city="San Diego" class="city-name">SD</div>
            <div data-city="US" class="city-name">U.S.</div>
        </div>

        <div class="toggle" data-value="2">
            <div class="value" data-value="2011">2011</div>
            <div class="slider"></div>
            <div class="value" data-value="2013">2013</div>
        </div>


</div>
        
    </section>
 
 
 