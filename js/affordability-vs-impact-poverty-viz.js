function soWhatDoesThisMean(){

// a = All Ages
// b = 0-18
// c = 18-64
// d = 18+
// e = 65+
// 




var ageRange = "0-18";
var dataSourceDir = base + '/wp-content/themes/om-musa/';

d3.csv(dataSourceDir+'js/data-svg/affordability-vs-impact-poverty.csv', function(error, data) {
         
           

      if (error) {
               // If error is not null, something went wrong.

               console.log(error);
                //Log the error.

                } 
  else 

  { 
            
            
               povertyData = data; 

               
            //     console.log(povertyData)
                 
                 

         
                      
               // .data(dataset.map(function(d) { return +d; }))
          

                    function clearDeck(){
                                  
                              };  // clearDeck()   

                    function showData(){

                              
                              // 65+

                              //Write a case/switch for each age range. Read it off of the data attribute.
                              // Return povertyData[i].County
                              
                              

                           for (var i = povertyData.length - 1; i >= 0; i--) {
                             
                             value2011 = d3.select('.what-does-this-mean [data-city="'+povertyData[i].County+'"][data-year="2011"]').append('text')
                             value2013 = d3.select('.what-does-this-mean [data-city="'+povertyData[i].County+'"][data-year="2013"]').append('text')

                              value2011
                              .attr('class', 'hidden purp')
                              .attr('data-value', function(){
                                        
                                        switch(ageRange) {
                                              case "0-18":
                                                return  povertyData[i].b2011;
                                              break;
                                              case "18-64":
                                                return  povertyData[i].c2011;
                                              break;
                                              case "65+":
                                                return  povertyData[i].d2011;
                                              break;
                                              case "all-ages":
                                                return  povertyData[i].a2011;
                                              break;
                                                
                                            }

                                             })
                                value2013
                                .attr('class', 'hidden purp')
                                .attr('data-value', function(){
                                        switch(ageRange) {
                                              case "0-18":
                                                return  povertyData[i].b2013;
                                              break;
                                              case "18-64":
                                                return  povertyData[i].c2013;
                                              break;
                                              case "65+":
                                                return  povertyData[i].d2013;
                                              break;
                                              case "all-ages":
                                                return  povertyData[i].a2013;
                                              break;
                                            }
                                               }) 

                                   }; //for loop
                               

                                   d3.selectAll('[data-age]')
                                     .on('click',function(){
                                        //$('.what-does-this-mean [data-age]').removeClass('active');
                                        

                                        ageRange = $(this).data('age');

                                        for (var i = povertyData.length - 1; i >= 0; i--) {

                                          $('.what-does-this-mean [data-city="'+povertyData[i].County+'"][data-year="2011"] text')
                                          .attr('data-value', function(){

                                              
                                                    
                                                    switch(ageRange) {
                                                          case "0-18":
                                                            return  povertyData[i].b2011;
                                                          break;
                                                          case "18-64":
                                                            return  povertyData[i].c2011;
                                                          break;
                                                          case "65+":
                                                            return  povertyData[i].d2011;
                                                          break;
                                                          case "all-ages":
                                                            return  povertyData[i].a2011;
                                                          break;
                                                            
                                                        }
                                                    
                                                });

                                              }; // Loop
                                          for (var i = povertyData.length - 1; i >= 0; i--) {

                                          $('.what-does-this-mean [data-city="'+povertyData[i].County+'"][data-year="2013"] text')
                                          .attr('data-value', function(){

                                              
                                                    
                                                    switch(ageRange) {
                                                          case "0-18":
                                                            return  povertyData[i].b2013;
                                                          break;
                                                          case "18-64":
                                                            return  povertyData[i].c2013;
                                                          break;
                                                          case "65+":
                                                            return  povertyData[i].d2013;
                                                          break;
                                                          case "all-ages":
                                                            return  povertyData[i].a2013;
                                                          break;
                                                            
                                                        }
                                                    
                                                });
                                              }; // Loop

                                           for (var i = povertyData.length - 1; i >= 0; i--) {
                                                
                                                $('.what-does-this-mean [data-city="'+povertyData[i].County+'"][data-poverty]').attr('data-poverty', function(){

                                                    switch(ageRange) {
                                                                case "0-18":
                                                                    var poverty = parseFloat(povertyData[i].b2013) - parseFloat(povertyData[i].b2011)

                                                                    if( poverty > 0) {
                                                                      return  "Poverty Increasing"    
                                                                    } else if (poverty = 0) {
                                                                      return "No Change"
                                                                    } else {
                                                                      return "Poverty Decreasing"
                                                                    }
                                                                  
                                                                break;
                                                                case "18-64":
                                                                  var poverty = parseFloat(povertyData[i].c2013) - parseFloat(povertyData[i].c2011)

                                                                    if( poverty > 0) {
                                                                      return  "Poverty Increasing"    
                                                                    } else if (poverty = 0) {
                                                                      return "No Change"
                                                                    } else {
                                                                      return "Poverty Decreasing"
                                                                    }
                                                                  
                                                                break;
                                                                case "65+":
                                                                  var poverty = parseFloat(povertyData[i].d2013) - parseFloat(povertyData[i].d2011)

                                                                    if( poverty > 0) {
                                                                      return  "Poverty Increasing"    
                                                                    } else if (poverty = 0) {
                                                                      return "No Change"
                                                                    } else {
                                                                      return "Poverty Decreasing"
                                                                    }
                                                                  
                                                                break;
                                                                case "all-ages":
                                                                  var poverty = parseFloat(povertyData[i].a2013) - parseFloat(povertyData[i].a2011)

                                                                    if( poverty > 0) {
                                                                      return  "Poverty Increasing"    
                                                                    } else if (poverty = 0) {
                                                                      return "No Change"
                                                                    } else {
                                                                      return "Poverty Decreasing"
                                                                    }
                                                                  
                                                                break;
                                                                  
                                                              }


                                                })
                                                  
                                                      

                                              };   


                                         $(this).toggleClass('active');                                           
                                          activeCity = $('.what-does-this-mean [data-city] span.active').text();
                                          // $('[data-city="'+activeCity+'"] text').toggleClass('hidden')
                                         //  $('[data-city="'+activeCity+'"] text').toggleClass('hidden')

                                     })
                              
                               } // showData
                     

                      
  } // Else


             $(document).ready(function(){
                  var clickCount = 0;
                  showData();
                   $('.what-does-this-mean [data-city="Miami"] text').removeClass('hidden');
                   $('.what-does-this-mean [data-city="Miami"].city-label').addClass('active');
                   $('.what-does-this-mean [data-age="0-18"]').addClass('active');
                   $('.what-does-this-mean [data-city="Miami"][data-poverty]').attr('data-poverty', function(){
                        var mia2013 = $('.what-does-this-mean [data-city="Miami"][data-year=2013] [data-value]').data('value');
                        var mia2011 = $('.what-does-this-mean [data-city="Miami"][data-year=2011] [data-value]').data('value');

                        var poverty = parseFloat(mia2013) - parseFloat(mia2011);

                        if (poverty > 0) {
                          return "Poverty Increasing"
                        } else if (poverty = 0) {
                          return "No Change"
                        } else {
                          return "Poverty Decreasing"
                        };
                   });
                   
                   $('.what-does-this-mean [data-city="Miami"][data-poverty]').addClass('active');
                   

                   //Shows all City Data
                   
                   $('.what-does-this-mean td[data-total="all-data"]').on('click', function(){
                        $(this).toggleClass('active');
                        
                        clickCount++
                       
                       if (clickCount % 2 === 0) {
                          $('.what-does-this-mean [data-city].city-label').removeClass('active')
                          $('.what-does-this-mean [data-city][data-poverty]').removeClass('active')
                          $('.what-does-this-mean [data-city] text:not(.hidden)').addClass('hidden')
                       } else {
                          
                          $('.what-does-this-mean [data-city].city-label:not(.active)').addClass('active')
                          $('.what-does-this-mean [data-city] text').removeClass('hidden')
                          $('.what-does-this-mean [data-city][data-poverty]:not(.active)').addClass('active')

                          
                          
                       };

                        

                   //   $('.what-does-this-mean [data-city].city-label.active').removeClass('active')
              
                      
                      
                      

                   })

                   $('.what-does-this-mean td[data-city] span').on('click', function(){
                      selectedCity = $(this).parent().data('city');
                      $('.what-does-this-mean [data-city="'+selectedCity+'"] text').toggleClass('hidden');
                      $('.what-does-this-mean [data-city="'+selectedCity+'"].city-label').toggleClass('active');
                      $('.what-does-this-mean [data-city="'+selectedCity+'"][data-poverty]').attr('data-poverty', function(){
                        var poverty2013 = $('.what-does-this-mean [data-city="'+selectedCity+'"][data-year=2013] [data-value]').data('value');
                        var poverty2011 = $('.what-does-this-mean [data-city="'+selectedCity+'"][data-year=2011] [data-value]').data('value');

                        var poverty = parseFloat(poverty2013) - parseFloat(poverty2011);

                        if (poverty > 0) {
                          return "Poverty Increasing"
                        } else if (poverty = 0) {
                          return "No Change"
                        } else {
                          return "Poverty Decreasing"
                        };
                   });
                      $('.what-does-this-mean [data-city="'+selectedCity+'"][data-poverty]').toggleClass('active');
                      
                   });

                   $('.what-does-this-mean td[data-age]').on('click', function(){
                      
                      
                       $('.what-does-this-mean td[data-age].active').toggleClass('active');
                       $(this).toggleClass('active');
                       // $(this).toggleClass('active');
                   });



             });

                 

   


}) // json








};

// $('#post-1539').waypoint(function(down){



      soWhatDoesThisMean();

// },{triggerOnce:true});