var residentsYear = "2012";
var cityName = "Miami";
var dataSourceDir = base + '/wp-content/themes/om-musa/';
var residentData;
var volunteerData;
var dataIndex = "0";

//create some global vars for our graphs
var civicBarGraph,
    chicagoRadial,
    houstonRadial,
    miamiRadial,
    nycRadial,
    sdRadial;


d3.json(dataSourceDir+"js/data-svg/residents-born-outside-civic-participation.json", function(error,data) {

            if (error) {
                // If error is not null, something went wrong.
                console.log(error);
                //Log the error.
            } else {
                residentData = data;

                d3.json(dataSourceDir+'/js/data-svg/residents-born-outside-volunteer-rate.json', function(err, data) {

                    //  console.log(data[0]["2010"][0])

                    volunteerData = data;


                    $(document).ready(function(){
                            var chicago = volunteerData[residentsYear]["Chicago"]["Volunteer"];
                            var houston = volunteerData[residentsYear]["Houston"]["Volunteer"];
                            var miami = volunteerData[residentsYear]["Miami"]["Volunteer"];
                            var nyc = volunteerData[residentsYear]["NYC"]["Volunteer"];
                            var sd = volunteerData[residentsYear]["San Diego"]["Volunteer"];

                           civicBarGraph = new BarGraph(residentData[residentsYear],'.residents-born-outside-civic-participation .graph');
                           chicagoRadial = new RadialGraph([chicago], '.residents-born-outside-civic-participation .radial',chicago, 'chicago'); //data, elementSelector, title
                           houstonRadial = new RadialGraph([houston], '.residents-born-outside-civic-participation .radial',houston, 'houston'); //data, elementSelector, title
                           miamiRadial = new RadialGraph([miami], '.residents-born-outside-civic-participation .radial',miami, 'miami'); //data, elementSelector, title
                           nycRadial = new RadialGraph([nyc], '.residents-born-outside-civic-participation .radial',nyc, 'nyc'); //data, elementSelector, title
                           sdRadial = new RadialGraph([sd], '.residents-born-outside-civic-participation .radial',sd, 'san-diego'); //data, elementSelector, title

                           $('.residents-born-outside-civic-participation .toggle .value').on('click',function(e){

                                residentsYear = $(this).attr('data-value');

                                var chicago = volunteerData[residentsYear]["Chicago"]["Volunteer"]
                                var houston = volunteerData[residentsYear]["Houston"]["Volunteer"]
                                var miami = volunteerData[residentsYear]["Miami"]["Volunteer"]
                                var nyc = volunteerData[residentsYear]["NYC"]["Volunteer"]
                                var sd = volunteerData[residentsYear]["San Diego"]["Volunteer"]

                                

                                civicBarGraph.update(residentData[residentsYear]);
                                chicagoRadial.update([chicago]);
                                houstonRadial.update([houston], '.residents-born-outside-civic-participation .radial',houston, 'houston'); //data, elementSelector, title
                                miamiRadial.update([miami], '.residents-born-outside-civic-participation .radial',miami, 'miami'); //data, elementSelector, title
                                nycRadial.update([nyc], '.residents-born-outside-civic-participation .radial',nyc, 'nyc'); //data, elementSelector, title
                                sdRadial.update([sd], '.residents-born-outside-civic-participation .radial',sd, 'san-diego'); //data, elementSelector, title


                                $('.residents-born-outside-civic-participation .toggle').attr('data-value', $(this).index())
                            });

                    }); // document ready

            }) // d3 json volunteer rate
        } // else
 }) // d3 json civic participation
