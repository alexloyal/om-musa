//global store for the data (fewer http requests)
var educationData = {
    unemploymentRate: null
}

//store the ways we'll filter data (two dimensions for the first one)
var opportunityState = {
    year: 2010,
    property: null
}

//first, load the general unemployment rate
d3.csv("js/data-svg/education-unemployment-rate.csv", function(error,data) {
    educationData.unemploymentRate = data;

    educationData.unemploymentRateByEducation = {
        "2010": {},
        "2012": {}
    };

    dataByEducation();
});

//next, load the data for unemployment by education, and finesse the data for the chart
function dataByEducation(){
    d3.csv("js/data-svg/education-unemployment-by-education.csv", function(error,data) {

        for (var i = 0; i < data.length; i++) {
            var record = data[i];
            var newrecord = {};

            var location = record["Central County of:"];

            for (var prop in record) {
                if (prop.indexOf("2010") > -1) {
                    var datum = {};
                    datum.location = location;
                    // datum.name = prop.substring(0, prop.indexOf(" 2010"));
                    datum.value = record[prop];
                    datum.baseline = educationData.unemploymentRate[i]["2011"];

                    if (educationData.unemploymentRateByEducation["2010"][prop.substring(0, prop.indexOf(" 2010"))]) {
                        educationData.unemploymentRateByEducation["2010"][prop.substring(0, prop.indexOf(" 2010"))].push(datum);
                    } else {
                        educationData.unemploymentRateByEducation["2010"][prop.substring(0, prop.indexOf(" 2010"))] = [datum];
                    }


                    //set the initial property to the first property
                    if (prop.indexOf("graduate") > -1) {
                        opportunityState.property = prop.substring(0, prop.indexOf(" 2010"));
                    }
                } else if (prop.indexOf("2012") > -1) {
                    var datum = {};
                    datum.location = location;
                    // datum.name = prop.substring(0, prop.indexOf(" 2012"));
                    datum.value = record[prop];
                    datum.baseline = educationData.unemploymentRate[i]["2013"];

                    if (educationData.unemploymentRateByEducation["2012"][prop.substring(0, prop.indexOf(" 2012"))]) {
                        educationData.unemploymentRateByEducation["2012"][prop.substring(0, prop.indexOf(" 2012"))].push(datum);
                    } else {
                        educationData.unemploymentRateByEducation["2012"][prop.substring(0, prop.indexOf(" 2012"))] = [datum];
                    }
                }
            }
        }

        console.log(educationData.unemploymentRateByEducation);

        renderEducationData();

        educationLegend();
    });
}

function educationLegend() {
    var legendItems = $(".unemployment-rate .legend .item");

    var index = 1;

    for(prop in educationData.unemploymentRateByEducation["2010"]) {
        $( $(".unemployment-rate .legend .item")[index] ).attr("data-property", prop);
        index++;
    }

    $(".unemployment-rate .legend .item").on("click", function(e) {
        if ($(this).is(".no-interaction")) { return; }
        opportunityState.property = $(this).attr("data-property");
        renderEducationData();
    })

    $(".unemployment-rate .legend .toggle .value").on("click", function(e) {
        $(".unemployment-rate .legend .toggle").attr("data-value", $(this).attr("data-value"));
        opportunityState.year = $(this).attr("data-value");
        renderEducationData();
    })
}


function renderEducationData() {

    var data = educationData.unemploymentRateByEducation[opportunityState.year][opportunityState.property];

    var bars = d3.select(".unemployment-rate .bars").selectAll("div.bar").data(data);

    var flatdata = [];

    for (var prop in educationData.unemploymentRateByEducation[opportunityState.year]) {
        flatdata = flatdata.concat(educationData.unemploymentRateByEducation[opportunityState.year][prop]);
    }

    var min = d3.min(flatdata, function(d) {
        return Math.min( parseFloat(d.value), parseFloat(d.baseline) )-1;
    });

    var max = d3.max(flatdata, function(d) {
        return Math.max( parseFloat(d.value), parseFloat(d.baseline) ) + 1;
    });

    var height = d3.scale.linear().domain([0, max]).range([0,100]);
    var top = d3.scale.linear().domain([0, max]).range([100, 0]);

    $(".unemployment-rate").attr("data-dimension", opportunityState.property.toLowerCase());

    bars
        .call(function(context) {
            this.select("div.rate")
                .style("height", function(d) {
                    return height(parseFloat(d.value)) + "%";
                })
                .select("div.value")
                .attr("data-value", function(d) {
                    return d.value.substring(0, d.value.indexOf("%"));
                });
        })
        .select("div.baseline")
        .attr("data-value", function(d) {
            return d.baseline.substring(0, d.baseline.indexOf("%"));
        })
        .style("top", function(d) {
            return top(parseFloat(d.baseline)) + "%";
        });

    bars.enter()
        .append("div")
        .attr("class", "bar")
        .attr("data-location", function(d) {
            return d.location;
        })

        .call(function(context) {
            this.append("div")
                .attr("class", "rate")
                .style("height", function(d) {
                    return height(parseFloat(d.value)) + "%";
                })
                .call(function(context) {
                    this.append("div")
                        .attr("class", "value")
                        .attr("data-value", function(d) {
                            return d.value.substring(0, d.value.indexOf("%"));
                        });

                })

            this.append("div")
                .attr("class", "baseline")
                .attr("data-value", function(d) {
                    return d.baseline.substring(0, d.baseline.indexOf("%"));
                })
                .style("top", function(d) {
                    return top(parseFloat(d.baseline)) + "%";
                })
        })
}