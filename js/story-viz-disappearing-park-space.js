function tourismImpact(){



   var activeYear = "2009";
   
   var toursimDollars = [];
   
   
   
   
   



// base is set in footer.php.

var dataSourceDir = base + '/wp-content/themes/om-musa/';

d3.tsv(dataSourceDir + "js/data-svg/data-tourism-impact.tsv", function(error,data) {

       

    console.log(data)


    
    
    $('#data-svg-1574 .values .tourism-dollars').attr('data-value',function(){
            return data[1][activeYear];
    })

    $('#data-svg-1574 .values .visitors').attr('data-value',function(){
            var visitors =  data[0][activeYear]
            totalVisitors = visitors.replace(/,/g, '')
            

            return Number(totalVisitors/1000000).toFixed(1)

            
    })

                function renderDudes(){
                    var dudes = (d3.select('#data-svg-1574 .visitors').attr('data-value'));
                   // console.log(cityDudes)

                    for (var i = dudes- 1; i >= 0; i--) {
                    d3.select('#dudes').append('span').attr('class', 'dude-icon active');
                    };

                    d3.select('#dudes').append('span').attr('id', 'dude-overlay')
                                       .call(function(){
                                        this.append('span').attr('class', 'dude-icon active overlap')
                                        this.append('span').attr('class', 'dude-icon overlap')
                                       })

                      
                     for (var i = (20-dudes)- 1; i >= 0; i--) {
                    d3.select('#dudes').append('span').attr('class', 'dude-icon');
                    }; 


                     
                };

                renderDudes();


                    d3.select('#tourism-slider').call(d3.slider().axis(true).min(2009).max(2013).step(1).on("slide", function(evt, value) {

                      activeYear = value;

                      // For the shifting dude. 
                      d3.select('span.dude-icon.overlap').style('width',function(){

                        switch(activeYear) {

                        case "2009":
                        break;
                        case "2010":
                        return "14px"
                        break;
                        case "2011":
                        return "14px"
                        break;
                        case "2012":
                        return "8px"
                        break;
                        case "2013":
                        return "8px"
                        break;
                      }

                      });
                      


                      // For drawing revenue data. 

                      $('#data-svg-1574 .values .tourism-dollars').attr('data-value',function(){
                                return data[1][activeYear];
                        })



                      // For drawing visitor data. 

                      $('#data-svg-1574 .values .visitors').attr('data-value',function(){
                                var visitors =  data[0][activeYear]
                                totalVisitors = visitors.replace(/,/g, '')
                                

                                return Number(totalVisitors/1000000).toFixed(1)
                        })    

                     d3.selectAll('.dude-icon').remove();
                     d3.selectAll('#dude-overlay').remove();     

                     renderDudes(); 

                     $('#dude-overlay').css('transform', 'translate(-1px)')   
                    
                          


                        }));


            });


};
 $(document).ready(function(){
    tourismImpact();
    

});
