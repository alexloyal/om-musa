// Waypoints
//post-1797 is MIAMI-DADE OFFERS CLEAN AIR & WATER TO ITS RESIDENTS

$('#post-1797 .data-disclosure [data-dimension="B"]').css('opacity', 0);
$('#post-1797 .data-disclosure [data-dimension="B"]').addClass('hide');


$('[data-trigger="10"]').waypoint(function(down) {



$('#post-1797 .swap.objective.one').css('opacity','1');
$('#post-1797 .data-disclosure').css('opacity','1');
$('#post-1797 .year-labels').css('opacity','1');



var dataSourceDir = base + '/wp-content/themes/om-musa/';


d3.json(dataSourceDir+"js/data-svg/iac-object-zero.json", function(error,zeroData) {

            if (error) {
                // If error is not null, something went wrong.
                console.log(error);
                //Log the error.
            } else {


            bars = new BarGraphDollars(zeroData,'.iac-ev-clean-air-water .graph');
            $('#ia-data-1797 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2011";
                                      } else {
                                        return "2013";
                                      }
                                    })

            $(document).ready(function(){

                d3.json(dataSourceDir+"js/data-svg/iac-ev-air-quality.json", function(error,data) {

                                    bars.update(data);

                              })

                    $('#post-1797 .data-svg .swap.objective.one .item span').on('click', function(){
                        var parentSwap = $(this).parent().data('swap');
                        var activeSwap = $('#post-1797 .swap .item[data-swap="1"]');
                        var dataDimension = $(this).parent().data('dimension');
                        console.log(dataDimension)
                        if (parentSwap = "0") {
                            activeSwap.attr('data-swap', '0')
                            $(this).parent().attr('data-swap', '1')
                        };

                        $('#post-1797 .data-disclosure [data-dimension]:not(.hide)').css('opacity', 0);
                        $('#post-1797 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                        $('#post-1797 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#post-1797 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

                        if (dataDimension == "A") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ev-air-quality.json", function(error,data) {

                                 bars.update(data,'.iac-ev-clean-air-water .graph');
                                 /*setTimeout(function(){
                                        bars.update(data,'.iac-ev-clean-air-water .graph');
                                    }, 1000); */

                              })
                        };

                        if (dataDimension == "B") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ev-water-quality.json", function(error,data) {

                                  bars.update(data,'.iac-ev-clean-air-water .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.iac-ev-clean-air-water .graph');
                                    }, 1000); */

                              })
                        };

                    });

            });






        } // else
 }); // d3 json civic participation

},{triggerOnce:true});//Waypoints
