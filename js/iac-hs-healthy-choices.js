var radialgraphs = {};

// post-1695 is MIAMI-DADE RESIDENTS MAKE HEALTHY CHOICES

$('#post-1695 .data-disclosure [data-dimension="B"],#post-1695 .data-disclosure [data-dimension="C"]').css('opacity', 0);
$('#post-1695 .data-disclosure [data-dimension="B"],#post-1695 .data-disclosure [data-dimension="C"]').addClass('hide');

$('[data-trigger="20"]').waypoint(function(down){

	
	$('#post-1695 .swap.objective.two').css('opacity','1');
	$('#post-1695 .data-disclosure').css('opacity','1');
	$('#post-1695 .year-labels').css('opacity','1');


	var dataSourceDir = base + '/wp-content/themes/om-musa/';



	d3.tsv(dataSourceDir+'js/data-svg/iac-hs-smokers.tsv', null, function(smokerdata) {

		for(var i = 0; i < smokerdata.length; i++) {

			var ry1 = "2010";
			var ry2 = "2012";
			var county = smokerdata[i]["County"]
			var cityName = $('#post-1695 .circles .radial[data-city="'+county+'"]')
			var dataYear = ry1;

			var label = county.toLowerCase().replace(/ /g, '-');

			$('#post-1695 .city-group .radial').attr('data-year', function(){
				var index = $(this).index();
				if (  index < 1 ) {
					return ry1;
				} else {
					return ry2;
				}
			})

			if(!radialgraphs[ry1]) { radialgraphs[ry1] = {} }
			if(!radialgraphs[ry2]) { radialgraphs[ry2] = {} }

			radialgraphs[ry1][county] = new RadialGraph([smokerdata[i][ry1]], '#post-1695 [data-year="'+ry1+'"][data-city="'+county+'"]', ''+label+'');
			radialgraphs[ry2][county] = new RadialGraph([smokerdata[i][ry2]], '#post-1695 [data-year="'+ry2+'"][data-city="'+county+'"]', ''+label+'');
		}

		$('#post-1695 .city-group').css('opacity', '1');


		$('#post-1695 .data-svg .swap.objective.two .item span').on('click', function(){


				var parentSwap = $(this).parent().data('swap');
                var activeSwap = $('#post-1695 .swap .item[data-swap="1"]');

				var dataDimension = $(this).parent().data('dimension');

				if (parentSwap = "0") {
	                activeSwap.attr('data-swap', '0')
	                $(this).parent().attr('data-swap', '1')
	            };
	            		$('#post-1695 .data-disclosure [data-dimension]:not(.hide)').css('opacity', 0);
	            		$('#post-1695 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                        $('#post-1695 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#post-1695 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

				switch(dataDimension) {

                	case "A":
						for(var i = 0; i < smokerdata.length; i++) {

							var ry1 = "2010";
							var ry2 = "2012";
							var county = smokerdata[i]["County"]
							var label = county.toLowerCase().replace(/ /g, '-');

							radialgraphs[ry1][county].update([smokerdata[i][ry1]]);
							radialgraphs[ry2][county].update([smokerdata[i][ry2]]);
						}
                		break;

					case "B":
                		d3.tsv(dataSourceDir+'js/data-svg/iac-hs-obese.tsv', function(obesitydata) {
                			for (var i = obesitydata.length - 1; i >= 0; i--) {

                				var ry1 = "2010";
								var ry2 = "2012";
                				var county = obesitydata[i]["County"];
                				
								radialgraphs[ry1][county].update([obesitydata[i][ry1]]);
								radialgraphs[ry2][county].update([obesitydata[i][ry2]]);
                			};

                		})
                		break;
                	
                }

		 });

	

	}, function(err, rows) {
	  console.log(err)
	});

},{triggerOnce:true}); // Waypoints
