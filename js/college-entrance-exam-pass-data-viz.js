 
var dataSourceDir = base + '/wp-content/themes/om-musa/';




$('[data-trigger="31"]').waypoint(function(down){
  collegeReadinessDashboard('FL',1200);

  collegeReadinessDashboard('MIAMI',1200);

},{triggerOnce:true}); // Waypoints



function collegeReadinessDashboard(region,duration){

      
        //first, load the general unemployment rate
        d3.json(dataSourceDir+"js/data-svg/college-readiness.json", function(error,data) {

             if (error) {
    // If error is not null, something went wrong.

        console.log(error);
    //Log the error.

        } else { 
          

                            var collegemiamiDadeCounty = data.MIAMI;
                            var collegefloridaState = data.FL;

          
                          // console.log(collegefloridaState)
 

                            var width = 260,
                                height = 160,
                                τ = 2 * Math.PI; // http://tauday.com/tau-manifesto
                                format = d3.format(".0%");

                            // An arc function with all values bound except the endAngle. So, to compute an
                            // SVG path string for a given angle, we pass an object with an endAngle
                            // property to the `arc` function, and it will return the corresponding string.
                            var arc = d3.svg.arc();

                             arc.innerRadius(70)
                                .outerRadius(80)
                                .startAngle(0);

                            // Create the SVG container, and apply a transform such that the origin is the
                            // center of the canvas. This way, we don't need to position arcs individually.
                            var svg = d3.select('#college-entrance-exam .svg[data-location="'+region+'"]').append("svg")
                                .attr("width", function(){
                                    return width + 'px'
                                })
                                .attr("height", function(){
                                    return height + 'px'
                                })
                              .append("g")
                                .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")

                            // Add the background arc, from 0 to 100% (τ).
                            var background = svg.append("path")
                                .attr('class', 'transparent arc') 
                                .datum({endAngle: τ})
                                .attr("d", arc);

                            // Add the foreground arc in orange, currently showing 12.7%.
                            var foreground = svg.append("path")
                                .datum({endAngle: .127 * τ})
                                .attr('class', 'blue arc')                                
                                .attr("d", arc);

                            var whiteCircle = svg.append("circle")
                                .attr('r', 70 )
                                .attr('class', 'white circle');

                            var label = svg.append("text")
                                .attr('class', 'region-name')
                                .attr('dx', 0)
                                .attr('dy', 10)
                                .attr('text-anchor', 'middle')
                                .text(function(){
                                  return region;
                                }); 

                             
                              

                              if (region == 'MIAMI') {
                                  var    y1 = [parseFloat(collegemiamiDadeCounty["2010"]["State Universities"])];
                                  var    y2 = [parseFloat(collegemiamiDadeCounty["2012"]["State Universities"])];

                                  
                              } else {

                                  var    y1 = [parseFloat(collegefloridaState["2010"]["State Universities"])];
                                  var    y2 = [parseFloat(collegefloridaState["2012"]["State Universities"])];


                                  
                              };

                              



                              var percentage = d3.select('#college-entrance-exam .svg[data-location="'+region+'"] svg g')

                                  percentageData = percentage.selectAll('text.blue')
                                            .data(y2);
                                            console.log(y2)
                                 
                                  function animatePercentage(){

                                    percentageData.enter()
                                      .append("text")
                                      .attr('class', 'number')
                                      .attr('dx', -130)
                                      .attr('dy', -60)
                                      .text(0)
                                      .attr("class", "blue txt percent")
                                      
                                      .transition()
                                      .duration(duration)
                                      .tween("text", function(d) {
                                              var i = d3.interpolate(this.textContent, d),
                                                  prec = (d + "").split("."),
                                                  round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;
                                            
                                              return function(t) {
                                                  this.textContent =(Math.round(i(t) * round) / round)+'%';

                                                   
                                              };
                                          })

                                  }

                                   animatePercentage();

                           
 
                            if (region == 'MIAMI') {
                                              collegeRate = collegemiamiDadeCounty["2010"]["State Universities"]  
                                          } else {

                                            collegeRate = collegefloridaState["2010"]["State Universities"]

                            }

                         

                            // Creates a tween on the specified transition's "d" attribute, transitioning
                            // any selected arcs from their current angle to the specified new angle.
                            function arcTween(transition, newAngle) {

                              // The function passed to attrTween is invoked for each selected element when
                              // the transition starts, and for each element returns the interpolator to use
                              // over the course of transition. This function is thus responsible for
                              // determining the starting angle of the transition (which is pulled from the
                              // element's bound datum, d.endAngle), and the ending angle (simply the
                              // newAngle argument to the enclosing function).
                              transition.attrTween("d", function(d) {

                                // To interpolate between the two angles, we use the default d3.interpolate.
                                // (Internally, this maps to d3.interpolateNumber, since both of the
                                // arguments to d3.interpolate are numbers.) The returned function takes a
                                // single argument t and returns a number between the starting angle and the
                                // ending angle. When t = 0, it returns d.endAngle; when t = 1, it returns
                                // newAngle; and for 0 < t < 1 it returns an angle in-between.
                                var interpolate = d3.interpolate(d.endAngle, newAngle);

                                // The return value of the attrTween is also a function: the function that
                                // we want to run for each tick of the transition. Because we used
                                // attrTween("d"), the return value of this last function will be set to the
                                // "d" attribute at every tick. (It's also possible to use transition.tween
                                // to run arbitrary code for every tick, say if you want to set multiple
                                // attributes from a single function.) The argument t ranges from 0, at the
                                // start of the transition, to 1, at the end.
                                return function(t) {

                                  // Calculate the current arc angle based on the transition time, t. Since
                                  // the t for the transition and the t for the interpolate both range from
                                  // 0 to 1, we can pass t directly to the interpolator.
                                  //
                                  // Note that the interpolated angle is written into the element's bound
                                  // data object! This is important: it means that if the transition were
                                  // interrupted, the data bound to the element would still be consistent
                                  // with its appearance. Whenever we start a new arc transition, the
                                  // correct starting angle can be inferred from the data.
                                  d.endAngle = interpolate(t);

                                  // Lastly, compute the arc path given the updated data! In effect, this
                                  // transition uses data-space interpolation: the data is interpolated
                                  // (that is, the end angle) rather than the path string itself.
                                  // Interpolating the angles in polar coordinates, rather than the raw path
                                  // string, produces valid intermediate arcs during the transition.
                                  return arc(d);
                                };
                              });
                            }


                             function innerArcInit(){
                                        var width = 260,
                                             height = 160,
                                             τ = 2 * Math.PI; // http://tauday.com/tau-manifesto
                              var innerArc = d3.svg.arc();

                             innerArc.innerRadius(48)
                                .outerRadius(58)
                                .startAngle(0);

                            // Create the SVG container, and apply a transform such that the origin is the
                            // center of the canvas. This way, we don't need to position arcs individually.
                            var innerSvg = d3.select('#college-entrance-exam .svg[data-location="'+region+'"]').append("svg")
                            .attr('class', 'inner-arc')
                                .attr("width", function(){
                                    return width + 'px'
                                })
                                .attr("height", function(){
                                    return height + 'px'
                                })
                              .append("g")
                                .attr("transform", "translate(" + 115 + "," + 113 + ")")

                            // Add the background arc, from 0 to 100% (τ).
                            var background = innerSvg.append("path")
                                .attr('class', 'transparent arc') 
                                .datum({endAngle: τ})
                                .attr("d", innerArc);

                            // Add the foreground arc in orange, currently showing 12.7%.
                            var foreground = innerSvg.append("path")
                                .datum({endAngle: .127 * τ})
                                .attr('class', 'purp arc')                                
                                .attr("d", innerArc);

                            var whiteCircle = innerSvg.append("circle")
                                .attr('r', 40 )
                                .attr('class', 'white circle');

                            


                            var label = innerSvg.append("text")
                                .attr('class', 'region-name')
                                .attr('dx', 0)
                                .attr('dy', 10)
                                .attr('text-anchor', 'middle')
                                 

                                .text(function(){
                                  return region;
                                }); 

                                if (region == 'MIAMI') {
                                  var    y1 = [parseFloat(collegemiamiDadeCounty["2010"]["Community Colleges"])];
                                  var    y2 = [parseFloat(collegemiamiDadeCounty["2012"]["Community Colleges"])];

                                  
                              } else {

                                  var    y1 = [parseFloat(collegefloridaState["2010"]["Community Colleges"])];
                                  var    y2 = [parseFloat(collegefloridaState["2012"]["Community Colleges"])];


                                  
                              };

                              



                              var percentage = d3.select('#college-entrance-exam .svg[data-location="'+region+'"] svg g')

                                  percentageData = percentage.selectAll('text.number')
                                            .data(y1);

                                  animatePercentage();

                    function animatePercentage(){

                                    percentageData.enter()
                                      .append("text")
                                      .attr('class', 'number')
                                      .attr('dx', 50)
                                      .attr('dy', 80)
                                      .text(0)
                                      .attr("class", "purp txt percent")
                                      
                                      .transition()
                                      .duration(duration)
                                      .tween("text", function(d) {
                                              var i = d3.interpolate(this.textContent, d),
                                                  prec = (d + "").split("."),
                                                  round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;
                                            
                                              return function(t) {
                                                  this.textContent =(Math.round(i(t) * round) / round)+'%';

                                                   
                                              };
                                          })

                                  }




                              if (region == 'MIAMI') {
                                            innerRate = collegemiamiDadeCounty["2010"]["Community Colleges"]  
                                          } else {

                                            innerRate = collegefloridaState["2010"]["Community Colleges"]

                            }

                          // Draw arc and percentage on document ready.
                            $(document).ready(function() {
                                
                              foreground.transition()
                                  .duration(duration)
                                  
                                  .call(arcTween, parseFloat(innerRate)/100 * τ);


                            });



                             $('#college-entrance-exam.pie .toggle .value').on('click',function() {
                                  if (this.dataset.value == "2010") {
                                      
                                      $('#college-entrance-exam .toggle').attr('data-value', this.dataset.value);  

                                      // console.log(this.dataset.value)
                                  
                                  
                                  

                                  // This draws the arc
                                  foreground.transition()
                                  .duration(duration)
                                  .call(arcTween, parseFloat(innerRate)/100 * τ);
                                  
                                  // This animates percentage

                                   percentage.selectAll('text.purp').remove();

                                    percentageData = percentage.selectAll('text.number')

                                                .data(y1)
                                                
                                                animatePercentage();
                                      

                                  } else if (this.dataset.value == "2012"){
                                    
                                               $('#college-entrance-exam .toggle').attr('data-value', this.dataset.value);  
                                  
                                            if (region == 'MIAMI') {
                                              innerRate = collegemiamiDadeCounty["2012"]["Community Colleges"]   
                                         

                                          } else {

                                            innerRate = collegefloridaState["2012"]["Community Colleges"] 
                                            
                                          }

                                  foreground.transition()
                                  .duration(duration)
                                  .call(arcTween, parseFloat(innerRate)/100 * τ);
                                  

                                 

                                percentage.selectAll('text.purp').remove();

                                  percentageData = percentage.selectAll('text.number')

                                                  .data(y2);

                                  animatePercentage();

                                } 
                                  
                            });

                            // Creates a tween on the specified transition's "d" attribute, transitioning
                            // any selected arcs from their current angle to the specified new angle.
                            function arcTween(transition, newAngle) {

                              // The function passed to attrTween is invoked for each selected element when
                              // the transition starts, and for each element returns the interpolator to use
                              // over the course of transition. This function is thus responsible for
                              // determining the starting angle of the transition (which is pulled from the
                              // element's bound datum, d.endAngle), and the ending angle (simply the
                              // newAngle argument to the enclosing function).
                              transition.attrTween("d", function(d) {

                                // To interpolate between the two angles, we use the default d3.interpolate.
                                // (Internally, this maps to d3.interpolateNumber, since both of the
                                // arguments to d3.interpolate are numbers.) The returned function takes a
                                // single argument t and returns a number between the starting angle and the
                                // ending angle. When t = 0, it returns d.endAngle; when t = 1, it returns
                                // newAngle; and for 0 < t < 1 it returns an angle in-between.
                                var interpolate = d3.interpolate(d.endAngle, newAngle);

                                // The return value of the attrTween is also a function: the function that
                                // we want to run for each tick of the transition. Because we used
                                // attrTween("d"), the return value of this last function will be set to the
                                // "d" attribute at every tick. (It's also possible to use transition.tween
                                // to run arbitrary code for every tick, say if you want to set multiple
                                // attributes from a single function.) The argument t ranges from 0, at the
                                // start of the transition, to 1, at the end.
                                return function(t) {

                                  // Calculate the current arc angle based on the transition time, t. Since
                                  // the t for the transition and the t for the interpolate both range from
                                  // 0 to 1, we can pass t directly to the interpolator.
                                  //
                                  // Note that the interpolated angle is written into the element's bound
                                  // data object! This is important: it means that if the transition were
                                  // interrupted, the data bound to the element would still be consistent
                                  // with its appearance. Whenever we start a new arc transition, the
                                  // correct starting angle can be inferred from the data.
                                  d.endAngle = interpolate(t);

                                  // Lastly, compute the arc path given the updated data! In effect, this
                                  // transition uses data-space interpolation: the data is interpolated
                                  // (that is, the end angle) rather than the path string itself.
                                  // Interpolating the angles in polar coordinates, rather than the raw path
                                  // string, produces valid intermediate arcs during the transition.
                                  return innerArc(d);
                                };
                              });
                            };



                      };

                      innerArcInit();

                       // Draw arc and percentage on document ready.
                            $(document).ready(function() {
                              foreground.transition()
                                  .duration(duration)
                                  
                                  .call(arcTween, parseFloat(collegeRate)/100 * τ);


                            });

                         // Click function for outter arc   

                             $('#college-entrance-exam.pie .toggle .value').on('click',function() {
                                  if (this.dataset.value == "2010") {
                                      
                                          

                                           $('#college-entrance-exam .toggle').attr('data-value', this.dataset.value);  

                                                
                                            
                                            
                                            if (region == 'MIAMI') {
                                                collegeRate = collegemiamiDadeCounty["2010"]["State Universities"]  
                                                innerRate = collegemiamiDadeCounty["2010"]["Community Colleges"]  
                                            } else {

                                              collegeRate = collegefloridaState["2010"]["State Universities"]
                                              innerRate = collegefloridaState["2010"]["Community Colleges"]  
                                              
                                            }

                                  // This draws the arc
                                  foreground.transition()
                                  .duration(duration)
                                  .call(arcTween, parseFloat(collegeRate)/100 * τ);
                                  
                                  // This animates percentage

                                   percentage.selectAll('text.blue').remove();
                                   percentageData = percentage.selectAll('text.number')
                                                      .data(y1)
                                                 
                                                    animatePercentage();
                                                     // console.log(y1)

                     } else if (this.dataset.value == "2012"){
                                    
                                  $('#college-entrance-exam .toggle').attr('data-value', this.dataset.value);  
                                  
                                                if (region == 'MIAMI') {
                                                collegeRate = collegemiamiDadeCounty["2012"]["State Universities"]  
                                                innerRate = collegemiamiDadeCounty["2012"]["Community Colleges"]  
                                            } else {

                                              collegeRate = collegefloridaState["2012"]["State Universities"]
                                              innerRate = collegefloridaState["2012"]["Community Colleges"]  
                                              
                                            }

                                  foreground.transition()
                                  .duration(duration)
                                  .call(arcTween, parseFloat(collegeRate)/100 * τ);
                                  percentage.selectAll('text.blue').remove();
                                  percentageData = percentage.selectAll('text.number')
                                                  .data(y2);

                                  animatePercentage();

                                } 
                                  
                            });

            


          } // else
        });

};
