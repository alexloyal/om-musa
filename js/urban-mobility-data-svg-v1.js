

function transportationData(){
    
    var commuteLegend = 'OF DRIVERS SPEND 30 MINUTES OR MORE ON COMMUTE';
    
    


    // Removes active class from current city and arc. 

    
    var dataset; // GLoabl variable
    var dataSource = base + '/wp-content/themes/om-musa/js/data-svg/auto-commute-time.csv';

    

    d3.csv(dataSource, function(error,data) {
        
                    if (error) {
                // If error is not null, something went wrong.

                    console.log(error);
                //Log the error.

                    } else {
                // If no error, the file loaded correctly.

                // These two below set the current city once the infographic has been initiated. 
                        
                        
                    
                  


                    w="100%";
                    h="100%";
                    x="520";
                    y="300";
                    dataset = data;
                    
                    var svg = d3.select(".post-1502 .data-svg")
                            .append("svg")
                            .attr('class', 'col-md-12 svg-content')
                            .attr('id','data-circle-box' )
                            .attr('height', h)
                            .attr('width', w)
                            .attr('viewBox', '0 0 900 600')
                            .attr('preserveAspectRatio', 'xMinYMin meet');
                    

                      


                             // Grey Circle

                    d3.select('#post-1502 .svg-content')                    
                            .append('div')
                            .attr('id', 'commuteContainer');

                    
                                      
                    d3.select('#post-1502 .svg-content')
                            .append('circle')
                            .attr('cx', '432' )
                            .attr('cy', '392' )
                            .attr('class','grey-circle' )
                            .attr('r', '156')
                    



                  /*  d3.select('#Layer_1')
                              .append('g')
                              .append('svg')
                              .attr('class', 'legend-container' )

                              .append('text')
                              .attr('x','420' )
                              .attr('y', '300')
                              .attr('class', 'legend')
                              .text('of drivers spend 30 minutes or more on commute'); */
                                     
                  
                            
                    d3.select("#cityNames")
                            .selectAll('div')
                            .data(dataset)
                            .enter()
                            .append("div")
                            .attr('id', function(d){
                                return (d.City);
                            })
                            .text(function(d){
                                return (d.City);
                            }) 
                            .attr('data-circle', function(d){
                                 
                                  return (d.y2012*100);
                                
                            })
                            .style('cursor', 'pointer')
                            .attr('class','city-name' )
                            
                              
                            
                            .on("click",function(d){
                              
                              d3.selectAll('.data-circle.active,.city-name.active').classed('active', false);
                              d3.selectAll('#'+d.City+'-arc,#'+d.City+'').classed('active', true);
                              drawCircleChart(
                              '#'+d.City+'-arc',
                              $('#'+d.City).data('circle'), // Pass variable variable here equal to percent.
                              382,
                              660,
                              ".35em"
                            );
                            
                            


                              

                            });         


                            


                            // Draws arcs

                    svg.selectAll("g")
                            .data(dataset)
                            .enter()
                            .append("g") 
                            // This is the position of the circle.
                            .attr('transform', 'translate(280,-20)')
                            .attr('id', function(d){
                                return (d.City+'-arc');
                            })
                            .attr('data-circle', function(d){
                                return (d.y2012*100);
                            })
                            .attr('class', 'data-circle');

                   
                    d3.selectAll('#MIA-arc,#MIA').classed('active',true);
                    
                    
                                     




                            var duration   = 600,
                            transition = 300;
                            

                            function calcPercent(percent) {
                              return [percent, 100-percent];
                            };
                            

                            function drawCircleChart(element, percent, width, height, text_y) {
                              width = typeof width !== 'undefined' ? width : 290;
                              height = typeof height !== 'undefined' ? height : 290;
                              text_y = typeof text_y !== 'undefined' ? text_y : "-.10em";

                              var dataset = {
                                    lower: calcPercent(0),
                                    upper: calcPercent(percent)
                                  },
                                  radius = Math.min(width, height) / 2.2,
                                  pie = d3.layout.pie().sort(null),
                                  format = d3.format(".0%");

                              var arc = d3.svg.arc()
                                    .innerRadius(radius - 22)
                                    .outerRadius(radius);

                              var svg = d3.select(element).append("svg")
                                    .attr("width", width)
                                    .attr("height", height)
                                    .append("g")
                                    .attr('class', 'find-me')
                                    .attr('class', 'blue-arc')
                                    .attr("transform", "translate(146,412)");

                              var path = svg.selectAll("path")
                                    .data(pie(dataset.lower))
                                    .enter().append("path")
                                    .attr("class", function(d, i) { return "color" + i })
                                    .attr("d", arc)
                                    .each(function(d) { this._current = d; }); // store the initial values

                                   

                              var text = svg.append("text")
                                    .attr("text-anchor", "middle")
                                    .attr("dy", text_y)
                                    .attr('class', 'circle-percent');

                              if (typeof(percent) === "string") {
                                text.text(percent);
                              }
                              else {
                                var progress = 0;
                                var timeout = setTimeout(function () {
                                  clearTimeout(timeout);
                                  path = path.data(pie(dataset.upper)); // update the data
                                  path.transition().duration(duration).attrTween("d", function (a) {
                                    // Store the displayed angles in _current.
                                    // Then, interpolate from _current to the new angles.
                                    // During the transition, _current is updated in-place by d3.interpolate.
                                    var i  = d3.interpolate(this._current, a);
                                    var i2 = d3.interpolate(progress, percent)
                                    this._current = i(0);
                                    return function(t) {
                                      text.text( format(i2(t) / 100) );
                                      return arc(i(t));
                                    };
                                  }); // redraw the arcs
                                }, 200);
                              }
                            };

                             


                           
                            
                            drawCircleChart(
                              '#MIA-arc',
                              $('#MIA-arc').data('circle'), // Pass variable variable here equal to percent.
                              382,
                              660,
                              '.35em'
                               );
                            
                            

                            
                    

                    };
    });


}

transportationData();

d3.select('#y2010').on('click', fy2010);

function fy2010(){
  $('#y2012').removeClass('active');
  $('#y2010').addClass('active');

    var dataset; // GLoabl variable
    var dataSource = base + '/wp-content/themes/om-musa/js/data-svg/auto-commute-time.csv';

    
    d3.select('#trigger .dot').attr('class','dot shift' );
    
    d3.csv(dataSource, function(error,data) {
        
                    if (error) {
                // If error is not null, something went wrong.

                    console.log(error);
                //Log the error.

                    } else {
                // If no error, the file loaded correctly.
                    
                        
                        // These two below set the current city once the infographic has been initiated. 

                        var cityIndex = d3.select('.city-name.active');
                        var cityID = cityIndex[0][0].id;

                     d3.select("#post-1502 .data-svg")
                     .select(".svg-content")
                     .remove(); 

                    w="100%";
                    h="100%";
                    x="520";
                    y="300";
                    dataset = data;
                    
                    var svg = d3.select("#post-1502 .data-svg")
                            .append("svg")
                            .attr('class', 'col-md-12 svg-content')
                            .attr('id','data-circle-box' )
                            .attr('height', h)
                            .attr('width', w)
                            .attr('viewBox', '0 0 900 600')
                            .attr('preserveAspectRatio', 'xMinYMin meet');
                            
                             // Grey Circle

                    d3.select('#post-1502 .svg-content')                    
                            .append('div')
                            .attr('id', 'commuteContainer');
                                      
                   d3.select('#post-1502 .svg-content')
                            .append('circle')
                            .attr('cx', '432' )
                            .attr('cy', '392' )
                            .attr('class','grey-circle' )
                            .attr('r', '155')
                    

                      

                        d3.select("#cityNames")
                            .selectAll('div')
                            .remove();
                            
                        svg.selectAll("g")
                            .remove();
                  
                            
                    d3.select("#cityNames")
                            .selectAll('div')
                            .data(dataset)
                            .enter()
                            .append("div")
                            .attr('id', function(d){
                                return (d.City);
                            })
                            .text(function(d){
                                return (d.City);
                            }) 
                            .attr('data-circle', function(d){
                                 
                                  return (d.y2010*100);
                                
                            })
                            
                            .attr('class','city-name' )
                            
                              
                            
                            .on("click",function(d){
                              
                              d3.selectAll('.data-circle.active,.city-name.active').classed('active', false);
                              d3.selectAll('#'+d.City+'-arc,#'+d.City+'').classed('active', true);
                              drawCircleChart(
                              '#'+d.City+'-arc',
                              $('#'+d.City).data('circle'), // Pass variable variable here equal to percent.
                              382,
                              660,
                              ".35em"
                            );
                            
                            


                              

                            });         


                            


                            // Draws arcs

                    svg.selectAll("g")
                            .data(dataset)
                            .enter()
                            .append("g") 
                            // This is the position of the circle.
                            .attr('transform', 'translate(280,-20)')
                            .attr('id', function(d){
                                return (d.City+'-arc');
                            })
                            .attr('data-circle', function(d){
                                return (d.y2010*100);
                            })
                            .attr('class', 'data-circle');

                   


                    var cityIDHash = '#'+cityID;
                    var cityIDHArc = '#'+cityID+'-arc';
                    

                    d3.selectAll(cityIDHash).classed('active',true);
                    d3.selectAll(cityIDHArc).classed('active',true);
                    
                    
                                     




                            var duration   = 600,
                            transition = 300;
                            

                            function calcPercent(percent) {
                              return [percent, 100-percent];
                            };
                            

                            function drawCircleChart(element, percent, width, height, text_y) {
                              width = typeof width !== 'undefined' ? width : 290;
                              height = typeof height !== 'undefined' ? height : 290;
                              text_y = typeof text_y !== 'undefined' ? text_y : "-.10em";

                              var dataset = {
                                    lower: calcPercent(0),
                                    upper: calcPercent(percent)
                                  },
                                  radius = Math.min(width, height) / 2.2,
                                  pie = d3.layout.pie().sort(null),
                                  format = d3.format(".0%");

                              var arc = d3.svg.arc()
                                    .innerRadius(radius - 22)
                                    .outerRadius(radius);

                              var svg = d3.select(element).append("svg")
                                    .attr("width", width)
                                    .attr("height", height)
                                    .append("g")
                                    .attr('class', 'blue-arc')
                                    .attr("transform", "translate(146,412)");

                              var path = svg.selectAll("path")
                                    .data(pie(dataset.lower))
                                    .enter().append("path")
                                    .attr("class", function(d, i) { return "color" + i })
                                    .attr("d", arc)
                                    .each(function(d) { this._current = d; }); // store the initial values

                                   

                              var text = svg.append("text")
                                    .attr("text-anchor", "middle")
                                    .attr("dy", text_y)
                                    .attr('class', 'circle-percent');

                              if (typeof(percent) === "string") {
                                text.text(percent);
                              }
                              else {
                                var progress = 0;
                                var timeout = setTimeout(function () {
                                  clearTimeout(timeout);
                                  path = path.data(pie(dataset.upper)); // update the data
                                  path.transition().duration(duration).attrTween("d", function (a) {
                                    // Store the displayed angles in _current.
                                    // Then, interpolate from _current to the new angles.
                                    // During the transition, _current is updated in-place by d3.interpolate.
                                    var i  = d3.interpolate(this._current, a);
                                    var i2 = d3.interpolate(progress, percent)
                                    this._current = i(0);
                                    return function(t) {
                                      text.text( format(i2(t) / 100) );
                                      return arc(i(t));
                                    };
                                  }); // redraw the arcs
                                }, 200);
                              }
                            };

                             


                           
                            
                            drawCircleChart(
                              cityIDHArc,
                              $(cityIDHash).data('circle'), // Pass variable variable here equal to percent.
                              382,
                              660,
                              '.35em'
                               );
                            
                            

                            
                    

                    };
    });

};

d3.select('#y2012').on('click', fy2012);

function fy2012(){
  $('#y2010').removeClass('active');
  $('#y2012').addClass('active');
  var dataset; // GLoabl variable
    var dataSource = base + '/wp-content/themes/om-musa/js/data-svg/auto-commute-time.csv';

    

    d3.csv(dataSource, function(error,data) {
        
                    if (error) {
                // If error is not null, something went wrong.

                    console.log(error);
                //Log the error.

                    } else {
                // If no error, the file loaded correctly.
                    d3.select('#trigger .dot').attr('class', 'dot')
                        
                        // These two below set the current city once the infographic has been initiated. 

                        var cityIndex = d3.select('.city-name.active');
                        var cityID = cityIndex[0][0].id;

                     d3.select("#post-1502 .data-svg")
                     .select(".svg-content")
                     .remove(); 

                    w="100%";
                    h="100%";
                    x="520";
                    y="300";
                    dataset = data;
                    
                    var svg = d3.select("#post-1502 .data-svg")
                            .append("svg")
                            .attr('class', 'col-md-12 svg-content')
                            .attr('id','data-circle-box' )
                            .attr('height', h)
                            .attr('width', w)
                            .attr('viewBox', '0 0 900 600')
                            .attr('preserveAspectRatio', 'xMinYMin meet');
                             
                             // Grey Circle

                   d3.select('#post-1502 .svg-content')
                            .append('div')
                            .attr('id', 'commuteContainer');
                   d3.select('#post-1502 .svg-content')
                            .append('circle')
                            .attr('cx', '432' )
                            .attr('cy', '392' )
                            .attr('class','grey-circle' )
                            .attr('r', '155')
                    



                      

                        d3.select("#cityNames")
                            .selectAll('div')
                            .remove();
                            
                        svg.selectAll("g")
                            .remove();
                  
                            
                    d3.select("#cityNames")
                            .selectAll('div')
                            .data(dataset)
                            .enter()
                            .append("div")
                            .attr('id', function(d){
                                return (d.City);
                            })
                            .text(function(d){
                                return (d.City);
                            }) 
                            .attr('data-circle', function(d){
                                 
                                  return (d.y2012*100);
                                
                            })
                            
                            .attr('class','city-name' )
                            
                              
                            
                            .on("click",function(d){
                              
                              d3.selectAll('.data-circle.active,.city-name.active').classed('active', false);
                              d3.selectAll('#'+d.City+'-arc,#'+d.City+'').classed('active', true);
                              drawCircleChart(
                              '#'+d.City+'-arc',
                              $('#'+d.City).data('circle'), // Pass variable variable here equal to percent.
                              382,
                              660,
                              ".35em"
                            );
                            
                            


                              

                            });         


                            


                            // Draws arcs

                    svg.selectAll("g")
                            .data(dataset)
                            .enter()
                            .append("g") 
                            // This is the position of the circle.
                            .attr('transform', 'translate(280,-20)')
                            .attr('id', function(d){
                                return (d.City+'-arc');
                            })
                            .attr('data-circle', function(d){
                                return (d.y2012*100);
                            })
                            .attr('class', 'data-circle');

                   
                    

                    var cityIDHash = '#'+cityID;
                    var cityIDHArc = '#'+cityID+'-arc';
                    

                    d3.selectAll(cityIDHash).classed('active',true);
                    d3.selectAll(cityIDHArc).classed('active',true);
                    
                    
                                     




                            var duration   = 600,
                            transition = 300;
                            

                            function calcPercent(percent) {
                              return [percent, 100-percent];
                            };
                            

                            function drawCircleChart(element, percent, width, height, text_y) {
                              width = typeof width !== 'undefined' ? width : 290;
                              height = typeof height !== 'undefined' ? height : 290;
                              text_y = typeof text_y !== 'undefined' ? text_y : "-.10em";

                              var dataset = {
                                    lower: calcPercent(0),
                                    upper: calcPercent(percent)
                                  },
                                  radius = Math.min(width, height) / 2.2,
                                  pie = d3.layout.pie().sort(null),
                                  format = d3.format(".0%");

                              var arc = d3.svg.arc()
                                    .innerRadius(radius - 22)
                                    .outerRadius(radius);

                              var svg = d3.select(element).append("svg")
                                    .attr("width", width)
                                    .attr("height", height)
                                    .append("g")
                                    .attr('class', 'blue-arc')
                                    .attr("transform", "translate(146,412)");

                              var path = svg.selectAll("path")
                                    .data(pie(dataset.lower))
                                    .enter().append("path")
                                    .attr("class", function(d, i) { return "color" + i })
                                    .attr("d", arc)
                                    .each(function(d) { this._current = d; }); // store the initial values

                                   

                              var text = svg.append("text")
                                    .attr("text-anchor", "middle")
                                    .attr("dy", text_y)
                                    .attr('class', 'circle-percent')


                              if (typeof(percent) === "string") {
                                text.text(percent);
                              }
                              else {
                                var progress = 0;
                                var timeout = setTimeout(function () {
                                  clearTimeout(timeout);
                                  path = path.data(pie(dataset.upper)); // update the data
                                  path.transition().duration(duration).attrTween("d", function (a) {
                                    // Store the displayed angles in _current.
                                    // Then, interpolate from _current to the new angles.
                                    // During the transition, _current is updated in-place by d3.interpolate.
                                    var i  = d3.interpolate(this._current, a);
                                    var i2 = d3.interpolate(progress, percent)
                                    this._current = i(0);
                                    return function(t) {
                                      text.text( format(i2(t) / 100) );
                                      return arc(i(t));
                                    };
                                  }); // redraw the arcs
                                }, 200);
                              }
                            };

                             


                           
                            
                            drawCircleChart(
                              cityIDHArc,
                              $(cityIDHash).data('circle'), // Pass variable variable here equal to percent.
                              382,
                              660,
                              '.35em'
                               );
                            
                            

                            
                    

                    };
    });

  


};



