// Waypoints
//post-1720 is MIAMI-DADE IS PEDESTRIAN- AND BIKE-FRIENDLY.
$('#post-1720 .data-disclosure [data-dimension="B"],#post-1720 .data-disclosure [data-dimension="C"],#post-1720 .data-disclosure [data-dimension="D"]').css('opacity',0);
$('#post-1720 .data-disclosure [data-dimension="B"],#post-1720 .data-disclosure [data-dimension="C"],#post-1720 .data-disclosure [data-dimension="D"]').addClass('hide');

$('[data-trigger="10"]').waypoint(function(down) {
  

$('#post-1720 .swap.objective.one').css('opacity','1');
$('#post-1720 .data-disclosure').css('opacity','1');
$('#post-1720 .year-labels').css('opacity','1');


//

 
var dataSourceDir = base + '/wp-content/themes/om-musa/';


d3.json(dataSourceDir+"js/data-svg/iac-ae-arts-courses-zero.json", function(error,zeroData) {

            if (error) {
                // If error is not null, something went wrong.
                console.log(error);
                //Log the error.
            } else {
                   

            bars = new BarGraphDollars(zeroData,'.iac-ae-cultural-opportunities .graph');

            $(document).ready(function(){
                
                d3.json(dataSourceDir+"js/data-svg/iac-ae-arts-courses.json", function(error,data) {
                                 
                                    bars.update(data);

                                    $("#ia-data-1720 .year-labels span.cyan").attr('data-value','2010-2011');
                                    $("#ia-data-1720 .year-labels span.purp").attr('data-value','2010-2011');

                                    console.log(data)

                              })

                    $('#post-1720 .data-svg .swap.objective.one .item span').on('click', function(){        
                        var parentSwap = $(this).parent().data('swap');
                        var activeSwap = $('#post-1720 .swap .item[data-swap="1"]');
                        var dataDimension = $(this).parent().data('dimension');
                        console.log(dataDimension)
                        if (parentSwap = "0") {
                            activeSwap.attr('data-swap', '0')
                            $(this).parent().attr('data-swap', '1')
                        };
                        $('#post-1720 .data-disclosure [data-dimension]:not(.hide)').css('opacity',0);
                        $('#post-1720 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                        $('#post-1720 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#post-1720 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);
                        if (dataDimension == "A") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ae-arts-courses.json", function(error,data) {
                                 
                                 bars.update(data,'.iac-ae-cultural-opportunities .graph');
                                 /*setTimeout(function(){
                                        bars.update(data,'.iac-ae-cultural-opportunities .graph');
                                    }, 1000); */
                                  $("#ia-data-1720 .year-labels span.cyan").attr('data-value','2010-2011');
                                  $("#ia-data-1720 .year-labels span.purp").attr('data-value','2010-2011');
                              })
                        };

                        if (dataDimension == "B") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ae-languages-taught.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-ae-cultural-opportunities .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.iac-ae-cultural-opportunities .graph');
                                    }, 1000); */
                                  $("#ia-data-1720 .year-labels span.cyan").attr('data-value','2010-2011');
                                  $("#ia-data-1720 .year-labels span.purp").attr('data-value','2010-2011');
                              })
                        };
                        if (dataDimension == "C") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ae-foreign-born.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-ae-cultural-opportunities .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.iac-ae-cultural-opportunities .graph');
                                    }, 1000); */
                                    $("#ia-data-1720 .year-labels span.cyan").attr('data-value','2011');
                                    $("#ia-data-1720 .year-labels span.purp").attr('data-value','2013');
                              })
                        };
                        if (dataDimension == "D") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ae-restaurants.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-ae-cultural-opportunities .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.iac-ae-cultural-opportunities .graph');
                                    }, 1000); */
                                    $("#ia-data-1720 .year-labels span.cyan").attr('data-value','2011');
                                    $("#ia-data-1720 .year-labels span.purp").attr('data-value','2013');
                              })
                        };

                    });
                      
            });

        

                 


        } // else
 }); // d3 json civic participation

},{triggerOnce:true});//Waypoints
 