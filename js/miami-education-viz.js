/**** UNEMPLOYMENT RATE - TOP GRAPH ****/

//global store for the data (fewer http requests)
var educationData = {
    unemploymentRate: null
}

//store the ways we'll filter data (two dimensions for the first one)
var opportunityState = {
    year: 2010,
    property: null
}

//first, load the general unemployment rate
d3.csv("js/data-svg/education-unemployment-rate.csv", function(error,data) {
    educationData.unemploymentRate = data;

    educationData.unemploymentRateByEducation = {
        "2010": {},
        "2012": {}
    };

    dataByEducation();
});

//next, load the data for unemployment by education, and finesse the data for the chart
function dataByEducation(){
    d3.csv("js/data-svg/education-unemployment-by-education.csv", function(error,data) {

        for (var i = 0; i < data.length; i++) {
            var record = data[i];
            var newrecord = {};

            var location = record["Central County of:"];

            for (var prop in record) {
                if (prop.indexOf("2010") > -1) {
                    var datum = {};
                    datum.location = location;
                    // datum.name = prop.substring(0, prop.indexOf(" 2010"));
                    datum.value = record[prop];
                    datum.baseline = educationData.unemploymentRate[i]["2011"];

                    if (educationData.unemploymentRateByEducation["2010"][prop.substring(0, prop.indexOf(" 2010"))]) {
                        educationData.unemploymentRateByEducation["2010"][prop.substring(0, prop.indexOf(" 2010"))].push(datum);
                    } else {
                        educationData.unemploymentRateByEducation["2010"][prop.substring(0, prop.indexOf(" 2010"))] = [datum];
                    }


                    //set the initial property to the first property
                    if (prop.indexOf("graduate") > -1) {
                        opportunityState.property = prop.substring(0, prop.indexOf(" 2010"));
                    }
                } else if (prop.indexOf("2012") > -1) {
                    var datum = {};
                    datum.location = location;
                    // datum.name = prop.substring(0, prop.indexOf(" 2012"));
                    datum.value = record[prop];
                    datum.baseline = educationData.unemploymentRate[i]["2013"];

                    if (educationData.unemploymentRateByEducation["2012"][prop.substring(0, prop.indexOf(" 2012"))]) {
                        educationData.unemploymentRateByEducation["2012"][prop.substring(0, prop.indexOf(" 2012"))].push(datum);
                    } else {
                        educationData.unemploymentRateByEducation["2012"][prop.substring(0, prop.indexOf(" 2012"))] = [datum];
                    }
                }
            }
        }

        // console.log(educationData.unemploymentRateByEducation);

        renderEducationData();

        educationLegend();
    });
}

function educationLegend() {
    var legendItems = $(".unemployment-rate .legend .item");

    var index = 1;

    for(prop in educationData.unemploymentRateByEducation["2010"]) {
        $( $(".unemployment-rate .legend .item")[index] ).attr("data-property", prop);
        index++;
    }

    $(".unemployment-rate .legend .item").on("click", function(e) {
        if ($(this).is(".no-interaction")) { return; }
        opportunityState.property = $(this).attr("data-property");
        renderEducationData();
    })

    $(".unemployment-rate .legend .toggle .value").on("click", function(e) {
        $(".unemployment-rate .legend .toggle").attr("data-value", $(this).index());
        opportunityState.year = $(this).attr("data-value");
        renderEducationData();
    })
}


function renderEducationData() {

    var data = educationData.unemploymentRateByEducation[opportunityState.year][opportunityState.property];

    var bars = d3.select(".unemployment-rate .bars").selectAll("div.bar").data(data);

    var flatdata = [];

    for (var prop in educationData.unemploymentRateByEducation[opportunityState.year]) {
        flatdata = flatdata.concat(educationData.unemploymentRateByEducation[opportunityState.year][prop]);
    }

    var min = d3.min(flatdata, function(d) {
        return Math.min( parseFloat(d.value), parseFloat(d.baseline) )-1;
    });

    var max = d3.max(flatdata, function(d) {
        return Math.max( parseFloat(d.value), parseFloat(d.baseline) ) + 1;
    });

    var height = d3.scale.linear().domain([0, max]).range([0,100]);
    var top = d3.scale.linear().domain([0, max]).range([100, 0]);

    $(".unemployment-rate").attr("data-dimension", opportunityState.property.toLowerCase());

    bars
        .call(function(context) {
            this.select("div.rate")
                .style("height", function(d) {
                    return height(parseFloat(d.value)) + "%";
                })
                .select("div.value")
                .attr("data-value", function(d) {
                    return d.value.substring(0, d.value.indexOf("%"));
                });
        })
        .select("div.baseline")
        .attr("data-value", function(d) {
            return d.baseline.substring(0, d.baseline.indexOf("%"));
        })
        .style("top", function(d) {
            return top(parseFloat(d.baseline)) + "%";
        });

    bars.enter()
        .append("div")
        .attr("class", "bar")
        .attr("data-location", function(d) {
            return d.location;
        })
        .call(function(context) {
            this.append("div")
                .attr("class", "rate")
                .style("height", function(d) {
                    return height(parseFloat(d.value)) + "%";
                })
                .call(function(context) {
                    this.append("div")
                        .attr("class", "value")
                        .attr("data-value", function(d) {
                            return d.value.substring(0, d.value.indexOf("%"));
                        });

                })

            this.append("div")
                .attr("class", "baseline")
                .attr("data-value", function(d) {
                    return d.baseline.substring(0, d.baseline.indexOf("%"));
                })
                .style("top", function(d) {
                    return top(parseFloat(d.baseline)) + "%";
                })
        })
}

/** GRADUATION RATE - MIDDLE GRAPH MAP **/

var graduationYear = "2010";
var graduationData = [];


d3.csv("js/data-svg/education-hs-graduation.csv", function(error,data) {
    for (var i = 0; i < data.length; i++) {
        var datum = data[i];

        if (datum.County != "US") {
            graduationData.push(datum);

            switch(datum.County) {
                case "Chicago":
                    datum.cx = 520;
                    datum.cy = 160;
                    break;
                case "Houston":
                    datum.cx = 420;
                    datum.cy = 380;
                    break;
                case "Miami":
                    datum.cx = 618;
                    datum.cy = 430;
                    break;
                case "NYC":
                    datum.cx = 680;
                    datum.cy = 210;
                    break;
                case "San Diego":
                    datum.cx = 110;
                    datum.cy = 280;
                    break;
            }
        }
    }

    renderGraduationRates();
});

function renderGraduationRates() {
    var cities = d3.select(".hs-graduation-rates .map")
                    .selectAll("g.city")
                    .data(graduationData);

    var radius = 40;

    var radiusScale = d3.scale.linear().domain([0, 100]).range([0, radius]);

    cities.call(function() {
            this.select("circle.value")
                .transition()
                .attr("r", function(d) {
                    return radiusScale( parseFloat(d[graduationYear]) );
                })

            this.select("text.value")
                .text(function(d) {
                    return Math.round( parseFloat(d[graduationYear]) ) + "%";
                })
        })

    cities.enter()
            .append("g")
            .attr("class", "city")
            .attr("data-city", function(d) {
                return d.County;
            })
            .call(function() {
                this.append("circle")
                    .attr("class", "total")
                    .attr("cx", function(d) {
                        return d.cx;
                    })
                    .attr("cy", function(d) {
                        return d.cy;
                    })
                    .attr("r", radius)

                this.append("circle")
                    .attr("class", "value")
                    .attr("cx", function(d) {
                        return d.cx;
                    })
                    .attr("cy", function(d) {
                        return d.cy
                    })
                    .transition()
                    .attr("r", function(d) {
                        return radiusScale( parseFloat(d[graduationYear]) );
                    })

                this.append("text")
                    .attr("class", "value")
                    .text(function(d) {
                        return Math.round( parseFloat(d[graduationYear]) ) + "%";
                    })
                    .attr("transform", function(d) {
                        var x,y;

                        switch(d.County) {
                            case "Chicago":
                                x = 389;
                                y = 175;
                                break;
                            case "Houston":
                                x = 395;
                                y = 462;
                                break;
                            case "Miami":
                                x = 665;
                                y = 460;
                                break;
                            case "NYC":
                                x = 662;
                                y = 290;
                                break;
                            case "San Diego":
                                x = 156;
                                y = 280;
                                break;
                        }

                        return "translate(" + x + "," + y + ")";
                    })

                this.append("text")
                    .attr("class", "label")
                    .text(function(d) {
                        return d.County;
                    })
                    .attr("transform", function(d) {
                        var x,y;

                        switch(d.County) {
                            case "Chicago":
                                x = d.cx-32;
                                y = 218;
                                break;
                            case "Houston":
                                x = d.cx-32;
                                y = 330;
                                break;
                            case "Miami":
                                x = 618;
                                y = 383;
                                break;
                            case "NYC":
                                x = 608;
                                y = d.cy+4;
                                break;
                            case "San Diego":
                                x = d.cx-38;
                                y = 338;
                                break;
                        }

                        return "translate(" + x + "," + y + ")";
                    })
            })
}

$(document).ready(function () {
    $(".hs-graduation-rates .toggle .value").on("click", function(e) {
        $(".hs-graduation-rates .toggle").attr("data-value", $(this).index());
        graduationYear = $(this).attr("data-value");
        renderGraduationRates();
    })
})

/** INSTRUCTIONAL EXPENDITURES - MIDDLE GRAPH HORIZONTAL BARS **/

d3.csv("js/data-svg/education-student-expenditures.csv", function(error,data) {

    // console.log(data);

    var bars = d3.select(".instructional-expenditures").selectAll("div.bar").data(data);

    var max = d3.max(data, function(d) {
        return Math.max( stripNumber(d["2008-2009"]), stripNumber(d["2008-2009"])) + 40;
    });


    var width = d3.scale.linear().domain([4000, max]).range([0,90]);

    function stripNumber(num) {
        num = num.split(",");
        num = num.join("");
        num = num.substring(1);
        num = parseInt(num);
        return num;
    }

    bars.enter()
        .append("div")
        .attr("class", "bar")
        .attr("data-location", function(d) {
            return d.GEOGRAPHY;
        })
        .call(function(d) {
            this.append("div")
                .attr("data-year", "2008-2009")
                .attr("data-cost", function(d) {
                    return d["2008-2009"];
                })
                .attr("class", function(d) {
                    return stripNumber(d["2008-2009"]) < stripNumber(d["2010-2011"]) ? "top" : "";
                })
                .style("width", function(d) {
                    return width( stripNumber(d["2008-2009"]) ) + "%";
                })

            this.append("div")
                .attr("data-year", "2010-2011")
                .attr("data-cost", function(d) {
                    return d["2010-2011"];
                })
                .attr("class", function(d) {
                    return stripNumber(d["2010-2011"]) < stripNumber(d["2008-2009"]) ? "top" : "";
                })
                .style("width", function(d) {
                    return width( stripNumber(d["2010-2011"]) )+"%";
                })
        })
});


//****** BOTTOM RADIAL GRAPHS ********//

d3.csv("js/data-svg/magnet-school-enrollment.csv", function(error,data) {
    var graphs = []

    for(var i = 0; i < data.length; i++) {
        graphs.push(new RadialGraph([data[i]["2008-2009"]], '.column[data-dimension="magnetschoolenrollment"] .graphs', data[i].GEOGRAPHY));
    }

    $('.column[data-dimension="magnetschoolenrollment"] .toggle .value').on("click", function(e) {
        $(this).parents(".toggle").attr("data-value", $(this).index());
        for(var i = 0; i < graphs.length; i++) {
            console.log(data);
            graphs[i].update([data[i][$(this).attr("data-value")]]);
        }
    })
});


d3.csv("js/data-svg/education-college-readiness.csv", function(error,data) {

    var newData = []

    for(var i = 0; i < data.length; i++) {
        var city = {
            GEOGRAPHY: data[i].GEOGRAPHY,
            2010: [],
            2012: []
        }
        for(prop in data[i]) {
            if(prop.indexOf("2010") > -1) {
                city["2010"].push(data[i][prop]);
            }else if(prop.indexOf("2012") > -1) {
                city["2012"].push(data[i][prop]);
            }
        }
        newData.push(city);
    }

    data = newData;

    var graphs = []

    for(var i = 0; i < data.length; i++) {
        graphs.push(new RadialGraph(data[i]["2010"], '.column[data-dimension="collegereadiness"] .graphs', data[i].GEOGRAPHY));
    }

    $('.column[data-dimension="collegereadiness"] .toggle .value').on("click", function(e) {
        $(this).parents(".toggle").attr("data-value", $(this).index());

        for(var i = 0; i < graphs.length; i++) {
            console.log(newData)
            graphs[i].update(data[i][$(this).attr("data-value")]);
        }

    })
});

d3.csv("js/data-svg/education-higher-education-percentage.csv", function(error,data) {

    var newData = []

    for(var i = 0; i < data.length; i++) {
        var city = {
            GEOGRAPHY: data[i].GEOGRAPHY,
            2010: [],
            2012: []
        }
        for(prop in data[i]) {
            if(prop.indexOf("2010") > -1) {
                city["2010"].push(data[i][prop]);
            }else if(prop.indexOf("2012") > -1) {
                city["2012"].push(data[i][prop]);
            }
        }
        newData.push(city);
    }

    data = newData;

    var graphs = []

    for(var i = 0; i < data.length; i++) {
        graphs.push(new RadialGraph(data[i]["2010"], '.column[data-dimension="adultpopulation"] .graphs', data[i].GEOGRAPHY));
    }


    $('.column[data-dimension="adultpopulation"] .toggle .value').on("click", function(e) {
        $(this).parents(".toggle").attr("data-value", $(this).index());

        for(var i = 0; i < graphs.length; i++) {
            console.log(newData)
            graphs[i].update(data[i][$(this).attr("data-value")]);
        }

    })
});
