function whatAboutHousing(){

var cityName = "Miami";

var constructionBars = d3.select(".what-about-housing .construction-cost")
                          .append("div")
                          .attr("class", "bar")
                       .call(function(d) {
                            this.append("div")
                                .attr("data-year", "2011")
                                .append("div")
                                .attr("class", "marker")
                                  

                            this.append("div")
                                .attr("data-year", "2013")
                                .append("div")
                                .attr("class", "marker")
                                
                                
                        })   

var homeSaleBars = d3.select(".what-about-housing .median-sale-price")
                          .append("div")
                          .attr("class", "bar")
                       .call(function(d) {
                            this.append("div")
                                .attr("data-year", "2011")
                                .append("div")
                                .attr("class", "marker")
                                

                            this.append("div")
                                .attr("data-year", "2013")
                                .append("div")
                                .attr("class", "marker")
                                
                        })   


  //$('#post-1541').waypoint(function(down){

                $('.what-about-housing li[data-city="Miami"]').addClass('active');

                renderConstructionCosts();

                renderHomeSaleValue();

                

                $('.what-about-housing .city.names [data-city]').on('click', function(){
                    
                    
                     
                    
                    cityName =  $(this).data('city');

                    renderConstructionCosts();
                    
                    renderHomeSaleValue();

                    
                    
                    $('.what-about-housing li.active').removeClass('active');
                    $('.what-about-housing li[data-city="'+cityName+'"]').addClass('active');
            
                  }); 
           
   //          },{triggerOnce:true});


   function renderConstructionCosts(){
    d3.tsv(dataSourceDir+'js/data-svg/avg-construction-costs.tsv', function(error, data) {
               

          if (error) {
                   // If error is not null, something went wrong.

                   console.log(error);
                    //Log the error.

                    } 
              else 

              { 

                console.log(data)
                constructionData = data;



                for (var i = constructionData.length - 1; i >= 0; i--) {
                  
                  var location = constructionData[i].County;

                  if (location.indexOf(cityName) >= 0) {
                      

                      
                                               

                      var max = d3.max(constructionData, function() {
                          
                          return Math.max( parseFloat(stripNumber(constructionData[i]["2011"])), 
                            parseFloat(stripNumber(constructionData[i]["2011"])));
                      });



                      var width = d3.scale.linear().domain([0, max]).range([0,50]);


                        

                      console.log(parseFloat(stripNumber(constructionData[i]["2013"])))
                     

                      constructionBars
                          
                          .attr("data-location", function() {
                              return constructionData[i].County;
                          })
                      .call(function(d) {
                            this.select("div[data-year='2011']")
                                .attr("data-cost", function() {
                                    return constructionData[i]["2011"];
                                })
                                .attr("class", function() {
                                    return constructionData[i]["2011"] < constructionData[i]["2013"] ? "top value" : "value";
                                })
                                
                                .style("width", function() {
                                    return width( parseFloat(stripNumber(constructionData[i]["2011"])) ) +"%";
                                })
                                .select('.marker')
                                .attr('data-cost', function(){
                                   return constructionData[i]["2011"];
                                })

                            this.select("div[data-year='2013']")
                                
                                .attr("data-cost", function() {
                                    return constructionData[i]["2013"];
                                })
                                .attr("class", function() {
                                    return constructionData[i]["2013"] < constructionData[i]["2011"] ? "top value" : "value";
                                })
                                
                                .style("width", function() {
                                    return width( parseFloat(stripNumber(constructionData[i]["2013"])) ) +"%";
                                })
                                .select('.marker')
                                .attr('data-cost', function(){
                                   return constructionData[i]["2013"];
                                })
                            d3.select(".what-about-housing .construction-cost .compare")
                              .attr("data-comparison", function(d) {
                              return constructionData[i]["% Growth"];
                            })
                        })


                  }
                  
                  };

                                  
      } // Else




    }) // d3


  }; // renderConstructionCosts





function renderHomeSaleValue(){
    d3.tsv(dataSourceDir+'js/data-svg/home-sale-value.tsv', function(error, data) {


                     
                     if(error) {
                      console.log(error)
                     } else {

                homesaleData = data;

                for (var i = homesaleData.length - 1; i >= 0; i--) {
                  
                  var location = homesaleData[i].County;

                  if (location.indexOf(cityName) >= 0) {
                      

                      var max = d3.max(homesaleData, function() {
                          
                          return Math.max( parseFloat(stripNumber(homesaleData[i]["2011"])), parseFloat(stripNumber(homesaleData[i]["2011"])))+40;
                      });

                      var width = d3.scale.linear().domain([0, max]).range([0,50]);

                      console.log(homesaleData[i]["2011"])

                      homeSaleBars
                          
                          .attr("data-location", function() {
                              return homesaleData[i].County;
                          })
                      
                      .call(function(d) {
                            this.select("div[data-year='2011']")
                                .attr("data-cost", function(d) {
                                    return homesaleData[i]["2011"];
                                })
                                .attr("class", function(d) {
                                    return homesaleData[i]["2011"] < homesaleData[i]["2013"] ? "top value" : "value";
                                })
                                
                                .style("width", function(d) {
                                    return width( parseFloat(stripNumber(homesaleData[i]["2011"])) )+"%";
                                })
                                .select('.marker')
                                .attr('data-cost', function(){
                                   return homesaleData[i]["2011"];
                                })
                            this.select("div[data-year='2013']")
                                
                                .attr("data-cost", function(d) {
                                    return homesaleData[i]["2013"];
                                })
                                .attr("class", function(d) {
                                    return homesaleData[i]["2013"] < homesaleData[i]["2011"] ? "top value" : "value";
                                })
                                
                                .style("width", function(d) {
                                    return width( parseFloat(stripNumber(homesaleData[i]["2013"])) )+"%";
                                })
                                .select('.marker')
                                .attr('data-cost', function(){
                                   return homesaleData[i]["2013"];
                                })
                            d3.select(".what-about-housing .median-sale-price .compare")
                               .attr("data-comparison", function(d) {
                               return homesaleData[i]["% Growth"];
                            })
                        })  

                      
                         

                  }
                  
                  };
                
                // 


                     } // else 
    })// d3

 
};

function cityPointer(){
          
          switch(cityName){

              case "Chicago":
            console.log(cityName)
              // $('.what-about-housing ul:after').css('bottom', "283px")
            
              break;

          };

};

function stripNumber(num) {
                            num = num.split(",");
                            num = num.join("");
                            num = num.substring(1);
                            num = parseInt(num);
                            return num;
                        }




}


whatAboutHousing();
