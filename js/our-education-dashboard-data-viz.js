  //global store for the data (fewer http requests)
        var magnetSchoolEnrollment = {
            enrollmentRate: null
        }

        //store the ways we'll filter data (two dimensions for the first one)
        var enrollmentRegion = {
            year: "2010 - 2011",
            region: "Miami-Dade County"
        }

        

        var dataSourceDir = base + '/wp-content/themes/om-musa/';

$('[data-trigger="31"]').waypoint(function(down){

    ourEducationDashboard('MIAMI',1200);

    ourEducationDashboard('FL',1200);

},{triggerOnce:true}); // Waypoints



function ourEducationDashboard(region,duration){

      
        //first, load the general unemployment rate
        d3.csv(dataSourceDir+"js/data-svg/magnet-school-enrollment-unformatted.csv", function(error,data) {

             if (error) {
    // If error is not null, something went wrong.

        console.log(error);
    //Log the error.

        } else { 
          

                            magnetSchoolEnrollment.enrollmentRate = data;

                            var miamiDadeCounty = data[0];
                            var floridaState = data[1];
                            
                         

                            

                            var width = 260,
                                height = 160,
                                τ = 2 * Math.PI; // http://tauday.com/tau-manifesto
                                format = d3.format(".0%");

                            // An arc function with all values bound except the endAngle. So, to compute an
                            // SVG path string for a given angle, we pass an object with an endAngle
                            // property to the `arc` function, and it will return the corresponding string.
                            var arc = d3.svg.arc();

                             arc.innerRadius(70)
                                .outerRadius(80)
                                .startAngle(0);

                            // Create the SVG container, and apply a transform such that the origin is the
                            // center of the canvas. This way, we don't need to position arcs individually.
                            var svg = d3.select('#magnet .svg[data-location="'+region+'"]').append("svg")
                                .attr("width", function(){
                                    return width + 'px'
                                })
                                .attr("height", function(){
                                    return height + 'px'
                                })
                              .append("g")
                                .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")

                            // Add the background arc, from 0 to 100% (τ).
                            var background = svg.append("path")
                                .attr('class', 'transparent arc') 
                                .datum({endAngle: τ})
                                .attr("d", arc);

                            // Add the foreground arc 
                            var foreground = svg.append("path")
                                .datum({endAngle: .127 * τ})
                                .attr('class', 'blue arc')                                
                                .attr("d", arc);

                            var whiteCircle = svg.append("circle")
                                .attr('r', 70 )
                                .attr('class', 'white circle');

                            var label = svg.append("text")
                                .attr('class', 'region-name')
                                .attr('dx', 0)
                                .attr('dy', 10)
                                .attr('text-anchor', 'middle')
                                .text(function(){
                                  return region;
                                }); 

                             
                              

                              if (region == 'MIAMI') {
                                  var    y1 = [parseFloat(miamiDadeCounty["2008 - 2009"])];
                                  var    y2 = [parseFloat(miamiDadeCounty["2010 - 2011"])];

                                  
                              } else {

                                  var    y1 = [parseFloat(floridaState["2008 - 2009"])];
                                  var    y2 = [parseFloat(floridaState["2010 - 2011"])];


                                  
                              };

                              



                              var percentage = d3.select('#magnet .svg[data-location="'+region+'"] svg g')

                                  percentageData = percentage.selectAll('text.number')
                                            .data(y2);

                                 

                                  function animatePercentage(){

                                    percentageData.enter()
                                      .append("text")
                                      .attr('class', 'number')
                                      .attr('dx', 73)
                                      .attr('dy', 0-50)
                                      .text(0)
                                      .attr("class", "blue txt percent")
                                      
                                      .transition()
                                      .duration(duration)
                                      .tween("text", function(d) {
                                              var i = d3.interpolate(this.textContent, d),
                                                  prec = (d + "").split("."),
                                                  round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;
                                            
                                              return function(t) {
                                                  this.textContent =(Math.round(i(t) * round) / round)+'%';

                                                   
                                              };
                                          })

                                  }

                              

                                   animatePercentage();


                           



                           /*  i = enrollmentRegion.year.indexOf("2008 - 2009");
                            if (i > -1) {
                              rateMia = miamiDadeCounty["2008 - 2009"]
                              rateFL = floridaState["2008 - 2009"]
                            } else {
                              rateMia = miamiDadeCounty["2010 - 2011"]
                              rateFL = floridaState["2010 - 2011"]
                            } */

                            if (region == 'MIAMI') {
                                              rate = miamiDadeCounty["2010 - 2011"]  
                                          } else {

                                            rate = floridaState["2010 - 2011"]

                            }

                   






                            
                            function arcTween(transition, newAngle) {

                           
                              transition.attrTween("d", function(d) {

                                
                                var interpolate = d3.interpolate(d.endAngle, newAngle);

                      
                                return function(t) {

                                  
                                  d.endAngle = interpolate(t);

                                  
                                  return arc(d);
                                };
                              });
                            }

               $(document).ready(function() {
                                        

                                  foreground.transition()
                                  .duration(duration)
                                  
                                  .call(arcTween, parseFloat(rate)/100 * τ);


                            });

                             $('#magnet.pie .toggle .value').on('click',function() {
                                  if (this.dataset.value == "2008 - 2009") {
                                      
                                      $('#magnet.pie .toggle').attr('data-value', this.dataset.value);  

                                      // console.log(this.dataset.value)
                                  
                                  
                                  if (region == 'MIAMI') {
                                      rate = miamiDadeCounty["2008 - 2009"]  
                                    // console.log(rate)
                                      

                                  } else {

                                    rate = floridaState["2008 - 2009"]

                                    
                                  }

                                  // This draws the arc
                                  foreground.transition()
                                  .duration(duration)
                                  .call(arcTween, parseFloat(rate)/100 * τ);
                                  
                                  // This animates percentage

                                      percentage.selectAll('text.txt').remove();

                                      percentageData = percentage.selectAll('text.number')

                                                .data(y1)
                                                
                                                animatePercentage();
                                      // console.log(y1)

                                  } else if (this.dataset.value == "2010 - 2011"){
                                    
                                               $('#magnet .toggle').attr('data-value', this.dataset.value);  
                                  
                                            if (region == 'MIAMI') {
                                              rate = miamiDadeCounty["2010 - 2011"]  
                                         

                                          } else {

                                            rate = floridaState["2010 - 2011"]
                                            // console.log(rate)
                                          }

                                  foreground.transition()
                                  .duration(duration)
                                  .call(arcTween, parseFloat(rate)/100 * τ);
                                  

                                 

                                  percentage.selectAll('text.txt').remove();

                                  percentageData = percentage.selectAll('text.number')

                                                  .data(y2);

                                  animatePercentage();

                                } 
                                  
                            });

            }
        });

};
