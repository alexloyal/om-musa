 


  /** Public Space and Air Quality **/

var activeYear = "2013";
var airQualityDays = [];
var parkLand = [];
var cities = [];

var dataSourceDir = base + '/wp-content/themes/om-musa/';

d3.tsv(dataSourceDir + "js/data-svg/data-public-space-air-quality.tsv", function(error,data) {
    for (var i = 0; i < data.length; i++) {
        var datum = data[i];



        if (datum.County != "US") {
            airQualityDays.push(datum);

            switch(datum.County) {
                case "Chicago":
                    datum.cx = 520;
                    datum.cy = 180;
                    break;
                case "Houston":
                    datum.cx = 420;
                    datum.cy = 430;
                    break;
                case "Miami":
                    datum.cx = 618;
                    datum.cy = 430;
                    break;
                case "New York":
                    datum.cx = 700;
                    datum.cy = 180;
                    break;
                case "San Diego":
                    datum.cx = 100;
                    datum.cy = 260;
                    break;
            }
        }
    }

// This renders the air quality icons.

    



    renderAirQualityDays();

  

    
});





d3.tsv(dataSourceDir + "js/data-svg/data-public-space-parkland.tsv", function(error,data) {
    for (var i = 0; i < data.length; i++) {
        var datum = data[i];

        if (datum.County != "US") {
            parkLand.push(datum);

            switch(datum.County) {
                case "Chicago":
                    datum.cx = 520;
                    datum.cy = 180;
                    break;
                case "Houston":
                    datum.cx = 420;
                    datum.cy = 430;
                    break;
                case "Miami":
                    datum.cx = 618;
                    datum.cy = 430;
                    break;
                case "New York":
                    datum.cx = 700;
                    datum.cy = 180;
                    break;
                case "San Diego":
                    datum.cx = 100;
                    datum.cy = 260;
                    break;
            }
        }
    }




setTimeout(function(){

    renderParkLand();
    },200);

});


function renderAirQualityDays() {

    var cities = d3.select(".public-space-air-quality .map")
                    .selectAll("g.city")
                    .data(airQualityDays);

    



    cities.call(function() {
          // These two are one dimention. Duplicate to create another dimenison and refer to line 85.
            

            this.select("text.value")
                .text(function(d) {
                    return Math.round( parseFloat(d[activeYear]) );
                })
                
        })

    cities.enter()
            .append("g")
            .attr("class", "city dot")
            .attr("data-city", function(d) {
                return d.County;
            })
            .attr('data-air-quality', function(d){
                return d[activeYear];
            })
            .call(function() {
                 
 

                

                this.append("text")
                    .attr("class", "value label air-quality cyan")
                    .text(function(d) {
                        return parseFloat(d[activeYear]);
                    })
                    .attr("transform", function(d) {
                        var x,y;

                        switch(d.County) {
                            case "Chicago":
                                x = 410;
                                y = 160;
                                break;
                            case "Houston":
                                x = 365;
                                y = 460;
                                break;
                            case "Miami":
                                x = 650;
                                y = 460;
                                break;
                            case "New York":
                                x = 710;
                                y = 200;
                                break;
                            case "San Diego":
                                x = 70;
                                y = 360;
                                break;
                        }

                        return "translate(" + x + "," + y + ")";
                    })



                this.append("text")
                    .attr("class", "city-name label")
                    .text(function(d) {
                        return d.County;
                    })
                    .attr("transform", function(d) {
                        var x,y;

                        switch(d.County) {
                            case "Chicago":
                                x = 400;
                                y = 120;
                                break;
                            case "Houston":
                                x = 360;
                                y = 360;
                                break;
                            case "Miami":
                                x = 680;
                                y = 420;
                                break;
                            case "New York":
                                x = 700;
                                y = 160;
                                break;
                            case "San Diego":
                                x = 40;
                                y = 320;
                                break;
                        }

                        return "translate(" + x + "," + y + ")";
                    })

    // Rendering city dots.

            d3.select('#data-svg-1576 .map')
                      .selectAll('g.city.dot')
                      .append("circle")
                            .attr('class','city-circle' )
                            .attr('stroke-width', '9')
                            .attr('r', '18');  
                       
                       
                      

                    d3.select('#data-svg-1576 .map .city.dot[data-city="Chicago"] .city-circle')
                            .attr('transform', 'translate(520,160)');
                    d3.select('#data-svg-1576 .map .city.dot[data-city="Houston"] .city-circle')
                            .attr('transform', 'translate(420,400)');
                    d3.select('#data-svg-1576 .map .city.dot[data-city="Miami"] .city-circle')
                            .attr('transform', 'translate(618,430)');
                    d3.select('#data-svg-1576 .map .city.dot[data-city="New York"] .city-circle')
                            .attr('transform', 'translate(680,210)'); 
                    d3.select('#data-svg-1576 .map .city.dot[data-city="San Diego"] .city-circle')
                            .attr('transform', 'translate(110,280)'); 

                  


    })

setTimeout(function(){
        for (var i = airQualityDays.length - 1; i >= 0; i--) {

        var city = airQualityDays[i]["County"];
        var balloons = Math.round((airQualityDays[i][activeYear])/10);
        d3.select('.air-quality.count[data-city="'+city+'"]')
            .call(function(){

                     for (var i = balloons- 1; i >= 0; i--) {
                        this.append('span').attr('class', 'air-icon');
                    };
            })
    };
},260);
    
 ;


} // renderAirQualityDays()



function renderParkLand(empty){
        var cities = d3.select(".public-space-air-quality .map")
                        .selectAll("g.city.air-quality")
                        .data(parkLand);


  


    cities.call(function() {
          // These two are one dimention. Duplicate to create another dimenison and refer to line 85.
             

            this.select("text.value")
                .text(function(d) {
                    return  parseFloat(d[activeYear]);
                })

        })

    cities.enter()
            .append("g")
            .attr("class", "city air-quality")
            .attr("data-city", function(d) {
                return d.County;
            })
            .attr('data-parkland',function(d){
                return d[activeYear];
            } )
            .call(function() {

 

                // Volunteer Rate Circle

                this.append("text")
                    .attr("class", "value label parkland apple green")
                    .text(function(d) {
                        return parseFloat(d[activeYear]);
                    })
                    .attr("transform", function(d) {
                        var x,y;

                        switch(d.County) {
                            case "Chicago":
                                x = 410;
                                y = 200;
                                break;
                            case "Houston":
                                x = 345;
                                y = 500;
                                break;
                            case "Miami":
                                x = 665;
                                y = 500;
                                break;
                            case "New York":
                                x = 720;
                                y = 240;
                                break;
                            case "San Diego":
                                x = 30;
                                y = 400;
                                break;
                        }

                        return "translate(" + x + "," + y + ")";
                    })
    })

            
            


 

    // This renders the park icons.

    for (var i = parkLand.length - 1; i >= 0; i--) {

        var city = parkLand[i]["County"];
            
        var parks = Math.round((parkLand[i][activeYear]));
        
        d3.select('.parkland.count[data-city="'+city+'"]')
            .call(function(){

                     for (var i = Number(parks)- 1; i >= 0; i--) {
                        this.append('span').attr('class', 'five-acres-icon');
                    };
            })
    };
 ;

}

function clearIcons() {
    d3.selectAll('.five-acres-icon').remove();
    d3.selectAll('.air-icon').remove();
    d3.selectAll('.city-circle').remove();
}
 


$(document).ready(function () {

          d3.selectAll('.air-quality .city-circle').remove();

          $(".public-space-air-quality .toggle .value").on("click", function(e) {
       
            $(".public-space-air-quality .toggle").attr("data-value", $(this).index());
            clearIcons();
            d3.selectAll('.air-quality .city-circle').remove();
            activeYear = $(this).attr("data-value");
            renderParkLand();
                renderAirQualityDays();
                setTimeout(function(){
                     $('#data-svg-1576 .five-acres-icon').animate({opacity:1},5);
                     $('#data-svg-1576 .air-icon').animate({opacity:1},5);  
                     $('#data-svg-1576 .city').animate({opacity:1},5);
            }, 290)

    })
})



 
