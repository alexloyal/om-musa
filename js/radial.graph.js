var RadialGraph = function (data, elementSelector, title, suppliedMaximum) {

    /**

    data input format:

    ["NN%", "NN%"]

    Where the first array item is the outer arc, and the second (optional) is the inner arc.
    Be sure to include the % and the entire value as a string (these get parsed out).

    **/

    var graph = this;

    this.data = [];

    d3.select(elementSelector)
                        .append("div")
                        .attr("class", "radial-graph "+title)
                        .append("svg")
                        .attr("width", "220")
                        .attr("height", "140");

    d3.select(elementSelector)
                        .select("div.radial-graph."+title)
                        .append("h4")
                        .attr("class", "graph-title")
                        .text(title)

    this.update = function(data, suppliedMaximum) {

        var max = suppliedMaximum ? suppliedMaximum : 100;

        var twoPi = 2 * Math.PI;

        var outerArc = d3.svg.arc()
            .innerRadius(40)
            .outerRadius(48)
            .startAngle(0);

        var innerArc = d3.svg.arc()
            .innerRadius(32)
            .outerRadius(37)
            .startAngle(0);

        for(var i = 0; i < data.length; i++) {

            var percent = isNaN(parseFloat(data[i])/max) ? data[i].replace(/[^0-9\.]+/g,"")/max : parseFloat(data[i])/max;

            var replaceData = {
                index: i,
                label: data[i],
                endAngle: percent * twoPi,
                oldAngle: graph.data[i] ? graph.data[i].endAngle : 0
            }

            this.data[i] = replaceData;
        }

        this.arcs = d3.select(elementSelector)
                            .select("div.radial-graph."+title)
                            .select("svg")
                            .selectAll("g.arc")
                            .data(this.data);

        this.arcs
            .call(function(context) {
                this.select("path.arc")
                    .transition()
                    .duration(750)
                    .ease("elastic")
                    .attrTween("d", arcTween);

                this.select("text.label")
                    .text(function(d) {
                        return d.label;
                    })
                    .transition()
                    .attr("transform", function(d) {
                        var r = 60;
                        var x = r * Math.cos( d.endAngle - Math.PI/2);
                        var y = 3 + r * Math.sin( d.endAngle - Math.PI/2);
                        return "translate(" + x + "," + y +")";
                    })
                    // .tween("text", function(d) {
                    //
                    //     var i = d3.interpolate(parseFloat(this.textContent), parseFloat(d.label)),
                    //         prec = (parseFloat(d.label) + "").split("."),
                    //         round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;
                    //
                    //     return function(t) {
                    //         this.textContent = Math.round(i(t) * round) / round;
                    //     };
                    // });​
            })

        this.arcs
            .enter()
            .append("g")
            .attr("class", "arc")
            .attr("transform", "translate(110,70)")
            .call(function() {

                this.append("circle")
                    .attr("class", "bkg")
                    .attr("cx", "0")
                    .attr("cy", "0")
                    .attr("r", "40");

                this.append("path")
                    .attr("class", function(d, i) {
                        var className = "arc ";
                        if(i > 0){
                            className += "inner"
                        }else {
                            className += "outer"
                        }
                        return className
                    })
                    .transition()
                    .ease("elastic")
                    .duration(750)
                    .attrTween("d", arcTween)

                this.append("text")
                    .attr("class", function(d, i) {
                        var className = "label ";
                        if(i > 0){
                            className += "inner"
                        }else {
                            className += "outer"
                        }
                        return className
                    })
                    .text(function(d) {
                        return d.label;
                    })
                    .attr("transform", function(d, i) {
                        var r = 60;
                        var x = r * Math.cos( d.endAngle - Math.PI/2);
                        var y = 3 + r * Math.sin( d.endAngle - Math.PI/2);

                        return "translate(" + x + "," + y +")";
                    })

            });

        function arcTween(d) {
            var i = d3.interpolate(d.oldAngle, d.endAngle);
            return function(t) {
                if(d.index > 0){
                    return innerArc({endAngle: i(t)});
                }else{
                    return outerArc({endAngle: i(t)});
                }

            };
        }
    }

    this.update(data, suppliedMaximum);


}
