/* Interaction Sheet */


/* Traffic & Volunteering */









 $(document).ready(function(){

     /* Lack of Civic Participation */


     $('.lack-civic-participation .legend, .lack-civic-participation h4').delay(200).animate({opacity:1},200);
     $('.lack-civic-participation .map').delay(200).animate({opacity:1},200);
    

     /* Education */

     $('.unemployment-rate .legend').delay(200).animate({opacity:1},200);
     $('.unemployment-rate .graph').delay(200).animate({opacity:1},200);


    $('#post-1502 .data-svg').delay(0).animate({opacity:1},600);

    /* Park Space */

    $('#data-svg-1568').delay(0).animate({opacity:1},600);

     $(window).on('scroll', function(){

             if ($(window).scrollTop() < 532){
                 /* IAC */
                // $('.swap.objective.one').delay(0).animate({opacity:1},0);
            }

             if ($(window).scrollTop() > 532) {
                 // dot 20
                 $('.traffic-volunteering svg#tacho').delay(0).animate({opacity:1},200);
                 $('.traffic-volunteering .legend').delay(0).animate({opacity:1},200);

                 $('.hs-graduation-rates').delay(0).animate({opacity:1},200);
                 $('.instructional-expenditures').delay(0).animate({opacity:1},200);

                 $('.what-drives-this .data-svg').delay(6).animate({opacity:1},1200);
                 $('.what-drives-this .legend, .what-drives-this .annotation').delay(0).animate({opacity:1},600);
                 $('#post-1465 .data-svg').delay(0).animate({opacity:1},600);
                 
                 $('#data-svg-1576').delay(0).animate({opacity:1},600);
                 // $('.park-investment').delay(0).animate({opacity:1},200);
                 $('.five-acres-icon').delay(200).animate({opacity:1},200);
                 $('.air-icon').delay(200).animate({opacity:1},200);
                 $('#data-svg-1576 .city').delay(200).animate({opacity:1},200);


            }

             if ($(window).scrollTop() > 1451) {
                // dot 30
                $('#post-1552 .pie .svg').delay(4).animate({opacity:1},600);

                $('.residents-born-outside-civic-participation .legend').delay(0).animate({opacity:1},600);
                $('.residents-born-outside-civic-participation .data-svg').delay(0).animate({opacity:1},600);
                $('.what-does-this-mean .table').delay(0).animate({opacity:1},600);
                $('.safety-first').delay(0).animate({opacity:1},600);
                





            }

            if ($(window).scrollTop() > 2100) {
                
                $('#data-svg-1572').delay(0).animate({opacity:1},600);
            };

             if ($(window).scrollTop() > 2334) {
                $('.unemployment-growht-vs-unemployment-rate .bar-graph .bars').delay(0).animate({opacity:1},600);
            }

            if ($(window).scrollTop() > 2450) {
               $('.what-about-housing .data-svg').delay(0).animate({opacity:1},600);
            }

            if ($(window).scrollTop() > 3100) {
                $('#data-svg-1574').delay(0).animate({opacity:1},600);
            }




        });


 });
