// Waypoints
//#post-1805 is MIAMI-DADE HAS AN ENGAGED CITIZENRY

$(document).ready(function(){
  $('#post-1805 .data-disclosure [data-dimension="B"],#post-1805 .data-disclosure [data-dimension="C"],#post-1805 .data-disclosure [data-dimension="D"]').css('opacity', 0);
  $('#post-1805 .data-disclosure [data-dimension="B"],#post-1805 .data-disclosure [data-dimension="C"],#post-1805 .data-disclosure [data-dimension="D"]').addClass('hide');

});


$('#dot-10').waypoint(function(down) {
  

$('#post-1805 .swap.objective.one').css('opacity','1');
$('#post-1805 .data-disclosure').css('opacity','1');
$('#post-1805 .year-labels').css('opacity','1');


//

 
var dataSourceDir = base + '/wp-content/themes/om-musa/';


d3.json(dataSourceDir+"js/data-svg/iac-ha-financial-resources-income-zero.json", function(error,zeroData) {

            if (error) {
                // If error is not null, something went wrong.
                console.log(error);
                //Log the error.
            } else {
                     
                      
            bars = new BarGraphDollars(zeroData,'.iac-ce-engaged-citizenry .graph');

            $(document).ready(function(){
                
                d3.json(dataSourceDir+"js/data-svg/iac-ce-voter-participation.json", function(error,data) {
                                 
                                    bars.update(data);
                                    $('#ia-data-1805 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2010";
                                      } else {
                                        return "2012";
                                      }
                                    })
                                    //$('.iac-ce-engaged-citizenry [data-label="US"] .bar:nth-child(odd) .value .marker').delay(2).attr('data-label','N/A')
                                    //$('.iac-ce-engaged-citizenry [data-label="US"] .bar:nth-child(odd) .value').delay(1).css('height','0')

                              })

                    $('#post-1805 .data-svg .swap.objective.one .item span').on('click', function(){        
                        var parentSwap = $(this).parent().data('swap');
                        var activeSwap = $('#ia-data-1805 .swap .item[data-swap="1"]');
                        var dataDimension = $(this).parent().data('dimension');
                        console.log(dataDimension)
                        if (parentSwap = "0") {
                            activeSwap.attr('data-swap', '0')
                            $(this).parent().attr('data-swap', '1')
                        };
                        $('#post-1805 .data-disclosure [data-dimension]:not(.hide)').css('opacity', 0);
                        $('#post-1805 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                        $('#post-1805 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#post-1805 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);



                        if (dataDimension == "A") {
                            

                            d3.json(dataSourceDir+"js/data-svg/iac-ce-voter-participation.json", function(error,data) {
                                 
                                 bars.update(data,'.iac-ce-engaged-citizenry .graph');
                                 /*setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                              
                                  $('#ia-data-1805 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2010";
                                      } else {
                                        return "2012";
                                      }
                                    })
                                  //$('.iac-ce-engaged-citizenry [data-label="US"] .bar:nth-child(odd) .value .marker').delay(2).attr('data-label','N/A')
                                 // $('.iac-ce-engaged-citizenry [data-label="US"] .bar:nth-child(odd) .value').delay(1).css('height','0')
                                  $('.iac-ce-engaged-citizenry .bar:nth-child(even) .value').css('opacity',1)
                              })

                        };

                        if (dataDimension == "B") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ce-volunteer-rates.json", function(error,data) {

                                console.log(data)

                                /*[
                                  {
                                      "label": "CHI",
                                      "values": ["1.4%", "0.9%"]
                                  },
                                ]*/
                                

                            /*    for (var i = 0; i < data.length; i++) {
                                  dy1 = "2011";
                                  dy2 = "2013";
                                  console.log(data.indexOf())
                                  var unemploymentRateData = []
                                  var unemploymentRate = {}
                                  unemploymentRate["label"] = String(data[i]["County"])
                                  unemploymentRate["values"] = [String(data[i][dy1]),String(data[i][dy2])]
                                  unemploymentRateData.push(unemploymentRate)
                                  console.log(unemploymentRateData)
                                };*/


                                 
                                bars.update(data,'.iac-ce-engaged-citizenry .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */

                                  $('#ia-data-1805 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2010";
                                      } else {
                                        return "2012";
                                      }
                                    })
                                  $('.iac-ce-engaged-citizenry .bar:nth-child(even) .value').css('opacity',1)
                              })
                        };

                        if (dataDimension == "C") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ce-charitable.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-ce-engaged-citizenry .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                  $('#ia-data-1805 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2008";
                                      } else {
                                        return "2011";
                                      }
                                    })
                                  $('.iac-ce-engaged-citizenry .bar:nth-child(even) .value').css('opacity',1)
                              })
                        };
                        if (dataDimension == "D") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ce-volunteer-25-34.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-ce-engaged-citizenry .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                 // $('.iac-ce-engaged-citizenry [data-label="US"] .bar:nth-child(odd) .value .marker').delay(2).attr('data-label','N/A')
                                 // $('.iac-ce-engaged-citizenry [data-label="US"] .bar:nth-child(odd) .value').delay(1).css('height','0')
                                  $('.iac-ce-engaged-citizenry .bar:nth-child(even) .value').css('opacity',0)
                                  $('#ia-data-1805 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "09-'12 Avg.";
                                      } else {
                                        return "";
                                      }
                                    })
                              })
                        };

                    });
                      
            });

        
          
                 


        } // else
 }); // d3 json civic participation

},{triggerOnce:true});//Waypoints
 