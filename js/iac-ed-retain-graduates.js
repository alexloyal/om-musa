// Waypoints
//post-1730 is MIAMI-DADE ATTRACTS & RETAINS COLLEGE GRADUATES
$('#ia-data-1730 .data-disclosure [data-dimension="B"]').css('opacity',0);
$('#ia-data-1730 .data-disclosure [data-dimension="B"]').addClass('hide');

$('[data-trigger="20"]').waypoint(function(down) {
  

  $('#ia-data-1730 .swap.objective.one').css('opacity','1');
  $('#ia-data-1730 .data-disclosure').css('opacity','1');
  $('#ia-data-1730 .year-labels').css('opacity','1');

//

 
var dataSourceDir = base + '/wp-content/themes/om-musa/';


d3.json(dataSourceDir+"js/data-svg/iac-ed-education-migration-zero.json", function(error,migrationZeroData) {

            if (error) {
                // If error is not null, something went wrong.
                console.log(error);
                //Log the error.
            } else {
            
          console.log(migrationZeroData)         
                
            retainsBars = new BarGraphDollars(migrationZeroData,'#ia-data-1730 .graph');
            $('#ia-data-1730 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2012";
                                      }  else {
                                        return "2013";
                                      } 

                                    })

            $(document).ready(function(){
                
                d3.json(dataSourceDir+"js/data-svg/iac-ed-education-migration.json", function(error,data) {
                                 
                                    retainsBars.update(data);

                              })

                    $('#ia-data-1730 .swap.objective.one .item span').on('click', function(e){        
                        var parentSwap = $(this).parent().data('swap');
                        var activeSwap = $('#ia-data-1730 .swap .item[data-swap="1"]');
                        var dataDimension = $(this).parent().data('dimension');
                        console.log(dataDimension)
                        if (parentSwap = "0") {
                            activeSwap.attr('data-swap', '0')
                            $(this).parent().attr('data-swap', '1')
                        };
                        $('#ia-data-1730 .data-disclosure [data-dimension]:not(.hide)').css('opacity',0);
                        $('#ia-data-1730 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                        $('#ia-data-1730 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#ia-data-1730 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

                        if (dataDimension == "A") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ed-education-migration.json", function(error,data) {
                                 
                                 retainsBars.update(data,'#ia-data-1730 .graph');
                                 /*setTimeout(function(){
                                        retainsBars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                $('#ia-data-1730 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2011";
                                      }  else {
                                        return "2013";
                                      } 

                                    })
                              })
                        };

                        if (dataDimension == "B") {
                            console.log(e)
                            d3.json(dataSourceDir+"js/data-svg/iac-ed-college-educated.json", function(error,data) {
                                 
                                  retainsBars.update(data,'#ia-data-1730 .graph');
                                 /* setTimeout(function(){
                                        retainsBars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                $('#ia-data-1730 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2011";
                                      }   else {
                                        return "2013";
                                      } 

                                    })

                              })
                        };


                    });
                      
            });

        

                 


        } // else
 }); // d3  

},{triggerOnce:true});//Waypoints
 