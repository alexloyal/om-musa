// Waypoints
//post-1728 is MIAMI-DADE CHILDREN HAVE ACCESS TO A QUALITY EDUCATION

$('#post-1728 .data-disclosure [data-dimension="B"],#post-1728 .data-disclosure [data-dimension="C"],#post-1728 .data-disclosure [data-dimension="D"]').css('opacity', 0);
$('#post-1728 .data-disclosure [data-dimension="B"],#post-1728 .data-disclosure [data-dimension="C"],#post-1728 .data-disclosure [data-dimension="D"]').addClass('hide');

$('[data-trigger="10"]').waypoint(function(down) {
  
  $('#post-1728 .swap.objective.one').css('opacity','1');
  $('#post-1728 .data-disclosure').css('opacity','1');
  $('#post-1728 .year-labels').css('opacity','1');



//

 
var dataSourceDir = base + '/wp-content/themes/om-musa/';


d3.json(dataSourceDir+"js/data-svg/iac-ed-student-expenditures-zero.json", function(error,zeroData) {

            if (error) {
                // If error is not null, something went wrong.
                console.log(error);
                //Log the error.
            } else {
                   

            bars = new BarGraphDollars(zeroData,'#post-1728 .iac-ed-quality-education .graph');

            $('#ia-data-1728 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2008 - 2009";
                                      } else {
                                        return "2010 - 2011";
                                      }
                                    })

            $(document).ready(function(){
                
                d3.json(dataSourceDir+"js/data-svg/iac-ed-student-expenditures.json", function(error,data) {
                                 
                                    bars.update(data);

                              })

                    $('#post-1728 .data-svg .iac-ed-quality-education .swap.objective.one .item span').on('click', function(){        
                        var parentSwap = $(this).parent().data('swap');
                        var activeSwap = $('#post-1728 .swap .item[data-swap="1"]');
                        var dataDimension = $(this).parent().data('dimension');
                        console.log(dataDimension)
                        if (parentSwap = "0") {
                            activeSwap.attr('data-swap', '0')
                            $(this).parent().attr('data-swap', '1')
                        };
                       


                        $('#ia-data-1728 .data-disclosure [data-dimension]:not(.hide)').css('opacity',0);
                        $('#ia-data-1728 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                        $('#ia-data-1728 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#ia-data-1728 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);



                        if (dataDimension == "A") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ed-student-expenditures.json", function(error,data) {
                                 
                                 bars.update(data,'#post-1728 .iac-ed-quality-education .graph');
                                 /*setTimeout(function(){
                                        bars.update(data,'#post-1728 .iac-ed-quality-education .graph');
                                    }, 1000); */
                              })
                            $('#ia-data-1728 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2008 - 2009";
                                      } else {
                                        return "2010 - 2011";
                                      }
                                    })

                        };

                        if (dataDimension == "B") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ed-teacher-quality.json", function(error,data) {
                                 
                                  bars.update(data,'#post-1728 .iac-ed-quality-education .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'#post-1728 .iac-ed-quality-education .graph');
                                    }, 1000); */
                                  $('#ia-data-1728 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2010 - 2011 ";
                                      } else {
                                        return "2012 - 2013 ";
                                      }
                                    })
                              })
                        };
                        if (dataDimension == "C") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ed-schools-a-b.json", function(error,data) {
                                 
                                  bars.update(data,'#post-1728 .iac-ed-quality-education .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'#post-1728 .iac-ed-quality-education .graph');
                                    }, 1000); */
                                   $('#ia-data-1728 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2010 - 2011 ";
                                      } else {
                                        return "2012 - 2013 ";
                                      }
                                    })
                              })
                        };
                        if (dataDimension == "D") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ed-magnet-enrollment.json", function(error,data) {
                                 
                                  bars.update(data,'#post-1728 .iac-ed-quality-education .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'#post-1728 .iac-ed-quality-education .graph');
                                    }, 1000); */

                             $('#ia-data-1728 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2008 - 2009";
                                      } else {
                                        return "2010 - 2011";
                                      }
                                    })
                              })
                        };

                    });
                      
            });

        

                 


        } // else
 }); // d3 json civic participation

},{triggerOnce:true});//Waypoints
 