var radialgraphsB = [];

// post-1692 is MIAMI-DADE RESIDENTS ARE HEALTHY

$('#post-1692 .data-disclosure [data-dimension="B"],#post-1692 .data-disclosure [data-dimension="C"]').css('opacity', 0);
$('#post-1692 .data-disclosure [data-dimension="B"],#post-1692 .data-disclosure [data-dimension="C"]').addClass('hide');

$('[data-trigger="30"]').waypoint(function(down){

	$('#post-1692 .swap.objective.two').css('opacity','1');
	$('#post-1692 .data-disclosure').css('opacity','1');
	$('#post-1692 .year-labels').css('opacity','1');





	var dataSourceDir = base + '/wp-content/themes/om-musa/';



	d3.tsv(dataSourceDir+'js/data-svg/iac-hs-heart-disease.tsv', null, function(heartDiseaseData) {

		for(var i = 0; i < heartDiseaseData.length; i++) {

			var ry1 = String(Object.keys(heartDiseaseData[i])[1])
			var ry2 = String(Object.keys(heartDiseaseData[i])[2])
			var county = heartDiseaseData[i]["County"]
			var cityName = $('#post-1695 .circles .radial[data-city="'+county+'"]')
			var dataYear = ry1;

			var label = county.toLowerCase().replace(/ /g, '-');

			$('#post-1692 .city-group .radial').attr('data-year', function(){
				var index = $(this).index();
				if (  index < 1 ) {
					return ry1;
				} else {
					return ry2;
				}
			})

			if(!radialgraphsB[0]) { radialgraphsB[0] = {} }
			if(!radialgraphsB[1]) { radialgraphsB[1] = {} }

			radialgraphsB[0][county] = new RadialGraph([heartDiseaseData[i][ry1]], '#post-1692 [data-year="'+ry1+'"][data-city="'+county+'"]', ''+label+'', 420);
			radialgraphsB[1][county] = new RadialGraph([heartDiseaseData[i][ry2]], '#post-1692 [data-year="'+ry2+'"][data-city="'+county+'"]', ''+label+'', 420);

			$("#ia-data-1692 .year-labels .item.green").html(ry1);
			$("#ia-data-1692 .year-labels .item.purp").html(ry2);
		}

		$('#post-1692 .city-group').css('opacity', '1');


		$('#post-1692 .data-svg .swap.objective.two .item span').on('click', function(){


				var parentSwap = $(this).parent().data('swap');
                var activeSwap = $('#post-1692 .swap .item[data-swap="1"]');

				var dataDimension = $(this).parent().data('dimension');

				if (parentSwap = "0") {
	                activeSwap.attr('data-swap', '0')
	                $(this).parent().attr('data-swap', '1')
	            };

	            		$('#post-1692 .data-disclosure [data-dimension]:not(.hide)').css('opacity', 0);
	            		$('#post-1692 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
						$('#post-1692 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#post-1692 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

				switch(dataDimension) {

                	case "A":
						for(var i = 0; i < heartDiseaseData.length; i++) {

							var ry1 = String(Object.keys(heartDiseaseData[i])[1])
							var ry2 = String(Object.keys(heartDiseaseData[i])[2])
							var county = heartDiseaseData[i]["County"]
							var label = county.toLowerCase().replace(/ /g, '-');

							radialgraphsB[0][county].update([heartDiseaseData[i][ry1]], 420);
							radialgraphsB[1][county].update([heartDiseaseData[i][ry2]], 420);

							$("#ia-data-1692 .year-labels .item.green").html(ry1);
							$("#ia-data-1692 .year-labels .item.purp").html(ry2);
						}
                		break;


                	case "B":

                		d3.tsv(dataSourceDir+'js/data-svg/iac-hs-stroke.tsv', function(strokeData) {
                			
                			for(var i = 0; i < strokeData.length; i++)  {
                				
                				var ry1 = "2006-2008 Average";
  								var ry2 = "2008-2010 Average";
                				var county = strokeData[i]["County"] == "New York" ? "NYC" : strokeData[i]["County"];
                				var label = county.toLowerCase().replace(/ /g, '-');
                				 
								radialgraphsB[0][county].update([strokeData[i][ry1]]);
								radialgraphsB[1][county].update([strokeData[i][ry2]]);

								$("#ia-data-1692 .year-labels .item.green").html(ry1);
								$("#ia-data-1692 .year-labels .item.purp").html(ry2);
                			};

                		})
                	break;
                	case "C":

                		d3.tsv(dataSourceDir+'js/data-svg/iac-hs-hiv.tsv', function(data) {

                			for (var i = data.length - 1; i >= 0; i--) {
								console.log(data.length)
								var ry1 = "2008";
								var ry2 = "2010";
								var county = data[i]["County"] == "New York" ? "NYC" : data[i]["County"];
								var label = county.toLowerCase().replace(/ /g, '-');
								radialgraphsB[0][county].update([data[i][ry1]]);
								radialgraphsB[1][county].update([data[i][ry2]]);

								$("#ia-data-1692 .year-labels .item.green").html(ry1);
								$("#ia-data-1692 .year-labels .item.purp").html(ry2);
								$('#ia-data-1692 .radial.first .radial-graph[data-id="sd"] svg g .label.outer').delay(2).html('N/A')

                			};

                		})
                	break;
                }

		 });



	}, function(err, rows) {
	  console.log(err)
	});

},{triggerOnce:true}); // Waypoints
