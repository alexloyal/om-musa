function whatAboutHousing(){

var cityName = "Miami";
constructionCosts = [];
constructionCostsByCity = {};
homeSaleDataByCity = {};
SFHmedianSalePrice = [];


   $(document).ready(function(){

                $('.what-about-housing .city.names [data-city]').on('click', function(){
                    
                     

                    console.log(constructionCosts);

                    constructionCosts = [];
                    constructionCostsByCity = {};
                    homeSaleDataByCity = {};
                    SFHmedianSalePrice = [];
                    
                    var cityName =  $(this).data('city');


                    renderConstructionCosts();
                    renderHomeSaleValue(); 
                    
                    console.log(cityName)
 
              
                    
                  }); 
              
 
                
                
             });


   function renderConstructionCosts(){
    d3.csv(dataSourceDir+'js/data-svg/avg-construction-costs.csv', function(error, constructionData) {
               

          if (error) {
                   // If error is not null, something went wrong.

                   console.log(error);
                    //Log the error.

                    } 
              else 

              { 
                for (var i = constructionData.length - 1; i >= 0; i--) {
                          
                          var location = constructionData[i]["County"];

                          
                          var constructionDataPerYear = [constructionData[i]["2011"],constructionData[i]["2013"]];

                          

                               constructionCostsByCity.label = location;
                               constructionCostsByCity.values = constructionDataPerYear;
                             
                if (location.indexOf(cityName) >= 0) {  
                            
                        constructionCosts.push(constructionCostsByCity)  
                 }

              };



       console.log(constructionCosts);

       BarGraphDollars2(constructionCosts, '.construction-cost');
                   
      } // Else




    }) // d3


  }; // renderConstructionCosts

renderConstructionCosts();

function renderHomeSaleValue(){
    d3.csv(dataSourceDir+'js/data-svg/home-sale-value.csv', function(error, homesaleData) {


                     
                     if(error) {
                      console.log(error)
                     } else {



                  
                          console.log(homesaleData)
                    
                   
                    

                    

                          
                              for (var i = homesaleData.length - 1; i >= 0; i--) {
                              
                              var location = homesaleData[i]["County"];

                              var homeSaleDataPerYear = [homesaleData[i]["2011"],homesaleData[i]["2013"]];

                              

                                  homeSaleDataByCity.label = location;
                                  homeSaleDataByCity.values = homeSaleDataPerYear;

                              
                             if (location.indexOf(cityName) >= 0) {

                                          SFHmedianSalePrice.push(homeSaleDataByCity)

                                        };

                                BarGraphDollars2(SFHmedianSalePrice, '.median-sale-price');



                             }; 

                          
                      

                            

 
           
              
            
                            
                   
                  
                 

                     } // else 
    })// d3

 
};

              renderHomeSaleValue(); 






}

         
console.log(parseFloat('504049'))

whatAboutHousing();

var BarGraphDollars2 = function (data, elementSelector) {

    /**

    Data is passed in as arrays of groups. If you don't have a grouped bar chart, you can pass
    an array of 1 (for a group of 1).

    Data input format:

    [
        {
            label: "City Name",
            values: ["10.2%", "4.3%"]
        }
        ...etc
    ]

    Then for each group, you get markup like this:

    <div class="group" data-label="City Name">

        <div class="bars">

            <div class="bar">
                <div class="value" style="height: NN%">
                    <div class="marker" data-label="10.2%"
                </div>
            </div>

            <div class="bar">
                <div class="value" style="height: NN%">
                    <div class="marker" data-label="4.3%"
                </div>
            </div>

        </div>
    </div>


    **/

    var graph = this;

    this.elementSelector = elementSelector;

    $(elementSelector).addClass("bar-graph");

    this.data = [];

    

    this.update = function(data) {

        this.max = d3.max(data, function (d) {
            return d3.max(d.values, function (d) {
                return parseFloat(d);
            })
        })

        this.min = d3.min(data, function (d) {
            return d3.min(d.values, function (d) {
                return parseFloat(d);
            });
        })

        var width = d3.scale.linear().domain([this.min/4, this.max]).range([0, 10000000]);

        var groups = d3.select(this.elementSelector).selectAll(".group").data(data);

        groups.call(function() {
                this.select(".bars")
                    .selectAll(".bar")
                    .data(function (d) {
                        return d.values;
                    })
                    .select(".value")
                    .style("width", function (d) {
                        return width( parseFloat(d) );
                    })
                    .select(".marker")
                    .attr("data-label", function (d) {
                        return d;
                    })


            })


        groups.enter()
            .append("div")
            .attr("class", "group")
            .attr("data-label", function (d) {
                return d.label
            })
            .call(function() {
                this.append("div")
                    .attr("class", "bars")
                    .selectAll(".bars .bar")
                    .data(function(d){
                        return d.values;
                    })
                    .enter()
                    .append("div")
                    .attr("class", "bar")
                    .append("div")
                    .attr("class", "value")
                    .style("width", function (d) {
                        return width( parseFloat(d) );
                    })
                    .append("div")
                    .attr("class", "marker")
                    .attr("data-label", function (d) {
                        return d;
                    })

                this.append("h4")
                    .attr("class", "label")
                    .text(function(d) {
                        return d.label;
                    })
            })

    }

    this.update(data);


}



/*



*/