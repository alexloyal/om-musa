var radialgraphs = {};

// post-1729 is MIAMI-DADE STUDENTS OF ALL AGES THRIVE ACADEMICALLY

$('#post-1729 .data-disclosure [data-dimension="B"],#post-1729 .data-disclosure [data-dimension="C"],#post-1729 .data-disclosure [data-dimension="D"],#post-1729 .data-disclosure [data-dimension="E"]').addClass('hide');

$('#post-1729').waypoint(function(down){

	 $('#post-1729 .swap.objective.two').css('opacity','1');
 	 $('#post-1729 .data-disclosure').css('opacity','1');
 	 $('#post-1729 .year-labels').css('opacity','1');



	var dataSourceDir = base + '/wp-content/themes/om-musa/';



	d3.tsv(dataSourceDir+'js/data-svg/iac-ed-math-reading-proficiency.tsv', null, function(burdendata) {

		for(var i = 0; i < burdendata.length; i++) {

			var ry1 = String(Object.keys(burdendata[i])[0])
			var ry2 = String(Object.keys(burdendata[i])[1])
			var county = burdendata[i]["County"]
			var cityName = $('.circles .radial[data-city="'+county+'"]')
			var dataYear = ry1;

			var label = county.toLowerCase().replace(/ /g, '-');

			$('.city-group .radial').attr('data-year', function(){
				var index = $(this).index();
				if (  index < 1 ) {
					return ry1;
				} else {
					return ry2;
				}
			})

			if(!radialgraphs[ry1]) { radialgraphs[ry1] = {} }
			if(!radialgraphs[ry2]) { radialgraphs[ry2] = {} }

			radialgraphs[ry1][county] = new RadialGraph([burdendata[i][ry1]], '[data-year="'+ry1+'"][data-city="'+county+'"]', ''+label+'');
			radialgraphs[ry2][county] = new RadialGraph([burdendata[i][ry2]], '[data-year="'+ry2+'"][data-city="'+county+'"]', ''+label+'');
		}

		$('.city-group').css('opacity', '1');


		$('.data-svg .swap.objective.two .item span').on('click', function(){


				var parentSwap = $(this).parent().data('swap');
                var activeSwap = $('.swap .item[data-swap="1"]');

				var dataDimension = $(this).parent().data('dimension');

				if (parentSwap = "0") {
	                activeSwap.attr('data-swap', '0')
	                $(this).parent().attr('data-swap', '1')
	            };

	            $('#post-1729 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                $('#post-1729 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');

				switch(dataDimension) {

                	case "A":
						for(var i = 0; i < burdendata.length; i++) {

							var ry1 = String(Object.keys(burdendata[i])[0])
							var ry2 = String(Object.keys(burdendata[i])[1])
							var county = burdendata[i]["County"]
							var label = county.toLowerCase().replace(/ /g, '-');

							radialgraphs[ry1][county].update([burdendata[i][ry1]]);
							radialgraphs[ry2][county].update([burdendata[i][ry2]]);
						}
                		break;

					case "B":
                		d3.tsv(dataSourceDir+'js/data-svg/iac-ed-hs-graduation.tsv', function(colidata) {
                			for (var i = colidata.length - 1; i >= 0; i--) {

                				var ry1 = parseInt(Object.keys(colidata[i])[0])-1;
  								var ry2 = parseInt(Object.keys(colidata[i])[1])-1;
                				var county = colidata[i]["County"];

								radialgraphs[ry1][county].update([colidata[i][ry1+1]], 250);
								radialgraphs[ry2][county].update([colidata[i][ry2+1]], 250);
                			};

                		})
                		break;
                	case "C":

                		d3.tsv(dataSourceDir+'js/data-svg/iac-ed-college-readiness.tsv', function(data) {

                			for (var i = data.length - 1; i >= 0; i--) {

                				var ry1 = String(Object.keys(data[i])[0])
  								var ry2 = String(Object.keys(data[i])[1])
                				var county = data[i]["County"]

								radialgraphs[ry1][county].update([data[i][ry1]]);
								radialgraphs[ry2][county].update([data[i][ry2]]);
                			};

                		})
                	break;
                	case "D":

                		d3.tsv(dataSourceDir+'js/data-svg/iac-ed-attainment.tsv', function(data) {

                			for (var i = data.length - 1; i >= 0; i--) {

                				var ry1 = parseInt(Object.keys(data[i])[0])-1;
  								var ry2 = parseInt(Object.keys(data[i])[1])-1;
                				var county = data[i]["County"] == "New York" ? "NYC" : data[i]["County"];

								radialgraphs[ry1][county].update([data[i][ry1+1]], 380000);
								radialgraphs[ry2][county].update([data[i][ry2+1]], 380000);
                			};

                		})
                	break;
                	case "E":

                		d3.tsv(dataSourceDir+'js/data-svg/iac-post-secondary.tsv', function(data) {

                			for (var i = data.length - 1; i >= 0; i--) {
								var ry1 = parseInt(Object.keys(data[i])[0])-1;
								var ry2 = parseInt(Object.keys(data[i])[1])-1;
								var county = data[i]["County"] == "New York" ? "NYC" : data[i]["County"];

								radialgraphs[ry1][county].update([data[i][ry1+1]], 470000);
								radialgraphs[ry2][county].update([data[i][ry2+1]], 470000);
                			};

                		})
                	break;
                }

		 });

	

	}, function(err, rows) {
	  console.log(err)
	});

},{triggerOnce:true}); // Waypoints
