/* Data Functions for Generating D3 SVGs */




// d3.select('#svg');

// How Mobile is Miami post=1502

/*

DEFAULT


 


function default(){


    var dataset; // GLoabl variable
    var dataSource = 'DATA SOURCE';

    d3.csv(dataSource, function(error,data) {
        
                    if (error) {
                // If error is not null, something went wrong.

                    console.log(error);
                //Log the error.

                    } else {
                // If no error, the file loaded correctly.
                    
                    console.log(data);
                    dataset = data;
                    w = 500;
                    h = 500;
                    var svg = d3.select("#data-svg")
                            .append("svg")
                            .attr('width', w)
                            .attr('height', h);

                    svg.selectAll("circle")
                            .data(dataset)
                            .enter()
                            // More options
                    


                // 

                    };
    });


};

default();

*/
 


function transportationData(){


    var dataset; // GLoabl variable
    var dataSource = '/ourmiami_musa/wp-content/themes/om-musa/js/data-svg/auto-commute-time.csv';

    

    d3.csv(dataSource, function(error,data) {
        
                    if (error) {
                // If error is not null, something went wrong.

                    console.log(error);
                //Log the error.

                    } else {
                // If no error, the file loaded correctly.
                    
                    console.log(data[0].y2010);
                    dataset = data;
                    
                    var svg = d3.select("#data-svg")
                            .append("svg")
                            .attr('class', 'col-md-6')
                             

                            

                    svg.selectAll("g")
                            .data(dataset)
                            .enter()
                            .append("g") 
                            .attr('data-circle', function(d){
                                return (d.y2010*100);
                            })
                            ;
                    

                            var duration   = 500,
                            transition = 200;

                            drawCircleChart(
                              'g',
                              $('g').data('circle'),
                              290,
                              290,
                              ".35em"
                            );

                            function drawCircleChart(element, percent, width, height, text_y) {
                              width = typeof width !== 'undefined' ? width : 290;
                              height = typeof height !== 'undefined' ? height : 290;
                              text_y = typeof text_y !== 'undefined' ? text_y : "-.10em";

                              var dataset = {
                                    lower: calcPercent(0),
                                    upper: calcPercent(percent)
                                  },
                                  radius = Math.min(width, height) / 2,
                                  pie = d3.layout.pie().sort(null),
                                  format = d3.format(".0%");

                              var arc = d3.svg.arc()
                                    .innerRadius(radius - 20)
                                    .outerRadius(radius);

                              var svg = d3.select(element).append("svg")
                                    .attr("width", width)
                                    .attr("height", height)
                                    .append("g")
                                    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

                              var path = svg.selectAll("path")
                                    .data(pie(dataset.lower))
                                    .enter().append("path")
                                    .attr("class", function(d, i) { return "color" + i })
                                    .attr("d", arc)
                                    .each(function(d) { this._current = d; }); // store the initial values

                              var text = svg.append("text")
                                    .attr("text-anchor", "middle")
                                    .attr("dy", text_y);

                              if (typeof(percent) === "string") {
                                text.text(percent);
                              }
                              else {
                                var progress = 0;
                                var timeout = setTimeout(function () {
                                  clearTimeout(timeout);
                                  path = path.data(pie(dataset.upper)); // update the data
                                  path.transition().duration(duration).attrTween("d", function (a) {
                                    // Store the displayed angles in _current.
                                    // Then, interpolate from _current to the new angles.
                                    // During the transition, _current is updated in-place by d3.interpolate.
                                    var i  = d3.interpolate(this._current, a);
                                    var i2 = d3.interpolate(progress, percent)
                                    this._current = i(0);
                                    return function(t) {
                                      text.text( format(i2(t) / 100) );
                                      return arc(i(t));
                                    };
                                  }); // redraw the arcs
                                }, 200);
                              }
                            };

                            function calcPercent(percent) {
                              return [percent, 100-percent];
                            };
                    

                    };
    });


};

transportationData();