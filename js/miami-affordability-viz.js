d3.csv("js/data-svg/education-unemployment-rate.csv", function(error,data) {

    var newData = []


    for(var i = 0; i < data.length; i++) {

        //FAKING THIS OBJECT SINCE I DON'T HAVE EMPLOYMENT GROWTH
        var group = {
            label: data[i].County,
            values: [ data[i]["2011"], data[i]["2012"] ]
        }

        newData.push(group)
    }

    var graph = new BarGraph(newData, 'section[data-graph="employment growth v. unemployment rate"] .graph');


    //REMOVE THIS FUNCTION AND REPLACE IT WITH THE TOGGLE LOGIC, UPDATE FUNCTION REFERENCE BELOW (NEW ARRAY OF GROUPS)
    setInterval(function(graph) {
        var newData = []


        for(var i = 0; i < data.length; i++) {

            var group = {
                label: data[i].County,
                values: [ Math.round(Math.random()*18) + "%", Math.round(Math.random()*18) + "%" ]
            }

            newData.push(group)
        }

        graph.update(newData);

    }, 3000, graph)


});
