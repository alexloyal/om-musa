function drawDudes(){
                      
                  

                  var cityPercentTrigger = $('.city-circle.active').data('city');              
                  d3.select('#dudes')
                                        
                                       
                  
                   var cityDudes = (d3.select('#percent-'+cityPercentTrigger).attr('data-percent')/5);
                   // console.log(cityDudes)

                    for (var i = cityDudes- 1; i >= 0; i--) {
                    d3.select('#dudes').append('span').attr('class', 'dude-icon active');
                    };

                    d3.select('#dudes').append('span').attr('id', 'dude-overlay')
                                       .call(function(){
                                        this.append('span').attr('class', 'dude-icon active overlap')
                                        this.append('span').attr('class', 'dude-icon overlap')
                                       })
                      
                     for (var i = (20-cityDudes)- 1; i >= 0; i--) {
                    d3.select('#dudes').append('span').attr('class', 'dude-icon');
                    };                                   

                    $('#dude-overlay').css('transform', 'translate(-1px)')  
}

                        

var dataYear = $('.years [data-year].active').data('year');

                    
                    


function investmentPaysOff(){


          
                   

          
          var invDataset; // Global variable
          var invDataSource = base + '/wp-content/themes/om-musa/js/data-svg/transit-operational-funding.csv';
          var workDataset; // Global variable
          var workDataSource = base + '/wp-content/themes/om-musa/js/data-svg/public-transit-to-work.csv';

          
        
    

    d3.csv(invDataSource, function(error,data) {
        
                    if (error) {
                // If error is not null, something went wrong.

                    console.log(error);
                //Log the error.

                    } else {
                // If no error, the file loaded correctly.
                     
                     console.log(data)

                    var margin = { top: 10, right: 10, bottom: 10, left: 10 };
                    var height = 600 - margin.left - margin.right,
                    width = 900 - margin.top - margin.bottom,
                     
                    padding = 20;

                    w= "100%";
                    h= "100%";
                    invDataset = data;
                    
                    
                    var svg = d3.select(".post-1465 .data-svg")
                            .append("svg")
                            .attr('class', 'col-md-12 svg-content')
                            .attr('id','data-circle-box' )
                            .attr('height', h)
                            .attr('width', w)
                            .attr('viewBox', '0 0 '+width+' '+height+'')
                            .attr('preserveAspectRatio', 'xMinYMin meet');
                    };


                    
                    var toggleColor = (function(){
                    var currentColor = "city-circle";
                      
                      return function(){
                          currentColor = currentColor == "city-circle" ? "city-circle active" : "city-circle";
                          d3.select('.active').attr("class", 'city-circle');
                          d3.select(this).attr("class", currentColor);
                      }
                  });

                  var scale = d3.scale.linear()
                                .domain([0,800])
                                .range([0,620]);
                                
                  
                  var dollars = d3.select('#dollarSliderValues')
                                .selectAll('div.investment-dollars')
                                .data(invDataset)
                                .enter()
                                .append('div')
                                .attr('class', 'investment-dollars')
                                .attr('id', function(d){
                                  return "money-"+(d.City)
                                })
                                
                                
                   
                    dollars.attr('data-value', function(d){
                                  if (dataYear == 2012) {
                                      return "$"+(d.y2012)   
                                  } else {
                                      return "$"+(d.y2010)   
                                  };
                                  
                                })
                                .style('left', function(d){

                                  // return 'translate(' +(d.y2010)*0.6+ 'px,0)'
                                  //return (d.y2010)*1.2+'px'
                                  if (dataYear == 2012){
                                    return scale(d.y2012)+'px';
                                  }
                                    else {
                                    return scale(d.y2010)+'px';
                                      
                                    }

                                  
                                });
                


                   

                    
                     
                    // Draws the dots on the map.
                     

                  var cityDots = d3.select('#usa')
                      .selectAll('g.city-dot')
                      .data(invDataset)
                      .enter()
                      .append('g')
                      .attr('class', function(d){
                        return 'city-dot '+(d.City)
                      })
                       
                      .attr('id', function(d){
                                return "dot-"+(d.City);
                            });

                    d3.select('#usa #dot-CHI')
                            .attr('transform', 'translate(520,160)')
                            
                            ;
                    d3.select('#usa #dot-HOU')
                            .attr('transform', 'translate(420,380)')
                            ;
                    d3.select('#usa #dot-MIA')
                            .attr('transform', 'translate(618,430)')
                            ;
                    d3.select('#usa #dot-NYC')
                            .attr('transform', 'translate(680,210)')
                            ; 
                    d3.select('#usa #dot-SD')
                            .attr('transform', 'translate(110,280)')
                            ; 

                  



                    cityDots.append("circle")
                            .attr('class','city-circle' )
                            .attr('stroke-width', '9')
                            .attr('r', '18')
                            .attr('data-city', function(d){
                                return (d.City);
                            })
                            .on('click',function(d){
                              
                              d3.selectAll('.city-circle.active').attr('class','city-circle' );
                              d3.selectAll('.investment-dollars.active').attr('class', 'investment-dollars');
                              d3.selectAll('.animated-percentage.active').attr('class', 'animated-percentage');
                              d3.selectAll('.city-dot.active').attr('class', 'city-dot');
                              d3.selectAll('#dudes .dude-icon').remove();
                              d3.selectAll('#dude-overlay').remove();
                              d3.select('#dot-'+d.City).attr('class','city-dot active' );
                              d3.select('#money-'+d.City).attr('class','investment-dollars active' );
                              d3.select('#percent-'+d.City).attr('class','animated-percentage active' );
                              d3.select(this).attr('class','city-circle active' );
                              d3.select('#dollarSlider .dot').style('left', function(data){
                                  
                                  
                                    
                                    // return 'translate(' +(d.y2010)*1.2+ 'px,0)'
                                    // return (d.y2010)*1.3+'px'
                                    var moneyPosition = $('#money-'+d.City).position();
                                    return moneyPosition.left+12+'px';
                              })
                              drawDudes();
                  
                              $('#dude-overlay').css('transform', 'translate(-1px)')  
                  
                  

                            });
                    cityDots.append('text')
                            .attr('class', 'city-label')
                            .attr('id', function(d){return "city-label-"+d.City})
                            .text(function(d){return d.City})
                            ;
                    d3.select('#city-label-HOU')
                            .attr('dx', function(d){
                              return 0-30
                            })
                            .attr('dy', function(d){
                              return 0-40
                            });
                    d3.select('#city-label-NYC')
                            .attr('dx', function(d){
                              return 30
                            })
                            .attr('dy', function(d){
                              return 30
                            });
                    d3.select('#city-label-MIA')
                            .attr('dx', function(d){
                              return 30
                            })
                            .attr('dy', function(d){
                              return 40
                            });
                    d3.select('#city-label-CHI')
                            .attr('dx', function(d){
                              return 0-90
                            })
                            .attr('dy', function(d){
                              return 5
                            });
                    d3.select('#city-label-SD')
                            .attr('dx', function(d){
                              return 0-60
                            })
                            .attr('dy', function(d){
                              return 5
                            });



              d3.csv(workDataSource, function(error,data){

              if (error) {

                console.log(error);
              } else { 

                 workDataset = data;

                  var commuters = d3.select('#commuters')
                                      .append('div')
                                      .attr('width', '200')
                                      .attr('height', '50')
                                      .selectAll('.animated-percentage')
                                      .data(workDataset)
                                      .enter()
                                      .append('div')
                                      .attr('class','animated-percentage' )
                                      .attr('id', function(d){
                                        return 'percent-'+(d.County);
                                      });

                       commuters.attr('data-percent', function(d){
                                        if (dataYear == 2012) {
                                          return (d.y2012)
                                        }
                                          else {
                                            return (d.y2010)
                                          };
                                        
                                      })
                                .attr('data-value',function(d){
                                        if (dataYear == 2012) {
                                          return (d.y2012)+'%' 
                                        } 
                                          else {
                                            return (d.y2010)+'%' 
                                          };
                                      })               


              
              };
              
              

              miaSlide = (invDataset[2].y2012);
              
              d3.select('#dot-MIA').attr('class', function(data){return 'city-dot active '+(data.City)});
              d3.select('#dot-MIA .city-circle').attr('class', 'city-circle active');
              d3.select('#percent-MIA').attr('class', 'animated-percentage active');
              d3.select('#money-MIA').attr('class', 'investment-dollars active');
              d3.select('#dollarSlider .dot').style('left', function(){

                                    
                                    // return 'translate(' +(d.y2010)*1.2+ 'px,0)'
                                    // return (d.y2010)*1.3+'px'
                                    
                                    return (miaSlide*1.2)+'px';

                                    
                              });

                    

                    for (var i = data.length - 1; i >= 0; i--) {
                    //  console.log(data[i].County);
                    //  console.log(data[i].y2010)

                    };

                    

                    drawDudes();

                     $('.years [data-year]').on('click', function(){

                        $('#years-2 .trigger .dot').toggleClass('shift');

                      if ($(this).hasClass('active')) {
                                                    
                          return

                      } else {
                       
                        $('.years [data-year].active').removeClass('active');
                       
                        $(this).addClass('active');
                        
                        dataYear = $('.years [data-year].active').data('year');
                        
                       

                        d3.selectAll('div.investment-dollars').data(invDataset)
                                   .attr('data-value', function(d){
                                      if (dataYear == "2012") {
                                          return "$"+(d.y2012) 
                                          console.log(dataYear)  
                                      } else {
                                          return "$"+(d.y2010)   
                                      };
                                      
                                    })
                                    .style('left', function(d){

                                      // return 'translate(' +(d.y2010)*0.6+ 'px,0)'
                                      //return (d.y2010)*1.2+'px'
                                      if (dataYear == 2012){
                                        return scale(d.y2012)+'px';
                                      }
                                        else {
                                        return scale(d.y2010)+'px';
                                          
                                        }

                                      
                                    });

                        d3.selectAll('.animated-percentage').data(workDataset)
                          .attr('data-percent', function(d){
                                        if (dataYear == 2012) {
                                          return (d.y2012)
                                        }
                                          else {
                                            return (d.y2010)
                                          };
                                        
                                      })
                                .attr('data-value',function(d){
                                        if (dataYear == 2012) {
                                          return (d.y2012)+'%' 
                                        } 
                                          else {
                                            return (d.y2010)+'%' 
                                          };
                                      });

                       


                      };

                })  



    });  
                            
}); // d3 csv
          
          

         
}




investmentPaysOff();

   




