var dataSourceDir = base + '/wp-content/themes/om-musa/';
var newData = []
var unemploymentYear = "2013";
var dataIndex = "1"

 

d3.json(dataSourceDir+"js/data-svg/employment-growth-vs-unemployment-rate.json", function(error,data) {

        
            if (error) {
                console.log(error)
            } else { 
                
 

            newData = data[dataIndex][unemploymentYear];
        
    console.log(data)
           // console.log(newData)
         

 

    var BarGraph = function (data, elementSelector) {

    /**

    Data is passed in as arrays of groups. If you don't have a grouped bar chart, you can pass
    an array of 1 (for a group of 1).

    Data input format:

    [
        {
            label: "City Name",
            values: ["10.2%", "4.3%"]
        }
        ...etc
    ]

    Then for each group, you get markup like this:

    <div class="group" data-label="City Name">

        <div class="bars">

            <div class="bar">
                <div class="value" style="height: NN%">
                    <div class="marker" data-label="10.2%"
                </div>
            </div>

            <div class="bar">
                <div class="value" style="hei0ght: NN%">
                    <div class="marker" data-label="4.3%"
                </div>
            </div>

        </div>
    </div>


    **/

    var graph = this;

    this.elementSelector = elementSelector;

    $(elementSelector).addClass("bar-graph");

    this.data = [];

    this.update = function(data) {

        this.max = d3.max(data, function (d) {
            return d3.max(d.values, function (d) {
                return parseFloat(d);
            })
        })

        this.min = d3.min(data, function (d) {
            return d3.min(d.values, function (d) {
                return parseFloat(d);
            });
        })

        var height = d3.scale.linear().domain([this.min/4, this.max]).range([0, 100]);

        var groups = d3.select(this.elementSelector).selectAll(".group").data(data);

        groups.call(function() {
                this.select(".bars")
                    .selectAll(".bar")
                    .data(function (d) {
                        return d.values;
                    })
                    .select(".value")
                    .style("height", function (d) {
                        return height( parseFloat(d) ) + "%";
                    })
                    .select(".marker")
                    .attr("data-label", function (d) {
                        return d;
                    })


            })


        groups.enter()
            .append("div")
            .attr("class", "group")
            .attr("data-label", function (d) {
                return d.label
            })
            .call(function() {
                this.append("div")
                    .attr("class", "bars")
                    .selectAll(".bars .bar")
                    .data(function(d){
                        return d.values;
                    })
                    .enter()
                    .append("div")
                    .attr("class", "bar")
                    .append("div")
                    .attr("class", "value")
                    .style('height', 0)
                    .style("height", function (d) {
                        return height( parseFloat(d) ) + "%";
                    })
                    .append("div")
                    .attr("class", "marker")
                    .attr("data-label", function (d) {
                        return d;
                    })

                this.append("h4")
                    .attr("class", "label")
                    .text(function(d) {
                        return d.label;
                    })
            })

    }

    this.update(data);



    } // BarGraph

} //else

// var graph = new BarGraph(newData, 'section[data-graph="employment growth v. unemployment rate"] .graph');
 






    //REMOVE THIS FUNCTION AND REPLACE IT WITH THE TOGGLE LOGIC, UPDATE FUNCTION REFERENCE BELOW (NEW ARRAY OF GROUPS)
    /*setInterval(function(graph) {
        var newData = []


        for(var i = 0; i < data.length; i++) {

            var group = {
                label: data[i].County,
                values: [ Math.round(Math.random()*18) + "%", Math.round(Math.random()*18) + "%" ]
            }

            newData.push(group)
        }

        graph.update(newData);

    }, 3000, graph); */
//$('#post-1612').waypoint(function(down){

        

         

         BarGraph(newData, 'section[data-graph="employment growth v. unemployment rate"] .graph');

         
         $('.unemployment-growht-vs-unemployment-rate .bar-graph').css('opacity',1);   

         

          $('.unemployment-growht-vs-unemployment-rate .toggle .value').on('click',function(e){

                        

                            
                            
                            unemploymentYear = $(this).attr('data-value');
                            dataIndex = $(this).attr('data-index');
                            newData = data[dataIndex][unemploymentYear];
                            BarGraph(newData, 'section[data-graph="employment growth v. unemployment rate"] .graph');
                            
                            
                        $('.unemployment-growht-vs-unemployment-rate .toggle').attr('data-value', $(this).index())


                             
                              
                               
                        
                        });

  //},{triggerOnce:true});
       

});//


