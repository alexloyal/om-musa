var radialgraphs = [];

// post-1738 is MIAMI-DADE RESIDENTS HAVE ACCESS TO AND USE PUBLIC TRANSIT

$('#post-1738 .data-disclosure [data-dimension="B"],#post-1738 .data-disclosure [data-dimension="C"]').css('opacity', 0);
$('#post-1738 .data-disclosure [data-dimension="B"],#post-1738 .data-disclosure [data-dimension="C"]').addClass('hide');



$('[data-trigger="20"]').waypoint(function(down){

	$('#post-1738 .swap.objective.two').css('opacity','1');
	$('#post-1738 .data-disclosure').css('opacity','1');
	$('#post-1738 .year-labels').css('opacity','1');



	var dataSourceDir = base + '/wp-content/themes/om-musa/';



	d3.tsv(dataSourceDir+'js/data-svg/iac-tr-public-transit.tsv', null, function(data) {

		//hide any graphs that are outside of the range of this array, in this case: US
		$("#ia-data-1738 .city-group").eq(6 - (6 - data.length) ).hide();

		for(var i = 0; i < data.length; i++) {

			$("#ia-data-1738 .city-group").eq(i).show();

			var ry1 = "2010";
			var ry2 = "2012";
			var county = data[i]["County"]
			var cityName = $('.circles .radial[data-city="'+county+'"]')
			var dataYear = ry1;

			var label = county.toLowerCase().replace(/ /g, '-');

			$('.city-group .radial').attr('data-year', function(){
				var index = $(this).index();
				if (  index < 1 ) {
					return ry1;
				} else {
					return ry2;
				}
			})

			if(!radialgraphs[0]) { radialgraphs[0] = {} }
			if(!radialgraphs[1]) { radialgraphs[1] = {} }

			$("#ia-data-1738 .year-labels .item.green").html(ry1);
			$("#ia-data-1738 .year-labels .item.purp").html(ry2);

			radialgraphs[0][county] = new RadialGraph([data[i][ry1]], '[data-year="'+ry1+'"][data-city="'+county+'"]', ''+label+'', 230);
			radialgraphs[1][county] = new RadialGraph([data[i][ry2]], '[data-year="'+ry2+'"][data-city="'+county+'"]', ''+label+'', 230);


		}

		$('.city-group').css('opacity', '1');


		$('#ia-data-1738.data-svg .swap.objective.two .item span').on('click', function(){


				var parentSwap = $(this).parent().data('swap');
                var activeSwap = $('#post-1738 .swap .item[data-swap="1"]');

				var dataDimension = $(this).parent().data('dimension');

				if (parentSwap = "0") {
	                activeSwap.attr('data-swap', '0')
	                $(this).parent().attr('data-swap', '1')
	            };
	            $('#post-1738 .data-disclosure [data-dimension]:not(.hide)').css('opacity', 0);
	            $('#post-1738 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
				$('#post-1738 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
				$('#post-1738 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

				switch(dataDimension) {

                	case "A":
						//hide any graphs that are outside of the range of this array, in this case: US
						$("#ia-data-1738 .city-group").eq(6 - (6 - data.length) ).hide();

						for(var i = 0; i < data.length; i++) {

							var ry1 = "2010";
							var ry2 = "2012";
							var county = data[i]["County"]
							var label = county.toLowerCase().replace(/ /g, '-');

							$("#ia-data-1738 .year-labels .item.green").html(ry1);
							$("#ia-data-1738 .year-labels .item.purp").html(ry2);

							radialgraphs[0][county].update([data[i][ry1]], 230);
							radialgraphs[1][county].update([data[i][ry2]], 230);
						}

                		break;

					case "B":
                		d3.tsv(dataSourceDir+'js/data-svg/iac-tr-investment-transit.tsv', function(data) {
							//hide any graphs that are outside of the range of this array, in this case: US
							$("#ia-data-1738 .city-group").eq(6 - (6 - data.length) ).hide();

                			for (var i = data.length - 1; i >= 0; i--) {

                				var ry1 = "2010";
								var ry2 = "2012";
                				var county = data[i]["County"];

								$("#ia-data-1738 .year-labels .item.green").html(ry1);
								$("#ia-data-1738 .year-labels .item.purp").html(ry2);

								radialgraphs[0][county].update([data[i][ry1]], 730);
								radialgraphs[1][county].update([data[i][ry2]], 730);
                			};

                		})
                		break;
                	case "C":

                		d3.tsv(dataSourceDir+'js/data-svg/iac-tr-transit-work.tsv', function(data) {

                			for (var i = data.length - 1; i >= 0; i--) {

								$("#ia-data-1738 .city-group").eq(i).show();

                				var ry1 = "2011";
								var ry2 = "2013";
								var county = data[i]["County"]
								var cityName = $('.circles .radial[data-city="'+county+'"]')
								var dataYear = ry1;

								var label = county.toLowerCase().replace(/ /g, '-');

								$("#ia-data-1738 .year-labels .item.green").html(ry1);
								$("#ia-data-1738 .year-labels .item.purp").html(ry2);

								if(radialgraphs[0][county]) {
									radialgraphs[0][county].update([data[i][ry1]]);
								}else {
									$('.city-group').eq(i).find('.radial').eq(0).attr('data-year', ry1);
									radialgraphs[0][county] = new RadialGraph([data[i][ry1]], '[data-year="'+ry1+'"][data-city="'+county+'"]', ''+label+'');
								}

								if(radialgraphs[1][county]) {
									radialgraphs[1][county].update([data[i][ry2]]);
								}else {
									$('.city-group').eq(i).find('.radial').eq(1).attr('data-year', ry2);
									radialgraphs[1][county] = new RadialGraph([data[i][ry2]], '[data-year="'+ry2+'"][data-city="'+county+'"]', ''+label+'');
								}


                			};

                		})
                	break;

                }

		 });



	}, function(err, rows) {
	  console.log(err)
	});

},{triggerOnce:true}); // Waypoints
