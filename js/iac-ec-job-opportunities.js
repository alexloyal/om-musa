// Waypoints
//#post-1790 is MIAMI-DADE INVESTS IN A EFFICIENT ROADWAY NETWORK

$('#post-1790 .data-disclosure [data-dimension="B"],#post-1790 .data-disclosure [data-dimension="C"]').css('opacity',0);
$('#post-1790 .data-disclosure [data-dimension="B"],#post-1790 .data-disclosure [data-dimension="C"]').addClass('hide');

$('[data-trigger="10"]').waypoint(function(down){
  



  $('#post-1790 .swap.objective.one').css('opacity','1');
  $('#post-1790 .data-disclosure').css('opacity','1');
  $('#post-1790 .year-labels').css('opacity','1');
//

 
var dataSourceDir = base + '/wp-content/themes/om-musa/';


d3.json(dataSourceDir+"js/data-svg/iac-ha-financial-resources-income-zero.json", function(error,zeroData) {

            if (error) {
                // If error is not null, something went wrong.
                console.log(error);
                //Log the error.
            } else {
                     
                
            bars = new BarGraphDollars(zeroData,'.iac-ec-job-opportunities .graph');

            $('#ia-data-1790 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2010-11";
                                      } else {
                                        return "2012-13";
                                      }
                                    })

            $(document).ready(function(){
                
                d3.json(dataSourceDir+"js/data-svg/iac-ec-employment-growth.json", function(error,data) {
                                 
                                    bars.update(data);

                              })

                    $('#post-1790 .data-svg .swap.objective.one .item span').on('click', function(){        
                        var parentSwap = $(this).parent().data('swap');
                        var activeSwap = $('#ia-data-1790 .swap .item[data-swap="1"]');
                        var dataDimension = $(this).parent().data('dimension');
                        console.log(dataDimension)
                        if (parentSwap = "0") {
                            activeSwap.attr('data-swap', '0')
                            $(this).parent().attr('data-swap', '1')
                        };
                        $('#post-1790 .data-disclosure [data-dimension]:not(.hide)').css('opacity',0);
                        $('#post-1790 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                        $('#post-1790 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#post-1790 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

                        if (dataDimension == "A") {
                            

                            d3.json(dataSourceDir+"js/data-svg/iac-ec-employment-growth.json", function(error,data) {
                                 
                                 bars.update(data,'.iac-ec-job-opportunities .graph');
                                 /*setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                  $('#ia-data-1790 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2010-11";
                                      } else {
                                        return "2012-13";
                                      }
                                    })
                              })
                        };

                        if (dataDimension == "B") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ec-unemployment-rate.json", function(error,data) {

                                console.log(data)

                                /*[
                                  {
                                      "label": "CHI",
                                      "values": ["1.4%", "0.9%"]
                                  },
                                ]*/
                                

                            /*    for (var i = 0; i < data.length; i++) {
                                  dy1 = "2011";
                                  dy2 = "2013";
                                  console.log(data.indexOf())
                                  var unemploymentRateData = []
                                  var unemploymentRate = {}
                                  unemploymentRate["label"] = String(data[i]["County"])
                                  unemploymentRate["values"] = [String(data[i][dy1]),String(data[i][dy2])]
                                  unemploymentRateData.push(unemploymentRate)
                                  console.log(unemploymentRateData)
                                };*/


                                 
                                bars.update(data,'.iac-ec-job-opportunities .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                $('#ia-data-1790 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2011";
                                      } else {
                                        return "2013";
                                      }
                                    })
                              })
                        };

                        if (dataDimension == "C") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ec-technology-growth.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-ec-job-opportunities .graph');

                                  $('#ia-data-1790 .group[data-label="NYC"] .bar:nth-child(odd) .value').css('height', "0%");
                                 /* setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                $('#ia-data-1790 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2011";
                                      } else {
                                        return "2013";
                                      }
                                    })
                              })
                        };

                    });
                      
            });

        

                 


        } // else
 }); // d3 json civic participation

},{triggerOnce:true});//Waypoints
 