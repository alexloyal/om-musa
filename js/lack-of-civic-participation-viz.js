function LackOfCivicParticipation(){


  /** Lack of Civic Participation **/

var votingYear = "2012";
var voterParticipation = [];
var volunteerRates = [];

var dataSourceDir = base + '/wp-content/themes/om-musa/';

d3.csv(dataSourceDir + "js/data-svg/lack-of-civic-participation.csv", function(error,civicData) {
    for (var i = 0; i < civicData.length; i++) {
        var datum = civicData[i];

        if (datum.County != "US") {
            voterParticipation.push(datum);

            switch(datum.County) {
                case "Chicago":
                    datum.cx = 520;
                    datum.cy = 180;
                    break;
                case "Houston":
                    datum.cx = 420;
                    datum.cy = 430;
                    break;
                case "Miami":
                    datum.cx = 618;
                    datum.cy = 430;
                    break;
                case "NYC":
                    datum.cx = 700;
                    datum.cy = 180;
                    break;
                case "San Diego":
                    datum.cx = 100;
                    datum.cy = 290;
                    break;
            }
        }
    }

    renderVoterParticipationRates();
})

d3.csv(dataSourceDir + "js/data-svg/volunteer-rates.csv", function(error,volunteerData) {
    for (var i = 0; i < volunteerData.length; i++) {
        var datum = volunteerData[i];

        if (datum.County != "US") {
            volunteerRates.push(datum);

            switch(datum.County) {
                case "Chicago":
                    datum.cx = 520;
                    datum.cy = 180;
                    break;
                case "Houston":
                    datum.cx = 420;
                    datum.cy = 430;
                    break;
                case "Miami":
                    datum.cx = 618;
                    datum.cy = 430;
                    break;
                case "NYC":
                    datum.cx = 700;
                    datum.cy = 180;
                    break;
                case "San Diego":
                    datum.cx = 100;
                    datum.cy = 290;
                    break;
            }
        }
    }

    renderVolunteerRates();
})


function renderVoterParticipationRates() {

    var cities = d3.select(".lack-civic-participation .map")
                    .selectAll("g.city.voter")
                    .data(voterParticipation);

    var radius = 40;

    var radiusScale = d3.scale.linear().domain([0, 100]).range([0, radius]);



    cities.call(function() {
          // These two are one dimention. Duplicate to create another dimenison and refer to line 85.
            this.select("circle.value")
                .transition()
                .attr("r", function(d) {

                //  var radius = isVolunteerShown ? radiusScale( thedatathing) : 0;
               //  return radius;

                    return radiusScale( parseFloat(d[votingYear]) );
                })

            this.select("text.value")
                .text(function(d) {
                    return Math.round( parseFloat(d[votingYear]) ) + "%";
                })
        }) // cities call function

    cities.enter()
            .append("g")
            .attr("class", "city voter")
            .attr("data-city", function(d) {
                return d.County;
            })
            .call(function() {
                this.append("circle")
                    .attr("class", "total")
                    .attr("cx", function(d) {
                        return d.cx;
                    })
                    .attr("cy", function(d) {
                        return d.cy;
                    })
                    .attr("r", radius)
                    // This is where you would duplicate to add the second circle.

                this.append("circle")
                    .attr("class", "value")
                    .attr("cx", function(d) {
                        return d.cx;
                    })
                    .attr("cy", function(d) {
                        return d.cy
                    })
                    .transition()
                    .attr("r", function(d) {
                        return radiusScale( parseFloat(d[votingYear]) );
                    })

                // Volunteer Rate Circle

                this.append("text")
                    .attr("class", "value cyan")
                    .text(function(d) {
                        return Math.round( parseFloat(d[votingYear]) ) + "%";
                    })
                    .attr("transform", function(d) {
                        var x,y;

                        switch(d.County) {
                            case "Chicago":
                                x = 410;
                                y = 260;
                                break;
                            case "Houston":
                                x = 345;
                                y = 510;
                                break;
                            case "Miami":
                                x = 580;
                                y = 510;
                                break;
                            case "NYC":
                                x = 670;
                                y = 260;
                                break;
                            case "San Diego":
                                x = 20;
                                y = 380;
                                break;
                        }

                        return "translate(" + x + "," + y + ")";
                    })



                this.append("text")
                    .attr("class", "city-name label")
                    .text(function(d) {
                        return d.County;
                    })
                    .attr("transform", function(d) {
                        var x,y;

                        switch(d.County) {
                            case "Chicago":
                                x = 400;
                                y = 180;
                                break;
                            case "Houston":
                                x = 300;
                                y = 440;
                                break;
                            case "Miami":
                                x = 680;
                                y = 440;
                                break;
                            case "NYC":
                                x = 620;
                                y = 180;
                                break;
                            case "San Diego":
                                x = 130;
                                y = 240;
                                break;
                        }

                        return "translate(" + x + "," + y + ")";
                    })

    }) // call line 119


} // renderVoterParticipationRates()

function renderVolunteerRates(empty){
        var cities = d3.select(".lack-civic-participation .map")
                        .selectAll("g.city.volunteer")
                        .data(volunteerRates);


    var radius = 40;

    var radiusScale = d3.scale.linear().domain([0, 100]).range([0, radius]);


    cities.call(function() {
          // These two are one dimention. Duplicate to create another dimenison and refer to line 85.
            this.select("circle.value")
                .transition()
                .attr("r", function(d) {
                    var val = empty ? 0 : parseFloat(d[votingYear])
                    return radiusScale(val);
                })

            this.select("text.value")
                .text(function(d) {
                    return Math.round( parseFloat(d[votingYear]) ) + "%";
                })
        })

    cities.enter()
            .append("g")
            .attr("class", "city volunteer")
            .attr("data-city", function(d) {
                return d.County;
            })
            .call(function() {


                this.append("circle")
                    .attr("class", "value purp volunteer")
                    .attr("cx", function(d) {
                        return d.cx;
                    })
                    .attr("cy", function(d) {
                        return d.cy
                    })
                    .attr("r", 0)
                    .transition()
                    .attr("r", function(d) {
                        var val = empty ? 0 : parseFloat(d[votingYear])
                        return radiusScale(val);
                    })

                // Volunteer Rate Circle

                this.append("text")
                    .attr("class", "value purp")
                    .text(function(d) {
                        return Math.round( parseFloat(d[votingYear]) ) + "%";
                    })
                    .attr("transform", function(d) {
                        var x,y;

                        switch(d.County) {
                            case "Chicago":
                                x = 500;
                                y = 260;
                                break;
                            case "Houston":
                                x = 445;
                                y = 510;
                                break;
                            case "Miami":
                                x = 665;
                                y = 510;
                                break;
                            case "NYC":
                                x = 760;
                                y = 260;
                                break;
                            case "San Diego":
                                x = 110;
                                y = 380;
                                break;
                        }

                        return "translate(" + x + "," + y + ")";
                    })
    });

}




$(document).ready(function () {

        
            renderVolunteerRates(true)
        
    
    $(".lack-civic-participation .toggle .value").on("click", function(e) {
       
        $(".lack-civic-participation .toggle").attr("data-value", $(this).index());
       
        votingYear = $(this).attr("data-value");
       
        renderVolunteerRates();
        renderVoterParticipationRates();

    })

        var clicker = 0;

    $('.lack-civic-participation .item.volunteer-rate-icon').on('click', function(){
        
        $(this).toggleClass('filled')
        
        ++clicker;

        if (clicker % 2 === 0) {
            $('.lack-civic-participation .map .city .purp').css('opacity', 0);
        } else {
            $('.lack-civic-participation .map .city .purp').css('opacity', 1);
        }

    })

 
})



};
setTimeout(function(){
    LackOfCivicParticipation();
},600);