/* Trigger script. 

    ~ alej.co

*/

$('.issue-buffer').on('click',function(){
    var url = $(this).data('url')
    window.location.href = url;
});

// http://stackoverflow.com/questions/2851663/how-do-i-simulate-a-hover-with-a-touch-in-touch-enabled-browsers/2891155#2891155

$(document).ready(function() {
    $('.hover').bind('touchstart touchend', function(e) {
        e.preventDefault();
        $(this).toggleClass('hover_effect');
    });
});


// http://stackoverflow.com/a/10520213

var timer;
var hideSearch;
$('input[type="search"]').on('keyup', function() {
    clearInterval(timer);  
    timer = setTimeout(function() {
        $('#search').addClass('hover');
    }, 500);
    hideSearch = setTimeout(function() {
        $('#search').removeClass('hover');
    }, 18000);
});







/*

$('input[type="search"]').keyup(function(){
    
});


$('#search').on('click',function(){
    $(this).removeClass('touch');
});

$('#search').mouseenter(function(){
    $(this).addClass('hover');
});

$('#search').on('click',function(){
    $(this).removeClass('hover');
});
*/

 
/* jCarousel */


(function($,Modernizr) {
    $(function() {
        $('.jcarousel').jcarousel({
            wrap: 'circular',
            transitions: Modernizr.csstransitions ? {
                transforms:   Modernizr.csstransforms,
                transforms3d: Modernizr.csstransforms3d,
                easing:       'ease'
            } : false
        });

        $('.jcarousel-control-prev')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .jcarouselPagination({
                'item': function(page, carouselItems) {
                    return '<a href="#' + page + '" class="">&bull;</a>';
                }
            });
        $('.jcarousel').jcarouselAutoscroll({
            interval: 6000
        });    
    });
})(jQuery, Modernizr);




(function() {

    $(document).ready(function(){
       /* fade in */
        $('.site-header').addClass('show').delay(5000);
        $('#main').addClass('show').delay(5000);

    });
    
    

    function triggers(){
        // Variables for capturing the viewport size and dynamically adjust proportions.
        var windowheight = $(window).height();
        toolsHeight = windowheight * 0.1;
        var windoWidth = $(window).width();
        var elementHeight = windowheight * 0.9;
        var subMenuHeight = $('.site-header .sub-menu').css('max-height',toolsHeight);
        var storyElementHeight = elementHeight;
        var mastheadHeight = $('.site-title').height();

        $('#menu-primary > li, #menu-primary > li a, #site-navigation').css('min-height', function(){
           return mastheadHeight-20;
        });
        $('#search .sub-menu').css('top',function(){
            return mastheadHeight +30;
        })

       // searchSubMenuHeight = $('#search .sub-menu').css('max-height',toolsHeight); 
        //searchIconHeight = $('.search-icon').css('max-height',toolsHeight);
        
         // $("ul.nav-menu > li").css('min-height',toolsHeight);
        //  $('#search').css('min-height', toolsHeight)
        //  $('.search-icon').css('height',toolsHeight);

        // Set the height for each zone.
        // Plus set the height for the story well.
        $('#home-message').height(elementHeight);
        // $('.single-story article').height(storyElementHeight*1.6);
        
        //$('#home-issues,#home-news,#home-about,#home-contact').height(windowheight);
        // $('.home #main').height(elementHeight);

        
       /* if ($('body').is('.home')) {
       
            $('#content').css('padding-top',toolsHeight);
       
        }  else {
       
            $('#content').css('padding-top',(toolsHeight*2));
        };*/

        $('.jcarousel ul li').each(function(){
        $(this).width(windoWidth);           
        $(this).height(elementHeight);
          
        });


        /* Disabling first click on menu items for touch devies */

        $(document).ready(function(){

            currentWidth = $(window).width();
            console.log(currentWidth)
            if (currentWidth < 768) {
            
            $('#menu-primary li > a').one("click", false); 

                 }

        });
        

           
        
         
         // #tools and menu architecture

        var tools = $('#tools');
        // tools.css('min-height',toolsHeight); 
        submenu = $('#site-navigation .sub-menu');
        searchSubmenu = $('#search .sub-menu');
        cyanBorder = $('#cyan').width();
        // $('.nav-menu .menu-item').height(toolsHeight);
        
        submenu.width(windoWidth);
        searchSubmenu.width(cyanBorder);

        // About

        $('#menu-item-1645 ul.sub-menu').css('left', function(){
            elementOffset = $('#menu-item-1645').offset();
            return -elementOffset.left;
        });
        // Our Study
        $('#menu-item-1441 ul.sub-menu').css('left', function(){
            elementOffset = $('#menu-item-1441').offset();
            return -elementOffset.left;
        });

        // Get Engaged
        $('#menu-item-1445 ul.sub-menu').css('left', function(){
            elementOffset = $('#menu-item-1445').offset();
            return -elementOffset.left;
        });

        // Blog & News

        $('#menu-item-1449 ul.sub-menu').css('left', function(){
            elementOffset = $('#menu-item-1449').offset();
            return -elementOffset.left;
        });

    /*     $('#search ul.sub-menu').css('left', function(){
            elementOffset4 = $('#search ul.sub-menu li').offset();
            return -elementOffset4.left;
        }); */

       /*   $('.jcarousel-wrapper').css('margin-top',function(){
            sectionHeader = $('.section.header').height();

            return -sectionHeader +'px';
         }); */

        
       /* // Deploy active submenu when navigating... 
        
        if ($('body').is('.page-template-page-blog-php, .archive, .single-post .single')) {

            $('#menu-item-1449, #menu-item-1449 .sub-menu').addClass('active');
            $('#menu-item-1645, #menu-item-1441, #menu-item-1445, #menu-item-1449, #search ').hover(function(){
                $('#menu-item-1449, #menu-item-1449 .sub-menu').removeClass('active');

            },
            function(){
                $('#menu-item-1449, #menu-item-1449 .sub-menu').addClass('active');
            });
        

        }; 

         // or... About

        if ($('body').is('.page-id-1417, .parent-pageid-1417')) {

            $('#menu-item-1645, #menu-item-1645 .sub-menu').addClass('active');
            $('#menu-item-1645, #menu-item-1441, #menu-item-1445, #menu-item-1449, #search ').hover(function(){
                $('#menu-item-1645, #menu-item-1445 .sub-menu').removeClass('active');

            },
            function(){
                $('#menu-item-1645, #menu-item-1645 .sub-menu').addClass('active');
            });
        

        };


         // or... Our Study

        if ($('body').is('.page-id-1427, .parent-pageid-1427')) {

            $('#menu-item-1441, #menu-item-1441 .sub-menu').addClass('active');
            $('#menu-item-1645, #menu-item-1441, #menu-item-1445, #menu-item-1449, #search ').hover(function(){
                $('#menu-item-1441, #menu-item-1445 .sub-menu').removeClass('active');

            },
            function(){
                $('#menu-item-1441, #menu-item-1441 .sub-menu').addClass('active');
            });
        

        };

        // or... Get Engaged page

        if ($('body').is('.page-id-812, .parent-pageid-812')) {

            $('#menu-item-1445, #menu-item-1445 .sub-menu').addClass('active');
            $('#menu-item-1645, #menu-item-1441, #menu-item-1445, #menu-item-1449, #search ').hover(function(){
                $('#menu-item-1445, #menu-item-1445 .sub-menu').removeClass('active');

            },
            function(){
                $('#menu-item-1445, #menu-item-1445 .sub-menu').addClass('active');
            });
        

        };


        // or... Search Results

        if ($('body').is('.search-results')) {

            $('#search, #search ul.sub-menu').addClass('active');
            $('#menu-item-1449, #menu-item-1645, #menu-item-1441, #menu-item-1445, #menu-item-1449').hover(function(){
                $('#search, #search ul.sub-menu').removeClass('active');

            },

            function(){
                $('#search, #search ul.sub-menu').addClass('active');
            });
        };

        */




        // Scroll to top

        $(window).scroll(function(){
                        var scrollTop = $(window).scrollTop();
                        
                        if (scrollTop >= 420) {
                           $('.anchor-link.top').css('opacity',1)

                            
                        } else if (scrollTop <= 421) {
                          $('.anchor-link.top').css('opacity',0)
                            
                        }
            });



       

       // first change = 576 window scroll top
        // second bullet changes


       






             
    };


  


/*

        function SetCarouselPaginationDotWidth(){

        // Measures how many items in the home-message dot navigation. So we can center them.

            
            messageDotPages = $('#message-wrapper span.controls.dot.jcarousel-pagination a').length;
            newWidthForDots = ($('#message-wrapper span.controls.dot.jcarousel-pagination').width())/messageDotPages;

            newsDotPages = $('#news-wrapper span.controls.dot.jcarousel-pagination a').length;
            newWidthForNewsDots = ($('#news-wrapper span.controls.dot.jcarousel-pagination').width())/newsDotPages;
            
             $('#message-wrapper span.controls.dot.jcarousel-pagination a').width(function(){
                   return (Math.round(newWidthForDots))-0.8;
                });

             // The larger the number the shorter the distance between dots. 
             
            
             $('#news-wrapper span.controls.dot.jcarousel-pagination a').width(function(){
                   return (Math.round(newWidthForNewsDots))-0.5;
                });

             }; */

        

    $(document).ready(
           
           function(){
             triggers();
            
            
           // SetCarouselPaginationDotWidth();
           
           
    });


    $(window).resize(function(){
            
        $(document).ready(
           
              function(){
                    triggers();
                     
                    // setTimeout(function(){
                        

                         // SetCarouselPaginationDotWidth();
           
                 //    }, 105);


                    $('.search-icon').css('height',toolsHeight);
           
           
         });
                  
    });


     

    // http://www.hnldesign.nl/work/code/mouseover-hover-on-touch-devices-using-jquery/


})(jQuery);

// Smooth scroll 

$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});


// Ajax load more posts. 

if ($('body').is('.page-template-page-blog-php,.search-results')) {
    (function() {

 
    var pageNum = parseInt(ajax_posts.startPage) + 1;
    var max = parseInt(ajax_posts.maxPages);
    var nextLink = ajax_posts.nextLink;
 
    if(pageNum <= max) {
     
        $('#more-posts')
            .append('<div class="ajax-posts-placeholder-'+ pageNum +'"></div>')
            .append('<p id="ajax-posts-load-posts"><a href="#">Load More Posts</a></p>');
            
       
        $('.paging-navigation').remove();
    }
    
    
  
    $('#ajax-posts-load-posts a').click(function() {
    
        // Are there more posts to load?
        if(pageNum <= max) {
        
        
            $(this).text('Loading posts...');
            
            $('.ajax-posts-placeholder-'+ pageNum).load(nextLink + ' .background',
                function() {
                    
                    pageNum++;
                    nextLink = nextLink.replace(/\/page\/[0-9]?/, '/page/'+ pageNum);
                    
                   
                    $('#ajax-posts-load-posts')
                        .before('<div class="ajax-posts-placeholder-'+ pageNum +'"></div>')
                    
                    // Update the button message.
                    if(pageNum <= max) {
                        $('#ajax-posts-load-posts a').text('Load More Posts');
                    } else {
                        $('#ajax-posts-load-posts a').text('No more posts to load.');
                    }
                }
            );
        } else {
            $('#ajax-posts-load-posts a').append('.');
        }   
        
        return false;
    });

    })(jQuery);
};



 

 $(document).ready(function(){

     $(window).on('scroll', function(){   

             if ($(window).scrollTop() < 532){
                $('span.story-dot-navigation a.text').removeClass('text');
                $('#dot-10 a[data-kicker]').addClass('text');
            }

             if ($(window).scrollTop() > 532) {
                $('span.story-dot-navigation a.text').removeClass('text');
                $('#dot-20 a[data-kicker]').addClass('text');
            }

             if ($(window).scrollTop() > 1451) {
                $('span.story-dot-navigation a.text').removeClass('text');
                $('#dot-30 a[data-kicker], #dot-31 a[data-kicker]').addClass('text');
            }

             if ($(window).scrollTop() > 2334) {
                $('span.story-dot-navigation a.text').removeClass('text');
                $('#dot-40 a[data-kicker]').addClass('text');
            }

            if ($(window).scrollTop() > 2834) {
                $('span.story-dot-navigation a.text').removeClass('text');
                $('#dot-50 a[data-kicker]').addClass('text');
                
            }

            if ($(window).scrollTop() > 3490) {
                $('span.story-dot-navigation a.text').removeClass('text');
                $('#dot-90 a[data-kicker]').addClass('text');
            }

            

            
        });


 });

$(document).ready(function(){
    var childPages = $('.page-parent .type-page');
        heights = [];
        for (var i = childPages.length - 1; i >= 0; i--) {
                
                

                heights.push($(childPages[i]).height());

                
        };

        childPages.height(function(){
            return Math.max.apply(null, heights)+"px";
        })
        
});

 // This is for Issue Area pages.
$(document).ready(function(){

    $(window).on('scroll', function(){

        if ($(window).scrollTop() < 532){
                $('.tax-issue-areas span.story-dot-navigation a.text').removeClass('text');
                $('.tax-issue-areas #dot-0 a[data-kicker]').addClass('text');
            }

             if ($(window).scrollTop() > 532) {
                $('.tax-issue-areas span.story-dot-navigation a.text').removeClass('text');
                $('.tax-issue-areas #dot-10 a[data-kicker]').addClass('text');
            }

             if ($(window).scrollTop() > 1451) {
                $('.tax-issue-areas span.story-dot-navigation a.text').removeClass('text');
                $('.tax-issue-areas #dot-20 a[data-kicker]').addClass('text');
            }

             if ($(window).scrollTop() > 2450) {

                $('.tax-issue-areas span.story-dot-navigation a.text').removeClass('text');
                $('.tax-issue-areas #dot-30 a[data-kicker]').addClass('text');
            }

            if ($(window).scrollTop() > 3328) {
                
                $('.tax-issue-areas span.story-dot-navigation a.text').removeClass('text');
                $('.tax-issue-areas #dot-40 a[data-kicker]').addClass('text');
            }

             
    });

});

// Issue & Area triggers


// Cystom styling on static pages.

$(document).ready(function(){

    $('.page-id-1419 .entry-header h1.entry-title').html(
             function(i,h){
        

                    return h.replace(/(OurMiami\?)/g,'<span class="ourmiami">$1</span>');
             
             });

});

// Scripts to read social share stats.

 $(document).ready(function() {
            $.each($('.share-facebook'), function(i, v){
                var url = $(this).attr('data-url');
                var parent = $(this);
                if (url !== undefined)
                {
                    $.getJSON("http://graph.facebook.com/?ids="+url+"&callback=?", function(data){
                        if (data[url].shares !== undefined)
                        {
                            parent.text(data[url].shares);
                        }
                    });
                }
            });
    

            $.each($('.share-twitter'), function(i, v){
                var url = $(this).attr('data-url');
                var parent = $(this);
                if (url !== undefined)
                {
                        $.getJSON("http://urls.api.twitter.com/1/urls/count.json?url="+url+"&callback=?",
                  function (data) {
                            if (data.count !== undefined)
                        {
                            parent.text(data.count);
                        }
                    });
 
                    }
            });
    });
       


