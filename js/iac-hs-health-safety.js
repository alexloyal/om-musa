// Waypoints
//post-1781 is MIAMI RESIDENTS ARE SAFE

$('#post-1781 .data-disclosure [data-dimension="B"],#post-1781 .data-disclosure [data-dimension="C"]').css('opacity', 0);
$('#post-1781 .data-disclosure [data-dimension="B"],#post-1781 .data-disclosure [data-dimension="C"]').addClass('hide');

$('[data-trigger="40"]').waypoint(function(down) {
  

$('.swap.objective.one').css('opacity','1');

$('#post-1781 .swap.objective.one').css('opacity','1');
$('#post-1781 .data-disclosure').css('opacity','1');
$('#post-1781 .year-labels').css('opacity','1');

 
var dataSourceDir = base + '/wp-content/themes/om-musa/';


d3.json(dataSourceDir+"js/data-svg/iac-ha-financial-resources-income-zero.json", function(error,zeroData) {

            if (error) {
                // If error is not null, something went wrong.
                console.log(error);
                //Log the error.
            } else {
                     
                
            bars = new BarGraphDollars(zeroData,'.ia-hs-health-safety .graph');
              $('#ia-data-1781 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2010";
                                      } else {
                                        return "2012";
                                      }
                                    })



            $(document).ready(function(){
                
                d3.json(dataSourceDir+"js/data-svg/iac-hs-homicide.json", function(error,data) {
                                 
                                    bars.update(data);
                                    $('.ia-hs-health-safety [data-label="CHI"] .bar:nth-child(odd) .value .marker').delay(2).attr('data-label','N/A')
                                  $('.ia-hs-health-safety [data-label="CHI"] .bar:nth-child(odd) .value').delay(1).css('height','0')

                              })

                    $('#post-1781 .data-svg .swap.objective.one .item span').on('click', function(){        
                        var parentSwap = $(this).parent().data('swap');
                        var activeSwap = $('#post-1781 .swap .item[data-swap="1"]');
                        var dataDimension = $(this).parent().data('dimension');
                        console.log(dataDimension)
                        if (parentSwap = "0") {
                            activeSwap.attr('data-swap', '0')
                            $(this).parent().attr('data-swap', '1')
                        };

                        $('#post-1781 .data-disclosure [data-dimension]:not(.hide)').css('opacity', 0);
                        $('#post-1781 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                        $('#post-1781 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#post-1781 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

                        if (dataDimension == "A") {
                            d3.json(dataSourceDir+"js/data-svg/iac-hs-homicide.json", function(error,data) {
                                 
                                 bars.update(data,'.ia-hs-health-safety .graph');
                                 /*setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                  $('.ia-hs-health-safety [data-label="CHI"] .bar:nth-child(odd) .value .marker').delay(2).attr('data-label','N/A')
                                  $('.ia-hs-health-safety [data-label="CHI"] .bar:nth-child(odd) .value').delay(1).css('height','0')
                                  $('#ia-data-1781 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2010";
                                      } else {
                                        return "2012";
                                      }
                                    })
                              })
                        };

                        if (dataDimension == "B") {
                            d3.json(dataSourceDir+"js/data-svg/iac-hs-robbery.json", function(error,data) {
                                 
                                  bars.update(data,'.ia-hs-health-safety .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                  $('.ia-hs-health-safety [data-label="CHI"] .bar:nth-child(odd) .value .marker').delay(2).attr('data-label','N/A')
                                  $('.ia-hs-health-safety [data-label="CHI"] .bar:nth-child(odd) .value').delay(1).css('height','0')
                                  $('#ia-data-1781 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2010";
                                      } else {
                                        return "2012";
                                      }
                                    })
                              })
                        };

                        if (dataDimension == "C") {
                            d3.json(dataSourceDir+"js/data-svg/iac-hs-incidence-child-abuse.json", function(error,data) {
                                 
                                  bars.update(data,'.ia-hs-health-safety .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                              })

                            $('#ia-data-1781 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "FY 2010";
                                      } else {
                                        return "FY 2012";
                                      }
                                    })

                        };

                    });
                      
            });

        

                 


        } // else
 }); // d3 json civic participation

},{triggerOnce:true});//Waypoints
 