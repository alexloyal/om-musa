var BarGraph = function (data, elementSelector, maximum) {

    /**

    Data is passed in as arrays of groups. If you don't have a grouped bar chart, you can pass
    an array of 1 (for a group of 1).

    Data input format:

    [
        {
            label: "City Name",
            values: ["10.2%", "4.3%"]
        }
        ...etc
    ]

    Then for each group, you get markup like this:

    <div class="group" data-label="City Name">

        <div class="bars">

            <div class="bar">
                <div class="value" style="height: NN%">
                    <div class="marker" data-label="10.2%"
                </div>
            </div>

            <div class="bar">
                <div class="value" style="height: NN%">
                    <div class="marker" data-label="4.3%"
                </div>
            </div>

        </div>
    </div>


    **/

    var graph = this;

    this.elementSelector = elementSelector;

    $(elementSelector).addClass("bar-graph");

    this.data = [];

    this.update = function(data) {

        function cleaned (num) {
            return parseFloat( String(num).replace(/\$|,|%/g, '') );
        }

        this.max = maximum ? maximum : d3.max(data, function (d) { return d3.max(d.values, function (d) { return cleaned(d); }) });

        this.min = d3.min(data, function (d) {
            return d3.min(d.values, function (d) {
                return cleaned(d);
            });
        })

        var height = d3.scale.linear().domain([this.min/4, this.max]).range([0, 100]);

        var groups = d3.select(this.elementSelector).selectAll(".group").data(data);

        groups.call(function() {
                var bars = this.select(".bars")
                    .selectAll(".bar")
                    .data(function (d) {
                        return d.values;
                    })

                bars.select(".value")
                    .style("height", function (d) {
                        return height( cleaned(d) ) + "%";
                    })
                    .select(".marker")
                    .attr("data-label", function (d) {
                        return d;
                    })

                bars.enter()
                    .append("div")
                    .attr("class", "bar")
                    .append("div")
                    .attr("class", "value")
                    .style("height", function (d) {
                        return height( cleaned(d) ) + "%";
                    })
                    .append("div")
                    .attr("class", "marker")
                    .attr("data-label", function (d) {
                        return d;
                    })

                bars.exit().remove()
            })


        groups.enter()
            .append("div")
            .attr("class", "group")
            .attr("data-label", function (d) {
                return d.label
            })
            .call(function() {
                this.append("div")
                    .attr("class", "bars")
                    .selectAll(".bars .bar")
                    .data(function(d){
                        return d.values;
                    })
                    .enter()
                    .append("div")
                    .attr("class", "bar")
                    .append("div")
                    .attr("class", "value")
                    .style("height", function (d) {
                        return height( cleaned(d) ) + "%";
                    })
                    .append("div")
                    .attr("class", "marker")
                    .attr("data-label", function (d) {
                        return d;
                    })

                this.append("h4")
                    .attr("class", "label")
                    .text(function(d) {
                        return d.label;
                    })
            })

        groups.exit()
                .transition(500)
                .style("opacity", "0")
                .remove("div.group");

    }

    this.update(data);


}
