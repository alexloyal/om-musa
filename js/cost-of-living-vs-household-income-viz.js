function whatDrivesThis(){


var costOfLivingYear = "2013";
var cityName = "Miami";
var dataSourceDir = base + '/wp-content/themes/om-musa/';
var householdIncome = [];
var costOfLiving = [];

d3.json(dataSourceDir+'js/data-svg/cost-of-living-vs-household-income.json', function(error, data) {
         
           

          if (error) {
         // If error is not null, something went wrong.

         console.log(error);
         //Log the error.

         } else { 
            
            
               whatDrivesThis = data; 
                console.log(whatDrivesThis)

               var svg = d3.select('.what-drives-this .data-svg')
                     .append('svg')
                     .attr('preserveAspectRatio', 'xMinYMin meet')
                     .attr('viewBox', '0 0 500 500')
                     .attr('x', '320px')
                     .attr('y', '260px')
                     .attr('dx', '320px')
                     .attr('dy', '260px')
                     .attr('width', '100%')
                     .attr('height', '100%')
                     .attr('id', 'what-drives-this');
               // .data(dataset.map(function(d) { return +d; }))
           function clearDeck(){
               d3.selectAll('.what-drives-this .data-svg svg circle').remove();
               d3.selectAll('.what-drives-this .data-svg svg .label').remove();
            };     

          
            console.log(data)
            // NEED TO ADD THE DATA HERE

              // console.log(costOfLivingYear)

               var cityData = whatDrivesThis[costOfLivingYear][cityName];
               
               
               var costOfLiving = cityData["Cost of Living"];
               var householdIncomeRaw = cityData["Household Income"].replace('$','');
               var householdIncome = parseFloat(householdIncomeRaw.replace(',','.').replace(' ',''))
               console.log()
               
               parseFloat(householdIncome)
               dx = 350;
               dy = 260;

               for(var cost in data) {
                  var obj = data[cost]
               }

//               console.log(data);
              

                      var costOfLivingCircle =   svg.append('circle')
                           .attr('class', 'cost-of-living' )
                            .attr('cx', dx )
                            .attr('cy', dy )
                            .attr('r', 0)
                            .transition()
                            
                      

                       var householdIncomeCircle = svg.append('circle')
                            .attr('class', 'household-income' )
                            .attr('cx', dx )
                            .attr('cy', dy )
                            .attr('r', 0)
                            .transition()
                            

                         svg.append("g")
                            .attr("class", "cost-of-living labels")
                            .call(function(){
                                this.append('text')
                                    .attr('class', 'cost-of-living label figure' )
                                    
                                    .text(function(){
                                      return (0)
                                    });
                                this.append('text')
                                      .attr('class', 'cost-of-living label' )
                                      .attr('dx',20 )
                                      .attr('dy',20 )
                                      .text('Cost of Living Index')     
                            }) 
                      
                    

                         svg.append('text')
                            .attr('class', 'household-income label figure' )
                            .attr('dx', dx-53 )
                            .attr('dy', dy )
                            .text(function(){
                              return "$"+0 +"k"
                            })
                            .style('fill', 'white');
                         svg.append('text')
                            .attr('class', 'household-income label caption' )
                            .attr('dx', dx-30 )
                            .attr('dy', dy+10 )
                            .text('Median')
                            .style('fill', 'white');
                            
                          svg.append('text')
                            .attr('class', 'household-income label caption' )
                            .attr('dx', dx-30 )
                            .attr('dy', dy+20 )
                            .text('Household')
                            .style('fill', 'white');
                           
                           svg.append('text')
                            .attr('class', 'household-income label caption' )
                            .attr('dx', dx-30 )
                            .attr('dy', dy+30 )
                            .text('Income')
                            .style('fill', 'white'); 


                       
        function renderValues() { 

                            d3.select('circle.cost-of-living').transition().attr('r', function(){
                                    return costOfLiving*1.08
                                  });
                            d3.select('.cost-of-living.labels').transition()
                                .attr('transform', function(d){
                                var radius = costOfLiving * 1.08;
                                var x = dx + radius * Math.cos(Math.PI/4)+20
                                var y = dy - radius * Math.sin(Math.PI/4)
                                
                                return "translate(" + x + "," + y + ")"
                              })  

                            d3.select('circle.household-income').transition().attr('r', function(){
                                    return  (parseFloat(householdIncome)*1.5)
                                  });    
                               
                            d3.select('.cost-of-living.label.figure').transition()
                               .text(function(){
                                return costOfLiving
                                  })   
                            d3.select('.household-income.label.figure').transition()  
                               .text(function(){

                                  return "$"+Math.round(householdIncome) +"k"
                                  }) 
                          

                            console.log(Number(householdIncome))   

                            var affordability = parseFloat(costOfLiving)-parseFloat(householdIncome);

                            if (affordability <= 60 ) {
                              console.log(affordability)
                              var highlight = $('.highlight[data-value="Affordable"]').index()
                              $('.highlight[data-value="Affordable"]').parent().attr('data-value', function(){
                               return highlight
                              })
                            };

                            if (affordability >= 61 && affordability <= 100 ) {
                               console.log(affordability)
                               var highlight = $('.highlight[data-value="Not So Affordable"]').index()
                                $('.highlight[data-value="Not So Affordable"]').parent().attr('data-value', function(){
                                  return highlight
                                })
                            } ;

                            if (affordability > 101 ){
                              console.log(affordability)
                              var highlight = $('.highlight[data-value="Unaffordable"]').index()
                                $('.highlight[data-value="Unaffordable"]').parent().attr('data-value', function(){
                                  return highlight
                                })
                            }  
                         } // renderValues 

                    renderValues();  

                    $('.what-drives-this [data-city="Miami"]').addClass('active')
                    
                    

                   
                    $('.what-drives-this .city.names .city-name').on('click',function(e){
                            
                            $('.what-drives-this .city.names').attr('data-city', $(this).index());
                            $('.what-drives-this .active').removeClass('active');
                            $(this).toggleClass('active')
                                  
                            cityName = $(this).data('city')
                                 
                                 cityData = whatDrivesThis[costOfLivingYear][cityName];
                                 householdIncomeRaw = cityData["Household Income"].replace('$','');
                                 householdIncome = parseFloat(householdIncomeRaw.replace(',','.').replace(' ',''))

                                 costOfLiving = cityData["Cost of Living"];
                                 console.log(costOfLiving)
                                 renderValues();  
                              

                                 
                    });

                    $('.what-drives-this .toggle .value').on('click',function(e){

                            costOfLivingYear = $(this).data('value')
                            console.log(costOfLivingYear)
                          
                            $('.what-drives-this .toggle').attr('data-value', $(this).index())
                                 
                                 cityData = whatDrivesThis[costOfLivingYear][cityName];

                                 
                                 householdIncomeRaw = cityData["Household Income"].replace('$','');
                                 householdIncome = parseFloat(householdIncomeRaw.replace(',','.').replace(' ',''))
                                 costOfLiving = cityData["Cost of Living"];
                              
                                 console.log(cityData)

                             renderValues();  
                              
                               
                        
                        });
                            
                     

                    

      } // Else

 

   


}) // json




}; // whatDrivesThis

// $('#post-1537').waypoint(function(down){
          
          

        whatDrivesThis();
        
// },{triggerOnce:true});

