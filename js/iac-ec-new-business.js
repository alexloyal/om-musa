var radialgraphs = [];

// post-1793 is MIAMI-DADE HOUSING IS AFFORDABLE

$('#post-1793 .data-disclosure [data-dimension="B"],#post-1793 .data-disclosure [data-dimension="C"],#post-1793 .data-disclosure [data-dimension="D"]').css('opacity',0);
$('#post-1793 .data-disclosure [data-dimension="B"],#post-1793 .data-disclosure [data-dimension="C"],#post-1793 .data-disclosure [data-dimension="D"]').addClass('hide');


$('[data-trigger="20"]').waypoint(function(down){


  $('#post-1793 .swap.objective.two').css('opacity','1');
  $('#post-1793 .data-disclosure').css('opacity','1');
  $('#post-1793 .year-labels').css('opacity','1');


	var dataSourceDir = base + '/wp-content/themes/om-musa/';

	d3.tsv(dataSourceDir+'js/data-svg/iac-ec-small-business.tsv', null, function(burdendata) {

		for(var i = 0; i < burdendata.length; i++) {

			var ry1 = "2009"
			var ry2 = "2011"
			var county = burdendata[i]["County"]
			var cityName = $('.circles .radial[data-city="'+county+'"]')
			var dataYear = ry1;

			var label = county.toLowerCase().replace(/ /g, '-');

			$('.city-group .radial').attr('data-year', function(){
				var index = $(this).index();
				if (  index < 1 ) {
					return ry1;
				} else {
					return ry2;
				}
			})

			if(!radialgraphs[0]) { radialgraphs[0] = {} }
			if(!radialgraphs[1]) { radialgraphs[1] = {} }

			radialgraphs[0][county] = new RadialGraph([burdendata[i][ry1]], '[data-year="'+ry1+'"][data-city="'+county+'"]', ''+label+'');
			radialgraphs[1][county] = new RadialGraph([burdendata[i][ry2]], '[data-year="'+ry2+'"][data-city="'+county+'"]', ''+label+'');

			$("#ia-data-1793 .year-labels .item.green").html(ry1);
			$("#ia-data-1793 .year-labels .item.purp").html(ry2);
		}

		$('.city-group').css('opacity', '1');


		$('#post-1793 .data-svg .swap.objective.two .item span').on('click', function(){


				var parentSwap = $(this).parent().data('swap');
                var activeSwap = $('#post-1793 .swap .item[data-swap="1"]');

				var dataDimension = $(this).parent().data('dimension');

				if (parentSwap = "0") {
	                activeSwap.attr('data-swap', '0')
	                $(this).parent().attr('data-swap', '1')
	            };
	            $('#post-1793 .data-disclosure [data-dimension]:not(.hide)').css('opacity',0);
	            $('#post-1793 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
				$('#post-1793 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
				$('#post-1793 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);
				switch(dataDimension) {

                	case "A":
						for(var i = 0; i < burdendata.length; i++) {

							var ry1 = "2009";
							var ry2 = "2011";
							var county = burdendata[i]["County"]
							var label = county.toLowerCase().replace(/ /g, '-');

							radialgraphs[0][county].update([burdendata[i][ry1]]);
							radialgraphs[1][county].update([burdendata[i][ry2]]);

							$("#ia-data-1793 .year-labels .item.green").html(ry1);
							$("#ia-data-1793 .year-labels .item.purp").html(ry2);
						}

                		break;

					case "B":
                		d3.tsv(dataSourceDir+'js/data-svg/iac-ec-cos-small-mid.tsv', function(colidata) {

                			for (var cos = colidata.length - 1; cos >= 0; cos--) {
								console.log(cos)
                				var ry1 = "2009";
  								var ry2 = "2011";
                				var county = colidata[cos]["County"];

								radialgraphs[0][county].update([colidata[cos][ry1]], 1);
								radialgraphs[1][county].update([colidata[cos][ry2]], 1);

								$("#ia-data-1793 .year-labels .item.green").html(ry1);
								$("#ia-data-1793 .year-labels .item.purp").html(ry2);
                			};

                		})
                		break;
                	case "C":

                		d3.tsv(dataSourceDir+'js/data-svg/iac-ec-venture-capital.tsv', function(data) {


                			for (var i = data.length - 1; i >= 0; i--) {

                				var ry1 = "2011";
  								var ry2 = "2013";
                				var county = data[i]["County"]

								radialgraphs[0][county].update([data[i][ry1]], 800);
								radialgraphs[1][county].update([data[i][ry2]], 800);

								$("#ia-data-1793 .year-labels .item.green").html(ry1);
								$("#ia-data-1793 .year-labels .item.purp").html(ry2);
                			};

                		})
                	break;
                	case "D":

                		d3.tsv(dataSourceDir+'js/data-svg/iac-ec-self-employment.tsv', function(data) {


                			for (var i = data.length - 1; i >= 0; i--) {

                				var ry1 = 2009;
  								var ry2 = 2011;
  								console.log(ry1)
  								console.log(ry2)
                				var county = data[i]["County"] == "New York" ? "NYC" : data[i]["County"];

								radialgraphs[0][county].update([data[i][ry1+1]], 60000);
								radialgraphs[1][county].update([data[i][ry2+1]], 60000);

								$("#ia-data-1793 .year-labels .item.green").html(ry1+1);
								$("#ia-data-1793 .year-labels .item.purp").html(ry2+1);
                			};

                		})
                	break;

                }

		 });



	}, function(err, rows) {
	  console.log(err)
	});

},{triggerOnce:true}); // Waypoints
