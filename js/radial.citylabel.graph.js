var RadialGraph = function (data, elementSelector, title, className) {

    /**

    data input format:

    ["NN%", "NN%"]

    Where the first array item is the outer arc, and the second (optional) is the inner arc.
    Be sure to include the % and the entire value as a string (these get parsed out).

    **/

    var graph = this;

    this.data = [];

    d3.select(elementSelector)
                        .append("div")
                        .attr("class", "radial-graph "+className)
                        .append("svg")
                        .attr("width", "80")
                        .attr("height", "80");

    d3.select(elementSelector)
                        .select("div.radial-graph."+className)
                       /* .append("h4")
                        .attr("class", "graph-title")
                        .text(title)*/

    this.update = function(data) {

        var twoPi = 2 * Math.PI;

        var outerArc = d3.svg.arc()
            .innerRadius(31)
            .outerRadius(39)
            .startAngle(0);

        var innerArc = d3.svg.arc()
            .innerRadius(23)
            .outerRadius(28)
            .startAngle(0);

        for(var i = 0; i < data.length; i++) {

            var replaceData = {
                index: i,
                label: data[i],
                endAngle: parseFloat(data[i])/100 * twoPi,
                oldAngle: graph.data[i] ? graph.data[i].endAngle : 0
            }

            this.data[i] = replaceData;
        }

        this.arcs = d3.select(elementSelector)
                            .select("div.radial-graph."+className)
                            .select("svg")
                            .selectAll("g.arc")
                            .data(this.data);

        this.arcs
            .call(function(context) {
                this.select("path.arc")
                    .transition()
                    .duration(750)
                    .ease("elastic")
                    .attrTween("d", arcTween);

                this.select("text.label")
                    .text(function(d) {
                        return d.label;
                    })
                    
                    .attr('transform', "translate(-22,4)")
                    /*
                    .transition()    
                    .attr("transform", function(d) {
                        var r = 60;
                        var x = r * Math.cos( d.endAngle - Math.PI/2);
                        var y = 3 + r * Math.sin( d.endAngle - Math.PI/2);
                        return "translate(" + x + "," + y +")";
                    })*/
            })

        this.arcs
            .enter()
            .append("g")
            .attr("class", "arc")
            .attr("transform", "translate(40,40)")
            .call(function() {

                this.append("circle")
                    .attr("class", "bkg")
                    .attr("cx", "0")
                    .attr("cy", "0")
                    .attr("r", "36");

                this.append("path")
                    .attr("class", function(d, i) {
                        var className = "arc ";
                        if(i > 0){
                            className += "inner"
                        }else {
                            className += "outer"
                        }
                        return className
                    })
                    .transition()
                    .ease("elastic")
                    .duration(750)
                    .attrTween("d", arcTween)

                this.append("text")
                    .attr("class", function(d, i) {
                        var className = "label ";
                        if(i > 0){
                            className += "inner"
                        }else {
                            className += "outer"
                        }
                        return className
                    })
                    .text(function(d) {
                        return d.label;
                    })
                    .attr('transform', "translate(-22,4)")
                    /*
                    .attr("transform", function(d, i) {
                        var r = 60;
                        var x = r * Math.cos( d.endAngle - Math.PI/2);
                        var y = 3 + r * Math.sin( d.endAngle - Math.PI/2);

                        return "translate(" + x + "," + y +")";
                    })*/

            });

        function arcTween(d) {
            var i = d3.interpolate(d.oldAngle, d.endAngle);
            return function(t) {
                if(d.index > 0){
                    return innerArc({endAngle: i(t)});
                }else{
                    return outerArc({endAngle: i(t)});
                }

            };
        }
    }

    this.update(data);


}
