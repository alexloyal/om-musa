// post-1729 is MIAMI-DADE STUDENTS OF ALL AGES THRIVE ACADEMICALLY

$('#post-1729 .data-disclosure [data-dimension="B"],#post-1729 .data-disclosure [data-dimension="C"],#post-1729 .data-disclosure [data-dimension="D"],#post-1729 .data-disclosure [data-dimension="E"], #post-1729 .data-set[data-dimension="B"],#post-1729 .data-set[data-dimension="C"],#post-1729 .data-set[data-dimension="D"],#post-1729 .data-set[data-dimension="E"]').css('opacity',0);
$('#post-1729 .data-disclosure [data-dimension="B"],#post-1729 .data-disclosure [data-dimension="C"],#post-1729 .data-disclosure [data-dimension="D"],#post-1729 .data-disclosure [data-dimension="E"]').addClass('hide');

$('[data-trigger="20"]').waypoint(function(down){

	 $('#post-1729 .swap.objective.two').css('opacity','1');
 	 $('#post-1729 .data-disclosure').css('opacity','1');
 	 $('#post-1729 .year-labels').css('opacity','1');
 	 $('#post-1729 .data-set[data-dimension="D"]').perfectScrollbar();

	var dataSourceDir = base + '/wp-content/themes/om-musa/';

	function removeRadials() {
		$("#ia-data-1729 .radial-graph").remove();
	}

	function cleaned (num) {
		return parseFloat( String(num).replace(/\$|,|%/g, '') );
	}

	function dataSet1() {

		$('#ia-data-1729 .city-group').css('opacity', '');
		$('#ia-data-1729 [data-dimension="A"] .city-group').css('opacity', 1);

		d3.tsv(dataSourceDir+'js/data-svg/iac-ed-math-reading-proficiency.tsv', null, function(data) {
			console.log(data)
			for(var i = 0; i < data.length; i++) {

				var ry1 = String(Object.keys(data[i])[1])
				var ry2 = String(Object.keys(data[i])[2])
				var ry3 = String(Object.keys(data[i])[3])
				var ry4 = String(Object.keys(data[i])[4])

				var segment = data[i]["GEOGRAPHY"]

				var dataYear = ry1;

				var label = segment.toLowerCase().replace(/ /g, '-');



				$('#ia-data-1729 [data-dimension="A"] .city-group .radial.first').attr('data-year', ry1)
				$('#ia-data-1729 [data-dimension="A"] .city-group .radial.second').attr('data-year', ry2)
				$('#ia-data-1729 [data-dimension="A"] .city-group .radial.third').attr('data-year', ry3)
				$('#ia-data-1729 [data-dimension="A"] .city-group .radial.fourth').attr('data-year', ry4)

				var max = Math.max(cleaned(data[i][ry1]) * 1.1, cleaned(data[i][ry2]) * 1.1);

				var radial1 = new RadialGraph([data[i][ry1]], '#ia-data-1729 [data-dimension="A"] [data-year="'+ry1+'"][data-segment="'+segment+'"]', ''+label+'', max);
				var radial2 = new RadialGraph([data[i][ry2]], '#ia-data-1729 [data-dimension="A"] [data-year="'+ry2+'"][data-segment="'+segment+'"]', ''+label+'', max);
				var radial3 = new RadialGraph([data[i][ry3]], '#ia-data-1729 [data-dimension="A"] [data-year="'+ry3+'"][data-segment="'+segment+'"]', ''+label+'', max);
				var radial4 = new RadialGraph([data[i][ry4]], '#ia-data-1729 [data-dimension="A"] [data-year="'+ry4+'"][data-segment="'+segment+'"]', ''+label+'', max);
				console.log(radial1)
				console.log(segment)

				$("#ia-data-1729 .year-labels .item.green").html("2011");
				$("#ia-data-1729 .year-labels .item.purp").html("2013");

			}
		});
	}

	function dataSet2() {

		$('#ia-data-1729 .city-group').css('opacity', '');
		$('#ia-data-1729 [data-dimension="B"] .city-group').css('opacity', 1);


		//FIRST, HIDE ALL CITY GROUPS EXCEPT THE CURRENT ONE, B
		$('#ia-data-1729 .city-group').css('opacity', '');
		$('#ia-data-1729 [data-dimension="B"] .city-group').css('opacity', 1);
		$('#ia-data-1729 [data-dimension="B"] .first,#ia-data-1729 [data-dimension="B"] .second').removeAttr("data-label");

		d3.tsv(dataSourceDir+'js/data-svg/iac-ed-hs-graduation.tsv', function(data) {

			for (var i = data.length - 1; i >= 0; i--) {

				//RY1 AND RY2 ARE ACTUALLY THOSE RANDOM DIMENSIONS OF DATA
				var ry1 = "2010-11";
				var ry2 = "2012-13";
				var label = data[i]["County"].toLowerCase().replace(/ /g, '-');
				console.log(ry1,ry2)
				//FOR THE FIRST AND SECOND RADIALS IN EACH GROUP, ADD THAT RANDOM DIMENSION AS A DATA LABEL
				$('#ia-data-1729 [data-dimension="B"] .first').eq(i).attr("data-label", ry1);
				$('#ia-data-1729 [data-dimension="B"] .second').eq(i).attr("data-label", ry2);

				//SET INDIVIDUAL MAXIMUMS FOR EACH OF THE FIRST + SECOND RADIALS
				var max = 100;


				//CREATE THE RADIALS
				var radial1 = new RadialGraph([data[i][ry1]], '#ia-data-1729 [data-dimension="B"] .first[data-label="' + ry1 + '"]', '' + label + '', max);
				var radial2 = new RadialGraph([data[i][ry2]], '#ia-data-1729 [data-dimension="B"] .second[data-label="' + ry2 + '"]', '' + label + '', max);

				$("#ia-data-1729 .year-labels .item.green").html(ry1);
				$("#ia-data-1729 .year-labels .item.purp").html(ry2);
			};

		})
	}

	function dataSet3() {

		$('#ia-data-1729 .city-group').css('opacity', '');
		$('#ia-data-1729 [data-dimension="C"] .city-group').css('opacity', 1);


		//FIRST, HIDE ALL CITY GROUPS EXCEPT THE CURRENT ONE, E
		$('#ia-data-1729 .city-group').css('opacity', '');
		$('#ia-data-1729 [data-dimension="C"] .city-group').css('opacity', 1);

		d3.tsv(dataSourceDir+'js/data-svg/iac-ed-college-readiness.tsv', function(data) {
				console.log(data)
			for(var i = 0; i < data.length; i++) {

				var ry1 = String(Object.keys(data[i])[1])
				var ry2 = String(Object.keys(data[i])[2])
				var ry3 = String(Object.keys(data[i])[3])
				var ry4 = String(Object.keys(data[i])[4])

				console.log(ry3, ry4)
				var segment = data[i]["County"]

				var dataYear = ry1;

				var label = segment.toLowerCase().replace(/ /g, '-');

				$('#ia-data-1729 [data-dimension="A"] .city-group .radial.first').attr('data-year', ry1)
				$('#ia-data-1729 [data-dimension="A"] .city-group .radial.second').attr('data-year', ry2)
				$('#ia-data-1729 [data-dimension="A"] .city-group .radial.third').attr('data-year', ry3)
				$('#ia-data-1729 [data-dimension="A"] .city-group .radial.fourth').attr('data-year', ry4)

				// var max = Math.max(cleaned(data[i][ry1]) * 1.1, cleaned(data[i][ry2]) * 1.1);
				var max = 100;

				var radial1 = new RadialGraph([data[i][ry1]], '#ia-data-1729 [data-dimension="C"] [data-year="'+ry1+'"][data-segment="'+segment+'"]', ''+label+'', max);
				var radial2 = new RadialGraph([data[i][ry2]], '#ia-data-1729 [data-dimension="C"] [data-year="'+ry2+'"][data-segment="'+segment+'"]', ''+label+'', max);
				var radial3 = new RadialGraph([data[i][ry3]], '#ia-data-1729 [data-dimension="C"] [data-year="'+ry3+'"][data-segment="'+segment+'"]', ''+label+'', max);
				var radial4 = new RadialGraph([data[i][ry4]], '#ia-data-1729 [data-dimension="C"] [data-year="'+ry4+'"][data-segment="'+segment+'"]', ''+label+'', max);

				console.log(radial1)
				console.log(segment)

				$("#ia-data-1729 .year-labels .item.green").html("State Universities");
				$("#ia-data-1729 .year-labels .item.purp").html("Community Colleges");

			}

		})
	}

	function dataSet4() {

		$('#ia-data-1729 .city-group').css('opacity', '');
		$('#ia-data-1729 [data-dimension="D"] .city-group').css('opacity', 1);


		//FIRST, HIDE ALL CITY GROUPS EXCEPT THE CURRENT ONE, E
		$('#ia-data-1729.city-group').css('opacity', '');
		$('#ia-data-1729 [data-dimension="D"] .city-group').css('opacity', 1);

		d3.tsv(dataSourceDir+'js/data-svg/iac-ed-attainment.tsv', function(data) {

			for (var i = data.reverse().length - 1; i >= 0; i--) {

				//RY1 AND RY2 ARE ACTUALLY THOSE RANDOM DIMENSIONS OF DATA
				var ry1 = "Did not graduate from High School 2011";
				var ry2 = "Did not graduate from High School 2013";
				var ry3 = "High School Diploma Only 2011";
				var ry4 = "High School Diploma Only 2013";
				var ry5 = "Some College to 2-year Associate's 2011";
				var ry6 = "Some College to 2-year Associate's 2013";
				var ry7 = "4-year Bachelor's or higher 2011";
				var ry8 = "4-year Bachelor's or higher 2013";

				console.log(ry1, ry2, ry3, ry4, ry5, ry6, ry7, ry8)
				var label = data[i]["County"].toLowerCase().replace(/ /g, '-');

				//FOR THE FIRST AND SECOND RADIALS IN EACH GROUP, ADD THAT RANDOM DIMENSION AS A DATA LABEL
				// $('#ia-data-1729 [data-dimension="D"] .first').eq(i).attr("data-label", ry1);
				//$('#ia-data-1729 [data-dimension="D"] .second').eq(i).attr("data-label", ry2);

				//SET INDIVIDUAL MAXIMUMS FOR EACH OF THE FIRST + SECOND RADIALS
				var max1 = 100;

				//CREATE THE RADIALS
				var radial1 = new RadialGraph([data[i][ry1]], '#ia-data-1729 [data-dimension="D"] .first[data-label="' + ry1 + '"]', '' + label + '', max1);
				var radial2 = new RadialGraph([data[i][ry2]], '#ia-data-1729 [data-dimension="D"] .second[data-label="' + ry2 + '"]', '' + label + '', max1);
				var radial3 = new RadialGraph([data[i][ry3]], '#ia-data-1729 [data-dimension="D"] .first[data-label="' + ry3 + '"]', '' + label + '', max1);
				var radial4 = new RadialGraph([data[i][ry4]], '#ia-data-1729 [data-dimension="D"] .second[data-label="' + ry4 + '"]', '' + label + '', max1);
				var radial5 = new RadialGraph([data[i][ry5]], '#ia-data-1729 [data-dimension="D"] .first[data-label="' + ry5 + '"]', '' + label + '', max1);
				var radial6 = new RadialGraph([data[i][ry6]], '#ia-data-1729 [data-dimension="D"] .second[data-label="' + ry6 + '"]', '' + label + '', max1);
				var radial7 = new RadialGraph([data[i][ry7]], '#ia-data-1729 [data-dimension="D"] .first[data-label="' + ry7 + '"]', '' + label + '', max1);
				var radial8 = new RadialGraph([data[i][ry8]], '#ia-data-1729 [data-dimension="D"] .second[data-label="' + ry8 + '"]', '' + label + '', max1);

			//	$("#ia-data-1729 .year-labels .item.green").html(ry1);
			//	$("#ia-data-1729 .year-labels .item.purp").html(ry2);
			};

		})
	}

	function dataSet5() {

		$('#ia-data-1729 .city-group').css('opacity', '');
		$('#ia-data-1729 [data-dimension="E"] .city-group').css('opacity', 1);


		//FIRST, HIDE ALL CITY GROUPS EXCEPT THE CURRENT ONE, E
		$('#ia-data-1729 .city-group').css('opacity', '');
		$('#ia-data-1729 [data-dimension="E"] .city-group').css('opacity', 1);
		$('#ia-data-1729 [data-dimension="E"] .first, #ia-data-1729 [data-dimension="E"] .second').removeAttr("data-label");


		d3.tsv(dataSourceDir+'js/data-svg/iac-ed-post-secondary.tsv', function(data) {

			for (var i = data.reverse().length - 1; i >= 0; i--) {

				//RY1 AND RY2 ARE ACTUALLY THOSE RANDOM DIMENSIONS OF DATA
				var ry1 = String(Object.keys(data[i])[1])
				var ry2 = String(Object.keys(data[i])[2])

				var label = data[i]["County"].toLowerCase().replace(/ /g, '-');
				console.log(ry1,ry2)
				//FOR THE FIRST AND SECOND RADIALS IN EACH GROUP, ADD THAT RANDOM DIMENSION AS A DATA LABEL
				$('#ia-data-1729 [data-dimension="E"] .first').eq(i).attr("data-label", ry1);
				$('#ia-data-1729 [data-dimension="E"] .second').eq(i).attr("data-label", ry2);

				//SET INDIVIDUAL MAXIMUMS FOR EACH OF THE FIRST + SECOND RADIALS
				var max1 = 100;

				//CREATE THE RADIALS
				var radial1 = new RadialGraph([data[i][ry1]], '#ia-data-1729 [data-dimension="E"] .first[data-label="' + ry1 + '"]', '' + label + '', max1);
				var radial2 = new RadialGraph([data[i][ry2]], '#ia-data-1729 [data-dimension="E"] .second[data-label="' + ry2 + '"]', '' + label + '', max1);

				$("#ia-data-1729 .year-labels .item.green").html(ry1);
				$("#ia-data-1729 .year-labels .item.purp").html(ry2);
			};

		})
	}

	$('#ia-data-1729 .swap.objective.two .item span').on('click', function() {

			var parentSwap = $(this).parent().data('swap');
            var activeSwap = $('#post-1729 .swap .item[data-swap="1"]');

			var dataDimension = $(this).parent().data('dimension');

			if (parentSwap = "0") {
                activeSwap.attr('data-swap', '0')
                $(this).parent().attr('data-swap', '1')
            };

            $('#ia-data-1729 .year-labels').attr('data-dimension-active', dataDimension );

			$('#post-1729 .data-disclosure [data-dimension]:not(.hide)').css('opacity',0);
			$('#post-1729 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
			$('#post-1729 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
			$('#post-1729 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

			
            

			$('#post-1729 .data-set[data-dimension]').css('opacity',0);
			$('#post-1729 .data-set[data-dimension] h4').css('opacity',0);
			$('#post-1729 .data-set[data-dimension="'+dataDimension+'"]').css('opacity',1);
			$('#post-1729 .data-set[data-dimension="'+dataDimension+'"] h4').animate({opacity:1},200);

			$("#post-1729 .data-set[data-dimension]").hide();
			$('#post-1729 .data-set[data-dimension="' + dataDimension + '"]').show();

			switch(dataDimension) {

            	case "A":
					removeRadials();
					dataSet1();
            		break;
				case "B":
            		removeRadials();
					dataSet2();
            		break;
            	case "C":
					removeRadials();
					dataSet3();
            		break;
            	case "D":
					removeRadials();
					dataSet4();
            		break;
            	case "E":
					removeRadials();
					dataSet5();
            		break;
            }

	 });



dataSet1();




},{triggerOnce:true}); // Waypoints
