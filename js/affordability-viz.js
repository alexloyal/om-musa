function LetsStartFromBottom(){


    /** GRADUATION RATE - MIDDLE GRAPH MAP **/

var graduationYear = "2012";
var graduationData = [];

var dataSourceDir = base + '/wp-content/themes/om-musa/';

d3.csv(dataSourceDir + "js/data-svg/affordability.csv", function(error,data) {
    for (var i = 0; i < data.length; i++) {
        var datum = data[i];

        if (datum.County != "US") {
            graduationData.push(datum);

            switch(datum.County) {
                case "Chicago":
                    datum.cx = 520;
                    datum.cy = 160;
                    break;
                case "Houston":
                    datum.cx = 420;
                    datum.cy = 380;
                    break;
                case "Miami":
                    datum.cx = 618;
                    datum.cy = 430;
                    break;
                case "NYC":
                    datum.cx = 680;
                    datum.cy = 210;
                    break;
                case "San Diego":
                    datum.cx = 110;
                    datum.cy = 280;
                    break;
            }
        }
    }
  //  console.log(graduationData)
    renderGraduationRates();
});

function renderGraduationRates() {
    var cities = d3.select(".affordability .map")
                    .selectAll("g.city")
                    .data(graduationData);

    var radius = 40;

    var radiusScale = d3.scale.linear().domain([0, 100]).range([0, radius]);

    cities.call(function() {
          // These two are one dimention. Duplicate to create another dimenison and refer to line 85.
            this.select("circle.value")
                .transition()
                .attr("r", function(d) {
                    
                //  var radius = isVolunteerShown ? radiusScale( thedatathing) : 0;
               //  return radius;

                    return radiusScale( parseFloat(d[graduationYear]) );
                })

            this.select("text.value")
                .text(function(d) {
                    return Math.round( parseFloat(d[graduationYear]) ) + "%";
                })
        })

    cities.enter()
            .append("g")
            .attr("class", "city")
            .attr("data-city", function(d) {
                return d.County;
            })
            .call(function() {
                this.append("circle")
                    .attr("class", "total")
                    .attr("cx", function(d) {
                        return d.cx;
                    })
                    .attr("cy", function(d) {
                        return d.cy;
                    })
                    .attr("r", radius)
// This is where you would duplicate to add the second circle. 
                this.append("circle")
                    .attr("class", "value")
                    .attr("cx", function(d) {
                        return d.cx;
                    })
                    .attr("cy", function(d) {
                        return d.cy
                    })
                    .transition()
                    .attr("r", function(d) {
                        return radiusScale( parseFloat(d[graduationYear]) );
                    })

                this.append("text")
                    .attr("class", "value")
                    .text(function(d) {
                        return Math.round( parseFloat(d[graduationYear]) ) + "%";
                    })
                    .attr("transform", function(d) {
                        var x,y;

                        switch(d.County) {
                            case "Chicago":
                                x = 389;
                                y = 175;
                                break;
                            case "Houston":
                                x = 395;
                                y = 462;
                                break;
                            case "Miami":
                                x = 665;
                                y = 460;
                                break;
                            case "NYC":
                                x = 662;
                                y = 290;
                                break;
                            case "San Diego":
                                x = 156;
                                y = 280;
                                break;
                        }

                        return "translate(" + x + "," + y + ")";
                    })

                this.append("text")
                    .attr("class", "label")
                    .text(function(d) {
                        return d.County;
                    })
                    .attr("transform", function(d) {
                        var x,y;

                        switch(d.County) {
                            case "Chicago":
                                x = d.cx-32;
                                y = 218;
                                break;
                            case "Houston":
                                x = d.cx-32;
                                y = 330;
                                break;
                            case "Miami":
                                x = 618;
                                y = 383;
                                break;
                            case "NYC":
                                x = 608;
                                y = d.cy+4;
                                break;
                            case "San Diego":
                                x = d.cx-38;
                                y = 338;
                                break;
                        }

                        return "translate(" + x + "," + y + ")";
                    })
            }) // Call
} // renderGraduationRates

$(document).ready(function () {
    $(".affordability .toggle .value").on("click", function(e) {
        $(".affordability .toggle").attr("data-value", $(this).index());
        graduationYear = $(this).attr("data-value");
        renderGraduationRates();
    })
})

    
 


};
 $(document).ready(function(){
    //$('.hentry .data-svg').css('opacity', 1)
    LetsStartFromBottom();
});

