function SeaLevelChange(){



   var dimension ="Inches above 1990 Sea Level";
   var barWidth = 30;
   var height = 350;
   var width = 500;
   var offset = 20;
   var seaLevel = [];
   var years = [];



// base is set in footer.php.

var dataSourceDir = base + '/wp-content/themes/om-musa/';

d3.tsv(dataSourceDir + "js/data-svg/data-sea-level.tsv", function(error,data) {

        for (var i = data.length - 1; i >= 0; i--) {
            seaLevel.push(data[i][dimension]);
            years.push(data[i]["Year"])


        };


   

    var yscale = d3.scale.linear().domain([0,d3.max(seaLevel)]).range([0,height]);
    var xscale = d3.scale.ordinal().domain(d3.range(0,data.length)).rangeBands([0,width]);

    var firstYear = d3.min(years);
    var lastYear = d3.max(years);





    var svg = d3.select('#data-svg-1568 .sea-levels').append('svg')
                    .attr('width',width)
                    .attr('height',height);


    var gradient =  svg.append('defs').append('linearGradient')
                        .attr('gradientUnits','userSpaceOnUse' )
                        .attr('x1',0).attr('y1', 0)
                        .attr('x2',0).attr('y2', 500)
                        .attr('id', 'sea-level-gradient')

                    gradient.append("stop")
                        .attr("offset", "0")
                        .transition()
                        .attr("stop-color", "#1AAFDB")



                    gradient.append("stop")
                        .attr("offset", "50%")
                        .transition()
                        .attr("stop-color", "#CCEFF8");



                    svg.selectAll('rect.bar.grey')
                        .data(data)
                        .enter()
                        .call(function(){
                            this.append('rect')
                                .attr('class', 'bar grey')
                                .attr('fill', 'url(#sea-level-gradient)')
                                .attr('width', function(d,i){
                                    return width/data.length;
                                })
                                .attr('data-year', function(d){
                                return d["Year"]
                                } )
                                .attr('y', function(d){
                                return height-yscale(d[dimension])
                                })
                                .attr('data-value', function(d){

                                return d[dimension]
                                })
                                .attr('height', function(d){
                                return yscale(d[dimension])
                                })
                                .attr('x', function(d,i){
                                return i * xscale.rangeBand()
                                })
                            this.append('text')
                                .text(function(d){
                                    return d[dimension]+'"'
                                })
                                .attr('data-year', function(d){
                                return d["Year"]
                                } )
                                .attr('dy', -1 )
                                .attr('dx', 10)
                                .attr('x', function(d,i){
                                return i * xscale.rangeBand()
                                })
                                .attr('y', function(d){
                                return height-yscale(d[dimension])
                                })
                                .attr('class', 'label hide')


                        })



                    d3.select('#sea-slider').call(d3.slider().axis(true).min(firstYear).max(lastYear).step(1).on("slide", function(evt, value) {

                          console.log(evt, value);

                          $('#data-svg-1568 .label[data-year="'+value+'"]').removeClass('hide');


                          d3.selectAll('rect.bar').attr('class', function(d,i){

                                if (parseInt(d["Year"]) > value) {
                                    return 'bar grey';
                                } else {
                                    return 'bar';
                                }
                          })


                          d3.selectAll('#data-svg-1568 text.label').attr('class', function(d,i){

                                if (parseInt(d["Year"]) <= value) {
                                    return 'label';
                                } else {
                                    return 'label hide';
                                }
                          })



                        }));


            });


};
 $(document).ready(function(){

    SeaLevelChange();
});
