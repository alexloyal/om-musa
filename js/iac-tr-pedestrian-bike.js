// Waypoints
//post-1736 is MIAMI-DADE IS PEDESTRIAN- AND BIKE-FRIENDLY.

$('#post-1736 .data-disclosure [data-dimension="B"],#post-1736 .data-disclosure [data-dimension="C"]').css('opacity', 0);
$('#post-1736 .data-disclosure [data-dimension="B"],#post-1736 .data-disclosure [data-dimension="C"]').addClass('hide');

$('[data-trigger="10"]').waypoint(function(down) {
  


$('#post-1736 .swap.objective.one').css('opacity','1');
$('#post-1736 .data-disclosure').css('opacity','1');
$('#post-1736 .year-labels').css('opacity','1');
 
var dataSourceDir = base + '/wp-content/themes/om-musa/';


d3.json(dataSourceDir+"js/data-svg/iac-ha-financial-resources-income-zero.json", function(error,zeroData) {

            if (error) {
                // If error is not null, something went wrong.
                console.log(error);
                //Log the error.
            } else {
                   

            bars = new BarGraphDollars(zeroData,'.iac-tr-pedestrian-bike .graph');
            $('#ia-data-1736 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2011";
                                      } else {
                                        return "2013";
                                      }
                                    })

            $(document).ready(function(){
                
                d3.json(dataSourceDir+"js/data-svg/iac-tr-pedestrian-bike.json", function(error,data) {
                                 
                                    bars.update(data);

                              })

                    $('#post-1736 .data-svg .swap.objective.one .item span').on('click', function(){        
                        var parentSwap = $(this).parent().data('swap');
                        var activeSwap = $('#post-1736 .swap .item[data-swap="1"]');
                        var dataDimension = $(this).parent().data('dimension');
                        console.log(dataDimension)
                        if (parentSwap = "0") {
                            activeSwap.attr('data-swap', '0')
                            $(this).parent().attr('data-swap', '1')
                        };

                        $('#post-1736 .data-disclosure [data-dimension]:not(.hide)').css('opacity', 0);
                        $('#post-1736 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                        $('#post-1736 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#post-1736 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);


                        if (dataDimension == "A") {
                            d3.json(dataSourceDir+"js/data-svg/iac-tr-pedestrian-bike.json", function(error,data) {
                                 
                                 bars.update(data,'.iac-tr-pedestrian-bike .graph');
                                 /*setTimeout(function(){
                                        bars.update(data,'.iac-tr-pedestrian-bike .graph');
                                    }, 1000); */
                             $('#ia-data-1736 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2011";
                                      } else {
                                        return "2013";
                                      }
                                    })
                              })
                        };

                        if (dataDimension == "B") {
                            d3.json(dataSourceDir+"js/data-svg/iac-tr-access-park.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-tr-pedestrian-bike .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.iac-tr-pedestrian-bike .graph');
                                    }, 1000); */
                               $('.iac-tr-pedestrian-bike [data-label="MIA"] .bar:nth-child(odd) .value .marker').delay(2).attr('data-label','N/A')
                               $('.iac-tr-pedestrian-bike [data-label="MIA"] .bar:nth-child(odd) .value').delay(1).css('height','0')
                               $('#ia-data-1736 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2011";
                                      } else {
                                        return "2013";
                                      }
                                    })  
                              })
                        };

                        if (dataDimension == "C") {
                            d3.json(dataSourceDir+"js/data-svg/iac-tr-pedestrian-fatalities.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-tr-pedestrian-bike .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.iac-tr-pedestrian-bike .graph');
                                    }, 1000); */
                                  $('#ia-data-1736 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2011";
                                      } else {
                                        return "2012";
                                      }
                                    })
                              })
                        };

                    });
                      
            });

        

                 


        } // else
 }); // d3 json civic participation

},{triggerOnce:true});//Waypoints
 