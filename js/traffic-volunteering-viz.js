var trafficYear = "2012";
var cityName = "Miami"; 
var dataSourceDir = base + '/wp-content/themes/om-musa/';

d3.json(dataSourceDir+"js/data-svg/traffic-volunteering.json", function(error,data) {
    if (error) {
    // If error is not null, something went wrong.

        console.log(error);
    //Log the error.

        } else { 

          //  console.log(data[2010]["Miami"]['Commute'])

                

 


 
 

 var RadialGraphSamePath = function (data, elementSelector, title, width, height, radius) {

    /**

    data input format:

    ["NN%", "NN%"]

    Where the first array item is the outer arc, and the second (optional) is the inner arc.
    Be sure to include the % and the entire value as a string (these get parsed out).

    **/

    var graph = this;

    this.data = data;

    d3.select(elementSelector)
                        .append("svg")
                        .attr("class", "radial-graph "+title)
                        .attr("width", width)
                        .attr("height", height)
                        .attr('x', -63 )
                        .attr('y', 29)
                        .attr('viewBox', function(){
                            return '0 0 '+ (width-400) +' ' +(height-400)+''
                        } )
                        .attr('preserveAspectRatio', 'xMinYMin meet');

    d3.select(elementSelector)
                        .append("h4")
                        .attr("class", "graph-title")
                        .text(title)
                        .style("width", "100px");

    this.update = function(data) {

        var twoPi = 2 * Math.PI;

        var outerArc = d3.svg.arc()
            .innerRadius(radius-10)
            .outerRadius(radius-2)
            .startAngle(0);

        var innerArc = d3.svg.arc()
            .innerRadius(radius-10)
            .outerRadius(radius-2)
            .startAngle(0);

        for(var i = 0; i < data.length; i++) {

            var replaceData = {
                index: i,
                label: data[i],
                endAngle: parseFloat(data[i])/100 * twoPi,
                oldAngle: graph.data[i].endAngle ? graph.data[i].endAngle : 0
            }

            data[i] = replaceData;
        }

       // console.log(data);

        graph.data = data;

        this.arcs = d3.select(elementSelector)
                            .select("svg")
                            .selectAll("g.arc")
                            .data(data);

        this.arcs
            .call(function(context) {
                this.select("path.arc")
                    .transition()
                    .duration(250)
                    .ease("circle")
                    .attrTween("d", arcTween);

                this.select("text.label")
                    .text(function(d) {
                        return d.label;
                    })
                     
            })

        this.arcs
            .enter()
            .append("g")
            .attr("class", "arc")
            .attr("transform", "translate(100,70)")
            .call(function() {

                this.append("circle")
                    .attr("class", function(d, i) {
                        var className = "bkg ";
                        if(i > 0){
                            className += "transparent"
                        }else {
                            className += "thick grey"
                        }
                        return className
                    })
                    .attr("x", "0")
                    .attr("y", "0")
                    .attr("r", function(){
                        return radius-10
                    });

                this.append("path")
                    .attr("class", function(d, i) {
                        var className = "arc ";
                        if(i > 0){
                            className += "inner purp fill"
                        }else {
                            className += "outer cyan fill"
                        }
                        return className
                    })
                    .transition()
                    .ease("elastic")
                    .duration(750)
                    .attrTween("d", arcTween)

                this.append("text")
                    .attr('x',function(d, i){
                        if(i > 0) {
                            return -10
                        } else {
                            return -29
                        }
                    })
                    .attr('y', function(d, i){
                        if(i > 0) {
                            return -26
                        } else {
                            return 12
                        }
                    })
                    .attr("class", function(d, i) {
                        var className = "label ";
                        if(i > 0){
                            className += "inner purp fill"
                        }else {
                            className += "outer cyan fill"
                        }
                        return className
                    })
                    .text(function(d) {
                        return d.label;
                    })
                     

            });

        function arcTween(d) {
            var i = d3.interpolate(d.oldAngle, d.endAngle);
            return function(t) {
                if(d.index > 0){
                    return innerArc({endAngle: i(t)});
                }else{
                    return outerArc({endAngle: i(t)});
                }

            };
        }
    }

    this.update(data);


}
            
function trafficVolunteeringArc() { 
                               // console.log(cityName);
                               // console.log(trafficYear);
                                RadialGraphSamePath(
                                [data[trafficYear][cityName]['Commute'], 
                                 data[trafficYear][cityName]['Volunteer']],
                                  '#tacho', "",'600','500', '50');
                            }



    $(document).ready(function(){
                    
                   $('.traffic-volunteering [data-city="Miami"]').addClass('active')
                    
                    trafficVolunteeringArc();

                   
                    $('.traffic-volunteering .city.names .city-name').on('click',function(e){
                        $('.traffic-volunteering .city.names').attr('data-city', $(this).index());
                            
                            $('.traffic-volunteering .active').removeClass('active');
                            
                            $(this).toggleClass('active')
                            
                                cityName = $(this).data('city')
                            
                             trafficVolunteeringArc();
                    });

                    $('.traffic-volunteering .toggle .value').on('click',function(e){

                            trafficYear = $(this).data('value')
                         
                          
                        $('.traffic-volunteering .toggle').attr('data-value', $(this).index())
                             
                             trafficVolunteeringArc();
                               
                        
                        });


                    

                            

                           

    });

          /*  for (var city in data) {
               var obj = data[city];
               for (var prop in obj) {
                  // important check that this is objects own property 
                  // not from prototype prop inherited
                  if(obj.hasOwnProperty(prop)){
                    
                    

                  }
               }
            } */

             

            //  var City = $('.track').data('city')

            

        }
})



