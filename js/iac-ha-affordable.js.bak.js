$(document).ready(function(){



	var dataSourceDir = base + '/wp-content/themes/om-musa/';



	d3.tsv(dataSourceDir+'js/data-svg/iac-ha-affordability-burden.tsv', function(data) {
	  
	    

	  console.log()

	  	console.log(data)
		var county = data["County"]
  		var cityName = $('.circles .radial[data-city="'+county+'"]')
  		$('.city-group .radial').attr('data-year', function(){
  			
  			var index = $(this).index();
  			if (  index < 0 ) {
  				return String(Object.keys(data)[0])	
  			} else {
  				return String(Object.keys(data)[1])
  			}
		  			
  		})

  		var dataYear = $('.circles .radial').data('year');
  		var ry1 = String(Object.keys(data)[0])	
  		var ry2 = String(Object.keys(data)[1])
  		label = county.toLowerCase().replace(/ /g, '-');
		var cityRadialA = new RadialGraph([data[ry1]], '[data-year="'+ry1+'"][data-city="'+county+'"]', ''+label+'');
		var cityRadialB = new RadialGraph([data[ry2]], '[data-year="'+ry2+'"][data-city="'+county+'"]', ''+label+'');		
	 	
		console.log(dataYear)
		$('.data-svg .swap.objective.two .item span').on('click', function(){ 
				
				
				var parentSwap = $(this).parent().data('swap');
                var activeSwap = $('.swap .item[data-swap="1"]');
                var dataDimension = $(this).parent().data('dimension');
                if (parentSwap = "0") {
                            activeSwap.attr('data-swap', '0')
                            $(this).parent().attr('data-swap', '1')
                        };
                switch(dataDimension) {
                	
                	case "A":
                		
                		cityRadialA.update([data[ry1]], '.first[data-city="'+county+'"]', ''+label+'');
						cityRadialB.update([data[ry2]], '.second[data-city="'+county+'"]', ''+label+'');	
                	break;
                	case "B":
				          
                		d3.tsv(dataSourceDir+'js/data-svg/iac-ha-affordability-coli.tsv', function(data) {
                			for (var i = data.length - 1; i >= 0; i--) {
                				
                				var ry1 = String(Object.keys(data[i])[0])	
  								var ry2 = String(Object.keys(data[i])[1])
                				var county = data[i]["County"]
                				cityRadialA.update([data[i][ry1]/10], '.first[data-city="'+county+'"]', ''+label+'');
								cityRadialB.update([data[i][ry2]/10], '.second[data-city="'+county+'"]', ''+label+'');			 
                			};
                		
                		})
                	break;
                	case "C":
				          
                		d3.tsv(dataSourceDir+'js/data-svg/iac-ha-affordability-homeownership.tsv', function(data) {
                			


                			for (var i = data.length - 1; i >= 0; i--) {
                				
                				var ry1 = String(Object.keys(data[i])[0])	
  								var ry2 = String(Object.keys(data[i])[1])
                				var county = data[i]["County"]
                				cityRadialA.update([data[i][ry1]], '.first[data-city="'+county+'"]', ''+label+'');
								cityRadialB.update([data[i][ry2]], '.second[data-city="'+county+'"]', ''+label+'');			 
                			};
                		
                		})
                	break;
                	case "D":
				          
                		d3.tsv(dataSourceDir+'js/data-svg/iac-ha-affordability-constcost.tsv', function(data) {
                			
                			console.log(data)

                			for (var i = data.length - 1; i >= 0; i--) {
                				
                				var ry1 = String(Object.keys(data[i])[0])	
  								var ry2 = String(Object.keys(data[i])[1])
                				var county = data[i]["County"]
                				var currency1 = data[i][ry1];
                				var currency2 = data[i][ry2];
                				var number1 = Number(currency1.replace(/[^0-9\.]+/g,""));
                				var number2 = Number(currency2.replace(/[^0-9\.]+/g,""));
                					console.log(number1)

                					
                				cityRadialA.update([number1/10000], '.first[data-city="'+county+'"]', ''+label+'');
								cityRadialB.update([number2/10000], '.second[data-city="'+county+'"]', ''+label+'');			 
                			};
                		
                		})
                	break;
                	case "E":
				          
                		d3.tsv(dataSourceDir+'js/data-svg/iac-ha-affordability-msp.tsv', function(data) {
                			
                			console.log(data)

                			for (var i = data.length - 1; i >= 0; i--) {
                				
                				var ry1 = String(Object.keys(data[i])[0])	
  								var ry2 = String(Object.keys(data[i])[1])
                				var county = data[i]["County"]
                				var currency1 = data[i][ry1];
                				var currency2 = data[i][ry2];
                				var number1 = Number(currency1.replace(/[^0-9\.]+/g,""));
                				var number2 = Number(currency2.replace(/[^0-9\.]+/g,""));
                					console.log(number1)

                					
                				cityRadialA.update([number1/10000], '.first[data-city="'+county+'"]', ''+label+'');
								cityRadialB.update([number2/10000], '.second[data-city="'+county+'"]', ''+label+'');			 
                			};
                		
                		})
                	break;
                }        

		 });

	 

	}, function(err, rows) {
	  console.log(err)
	});

}); // document ready