



function pedestrianFatalities(){

    var pDataset; // GLoabl variable
    var dataSource = base + '/wp-content/themes/om-musa/js/data-svg/pedestrian-fatalities.csv';
           
    d3.csv(dataSource,  function(error,data) {
        
        if (error) {
    // If error is not null, something went wrong.

        console.log(error);
    //Log the error.

        } else { 
          
          pDataset = data;

          

          var nest = d3.nest()
                    .key(function(d) { return d.City; })
                    .key(function(d) { return d.y2009; })
                    .entries(data);




          

          nest.sort(function(a,b){
            return d3.descending(a.key, b.key);
          });





          // Creating an array with each city name

         var cities = [];

          for (var i = nest.length - 1; i >= 0; i--) {
            cities.push(nest[i].key)

          };

          

          // This was a good year 2009
          var year2009 = [];

          for (var i = nest.length - 1; i >= 0; i--) {
            year2009.push(nest[i].values[0].key)
          };

          // Taking it way back to 2010
          
          var year2010 =[]
          for (var i = nest.length - 1; i >= 0; i--) {
            year2010.push(nest[i].values[0].values[0].y2010)
          };

          // This year was kind of a blurr actually, 2011
          
          var year2011 =[]
          for (var i = nest.length - 1; i >= 0; i--) {
            year2011.push(nest[i].values[0].values[0].y2011)
          };

          // This year was magnificent, 2012

          var year2012 =[]
          for (var i = nest.length - 1; i >= 0; i--) {
            year2012.push(nest[i].values[0].values[0].y2012)
          };



          

          

          // Breaking up each individual city with its data.

          var chicago = nest[5].values[0].values[0];
          var houston = nest[4].values[0].values[0];
          var miami = nest[3].values[0].values[0];
          var nyc = nest[2].values[0].values[0];
          var sandiego = nest[1].values[0].values[0];
          var usa = nest[0].values[0].values[0];


      
          
          var margin = { top: 10, right: 10, bottom: 10, left: 10 };
          var height = 600 - margin.left - margin.right,
              width = 900 - margin.top - margin.bottom,
              trackWidth = 20,
              trackOffset = 85,
              padding = 20;

          yScale = d3.scale.linear()
                  .domain([0,3])
                  .range([0,height]);

          xScale = d3.scale.ordinal()
                  .domain(d3.range(0,cities.length))
                  .rangeBands([0,width], 0.8,0.2);



          // Draws the  main svg and then the tracks. 

          d3.select('.post-1513 .data-svg')
                    .append('svg')
                    .attr('viewBox', '0 0 '+width+' '+height+'')
                    .attr('class','data-container' )
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', height + margin.top + margin.bottom)
                    .append('g')
                    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
                    .selectAll('rect')
                    .data(cities)
                    .enter().append('rect')
                        .attr('x', function(d,i){
                           return xScale(i)
                        })
                        .attr('id', function(d,i){
                          return 'track-'+cities[i]
                        })
                        .attr('class','spin-tha-track')
                        .attr('rx', 15)
                        .attr('y',0 )
                        .attr('width', xScale.rangeBand())
                        .attr('height', height)
                        .style('fill', 'rgb(0,175,219)');

            


            // Drawing the circles 

            d3.select('.data-container')
                    .append('g')
                    .attr('class','pedestrian-circles' )
                    .selectAll('circle')
                    .data(cities)
                    .enter().append('circle')
                          
                          .attr('cx', function(d,i){
                           return xScale(i)+25
                          })
                          .attr('id', function(d,i){
                          return 'trackdot-'+cities[i]
                          })
                          .attr('r', '10')
                          .style('fill', '#fff');             
              
             


                
                     
           
            


            // Adding Cities
            
          d3.select('.post-1513 .data-svg')
                    .append('div')
                    .attr('class', 'city-labels')
                    .append('svg')
                    .style('padding', '15px')
                    .attr('viewBox', '0 0 '+width+' 20')
                    .attr('class','data-container' )
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', margin.top + margin.bottom)
                    .append('g')  
                    .selectAll('text')
                    .data(cities)
                    .enter().append('text')
                        .text(function(d,i){
                          return cities[i]
                        })
                        .style('font-family', 'gotham_htfbook')
                        .style('font-size', '1.6em')
                        .style('text-align', 'middle')
                        .attr('dx', function(d,i){
                           return xScale(i)+10
                        })
                        
                        .attr('dy',padding )
                        .attr('width', xScale.rangeBand())
                        .attr('height', height)
                        .style('fill', 'rgb(39,38,38) ');


            // Adding years



            var yearLabels = ['2009','2010','2011','2012'];

            yearXScale = d3.scale.ordinal()
                  .domain(d3.range(0,yearLabels.length))
                  .rangeBands([0,width], 0.7,0.2);

            d3.select('.post-1513 .data-svg')
                    .append('div')
                    .attr('class', 'pedestrian-years')
                    .append('svg')
                    
                    .attr('viewBox', '0 0 '+width+' 20')
                    .attr('class','data-container' )
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', 60 + margin.top + margin.bottom)
                    .append('g')
                    .attr('id', 'pedestrian-year-labels' )  
                    .selectAll('rect')
                    .data(yearLabels)
                    .enter().append('rect')
                        .attr('x', function(d,i){
                           return yearXScale(i)
                        })
                        .attr('y', 5)
                        .style('padding', padding)
                        .attr('width', yearXScale.rangeBand())
                        .attr('height', 30)
                        .attr('class', 'year-label-bg')
                        .attr('id', function(d,i){
                          return 'lbg-y'+yearLabels[i]
                        })
                d3.select('#pedestrian-year-labels')
                        .selectAll('text')
                        .data(yearLabels)
                        .enter()
                        .append('text')
                        .text(function(d,i){
                          return yearLabels[i]
                        })
                        .attr('id', function(d,i){
                          return 'py'+yearLabels[i]
                        })
                        
                        
                        .attr('dx', function(d,i){
                           return yearXScale(i)+10
                        })
                        
                        .attr('dy',padding+10 )
                        .attr('width', yearXScale.rangeBand())
                        .attr('height', height)
                        .attr('class', 'p-year-label')
                d3.select('#pedestrian-year-labels')
                        .append('rect')
                        .attr('id', 'year-labels-slider-path')
                        .attr('width', width)
                        .attr('height', 5)
                        .attr('y', 0)
                d3.select('#pedestrian-year-labels')
                         .append('circle')
                         .attr('id', 'year-labels-slider-dot')
                         .attr('cx', 0)
                         .attr('cy', 0)
                         .attr('r', 9)
                         .style('fill', 'rgb(125,65,153)')

                        ;
                d3.select('.pedestrian-years')
                        .append('span')
                        .attr('class', 'tiny-explore')
                        .append('h4')
                        .append('text')
                        .text('EXPLORE');



                         // This places the circles  and labels   

                function drawValues(y){
                  for (var i = cities.length - 1; i >= 0; i--) {
                   d3.select('#trackdot-'+cities[i])
                      .transition().duration(600)
                      // This sets the height of the dot.
                      .attr('cy', height-(y[i]*186) )
                   d3.select('.pedestrian-circles') 
                        .selectAll('text')
                        .attr('x', function(cities,i){
                           return width-85-xScale(i)
                          });
                   d3.select('.pedestrian-circles')   
                      
                      .append('text')
                      .text(y[i])
                        .data(cities)
                        .attr('id', function(d,i){
                            
                            return 'data-label-'+d
                        })
                        
                        .attr('width', xScale.rangeBand())
                        .attr('height', height-padding)
                        .style('fill', 'rgb(0,175,219)')

                        // This is what sets the height of the label
                        .attr('dx', function(){
                          
                        })
                        .attr('dy', height )
                        .style('font-family', 'gotham_htfblack')
                        .style('font-size', '2.2em')
                        .style('text-align', 'middle')
                        .transition().duration(600)
                        .attr('dy', height-(y[i]*186) )

                        
                    
                 }; 

                }

                function clearData(){

                  d3.select('.pedestrian-circles')                         
                      .selectAll('text')
                      .remove();
                  d3.select('#trackdot-'+cities[i])
                      .attr('cy', 0 )
                };

                
             // This is the golden ticket

                $(document).ready(function(){
                        d3.select('#py2009').attr('class', 'p-year-label  active');
                        d3.select('#lbg-y2009').attr('class', 'year-label-bg  active');
                        var dotX = d3.select('#lbg-y2009').attr('x');
                        d3.select('#year-labels-slider-dot').transition().attr('cx',function(){
                          return Number(dotX) + 30
                        });
                        drawValues(year2009);

                });

                $('#py2009').on('click', function(){
                    d3.select('#pedestrian-year-labels .p-year-label.active').attr('class', 'p-year-label ');
                    d3.select('.year-label-bg.active').attr('class', 'year-label-bg ');
                    d3.select('#py2009').attr('class', 'p-year-label active');
                    d3.select('#lbg-y2009').attr('class', 'year-label-bg active');
                    var dotX = d3.select('#lbg-y2009').attr('x');
                        d3.select('#year-labels-slider-dot').transition().attr('cx',function(){
                          return Number(dotX) + 30
                        });
                    clearData();
                    drawValues(year2009);
                });

                $('#py2010').on('click', function(){
                  d3.select('#pedestrian-year-labels .p-year-label.active').attr('class', 'p-year-label ');
                  d3.select('.year-label-bg.active').attr('class', 'year-label-bg ');
                  d3.select('#py2010').attr('class', 'p-year-label active');
                  d3.select('#lbg-y2010').attr('class', 'year-label-bg active');
                  var dotX = d3.select('#lbg-y2010').attr('x');
                        d3.select('#year-labels-slider-dot').transition().attr('cx',function(){
                          return Number(dotX) + 30
                        });
                    clearData();
                    drawValues(year2010);
                });

                $('#py2011').on('click', function(){
                  d3.select('#pedestrian-year-labels .p-year-label.active').attr('class', 'p-year-label ');
                  d3.select('.year-label-bg.active').attr('class', 'year-label-bg ');
                  d3.select('#py2011').attr('class', 'p-year-label active');
                  d3.select('#lbg-y2011').attr('class', 'year-label-bg active');
                  var dotX = d3.select('#lbg-y2011').attr('x');
                        d3.select('#year-labels-slider-dot').transition().attr('cx',function(){
                          return Number(dotX) + 30
                        });
                    clearData();
                    drawValues(year2011);
                });

                $('#py2012').on('click', function(){
                  d3.select('#pedestrian-year-labels .p-year-label.active').attr('class', 'p-year-label ');
                  d3.select('.year-label-bg.active').attr('class', 'year-label-bg ');
                  d3.select('#py2012').attr('class', 'p-year-label active');
                  d3.select('#lbg-y2012').attr('class', 'year-label-bg active');
                  var dotX = d3.select('#lbg-y2012').attr('x');
                        d3.select('#year-labels-slider-dot').transition().attr('cx',function(){
                          return Number(dotX) + 30
                        });
                    clearData();
                    drawValues(year2012);
                });


                     
 
                       


                    } // Else
        }); // d3 csv
};


pedestrianFatalities();