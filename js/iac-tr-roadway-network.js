// Waypoints
//post-1761 is MIAMI-DADE INVESTS IN A EFFICIENT ROADWAY NETWORK

$('#post-1761 .data-disclosure [data-dimension="B"],#post-1761 .data-disclosure [data-dimension="C"]').css('opacity',0);
$('#post-1761 .data-disclosure [data-dimension="B"],#post-1761 .data-disclosure [data-dimension="C"]').addClass('hide');

$('[data-trigger="30"]').waypoint(function(down) {
  

$('#post-1761 .swap.objective.one').css('opacity','1');
$('#post-1761 .data-disclosure').css('opacity','1');
$('#post-1761 .year-labels').css('opacity','1');



//

 
var dataSourceDir = base + '/wp-content/themes/om-musa/';


d3.json(dataSourceDir+"js/data-svg/iac-ha-financial-resources-income-zero.json", function(error,zeroData) {

            if (error) {
                // If error is not null, something went wrong.
                console.log(error);
                //Log the error.
            } else {
                     
                
            bars = new BarGraphDollars(zeroData,'.iac-tr-roadway-network .graph');
               $('#ia-data-1761 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2011";
                                      } else {
                                        return "2013";
                                      }
                                    })

            $(document).ready(function(){
                
                d3.json(dataSourceDir+"js/data-svg/iac-tr-auto-commute-time.json", function(error,data) {
                                 
                                    bars.update(data);

                              })

                    $('#ia-data-1761.data-svg .swap.objective.one .item span').on('click', function(){        
                        var parentSwap = $(this).parent().data('swap');
                        var activeSwap = $('#ia-data-1761 .swap .item[data-swap="1"]');
                        var dataDimension = $(this).parent().data('dimension');
                        console.log(dataDimension)
                        if (parentSwap = "0") {
                            activeSwap.attr('data-swap', '0')
                            $(this).parent().attr('data-swap', '1')
                        };
                        $('#post-1761 .data-disclosure [data-dimension]:not(.hide)').css('opacity',0);
                        $('#post-1761 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                        $('#post-1761 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#post-1761 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

                        if (dataDimension == "A") {
                            

                            d3.json(dataSourceDir+"js/data-svg/iac-tr-auto-commute-time.json", function(error,data) {
                                 
                                 bars.update(data,'.iac-tr-roadway-network .graph');
                                 /*setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                $('#ia-data-1761 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2011";
                                      } else {
                                        return "2013";
                                      }
                                    })
                              })
                        };

                        if (dataDimension == "B") {
                            d3.json(dataSourceDir+"js/data-svg/iac-tr-congestion-costs.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-tr-roadway-network .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                $('#ia-data-1761 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2009";
                                      } else {
                                        return "2011";
                                      }
                                    })
                              })
                        };

                        if (dataDimension == "C") {
                            d3.json(dataSourceDir+"js/data-svg/iac-tr-road-investment.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-tr-roadway-network .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                  $('#ia-data-1761 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2009";
                                      } else {
                                        return "2011";
                                      }
                                    })
                              })
                        };

                    });
                      
            });

        

                 


        } // else
 }); // d3 json civic participation

},{triggerOnce:true});//Waypoints
 