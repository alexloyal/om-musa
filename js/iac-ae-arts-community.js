// post-1721 is MIAMI-DADE SUPPORTS A THRIVING CREATIVE ARTS COMMUNITY

$('#post-1721 .data-disclosure [data-dimension="B"],#post-1721 .data-disclosure [data-dimension="C"],#post-1721 .data-disclosure [data-dimension="D"],#post-1721 .data-disclosure [data-dimension="E"], #post-1721 .data-set[data-dimension="B"],#post-1721 .data-set[data-dimension="C"],#post-1721 .data-set[data-dimension="D"],#post-1721 .data-set[data-dimension="E"]').css('opacity',0);
$('#post-1721 .data-disclosure [data-dimension="B"],#post-1721 .data-disclosure [data-dimension="C"],#post-1721 .data-disclosure [data-dimension="D"],#post-1721 .data-disclosure [data-dimension="E"]').addClass('hide');



$('#post-1721 .data-set[data-dimension="B"]').perfectScrollbar();
$('#post-1721 .data-set[data-dimension="C"]').perfectScrollbar();
$('#post-1721 .data-set[data-dimension="D"]').perfectScrollbar();
$('#post-1721 .data-set[data-dimension="E"]').perfectScrollbar();

$('[data-trigger="20"]').waypoint(function(down){

	$('#post-1721 .swap.objective.one').css('opacity','1');
	$('#post-1721 .swap.objective.two').css('opacity','1');
	$('#post-1721 .data-disclosure').css('opacity','1');
	$('#post-1721 .year-labels').css('opacity','1');

	var dataSourceDir = base + '/wp-content/themes/om-musa/';

	function removeRadials() {
		$(".radial-graph").remove();
	}

	function cleaned (num) {
		return parseFloat( String(num).replace(/\$|,|%/g, '') );
	}

	function dataSet1() {

		$('#ia-data-1721 .city-group').css('opacity', '');
		$('#ia-data-1721 [data-dimension="A"] .city-group').css('opacity', 1);

		d3.tsv(dataSourceDir+'js/data-svg/iac-ae-arts-attendance.tsv', null, function(data) {
			for(var i = 0; i < data.length; i++) {

				var ry1 = String(Object.keys(data[i])[1])
				var ry2 = String(Object.keys(data[i])[2])

				var segment = data[i]["Largest Arts Organizations"]

				var dataYear = ry1;

				var label = segment.toLowerCase().replace(/ /g, '-');

				$('#ia-data-1721 [data-dimension="A"] .city-group .radial').attr('data-year', function(){
					var index = $(this).index();
					if (  index < 1 ) {
						return ry1;
					} else {
						return ry2;
					}
				})

				var max = Math.max(cleaned(data[i][ry1]) * 1.1, cleaned(data[i][ry2]) * 1.1);

				var radial1 = new RadialGraph([data[i][ry1]], '[data-dimension="A"] [data-year="'+ry1+'"][data-segment="'+segment+'"]', ''+label+'', max);
				var radial2 = new RadialGraph([data[i][ry2]], '[data-dimension="A"] [data-year="'+ry2+'"][data-segment="'+segment+'"]', ''+label+'', max);

				console.log(segment)

				$("#ia-data-1721 .year-labels .item.green").html(ry1);
				$("#ia-data-1721 .year-labels .item.purp").html(ry2);

			}
		});
	}

	function dataSet2() {

		$('#ia-data-1721 .city-group').css('opacity', '');
		$('#ia-data-1721 [data-dimension="B"] .city-group').css('opacity', 1);


		d3.tsv(dataSourceDir+'js/data-svg/iac-ae-employment-arts-pc.tsv', function(data) {
			for (var i = data.length - 1; i >= 0; i--) {

				var ry1 = "2011"
				var ry2 = "2013"
				var county = data[i]["County"]
				var label = county.toLowerCase().replace(/ /g, '-');

				$('#ia-data-1721 [data-dimension="B"] .city-group .radial').attr('data-year', function(){
					var index = $(this).index();
					if (  index < 1 ) {
						return ry1;
					} else {
						return ry2;
					}
				})

//				var max = Math.max(cleaned(data[i][ry1]) * 1.1, cleaned(data[i][ry2]) * 1.1);

				var radial1 = new RadialGraph([data[i][ry1]], '[data-dimension="B"] [data-segment="1"] [data-year="'+ry1+'"][data-city="'+county+'"]', ''+label+'', 1500);
				var radial2 = new RadialGraph([data[i][ry2]], '[data-dimension="B"] [data-segment="1"] [data-year="'+ry2+'"][data-city="'+county+'"]', ''+label+'', 1500);

				$("#ia-data-1721 .year-labels .item.green").html(ry1);
				$("#ia-data-1721 .year-labels .item.purp").html(ry2);

			};
		})
	}

	function dataSet3() {

		$('.city-group').css('opacity', '');
		$('[data-dimension="C"] .city-group').css('opacity', 1);

		d3.tsv(dataSourceDir+'js/data-svg/iac-ae-art-dealers-pc.tsv', function(data) {

			for (var i = data.length - 1; i >= 0; i--) {

				var ry1 = "2011";
				var ry2 = "2013";
				var county = data[i]["County"].toLowerCase();
				var label = data[i]["County"];

				var max = Math.max(cleaned(data[i][ry1]) * 1.1, cleaned(data[i][ry2]) * 1.1);

				var radial1 = new RadialGraph([data[i][ry1]], '[data-dimension="C"] .first[data-city="' + label + '"]', '' + label + '', max);
				var radial2 = new RadialGraph([data[i][ry2]], '[data-dimension="C"] .second[data-city="' + label + '"]', '' + label + '', max);

				$("#ia-data-1721 .year-labels .item.green").html(ry1);
				$("#ia-data-1721 .year-labels .item.purp").html(ry2);

			};

		})
	}

	function dataSet4() {

		$('.city-group').css('opacity', '');
		$('[data-dimension="D"] .city-group').css('opacity', 1);

		d3.tsv(dataSourceDir+'js/data-svg/iac-ae-nea-grants.tsv', function(data) {

			for (var i = data.length - 1; i >= 0; i--) {

				var ry1 = "2012";
				var ry2 = "2013";
				var county = data[i]["County"].toLowerCase();
				var label = data[i]["County"];

				var max = .5;

				var radial1 = new RadialGraph([data[i][ry1]], '[data-dimension="D"] .first[data-city="' + label + '"]', '' + label + '', max);
				var radial2 = new RadialGraph([data[i][ry2]], '[data-dimension="D"] .second[data-city="' + label + '"]', '' + label + '', max);


				$("#ia-data-1721 .year-labels .item.green").html(ry1);
				$("#ia-data-1721 .year-labels .item.purp").html(ry2);
			};
		})
	}

	/** THIS ONE HERE IS THE MOST REUSABLE **/

	function dataSet5() {

		//FIRST, HIDE ALL CITY GROUPS EXCEPT THE CURRENT ONE, E
		$('.city-group').css('opacity', '');
		$('[data-dimension="E"] .city-group').css('opacity', 1);
		$('[data-dimension="E"] .first, [data-dimension="E"] .second').removeAttr('data-label');

		d3.tsv(dataSourceDir+'js/data-svg/iac-ae-tourism.tsv', function(data) {

			for (var i = data.reverse().length - 1; i >= 0; i--) {

				//RY1 AND RY2 ARE ACTUALLY THOSE RANDOM DIMENSIONS OF DATA
				var ry1 = String(Object.keys(data[i])[1]);
				var ry2 = String(Object.keys(data[i])[2]);
				var label = data[i]["Year"].toLowerCase().replace(/ /g, '-');
				console.log(ry1,ry2)
				//FOR THE FIRST AND SECOND RADIALS IN EACH GROUP, ADD THAT RANDOM DIMENSION AS A DATA LABEL
				$('[data-dimension="E"] .first').eq(i).attr("data-label", ry1);
				$('[data-dimension="E"] .second').eq(i).attr("data-label", ry2);

				//SET INDIVIDUAL MAXIMUMS FOR EACH OF THE FIRST + SECOND RADIALS
				var max1 = 15000000;
				var max2 = 30;

				//CREATE THE RADIALS
				var radial1 = new RadialGraph([data[i][ry1]], '[data-dimension="E"] .first[data-label="' + ry1 + '"]', '' + label + '', max1);
				var radial2 = new RadialGraph([data[i][ry2]], '[data-dimension="E"] .second[data-label="' + ry2 + '"]', '' + label + '', max2);

				$("#ia-data-1721 .year-labels .item.green").html(ry1);
				$("#ia-data-1721 .year-labels .item.purp").html(ry2);
			};

		})
	}



	$('#ia-data-1721.data-svg .swap.objective.two .item span').on('click', function() {

			var parentSwap = $(this).parent().data('swap');
            var activeSwap = $('#post-1721 .swap .item[data-swap="1"]');

			var dataDimension = $(this).parent().data('dimension');

			if (parentSwap = "0") {
                activeSwap.attr('data-swap', '0')
                $(this).parent().attr('data-swap', '1')
            };

            $('#ia-data-1721 .year-labels').attr('data-dimension-active', dataDimension );

            $('#post-1721 .data-disclosure [data-dimension]').css('opacity',0);
            $('#post-1721 .data-disclosure [data-dimension]').addClass('hide');
			$('#post-1721 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
			$('#post-1721 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

			$('#post-1721 .data-set[data-dimension]').css('opacity',0);
			$('#post-1721 .data-set[data-dimension] h4').css('opacity',0);
			$('#post-1721 .data-set[data-dimension="'+dataDimension+'"]').css('opacity',1);
			$('#post-1721 .data-set[data-dimension="'+dataDimension+'"] h4').css('opacity',1);

			$(".data-set[data-dimension]").hide();
			$('.data-set[data-dimension="' + dataDimension + '"]').show();

			switch(dataDimension) {

            	case "A":
					removeRadials();
					dataSet1();
            		break;
				case "B":
            		removeRadials();
					dataSet2();
            		break;
            	case "C":
					removeRadials();
					dataSet3();
            		break;
            	case "D":
					removeRadials();
					dataSet4();
            		break;
            	case "E":
					removeRadials();
					dataSet5();
            		break;
            }

	 });

	dataSet1();

},{triggerOnce:true}); // Waypoints
