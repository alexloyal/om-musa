var radialgraphs = [];

// post-1600 is MIAMI-DADE HOUSING IS AFFORDABLE

$('#post-1600 .data-disclosure [data-dimension="B"],#post-1600 .data-disclosure [data-dimension="C"],#post-1600 .data-disclosure [data-dimension="D"],#post-1600 .data-disclosure [data-dimension="E"]').css('opacity', 0);
$('#post-1600 .data-disclosure [data-dimension="B"],#post-1600 .data-disclosure [data-dimension="C"],#post-1600 .data-disclosure [data-dimension="D"],#post-1600 .data-disclosure [data-dimension="E"]').addClass('hide');

$('[data-trigger="20"]').waypoint(function(down){

$('#post-1600 .swap.objective.two').css('opacity','1');
$('#post-1600 .data-disclosure').css('opacity','1');
$('#post-1600 .year-labels').css('opacity','1');




	var dataSourceDir = base + '/wp-content/themes/om-musa/';



	d3.tsv(dataSourceDir+'js/data-svg/iac-ha-affordability-burden.tsv', null, function(burdendata) {

		for(var i = 0; i < burdendata.length; i++) {

			// var ry1 = String(Object.keys(burdendata[i])[0]);
			// var ry2 = String(Object.keys(burdendata[i])[1]);
			var ry1 = "2010";
			var ry2 = "2012";
			var county = burdendata[i]["County"]
			var cityName = $('.circles .radial[data-city="'+county+'"]')
			var dataYear = ry1;

			var label = county.toLowerCase().replace(/ /g, '-');

			$('.city-group .radial').attr('data-year', function(){
				var index = $(this).index();
				if (  index < 1 ) {
					return ry1;
				} else {
					return ry2;
				}
			})

			if(!radialgraphs[0]) { radialgraphs[0] = {} }
			if(!radialgraphs[1]) { radialgraphs[1] = {} }

			radialgraphs[0][county] = new RadialGraph([burdendata[i][ry1]], '[data-year="'+ry1+'"][data-city="'+county+'"]', ''+label+'');
			radialgraphs[1][county] = new RadialGraph([burdendata[i][ry2]], '[data-year="'+ry2+'"][data-city="'+county+'"]', ''+label+'');

			$("#ia-data-1600 .year-labels .item.green").html(ry1);
			$("#ia-data-1600 .year-labels .item.purp").html(ry2);
		}

		$('.city-group').css('opacity', '1');


		$('#ia-data-1600.data-svg .swap.objective.two .item span').on('click', function(){


				var parentSwap = $(this).parent().data('swap');
                var activeSwap = $('#ia-data-1600 .swap .item[data-swap="1"]');

				var dataDimension = $(this).parent().data('dimension');

				if (parentSwap = "0") {
	                activeSwap.attr('data-swap', '0')
	                $(this).parent().attr('data-swap', '1')
	            };

	            $('#post-1600 .data-disclosure [data-dimension]:not(.hide)').css('opacity', 0);
	            $('#post-1600 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
				$('#post-1600 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
				$('#post-1600 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

				switch(dataDimension) {

                	case "A":
						for(var i = 0; i < burdendata.length; i++) {

							var ry1 = String(Object.keys(burdendata[i])[0])
							var ry2 = String(Object.keys(burdendata[i])[1])
							var county = burdendata[i]["County"]
							var label = county.toLowerCase().replace(/ /g, '-');

							$("#ia-data-1600 .year-labels .item.green").html(ry1);
							$("#ia-data-1600 .year-labels .item.purp").html(ry2);

							radialgraphs[0][county].update([burdendata[i][ry1]]);
							radialgraphs[1][county].update([burdendata[i][ry2]]);
						}
                		break;

					case "B":
                		d3.tsv(dataSourceDir+'js/data-svg/iac-ha-affordability-coli.tsv', function(colidata) {
                			for (var i = colidata.length - 1; i >= 0; i--) {

                				// var ry1 = parseInt(Object.keys(colidata[i])[0])-1;
  								// var ry2 = parseInt(Object.keys(colidata[i])[1])-1;
  								var ry1 = "2011";
								var ry2 = "2013";

                				var county = colidata[i]["County"];

								$("#ia-data-1600 .year-labels .item.green").html(ry1);
								$("#ia-data-1600 .year-labels .item.purp").html(ry2);

								radialgraphs[0][county].update([colidata[i][ry1]], 250);
								radialgraphs[1][county].update([colidata[i][ry2]], 250);
                			};

                		})
                		break;
                	case "C":

                		d3.tsv(dataSourceDir+'js/data-svg/iac-ha-affordability-homeownership.tsv', function(data) {

                			for (var i = data.length - 1; i >= 0; i--) {

                				// var ry1 = String(Object.keys(data[i])[0])
  								// var ry2 = String(Object.keys(data[i])[1])
  								var ry1 = "2011";
								var ry2 = "2013";
                				var county = data[i]["County"]

								$("#ia-data-1600 .year-labels .item.green").html(parseFloat(ry1));
								$("#ia-data-1600 .year-labels .item.purp").html(parseFloat(ry2));

								radialgraphs[0][county].update([data[i][ry1]]);
								radialgraphs[1][county].update([data[i][ry2]]);
                			};

                		})
                	break;
                	case "D":

                		d3.tsv(dataSourceDir+'js/data-svg/iac-ha-affordability-constcost.tsv', function(data) {

                			for (var i = data.length - 1; i >= 0; i--) {

                				// var ry1 = String(Object.keys(data[i])[0])
  								// var ry2 = String(Object.keys(data[i])[1])
  								var ry1 = "2011";
								var ry2 = "2013";
                				var county = data[i]["County"] == "New York" ? "NYC" : data[i]["County"];

								$("#ia-data-1600 .year-labels .item.green").html(ry1);
								$("#ia-data-1600 .year-labels .item.purp").html(ry2);

								radialgraphs[0][county].update([data[i][ry1]], 380000);
								radialgraphs[1][county].update([data[i][ry2]], 380000);
                			};

                		})
                	break;
                	case "E":

                		d3.tsv(dataSourceDir+'js/data-svg/iac-ha-affordability-msp.tsv', function(data) {

                			for (var i = data.length - 1; i >= 0; i--) {
								//var ry1 = parseInt(Object.keys(data[i])[0]);
								//var ry2 = parseInt(Object.keys(data[i])[1]);
								var ry1 = "2011";
								var ry2 = "2013";
								var county = data[i]["County"] == "New York" ? "NYC" : data[i]["County"];

								$("#ia-data-1600 .year-labels .item.green").html(ry1);
								$("#ia-data-1600 .year-labels .item.purp").html(ry2);

								radialgraphs[0][county].update([data[i][ry1]], 470000);
								radialgraphs[1][county].update([data[i][ry2]], 470000);
                			};

                		})
                	break;
                }

		 });



	}, function(err, rows) {
	  console.log(err)
	});

},{triggerOnce:true}); // Waypoints
