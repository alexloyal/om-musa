  // Waypoints
//post-1801 is MIAMI-DADE INVESTS IN A EFFICIENT ROADWAY NETWORK

$('#post-1801 .data-disclosure [data-dimension="B"],#post-1801 .data-disclosure [data-dimension="C"]').css('opacity', 0)
$('#post-1801 .data-disclosure [data-dimension="B"],#post-1801 .data-disclosure [data-dimension="C"]').addClass('hide')

$('[data-trigger="30"]').waypoint(function(down) {
  

$('#post-1801 .swap.objective.one').css('opacity','1');
$('#post-1801 .data-disclosure').css('opacity','1');
$('#post-1801 .year-labels').css('opacity','1');

 
var dataSourceDir = base + '/wp-content/themes/om-musa/';


d3.json(dataSourceDir+"js/data-svg/iac-ec-object-zero.json", function(error,zeroData) {

            if (error) {
                // If error is not null, something went wrong.
                console.log(error);
                //Log the error.
            } else {
                     
                
            bars = new BarGraphDollars(zeroData,'.iac-ev-sustainability .graph');
              $('#ia-data-1801 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "2010";
                                      } else {
                                        return "2012";
                                      }
                                    })

            $(document).ready(function(){
                
                d3.json(dataSourceDir+"js/data-svg/iac-ev-recycling.json", function(error,data) {
                                 
                                    bars.update(data);
                                    console.log(data)
                              })

                    $('#ia-data-1801.data-svg .swap.objective.one .item span').on('click', function(){        
                        var parentSwap = $(this).parent().data('swap');
                        var activeSwap = $('#ia-data-1801 .swap .item[data-swap="1"]');
                        var dataDimension = $(this).parent().data('dimension');
                        $('.iac-ev-sustainability').attr('data-dimension',dataDimension)
                        console.log(dataDimension)
                        if (parentSwap = "0") {
                            activeSwap.attr('data-swap', '0')
                            $(this).parent().attr('data-swap', '1')
                        };
                        $('#post-1801 .data-disclosure [data-dimension]:not(.hide)').css('opacity', 0);
                        $('#post-1801 .data-disclosure [data-dimension]:not(.hide)').addClass('hide');
                        $('#post-1801 .data-disclosure [data-dimension="'+dataDimension+'"]').removeClass('hide');
                        $('#post-1801 .data-disclosure [data-dimension="'+dataDimension+'"]').animate({opacity:1},200);

                        if (dataDimension == "A") {
                            

                            d3.json(dataSourceDir+"js/data-svg/iac-ev-recycling.json", function(error,data) {
                                 
                                 bars.update(data,'.iac-ev-sustainability .graph');
                                 /*setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                              })
                        };

                        if (dataDimension == "B") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ev-garbage-collection.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-ev-sustainability .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                              })
                        };

                        if (dataDimension == "C") {
                            d3.json(dataSourceDir+"js/data-svg/iac-ev-sea-level-change.json", function(error,data) {
                                 
                                  bars.update(data,'.iac-ev-sustainability .graph');
                                 /* setTimeout(function(){
                                        bars.update(data,'.ia-ha-financial-resources .graph');
                                    }, 1000); */
                                  $('#ia-data-1801 .year-labels span').attr('data-value', function(){
                                      var index = $(this).index();
                                      if (  index < 1 ) {
                                        return "";
                                      } else {
                                        return "";
                                      }
                                    })
                              })
                        };

                    });
                      
            });

        

                 


        } // else
 }); // d3 json civic participation

},{triggerOnce:true});//Waypoints
 