<?php
/**
 * Issues zone on homepage. 
 *
 *
 * @package om_musa
 */

 ?>
<!-- home-issues -->

		<div id="issues-title" class="col-md-12 centered"></div>

  		<div class="col-md-12 issue-wrapper">
  				<?php
					
					

					$args = array(
					  'orderby' => 'name',
					  'order' => 'ASC',
					  'parent' => 0,
					  'taxonomy'=>'issue-areas'
					  
					  );

					$issueAreas = get_terms('issue-areas',$args);
					
					  foreach($issueAreas as $issue) { 
					  //  echo '<p>Category: <a href="' . get_term_link( $issue ) . '" title="' . sprintf( __( "View all posts in %s" ), $issue->name ) . '" ' . '>' . $issue->name.'</a> </p> ';
					   // echo '<p> Description:'. $issue->description . '</p>';

					$issueName = (string)$issue->slug;
					?>
					 

					<div id=" " class="issue-buffer col-md-3 col-xs-6">

						<div id="<?php echo $issueName; ?>" class="issue-thumb" >
							<div class="issue">
									<span class="issue-wrapper">
										<h4>
											<?php echo $issue->name; ?>
										</h4>
									 

									<?php 
										 
										 $moreArgs = array(
										 		'post_type' => 'story',
										 		'posts_per_page' => 1,
										 		'parent' => 0,
										 		'tax_query' => array(
										 				array(
										 			'taxonomy'=>'issue-areas',
										 			'field'=>'slug',
										 			'terms'=>$issueName
										 				)
										 			)
										 	);

										 $recentStoryQuery =  new WP_Query( $moreArgs );
										 if ( $recentStoryQuery->have_posts() ) {
													while ( $recentStoryQuery->have_posts() ) {
														$recentStoryQuery->the_post();
														?> 
														<p><?php the_title( ); ?></p>
														<p><a href="<?php the_permalink();  ?>">Go</a></p>
														<?php
													}
												} else {
												?> <p><a href="<?php echo get_term_link( $issue );  ?>">Go</a></p> <?php
												}





												// Restore original Post Data
												wp_reset_postdata();
										 


									 ?>	





									</span>
									
							</div><!-- .issue -->
							 
						</div>
							
							 
					</div>

				<?php					     } 


					?>
  		</div>
 
