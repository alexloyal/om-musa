<?php
/*
	Template Name: Match 305 Page
*/

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package om_musa
 */

get_header(); ?>
<!-- page -->
	<div id="primary" class="content-area row">
		<main id="main" class="site-main col-md-12" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

			<div class="background" style="background-image:url('<?php 
											$coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
											echo $coverimageurl
										 ?>') ;" class="">	

				 	
				<?php get_template_part( 'content', 'page' ); ?>


			</div>
			<div class="page-footer">

				<div class="col-md-7 centered psc-partnerships">
					<h4>Developed in Partnership With</h4>
						<?php
						$publications = get_bookmarks( array(
							'orderby'        => 'name',
							'order'          => 'ASC',
							'category_name'  => 'Match 305'
						));

						
						foreach ( $publications as $publication ) { ?>

							<div class="issue-buffer col-md-4 col-xs-6">
									<div class="issue-thumb" style="background-image:url(<?php printf($publication->link_image); ?>);">
												
										<div class="issue" >
											<span class="issue-wrapper">
											<h4 class="issue-title">
												<?php printf( '<a class="relatedlink" href="%s">%s</a><br />', $publication->link_url, $publication->link_name ); ?>
											</h4>

											<p><?php printf($publication->link_description); ?></p>

											<p class="go"><a href="<?php echo $publication->link_url; ?>">Go</a></p>
										</span>
										
										</div>
									</div>
							</div>


						<?php
						    
						}
						?>

				</div>

						<a href="<?php 
							$pageFooterLink = get_post_meta($post->ID, 'page-footer-cta-link', true);
							echo $pageFooterLink;
							?>" 

							title="<?php echo $pageFooterCTA;  ?>" 

							class="page-footer-link ideas-icon">
								<?php 
				 			 	$pageFooterCTA = get_post_meta($post->ID, 'page-footer-cta', true);
				 				 if ($pageFooterCTA !="" ) {
				 				 	?>
				 			 	
				 			 		<?php echo $pageFooterCTA; ?>

				 			 	<?php
				 			 	

				 				 } else {
				 			 		?>
				 			 		<span>LEARN ABOUT OUR STUDY AND STORIES</span>
				 			 	<?php
				 			  

				 			 }?>
		 				
		 				</a>
				</div>				

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
