<?php
/**
 * @package om_musa
 */
?>

<?php 

	 wp_enqueue_script( 'dashboard-data', get_template_directory_uri() . '/js/our-education-dashboard-data-viz.js', array('d3'), '1.0', true );

 ?>
  <!-- story-our-educatoin-dashboard -->
<div id="magnet" class="col-md-4 col-xs-12 pie">

        <h4>MAGNET SCHOOL ENROLLMENT   </h4>

            <div class="svg" data-location="MIAMI"></div>   

            <div class="svg" data-location="FL"></div>  

             <p>One bright spot in our education story is that more than 20% of students currently attend a magnet school. </p>

            <div class="toggle" data-value="2010 - 2011">
                <div class="value" data-value="2008 - 2009">2008-2009</div>
                <div class="slider"></div>
                <div class="value" data-value="2010 - 2011">2010-2011</div>
            </div>


</div>

<div id="college-entrance-exam" class="col-md-4 col-xs-12 college pie">
    <?php 

     wp_enqueue_script( 'college-entrance-exam-data', get_template_directory_uri() . '/js/college-entrance-exam-pass-data-viz.js', array('d3'), '1.0', true );

 ?>

    <h4>College Placement Test Scores Above Cutoff</h4>

            <span class="cyan label">State Universities</span>

            <span class="purp label">Community Colleges</span>


            <!-- <div class="chartwell-container">
                    <span class="chartwell pies">
                        <span style="color: rgb(0,175,219)">17</span>
                        <span style="color:rgb(255,255,255)">T</span>
                    </span>
                    <span class="small caption">FL</span>
            </div> -->


        <div class="svg" data-location="MIAMI"></div>   

        <div class="svg" data-location="FL"></div>  

        <p>Miami-Dade students perform below the state average on college placement tests such as the SAT and ACT, requiring a higher percentage to take remedial courses.</p>

            <div class="toggle" data-value="2012">
                <div class="value" data-value="2010">2010</div>
                <div class="slider"></div>
                <div class="value" data-value="2012">2012</div>
            </div>


</div>


<div id="adult-population-degree" class="col-md-4 col-xs-12 college pie">

     <?php 

     wp_enqueue_script( 'adult-population-degree-data', get_template_directory_uri() . '/js/adult-population-degree-data-viz.js', array('d3'), '1.0', true );

 ?>


    <h4>% of Adult (25+) Population with degree</h4>

            <span class="cyan label">Associate's Degree</span>

            <span class="purp label">Bachelor's Degree</span>

             

        <div class="svg" data-location="MIAMI"></div>   

        <div class="svg" data-location="US"></div>  

    <p>Miami-Dade residents outperform the US in getting associate degrees, but underperform in getting bachelor’s degrees.</p>

    <div class="toggle" data-value="2013">
                <div class="value" data-value="2011">2011</div>
                <div class="slider"></div>
                <div class="value" data-value="2013">2013</div>
    </div>



</div>

 
 