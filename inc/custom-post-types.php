<?php 

// Register Custom Post Type
function register_story() {

	$labels = array(
		'name'                => _x( 'Stories', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Story', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Story', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Stories', 'text_domain' ),
		'view_item'           => __( 'View Story', 'text_domain' ),
		'add_new_item'        => __( 'Add New Story', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                => 'stories',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'story', 'text_domain' ),
		'description'         => __( 'story_description', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'issue-areas', 'post_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'story', $args );

}

// Hook into the 'init' action
add_action( 'init', 'register_story', 0 );

// Register Custom Taxonomy
function register_issue_areas() {

	$labels = array(
		'name'                       => _x( 'Issue Areas', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Issue Area', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Issue Area', 'text_domain' ),
		'all_items'                  => __( 'Issue Areas', 'text_domain' ),
		'parent_item'                => __( 'Main Issue Area', 'text_domain' ),
		'parent_item_colon'          => __( 'Main Issue Area:', 'text_domain' ),
		'new_item_name'              => __( 'New Issue Area', 'text_domain' ),
		'add_new_item'               => __( 'Add New', 'text_domain' ),
		'edit_item'                  => __( 'Edit', 'text_domain' ),
		'update_item'                => __( 'Update', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate', 'text_domain' ),
		'search_items'               => __( 'Search', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or Remove', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose From Most Used Issue Areas', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                       => 'issue-areas',
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'issue-areas', array( 'post', 'story' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'register_issue_areas', 0 );

 
 function register_issue_area_content() {

	$labels = array(
		'name'                => _x( 'Issue Area Content', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Issue Area Content', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Issue Area Content', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Issue Area Content Items', 'text_domain' ),
		'view_item'           => __( 'View Issue Area Content', 'text_domain' ),
		'add_new_item'        => __( 'Add New Issue Area Content', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                => 'issue-area-data',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'story', 'text_domain' ),
		'description'         => __( 'story_description', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'issue-areas', 'post_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'issue-area-content', $args );

}

// Hook into the 'init' action
add_action( 'init', 'register_issue_area_content', 0 );

// Register Custom Post Type
/*function custom_post_type() {

	$labels = array(
		'name'                => _x( 'Partners', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Partners', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Partners', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Partner:', 'text_domain' ),
		'all_items'           => __( 'All Partner', 'text_domain' ),
		'view_item'           => __( 'View Partner', 'text_domain' ),
		'add_new_item'        => __( 'Add New Partner', 'text_domain' ),
		'add_new'             => __( 'Add New Partner', 'text_domain' ),
		'edit_item'           => __( 'Edit Partner', 'text_domain' ),
		'update_item'         => __( 'Update Partner', 'text_domain' ),
		'search_items'        => __( 'Search Partners', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'our_partners', 'text_domain' ),
		'description'         => __( 'SOME OF THE ORGANIZATIONS THAT MAKE OUR WORK POSSIBLE', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'taxonomies'          => array( 'category', 'post_tag', 'issue-areas' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'our_partners', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_type', 0 ); */

// Register Custom Post Type
/*
function register_local_non_profits() {

	$labels = array(
		'name'                => _x( 'Non-Profits', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Organization', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Local Non-Profits', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Organizations', 'text_domain' ),
		'view_item'           => __( 'View Organization', 'text_domain' ),
		'add_new_item'        => __( 'Add New Organization', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$rewrite = array(
		'slug'                => 'local-non-profits',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'local_non_profits', 'text_domain' ),
		'description'         => __( 'local_non_profits_description', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'issue-areas', 'post_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'local_non_profits', $args );

}

add_action( 'init', 'register_local_non_profits', 0 );
*/



 ?>