<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package om_musa
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function om_musa_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
	) );
}
add_action( 'after_setup_theme', 'om_musa_jetpack_setup' );
