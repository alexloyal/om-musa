<?php
/**
 * @package om_musa
 */
?>

<?php 

	wp_enqueue_script( 'data-svg', get_template_directory_uri() . '/js/miami-education-viz-edit.js', array('d3'), '1.0', true );

 ?>

     <section class="unemployment-rate">
        <div class="graph">
            <div class="bars"></div>
        </div>
        <div class="legend">
            <div class="item no-interaction" data-property="general unemployment">
                <label class="general-unemployment-icon">GENERAL UNEMPLOYMENT</label>
            </div>
            <div class="item" data-property="">
                <label class="did-not-graduate-icon">DID NOT GRADUATE FROM HIGH SCHOOL</label>
            </div>
            <div class="item" data-property="">
                <label class="high-school-diploma-only-icon">HIGH SCHOOL DIPLOMA ONLY</label>
            </div>
            <div class="item" data-property="">
                <label class="some-college-icon">SOME COLLEGE OR ASSOCIATES</label>
            </div>
            <div class="item" data-property="">
                <label class="bachelors-degree-icon">BACHELOR'S DEGREE OR HIGHER</label>
            </div>
            <div class="toggle" data-value="2013">
                <div class="value" data-value="2011">2011</div>
                <div class="slider"></div>
                <div class="value" data-value="2013">2013</div>
            </div>
        </div>
    </section>


    
