<?php
/**
 * @package om_musa
 */
?>
<!-- story safety first -->
<?php 

	wp_enqueue_script( 'pdata-svg', get_template_directory_uri() . '/js/pedestrian-fatalities-data-svg.js', array('d3'), '1.0', true );
		wp_enqueue_script( 'bar-graph', get_template_directory_uri() . '/js/bar.graph.js', array('d3'), '1.0', true );

 ?>

  <h4>PEDESTRIAN FATALITY RATE PER 100K POPULATION</h4>

					<div class="vertical-track-limit">
						<h3>3.0</h3>
					</div>     

 <div class="safety-first">

 		<div class="data-container col-md-12"></div>

 		<div class="city years centered col-md-10 legend">
 			<div class="track"><span class="dot">&bull;</span></div>
 			<div class="year" data-year="2009"><span>2009</span></div>
 			<div class="year" data-year="2010"><span>2010</span></div>
 			<div class="year" data-year="2011"><span>2011</span></div>
 			<div class="year active" data-year="2012"><span>2012</span></div>
 		</div>

 </div>