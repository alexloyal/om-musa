<?php
/**
 * Message zone on homepage. 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package om_musa
 */

 ?>
<!-- home-message -->

  	<div class="col-md-12 site-main message" role="main">
					<div id="explore" class="col-md-12 centered section header">

			  			<h4 class="center align">Explore The Stories</h4>

			  		</div>
  		

  		<div id="message-wrapper" class="story jcarousel-wrapper">
  			

	  		<div class="jcarousel">
	  			
	  			 	

	  			 <ul class="hook row">


	  			 	<?php 
	  			 			$args = array(
								'post_type' => 'story'
							);
							$quotesQuery = new WP_Query( $args );

	  			 	 ?>
	  			 	
	  			 	<?php if ( $quotesQuery->have_posts() ) : ?>

							<?php /* Start the Loop */ ?>
							<?php while ( $quotesQuery->have_posts() ) : $quotesQuery->the_post(); ?>

								<li class="post-wrapper col-md-12 violet background">
									
										<div class="background">

																<?php
																		/* Include the Post-Format-specific template for the content.
																		 * If you want to override this in a child theme, then include a file
																		 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
																		 */
																		get_template_part( 'content', 'hook' );
																	?>

										</div>

										<style type="text/css">


											


										.violet	div.background:after {
												background-image:url('<?php 
												$coverimageurl =	wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
												echo $coverimageurl ?>');
												background-repeat: no-repeat;
												opacity:0.1;
												display: block;	
												content:'';
												z-index:-2;
												width:100%;
												height:100%;
											}
										</style>


								</li>

								

							<?php endwhile; ?>

							 

						<?php else : ?>

							<?php get_template_part( 'content', 'none' ); ?>

						<?php endif; 

							wp_reset_postdata();

						?>

	  			 </ul>	
	  			
	  		</div><!-- .jcarousel -->
	  		<span class="controls"><a href="#" class="arrow-left3-icon jcarousel-control-prev"></a></span>
	  		<span class="controls "><a href="#" class="arrow-right3-icon jcarousel-control-next"></a></span>
	  	 

	  		<span class="controls dot jcarousel-pagination">

	  		</span>


  		</div>


  	</div>
 
