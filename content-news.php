<?php
/**
 * @package om_musa
 */
?>
<!-- content-news -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-9 centered'); ?>>
	
	<div class="news meta col-md-3 col-xs-3">
		
		<span class="avatar"><?php //echo get_avatar( get_the_author_meta( 'ID' ), 96 ); ?>
			<img src="<?php echo get_template_directory_uri()?>/images/Our_Miami_Logos_circle2.png">
		</span>
		<span class="pubby caps"><p>Published by</p></span>
		<span class="author caps"><p>
			
					<?php 
				$guest_author = get_post_meta( get_the_ID(), 'om_musa_guest_author', true );
				// check if the custom field has a value
				if( ! empty( $guest_author ) ) {
				  ?><?php echo $guest_author; ?> <?php 
				} else {
					?> <?php the_author(); ?>
					<?php
				} ?>



		</p></span>
		<span class="pub date caps"><h5><?php the_time('F j');  ?></h5></span>
		<span class="tag list caps"><span class="kicker">Tagged Under</span><?php the_terms( $post->ID, 'issue-areas', '', '' ); ?></span>

	</div>

	<div class="col-md-9 col-xs-12 news main">
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

					<div class="entry-meta">
						
					</div><!-- .entry-meta -->
				</header><!-- .entry-header -->

				<div class="entry-content">

					<?php the_excerpt(); ?>
					 <div class="tap quote">
												<a href="<?php the_permalink(); ?>" class="<?php the_id(); ?>">
													More
												</a>
					</div>
				</div><!-- .entry-content -->

	</div>	

 
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
