<?php
/**
 * @package om_musa
 */

// wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.ia.graph.js', array('d3'), '1.0', true );
wp_enqueue_script( 'iac-ed-thrive-academically', get_template_directory_uri() . '/js/iac-ed-thrive-academically.js', array('d3'), '1.0', true );


?>

<div class="swap objective two">

	<div class="item" data-swap="1" data-dimension="A">
		<span>Grade School Proficiency (Reading &amp; Math)</span>
	</div>

	<div class="item" data-swap="0" data-dimension="B">
		<span>High School Graduation Rates</span>
	</div>

	<div class="item" data-swap="0" data-dimension="C">
		<span>College Readiness</span>
	</div>

	<div class="item" data-swap="0" data-dimension="D">
		<span>Educational Attainment of Working Age (25-64) Population</span>
	</div>

	<div class="item" data-swap="0" data-dimension="E">
		<span>Post-Secondary Enrollment Rate</span>
	</div>

</div>
	
	<div class="col-md-12 centered year-labels" data-dimension-active="">
		

			<div class="item col-md-6 col-xs-3 apple green"></div>
			<div class="item col-md-6 col-xs-3 purp"></div>

		
	</div>

	<div class="data-sets">

		<div class="data-set col-md-12" data-dimension="A">
				<div class="inline">
							<h4>NAEP Math</h4>
							<h4>NAEP Reading</h4>
				</div>

			
			

				<div class="col-md-12 col-xs-12 circles">
					<div class="col-md-6 city-group" data-label="Miami">
						<div class="col-md-6 col-xs-6 radial first" data-year="NAEP Math 2011" data-segment="Miami"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="NAEP Math 2013" data-segment="Miami"></div>
						<div class="col-md-6 col-xs-6 radial third" data-year="NAEP Reading 2011" data-segment="Miami"></div>
						<div class="col-md-6 col-xs-6 radial fourth" data-year="NAEP Reading 2013" data-segment="Miami"></div>
						
					</div>

					<div class="col-md-6 city-group" data-label="US">
						<div class="col-md-6 col-xs-6 radial first" data-year="NAEP Math 2011" data-segment="US"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="NAEP Math 2013" data-segment="US"></div>
						<div class="col-md-6 col-xs-6 radial third" data-year="NAEP Reading 2011" data-segment="US"></div>
						<div class="col-md-6 col-xs-6 radial fourth" data-year="NAEP Reading 2013" data-segment="US"></div>

					</div>

					
				</div>


		</div>

		<div class="data-set col-md-12" data-dimension="B">

				<div class="col-md-12 col-xs-12 circles">
					<div class="col-md-6 city-group" data-label="chi">
						<div class="col-md-6 col-xs-6 radial first" data-year="2010-10" data-city="Chicago"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="2012-13" data-city="Chicago"></div>
					</div>

					<div class="col-md-6 city-group" data-label="hou">
						<div class="col-md-6 col-xs-6 radial first" data-year="2010-10" data-city="Houston"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="2012-13" data-city="Houston"></div>
					</div>

					<div class="col-md-6 city-group" data-label="mia">
						<div class="col-md-6 col-xs-6 radial first" data-year="2010-10" data-city="Miami"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="2012-13" data-city="Miami"></div>
					</div>


					<div class="col-md-6 city-group" data-label="nyc">
						<div class="col-md-6 col-xs-6 radial first" data-year="2010-10" data-city="NYC"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="2012-13" data-city="NYC"></div>
					</div>

					<div class="col-md-6 city-group" data-label="sd">
						<div class="col-md-6 col-xs-6 radial first" data-year="2010-10" data-city="SD"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="2012-13" data-city="SD"></div>
					</div>

					<div class="col-md-6 city-group" data-label="us">
						<div class="col-md-6 col-xs-6 radial first" data-year="2010-10" data-city="US"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="2012-13" data-city="US"></div>
					</div>
				</div>
		</div>

		<div class="data-set col-md-12" data-dimension="C">
				
			
				<div class="col-md-12 col-xs-12 circles">
					<div class="col-md-6 city-group" data-label="Miami">
						<div class="inline">
							<h4>2010</h4>
							<h4>2012</h4>
						</div>
						<div class="col-md-6 col-xs-6 radial first" data-year="State Universities 2010" data-segment="Miami-Dade County"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="Community Colleges 2010" data-segment="Miami-Dade County"></div>
						<div class="col-md-6 col-xs-6 radial third" data-year="State Universities 2012" data-segment="Miami-Dade County"></div>
						<div class="col-md-6 col-xs-6 radial fourth" data-year="Community Colleges 2012" data-segment="Miami-Dade County"></div>

					</div>

					<div class="col-md-6 city-group" data-label="US">
						<div class="inline">
							<h4>2010</h4>
							<h4>2012</h4>
						</div>
						<div class="col-md-6 col-xs-6 radial first" data-year="State Universities 2010" data-segment="Florida"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="Community Colleges 2010" data-segment="Florida"></div>
						<div class="col-md-6 col-xs-6 radial third" data-year="State Universities 2012" data-segment="Florida"></div>
						<div class="col-md-6 col-xs-6 radial fourth" data-year="Community Colleges 2012" data-segment="Florida"></div>

					</div>

					
				</div>
		</div>

		<div class="data-set col-md-12" data-dimension="D">

				<div class="col-md-12 col-xs-12 circles">
					<div class="col-md-6 city-group" data-label="Did not graduate from High School">
						<div class="col-md-6 col-xs-6 radial first" data-label="Did not graduate from High School 2011" data-city=""></div>
						<div class="col-md-6 col-xs-6 radial second" data-label="Did not graduate from High School 2013" data-city=""></div>
						
					</div>

					<div class="col-md-6 city-group" data-label="High School Diploma Only">
						<div class="col-md-6 col-xs-6 radial first" data-label="High School Diploma Only 2011" data-city=""></div>
						<div class="col-md-6 col-xs-6 radial second" data-label="High School Diploma Only 2013" data-city=""></div>
						
					</div>
				</div>

				<div class="col-md-12 col-xs-12 circles">
						<div class="col-md-6 city-group" data-label="Some College to 2-year Associate's">
						<div class="col-md-6 col-xs-6 radial first" data-label="Some College to 2-year Associate's 2011" data-city=""></div>
						<div class="col-md-6 col-xs-6 radial second" data-label="Some College to 2-year Associate's 2013" data-city=""></div>
						 
					</div>

					<div class="col-md-6 city-group" data-label="4-year Bachelor's or higher">
						<div class="col-md-6 col-xs-6 radial first" data-label="4-year Bachelor's or higher 2011" data-city=""></div>
						<div class="col-md-6 col-xs-6 radial second" data-label="4-year Bachelor's or higher 2013" data-city=""></div>
						
					</div>
				</div>
		</div>

		<div class="data-set col-md-12" data-dimension="E">

			<h4>Miami-Dade County</h4>
				<div class="col-md-12 col-xs-12 circles">
					<div class="col-md-6 city-group" data-label="Miami">
						<div class="col-md-6 col-xs-6 radial first" data-year="2009 - 2010" data-segment="Miami"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="2011 - 2012" data-segment="Miami"></div>
						

					</div>

					<div class="col-md-6 city-group" data-label="FL">
						<div class="col-md-6 col-xs-6 radial first" data-year="2009 - 2010" data-segment="Florida"></div>
						<div class="col-md-6 col-xs-6 radial second" data-year="2011 - 2012" data-segment="Florida"></div>
						

					</div>

					
				</div>
		</div>

	</div>

	

<div class="data-disclosure col-md-12">
	<div data-dimension="A">
		<h5>4TH GRADE PROFICIENCY LEVELS (Out of 500)</h5>

		<p>One of the primary purposes of primary and secondary education is to ensure that all students obtain basic reading and math skills.</p>

		<p>Miami-Dade County students achieve reading and math scores that are comparable to US averages.</p>

		<p class="source">Source: <a href="http://nces.ed.gov/nationsreportcard/naepdata/dataset.aspx" title="National Center for Education Statistics">National Center for Education Statistics</a><br>Data Showing:  Central County / National</p>
	</div>

	<div data-dimension="B">
		<h5>High School Graduation Rates (4-year Cohort)</h5>

		<p>Successfully completing high school is increasingly the first major milestone in obtaining the skills necessary to fully participate in the workforce.</p>

		<p>High school graduation rates in Miami-Dade County are improving.  76% of students graduated from high school in Miami-Dade County. However, our graduation rate is lower than the US average and other major metros.</p>

		<p class="source">Source: Individual State Departments of Education<!--, Miami-Dade, San Diego, <a href="http://www.cps.edu/SchoolData/Pages/SchoolData.aspx" title="Chicago Public Schools">Chicago</a>, NYC, <a href="http://ritter.tea.state.tx.us/acctres/completion/2012/district.html" title="Texas Educatoin Agency">Houston ISD</a>-->.
			<br>
			Data Showing:  Metro-level
		</p>

		 
	</div>

	<div data-dimension="C">
		<h5>College Readiness</h5>

		<p>College ready students are not only more likely to graduate, but can often do so more quickly and at lower cost than their counterparts that require significant remedial education.</p>

		<p>Miami-Dade students perform below the state average on college placement tests.</p>

		<p class="source"><a href="http://www.fldoe.org/articulation/perfCPT/default.asp" title="Florida Department of Education">Florida Department of Education</a><br>

			Data Showing:  Metro-level</p>
	</div>
	<div data-dimension="D">
		<h5>EDUCATIONAL ATTAINMENT OF WORKING AGE (25-64) POPULATION</h5>

		<p>Higher levels of education are associated with higher employment rates and higher incomes.</p>

		<p>Miami-Dade's working-age population is less likely to not graduate from high school or stop schooling after high school.  Still, educational attainment is improving.  </p>

		<p class="source">Source: US Census American Community Survey<br>
		Data Showing: Central County
		</p>


	</div>

	<div data-dimension="E">
		<h5>Post-Secondary Enrollment Rate</h5>

		<p>High-school is the first step in obtaining gainful employment. Attending college or university significantly increases one’s employment prospects.</p>

		<p>A high percentage of high school graduates Miami-Dade county go on to college.</p>

		<p class="source">Source: <a href="http://www.fldoe.org/fetpip/high.asp" title="Florida Department of Education">Florida Department of Education</a><br>
			Data Showing:  Central County</p>
	</div>

	

</div>
