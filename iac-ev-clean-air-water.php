<?php
/**
 * @package om_musa
 */
	wp_enqueue_script( 'iac-ev-clean-air-water', get_template_directory_uri() . '/js/iac-ev-clean-air-water.js', array('d3'), '1.0', true );
	
    wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );
?>

<div class="iac-ev-clean-air-water">

	<div class="swap objective one">

		<div class="item" data-swap="1" data-dimension="A">
			<span>Air Quality Index<br></span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>Water System Quality<br></span>
		</div>

	 

		

	</div>

<div class="col-md-6 year-labels centered">
	<span class="cyan" data-value=""></span>
	<span class="purp" data-value=""></span>
</div>


<div class="iac-ev-clean-air-water">
		<div class="graph"></div>
</div>



<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->

<div class="data-disclosure col-md-12">
	<div data-dimension="A">
		<h5>Annual Number of 'Good' Air Quality Days</h5>
			<p>Air quality affects the health of every citizen.</p>


			<p>Miami-Dade county enjoys a very high level of air quality compared with other major metros.</p>

			<p class="source">Source: <a href="http://www.epa.gov/airdata/ad_rep_aqi.html" title="EPA">EPA</a>
				<br>
			Data Showing: Central County
			</p>
	</div>

	<div data-dimension="B">

			<h5>Annual Number of Boil Water Orders Issued</h5>
			
			<p>Boil water orders are an indication of the quality of water service.</p>


			<p>Minimal boil water orders have been issued in recent years.</p>

			<p class="source">Source: <a href="http://www.miamidade.gov/water/water-quality-reports.asp" title="City of Miami">City of Miami</a><br>
			Data Showing: Central County</p>

	</div>


</div>



</div>

 
 
 