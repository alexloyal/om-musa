<?php
/**
 * @package om_musa
 */
	wp_enqueue_script( 'iac-ev-sustainability', get_template_directory_uri() . '/js/iac-ev-sustainability.js', array('d3'), '1.0', true );
	
    wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );
?>

<div class="iac-ev-sustainability-content">

	<div class="swap objective one">

		<div class="item" data-swap="1" data-dimension="A">
			<span>Household Recycling</span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>Reduction in Waste Collection (Reuse/Recycling)</span>
		</div>

		<div class="item" data-swap="0" data-dimension="C">
			<span>Inches above sea level in 1990</span>
		</div>

	</div>
<div class="col-md-6 year-labels centered">
	<span class="apple green" data-value=""></span>
	<span class="cyan" data-value=""></span>
</div>
<div class="iac-ev-sustainability">
		<div class="graph"></div>
</div>



<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->

<div class="data-disclosure">
<div data-dimension="A">
		<h5>Pounds of Recycling per Household</h5>			
		
		<p>Recycling programs help the environment and reduce waste.</p>
		<p>Miami-Dade residents recycled more pounds per capita in 2012 than in 2010.</p>
	

	<p class="source">Source: <a href="http://www.miamidade.gov/publicworks/" title="Miami-Dade County Public Works &amp; Waste Management Department">Miami-Dade County Public Works &amp; Waste Management Department</a>
		</p>

</div>
<div data-dimension="B">
		<h5>Garbage &amp; Trash Collection (Annual Pounds Collected per Household)</h5>
		<p>Trash collection costs local governments and citizens money, and can harm the environment.</p>
		<p>Miami-Dade county residents threw away fewer pounds of trash in 2012 than in 2010.</p>
	

	<p class="source">Source: <a href="http://www.miamidade.gov/publicworks/" title="City of Miami Department of Solid Waste">City of Miami Department of Solid Waste</a><br> Data Showing: City</p>

</div>

<div data-dimension="C">
		<h5>Sea Level Change</h5>
		<p>The rising sea level will have dramatic, long-term impacts on Miami-Dade County, from flooding to water quality to how we grow.</p>
		<p>In just two years, the average sea level in Miami-Dade county increased by two inches.  The sea level fell from its peak in 2007 but has quickly risen since 2011.</p>
	

	<p class="source">Source: <a href="http://tidesandcurrents.noaa.gov/waterlevels.html?id=8724580&units=standard&bdate=19200101&edate=20101231&timezone=GMT&datum=MLLW&interval=m&a" title="National Oceanic &amp; Atmospheric Administration">National Oceanic &amp; Atmospheric Administration</a><br> Data Showing: City</p>

</div>

</div>


</div>

 
 
 