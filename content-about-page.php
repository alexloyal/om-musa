<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package om_musa
 */
?>
<!-- content about page -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-6 centered well'); ?>>
	 
	 <div class="meta col-md-4 post-thumb">
		
		<span class="avatar"><?php echo get_avatar( get_the_author_meta( 'ID' ), 96 ); ?>
			<!-- <img src="<?php //echo get_template_directory_uri()?>/images/amy-web.jpg"> -->
		</span>
		<span class="pubby caps"><p>Published by</p></span>
		<span class="author caps"><p><?php the_author(); ?></p></span>
		<span class="pub date caps"><h5><?php the_time('F j');  ?></h5></span>
		

	</div>

	<div class="col-md-8 main">
				 

				<div class="entry-content">

					<?php the_content(); ?>
					 <div class="tap quote">
												<a href="<?php the_permalink(); ?>" class="<?php the_id(); ?>">
													More
												</a>
					</div>
					 <?php get_template_part( 'util', 'social-share' ); ?>
				</div><!-- .entry-content -->

	</div>	

 
</article><!-- #post-## -->
