<?php
/**
 * @package om_musa
 */
	wp_enqueue_script( 'iac-ec-job-opportunities', get_template_directory_uri() . '/js/iac-ec-job-opportunities.js', array('d3'), '1.0', true );
	
    wp_enqueue_script( 'bar-dollars-graph', get_template_directory_uri() . '/js/bar.dollars.graph.js', array('d3'), '1.0', true );
?>

<div class="iac-ec-job-opportunities-content">

	<div class="swap objective one">

		<div class="item" data-swap="1" data-dimension="A">
			<span>Employment Growth Rate</span>
		</div>

		<div class="item" data-swap="0" data-dimension="B">
			<span>Unemployment Rate</span>
		</div>

		<div class="item" data-swap="0" data-dimension="C">
			<span>Technology Growth (Number of Firms)</span>
		</div>

	</div>

<div class="col-md-6 year-labels centered">
	<span class="cyan" data-value=""></span>
	<span class="purp" data-value=""></span>
</div>


<div class="iac-ec-job-opportunities">
		<div class="graph"></div>
</div>


<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/housing-affordability-1-mhi.png"> -->

<div class="data-disclosure">
	<div data-dimension="A">
			<h5>Employment Growth</h5>
			<p>Strong employment growth creates job opportunities and boosts wages.</p>
			<p>Miami-Dade is growing faster than the US and other counties of similar size.</p>
		

		<p class="source">Source: <a href="http://data.bls.gov/cgi-bin/dsrv?en" title="BLS QCEW">BLS QCEW</a>
			<br>Data Showing: Central County
		</p>

	</div>
	<div data-dimension="B">
			<h5>Unemployment Rate</h5>
			<p>The local unemployment rate is a frequently cited indicator of a community's economic health.</p>
			<p>Miami-Dade's unemployment rate, currently 8.4%, is higher than the rates for the US and other large counties.  Fortunately, unemployment in the county has dropped significantly from two years ago.</p>
		

		<p class="source">Source: <a href="http://www.bls.gov/lau/" title="BLS LAUS">BLS LAUS</a>
			<br>Data Showing: Central County
		</p>

	</div>

	<div data-dimension="C">
			<h5>Technology Growth (Number of Firms)</h5>
			<p>The presence of tech firms often indicates a region rich with talent and innovation.</p>
			<p>Miami-Dade county is growing its technology firm base quickly - faster than major metros and the US.</p>
		

		<p class="source">Source: <a href="http://data.bls.gov/cgi-bin/dsrv?en" title="BLS LAUS">BLS LAUS</a>
			<br>Data Showing: Metro-Level
		</p>

	</div>

</div>


</div>

 
 
 