<?php
/**
 * @package om_musa
 */
?>

<?php 

	 wp_enqueue_script( 'affordability-vs-impact-poverty', get_template_directory_uri() . '/js/affordability-vs-impact-poverty-viz.js', array('d3'), '1.0', true );
     // wp_enqueue_script( 'radial-graph', get_template_directory_uri() . '/js/radial.graph.js', array('d3'), '1.0', true );

 ?>
<section class="what-does-this-mean">

              <div class="legend">

           
                <table class="table">
                    <tr>
                        <td class="dob-icon"></td>
                        <td class="location-icon"></td>
                        <td class="percentage-box-icon" colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td>2011</td>
                        <td>2013</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td data-age="0-18"><span>0-18</span></td>
                        <td data-city="Chicago" class="city-label"><span>Chicago</span></td>
                        <td data-city="Chicago" data-year="2011"></td>
                        <td data-city="Chicago" data-year="2013"></td>
                        <td data-city="Chicago" data-poverty=""></td>
                    </tr>
                    <tr>
                        <td data-age="18-64"><span>18-64</span></td>
                        <td data-city="Houston" class="city-label"><span>Houston</span></td>
                        <td data-city="Houston" data-year="2011"></td>
                        <td data-city="Houston" data-year="2013"></td>
                        <td data-city="Houston" data-poverty=""></td>
                    </tr>
                    <tr>
                        <td data-age="65+"><span>65+</span></td>
                        <td data-city="Miami" class="city-label"><span>Miami</span></td>
                        <td data-city="Miami" data-year="2011"></td>
                        <td data-city="Miami" data-year="2013"></td>
                        <td data-city="Miami" data-poverty=""></td>
                    </tr>
                    <tr>
                        <td data-age="all-ages"><span>All ages</span></td>
                        <td data-city="SD" class="city-label"><span>San Diego</span></td>
                        <td data-city="SD" data-year="2011"></td>
                        <td data-city="SD" data-year="2013"></td>
                        <td data-city="SD" data-poverty=""></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td data-city="NYC" class="city-label"><span>NYC</span></td>
                        <td data-city="NYC" data-year="2011"></td>
                        <td data-city="NYC" data-year="2013"></td>
                        <td data-city="NYC" data-poverty=""></td>
                    </tr>
                    <tr>
                        <td data-total="all-data" colspan="2">
                            <span>All Data</span>
                        </td>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                </table>



         </div>
        
       
        
</section>
 
 


 