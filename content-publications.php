<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package om_musa
 */
?>
<!-- content-publications -->

<div class="col-md-8 centered sub-page-container">
	<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12 centered'); ?>>
		

		 
		<div class="col-md-10 center entry-wrapper">
			<header class="entry-header">
				<?php 

			 			the_title( '<h1 class="entry-title internal center align">', '</h1>' );

						$story_hook = get_post_meta( get_the_ID(), 'om_musa_story_hook', true );
						// check if the custom field has a value
						if( ! empty( $story_hook ) ) {
						  ?> <h2 class="subhead"> <?php echo $story_hook; ?></h2> <?php 
						} else {
							 
						} ?>

			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php the_content(); ?>

				<div class="publications col-md-12">
					 
					 <?php
						$publications = get_bookmarks( array(
							'orderby'        => 'rating',
							'order'          => 'ASC',
							'category_name'  => 'Publications'
						));

						
						foreach ( $publications as $publication ) { ?>

							<div class="issue-buffer col-md-4 col-xs-6" data-url="<?php echo $publication->link_url ?>">
									<div class="issue-thumb" style="background-image:url(<?php printf($publication->link_image); ?>);">
												
										<div class="issue" >
											<span class="issue-wrapper">
											<h4 class="issue-title">
												<?php printf( '<a class="relatedlink" href="%s">%s</a><br />', $publication->link_url, $publication->link_name ); ?>
											</h4>

											<p><?php printf($publication->link_description); ?></p>

											<p class="go">
												<?php printf( '<a class="relatedlink" href="%s">%s</a><br />', $publication->link_url, "Go" ); ?>
											</p>
										</span>
										
										</div>
									</div>
							</div>


						<?php
						    
						}
						?>
				</div>	

				

				 <?php // get_template_part( 'util', 'social-share' ); ?>
			</div><!-- .entry-content -->

			 
			<footer class="entry-footer">
				<?php edit_post_link( __( 'Edit', 'om-musa' ), '<span class="edit-link">', '</span>' ); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->
</div>
