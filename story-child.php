<?php
/**
 * @package om_musa
 */
?>
<!-- story-child -->

<?php $menu_o = $wpdb->get_var( "SELECT menu_order FROM $wpdb->posts WHERE ID=" . $post->ID  ); ?>

<?php if ($menu_o == 90) {
				
				get_template_part( 'story', 'issue-areas' );
			} else {

				?> 

				

				<?php
			}
 ?>


<div class="story-anchor col-md-12">

		 	
		<span id="dot-<?php echo $menu_o; ?>" class="story-dot-navigation" data-section="<?php echo $post->post_name; ?>">

			
 			 <?php 
 			 	$storyKicker = get_post_meta($post->ID, 'story-kicker', true);
 			 if ($storyKicker !="" ) {
 			 	?>
 			 	<a href="#<?php echo $post->post_name; ?>" data-kicker="<?php echo $storyKicker; ?>" class="">

 			 	<?php
 			 	

 			 } else {
 			 	?>
 			 	<a href="#<?php echo $post->post_name; ?>" data-kicker="" class="">
 			 	<?php
 			  

 			 }?>
 				
 			</a>
   

		</span>

			
<div data-trigger="<?php echo $menu_o; ?>"></div>
 	
	<!-- 	<a href="#<?php // echo $post->post_name; ?>" class="story-anchoranchor-link">
 			<span class="anchor-link section arrow-down2-icon">&nbsp;</span>
 		</a>
   -->

</div>


<?php 

		// This allows to add a custom css class to the post in order to style differently.

		get_template_part('util','story-class' ); ?>

<?php 
	
	$story_class = get_post_meta( $post->ID, 'storyclass', true );
	if($story_class !="" ) {
		

		$storyClasses = array(
					'col-md-12',
					"menu-" . $menu_o,
					(string)$story_class
				);
	} else {


		$storyClasses = array(
					'col-md-12',
					"menu-" . $menu_o
					
				);
	} 
				
 ?>


<article id="post-<?php the_id(); ?>" <?php post_class($storyClasses); ?>>

	<div class="centered">

		<header class="entry-header">
			
			<?php  
				?>  <span id="<?php echo $post->post_name; ?>" class="anchor"></span> <?php
			?>

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<?php 
				$story_hook = get_post_meta( get_the_ID(), 'om_musa_story_hook', true );
				// check if the custom field has a value
				if( ! empty( $story_hook ) ) {
				  ?> <h2> <?php echo $story_hook; ?></h2> <?php 
				} else {
					the_excerpt(  );
				} ?>

			
		</header><!-- .entry-header -->

		<?php 
			
			// Menu order 90 means the very last section in the main story template. 

			
			if ($menu_o == 90 or $menu_o == 31 ) {
				
				$storyClass = "col-md-11 menu-".$menu_o;
			} else {

				$storyClass = "col-md-4";
			}

		 ?>

		 <!-- Menu Order: <?php echo $menu_o; ?> -->
		<div class="entry-content <?php echo $storyClass ?>">

			<!-- <?php echo get_the_title( $post->post_parent );	  ?> -->

			<?php the_content(); ?>
			 
			


		</div><!-- .entry-content -->



		<?php 
		
// This conditional is necessary for the Education Dashboard story so the width of the post reaches the width of the content area.
				
		 if ( $post->ID == '1552'  ) {

		 		?>

						<div id="" class="post-<?php echo $post->ID ?> data-svg col-md-12">
					<?php
	     
	 				 get_template_part( 'story', 'our-education-dashboard' );
						
	 				 	?>

	 				 </div>

						<?php

						} 
					
					else


		 	{  ?>

	<div id="data-svg-<?php echo $post->ID ?>" class="post-<?php echo $post->ID ?> data-svg col-md-8">
				 
				<!-- data-svg goes here -->

					<?php if ( $post->ID == '1465'  ) { ?>

						<h4>Investment Made To Public Transit Per Resident.</h4>
					<?php
	     
	 				 get_template_part( 'story', 'investment-pays-off' );
						}
				   if ( $post->ID == '1513'  ) { ?>

					
					<?php
		 				 get_template_part( 'story', 'safety-first' );
							}

					if ( $post->ID == '1550'  ) { ?>

					

					 
					<?php
	     
		 				get_template_part( 'story', 'lets-start-bottom' );
					}

					if ( $post->ID == '1560'  ) { ?>

					

					 
					<?php
	     
		 				get_template_part( 'story', 'traffic-volunteering' );
					}

					if ( $post->ID == '1537'  ) { ?>

					

					 
					<?php
	     
		 				get_template_part( 'story', 'what-drives-this' );
					}


					if ( $post->ID == '1539'  ) { ?>

					

					 
					<?php
	     
		 				get_template_part( 'story', 'so-what-does-this-mean' );
					}


					
					if ( $post->ID == '1562'  ) { ?>

					

					 
					<?php
	     
		 				get_template_part( 'story', 'migration-impact-civic-participation' );
					}



					if ( $post->ID == '1612'  ) { ?>

					

					 
					<?php
	     
		 				get_template_part( 'story', 'what-can-we-do-to-fix-the-situation' );
					}


					if ( $post->ID == '1541'  ) { ?>

					

					 
					<?php
	     
		 				get_template_part( 'story', 'what-about-housing' );
					}


					if ( $post->ID == '1576'  ) { ?>

					

					 
					<?php
	     
		 				get_template_part( 'story', 'public-space-air-quality' );
					}



					if ( $post->ID == '1570'  ) { ?>

					

					 
					<?php
	     
		 				get_template_part( 'story', 'park-investment' );
					}

					if ( $post->ID == '1572'  ) { ?>

					

					 
					<?php
	     
		 				get_template_part( 'story', 'parks-and-population' );
					}



					if ( $post->ID == '1574'  ) { ?>

					

					 
					<?php
	     
		 				get_template_part( 'story', 'disappearing-park-space' );
					}




					




					 ?>

					 

		 
		</div>
 
 	


 	<?php }  ?>

		

	</div>

	 
</article><!-- #post-## -->


