<!-- Kicker and Small Subhead -->

<?php 
				 			if ( is_page() && $post->post_parent > 0 ) { 
							 	$parent_title = get_the_title($post->post_parent);
							    ?> <h4 class="top kicker center align">
							    	<?php echo $parent_title; ?>
							    </h4> <?php
							}
				 		 ?>
				 		<?php 
							$story_kicker = get_post_meta( get_the_ID(), 'story-kicker', true );
							// check if the custom field has a value
							if( ! empty( $story_kicker ) ) {
							  ?> <h4 class="kicker center align"> <?php echo $story_kicker; ?></h4> <?php 
							} else {
								the_title( '<h4 class="kicker center align">', '</h4>' );
							}
						?> 


<?php 
													$story_kicker = get_post_meta( get_the_ID(), 'story-kicker', true );
													// check if the custom field has a value
													if( ! empty( $story_kicker ) ) {
													  ?> <h4 class="kicker center align"> <?php echo $story_kicker; ?></h4> <?php 
													} else {
														the_title( '<h4 class="kicker center align">', '</h4>' );
													} ?>



<!-- page footer with custom CTA link -->


<div class="page-footer">
						<a href="<?php 
							$pageFooterLink = get_post_meta($post->ID, 'page-footer-cta-link', true);
							echo $pageFooterLink;
							?>" 

							title="<?php echo $pageFooterCTA;  ?>" 

							class="page-footer-link">
								<?php 
				 			 	$pageFooterCTA = get_post_meta($post->ID, 'page-footer-cta', true);
				 				 if ($pageFooterCTA !="" ) {
				 				 	?>
				 			 	
				 			 		<?php echo $pageFooterCTA; ?>

				 			 	<?php
				 			 	

				 				 } else {
				 			 		?>
				 			 		More
				 			 	<?php
				 			  

				 			 }?>
		 				
		 				</a>
				</div>